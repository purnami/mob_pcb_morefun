package id.co.softorb.wizarpaxsample;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import id.co.softorb.lib.smartcard.PASSTIListener;

import static id.co.softorb.lib.passti.helper.ErrorCode.ERR_LIB_NOTINIT;
import static id.co.softorb.lib.passti.helper.ErrorCode.WAITING;

public class StartupActivity  extends Activity   {

    Button passti, pinpad;
    Context ctx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        ctx= getApplicationContext();
        passti = findViewById(R.id.bActivityTest);
        pinpad = findViewById(R.id.bActivityPinpad);

        passti.setOnClickListener(clickpassti);

        pinpad.setOnClickListener(clickpinpad);
    }

    View.OnClickListener clickpinpad=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent trx = new Intent(ctx,PinpadActivity.class);
           startActivity(trx);




        }

    };

    View.OnClickListener clickpassti=new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent trx = new Intent(ctx,TestActivity.class);
            startActivity(trx);

        }

    };
}
