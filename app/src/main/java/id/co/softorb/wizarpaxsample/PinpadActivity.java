package id.co.softorb.wizarpaxsample;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.STIUtility;
import id.co.softorb.lib.smartcard.PASSTIListener;
import id.co.softorb.lib.smartcard.wizar.pinpad;

import static id.co.softorb.lib.passti.helper.ErrorCode.*;

public class PinpadActivity  extends Activity implements PASSTIListener{

    private static final String TAG = "PinpadActivity";
    String tagclass= this.getClass().getSimpleName();
    Context ctx;
    TextView tvTeks,pinpadsn;
    int device ;
    STIUtility sti;
    pinpad devicepinpad;
    Button clear,closepinpad, openpinpad, back2main, getpinpadsn,getrandom,updateuserkey,setpinlength,sessionkeycheck,encryptdata,calcdukptmac,verifyrespmac,getmkstatus,getskstatus,getdukptstatus,listenpinblok,waitpinblock,cancelrequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinpad);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        ctx= getApplicationContext();
        InitScrollableTextView();
        device=9;//set default to wizar
        bindSdkDeviceService();
        tvTeks = findViewById(R.id.tvTitlePinPad);
        pinpadsn = findViewById(R.id.valuepinpadsn);
        closepinpad = findViewById(R.id.bpinpadclose);
        openpinpad = findViewById(R.id.bOpenPInpad);
        back2main=findViewById(R.id.bPinpad2MainMenu);
        clear=findViewById(R.id.bClearScreen);

        getpinpadsn = findViewById(R.id.bPinpadSN);
        getrandom = findViewById(R.id.bpinpadrandom);
        updateuserkey = findViewById(R.id.bUpdateUserKey);
        setpinlength = findViewById(R.id.bSetPinLength);

        sessionkeycheck = findViewById(R.id.bGetSessionKeyCheckValue);
        encryptdata = findViewById(R.id.bEncryptdata);
        calcdukptmac = findViewById(R.id.bCalculateMAC);

        verifyrespmac = findViewById(R.id.bVerifyRespMAC);
        getmkstatus = findViewById(R.id.bGetmkstatus);
        getskstatus = findViewById(R.id.bGetskstatus);

        getdukptstatus = findViewById(R.id.bGetDUKPTStatus);
        listenpinblok = findViewById(R.id.bListenPinBlock);



        waitpinblock=findViewById(R.id.bWaitPINBlock);
        cancelrequest=findViewById(R.id.bCancelRequest);

        /***********************Important***********************/
        closepinpad.setOnClickListener(clickclosepinpad);
        openpinpad.setOnClickListener(clickopenpinpad);
        back2main.setOnClickListener(clickfinish);
        clear.setOnClickListener(clickclear);
        getpinpadsn.setOnClickListener(clickpinpadsn);

        getrandom.setOnClickListener(clickgetrandom);

        updateuserkey.setOnClickListener(clickupdateuserkey);

        setpinlength.setOnClickListener(clicksetpinlength);

        sessionkeycheck.setOnClickListener(clicksessionkeycheck);

        encryptdata.setOnClickListener(clickencrypt);

        calcdukptmac.setOnClickListener(clickcalcdukptmac);

        verifyrespmac.setOnClickListener(clickverifyrespmac);
        getmkstatus.setOnClickListener(clickmkstatus);
        setpinlength.setOnClickListener(clicksetpinlength);

        getskstatus.setOnClickListener(clickskstatus);

        getdukptstatus.setOnClickListener(clickdukptstatus);

        listenpinblok.setOnClickListener(clicklistenpinblock);

        waitpinblock.setOnClickListener(clickwaitpinblock);
        cancelrequest.setOnClickListener(clickcancelrequest);
    }

    View.OnClickListener clickfinish=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();

        }
    };
    View.OnClickListener clickgetrandom=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //showText("Random number  : "+ Hex.bytesToHexString(devicepinpad.GenerateRandom(8)));

        }
    };
    View.OnClickListener clickupdateuserkey=new View.OnClickListener() {
        byte[] key={0x71,0x72,0x73,0x73,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x70};
        @Override
        public void onClick(View v) {
            showText("UpdateUserKey, code : "+ devicepinpad.UpdateUserKey(0,0,key));

        }
    };

    View.OnClickListener clickencrypt=new View.OnClickListener() {
        byte[] data={0x01,0x02,0x03,0x03,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x77};
        @Override
        public void onClick(View v) {
            showText("Encrypted data : "+ Hex.bytesToHexString(devicepinpad.encrypt(0,0,1,data)));

        }
    };
    View.OnClickListener clicksessionkeycheck=new View.OnClickListener() {
        byte[] data={0x01,0x02,0x03,0x03,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x77};
        @Override
        public void onClick(View v) {
            showText("Session Key  : "+ Hex.bytesToHexString(devicepinpad.getSessionKey(0,0,1)));

        }
    };
    View.OnClickListener clickcalcdukptmac=new View.OnClickListener() {
        byte[] data={0x01,0x02,0x03,0x03,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x77};
        @Override
        public void onClick(View v) {
            showText("DUKPT MAC  : "+Hex.bytesToHexString(devicepinpad.calculatedukptmac(data)));

        }
    };
    View.OnClickListener clickverifyrespmac=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showText("VerifyRespMac Status : "+devicepinpad.verifymac(0,0,null,null));

        }
    };
    View.OnClickListener clicksetpinlength=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showText("SetPINLength Status : "+devicepinpad.SetPINLength(10,16));

        }
    };
    View.OnClickListener clickmkstatus=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showText("MK Status : "+devicepinpad.getMKStatus(0));

        }
    };
    View.OnClickListener clickskstatus=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showText("SK Status : "+devicepinpad.getSKStatus(0,0));

        }
    };
    View.OnClickListener clickdukptstatus=new View.OnClickListener() {
        byte[] data={0x01,0x02,0x03,0x03,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x77};
        @Override
        public void onClick(View v) {
            showText("DUKPT Status : "+devicepinpad.getDUKPTStatus(0,data));

        }
    };
    View.OnClickListener clicklistenpinblock=new View.OnClickListener() {
        byte[] data={0x01,0x02,0x03,0x03,0x05,0x06,0x07,0x08,0x09,0x77};
        @Override
        public void onClick(View v) {
            devicepinpad.async_pinblock(0,0,data);

        }
    };
    View.OnClickListener clickwaitpinblock=new View.OnClickListener() {
        byte[] data={0x01,0x02,0x03,0x03,0x05,0x06,0x07,0x08,0x09,0x77};
        @Override
        public void onClick(View v) {
            devicepinpad.sync_pinblock(0,0,data);

        }
    };
    View.OnClickListener clickcancelrequest=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                devicepinpad.cancel();

        }
    };
    View.OnClickListener clickclear=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
               if(tvTeks!=null)
               {
                   showText("");
               }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener clickpinpadsn=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                pinpadsn.setText(devicepinpad.SN());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener clickclosepinpad=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {

                devicepinpad.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener clickopenpinpad=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                devicepinpad= (pinpad)sti.getPinPad();
                if(devicepinpad!=null)
                showText("pinpad open success");
                else
                {
                    showText("pinpad open fail, null object");
                }
            } catch (Exception e) {
                e.printStackTrace();
                showText("pinpad open fail");
            }
        }
    };


    void InitScrollableTextView()
    {
        tvTeks = findViewById(R.id.tvTitlePinPad);
        tvTeks.setMovementMethod(new ScrollingMovementMethod());
    }


    @Override
    protected void onDestroy() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        super.onDestroy();
        if(sti!=null)
        {
            sti.CloseDevice();
        }

    }

    private void bindSdkDeviceService() {
        String teks;
        int result=ERR_UNKNOWN;
        try{


            if(sti==null)
            {

                sti = new STIUtility(getApplicationContext(),this);


                result=sti.SetDeviceService(this,device);//10=pax,9=wizar

            }

            //teks = "Serial #: " + posTerminal. + "\n"+ "OSVersion: " + deviceInfo.getAndroidOSVersion() + "\n" + "IMEI: " + deviceInfo.getIMEI() + "\n";
            // Log.d(TAG, teks);
            if(result!=OK)
            {
                teks="Device init. fail, code : "+result;
            }
            else {
                int res = sti.initLib(ctx.getString(R.string.clientKey_dev));
                sti.initCTL_SAM(1,1);
                teks = "Init device successful";
            }

        }catch (Exception e){
            e.printStackTrace();
            teks="Device init. fail, message "+e.getMessage();

        }
        showText(teks);
    }

    private void showText(final String text)
    {
        new Thread() {
            public void run() {

                try {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            tvTeks.setText(text);
                            tvTeks.scrollTo(0, 0);
                            tvTeks.invalidate();
                        }
                    });
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }.start();

    }
    @Override
    public void onRFDetected(byte[] uid, int mode, ArrayList[] tagdata) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
    }

    @Override
    public void onContactDetected(byte[] uid, ArrayList[] tagdata) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
    }

    @Override
    public void onMSRDetected(byte[][] trackdata) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
    }

    @Override
    public void onSAMDetected(int slot, byte[] atr) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
    }


    @Override
    public void onPinPadHandler(byte[] data) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
    }
}
