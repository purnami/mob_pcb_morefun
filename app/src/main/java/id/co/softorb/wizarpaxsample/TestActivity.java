package id.co.softorb.wizarpaxsample;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.helper.printerparam;
import id.co.softorb.lib.passti.ClientServerListener;
import id.co.softorb.lib.passti.STIUtility;
import id.co.softorb.lib.passti.emv.tag;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.passti.helper.converter;
import id.co.softorb.lib.passti.helper.util;
import id.co.softorb.lib.smartcard.PASSTIListener;

import id.co.softorb.lib.smartcard.morefun.led;
//import id.co.softorb.lib.smartcard.wizar.led;

import static id.co.softorb.lib.passti.helper.ErrorCode.ERR_LIB_NOTINIT;
import static id.co.softorb.lib.passti.helper.ErrorCode.ERR_UNKNOWN;
import static id.co.softorb.lib.passti.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.passti.helper.ErrorCode.WAITING;

//import android.support.v7.app.AppCompatActivity;


public class TestActivity extends Activity implements PASSTIListener, AdapterView.OnItemSelectedListener {

	int code;
	TextView tvTeks, passtiversion,aposversion,devicetype;
    Button initpower, checkbalance, deduct, initdevice,initlib,login,post,clearscreen,print,findcontact,msr,ctl;
    EditText nominal,samtype,samslot;
//    private IccCardType iccCardType;
    private static final String TAG = "TestActivity";
    String tagclass= this.getClass().getSimpleName();
    Context ctx;

	private ProgressDialog progressBar;
	STIUtility sti;
    int trxcounter;
    byte[] PIN = {(byte) 0x01, (byte) 0x23, (byte) 0x45, (byte) 0x67, (byte) 0x89, (byte) 0xAB, (byte) 0xCD, (byte) 0xEF};
    byte[] ins_id = {(byte) 0x00, (byte) 0x01};
    byte[] tid = {(byte) 0x12, (byte) 0x34, (byte) 0x56, (byte) 0x78};


    byte[] deviceTID={(byte) 0x22, (byte) 0x33, (byte) 0x44, (byte) 0x55};
    byte[] deviceMID={(byte) 0xAA, (byte) 0xBB, (byte) 0xCC, (byte) 0xDD,(byte) 0xEE, (byte) 0xFF, (byte) 0x44, (byte) 0x55};
    String cashlezz="DFE29C4496564B4A8E05A6CD9BD83A30";
    String passti_kit="758F40D46D95D1641448AA19B9282C05";
    String bnisam7050000000000210_mrgcode="9E351D7AB236572589FDB3378D568898";
    String bnisam1100000000599891_mrgcode="FC78C9C643302CC039807F6116DDDCCD";
    String dummy_BNI_MID="0000000000000000";
    String dummy_BNI_TID="12345678";
    String dummy_BRI_Procode="801020";
    String dummy_BRI_Batch="03";
    String dummy_BRI_MID="FFFFFFFFFFFFFFFF";
    String dummy_BRI_TID="FFFFFFFF";


    
    private int progressBarStatus = 0;
    private Handler progressBarbHandler = new Handler();
    String action="";

    int OK = 0;
    int device ;



    
    int brirefno;
    ClientServerListener callback;
  //  POSTerminal posTerminal;
    /*public void CardType(int cardtype)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        Log.d(TAG,"cardtype "+cardtype);

    }*/
  Spinner spinner;
  public void onItemSelected(AdapterView<?> parent, View view,
                             int pos, long id) {
      // An item was selected. You can retrieve the selected item using
      // parent.getItemAtPosition(pos)
      device=9;
      Log.e(TAG,"device selected "+parent.getItemAtPosition(pos));
      switch(pos)
      {
          case 0 :
              device = 9;

                break;
          case 1:
              device = 10;break;

          case 2:
              device=11;
              break;
          case 3:
              device=3;
              break;

      }
      Log.d("device", ""+device);
  }
    int counter=0;
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    void test_LED()
    {
        int result;
        led deviceled=(led)sti.getLED();
//available color RED,BLUE,YELLOW,GREEN
        result=deviceled.open(deviceled.YELLOW);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result=deviceled.on();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result=deviceled.off();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result= deviceled.blink();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result=deviceled.close();


    }
    void test_LED_Morefun()
    {
        int result;
        led deviceled = (led)sti.getLED();
//available color RED,BLUE,YELLOW,GREEN
        result=deviceled.open(deviceled.YELLOW);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result=deviceled.on();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result=deviceled.off();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        result= deviceled.blink();
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        result=deviceled.close();


    }

    void test_printer()
    {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(ctx.getResources().getAssets()
                    .open("rocket.png"));

            bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, false);
        } catch (Exception e1) {
            e1.printStackTrace();

        }

        counter++;
        String test="Jumpa lagi "+Integer.toString(counter);
//        sti.print_bitmap(printerparam.ALIGN_CENTER,bitmap);
        Log.d("selesai print gambar", "ok");

        sti.print_text(printerparam.FONT_BOLD, printerparam.ALIGN_RIGHT,"Jumpa Lagi2", false);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sti.print_bitmap(printerparam.ALIGN_LEFT,bitmap);
        sti.printer_cutpaper();
        //        sti.print_text(printerparam.FONT_BOLD, printerparam.ALIGN_RIGHT,"Jumpa Lagi2", false);
//        sti.print_text(printerparam.FONT_ITALIC,printerparam.ALIGN_LEFT,test,false);

//        sti.print_text(printerparam.FONT_BOLD,printerparam.ALIGN_CENTER,"Jumpa lagi",false);
//        sti.printer_linefeed();
//        sti.print_text(printerparam.FONT_NORMAL,printerparam.ALIGN_CENTER,"Jumpa lagi",true);
/*
        sti.printer_linefeed();
        sti.print_text(printerparam.FONT_ITALIC,printerparam.ALIGN_LEFT,"Jumpa lagi",false);
        sti.printer_linefeed();
        sti.print_text(printerparam.FONT_BOLD,printerparam.ALIGN_LEFT,"Jumpa lagi",false);
        sti.printer_linefeed();
        sti.print_text(printerparam.FONT_NORMAL,printerparam.ALIGN_LEFT,"Jumpa lagi",false);
        sti.printer_linefeed();
        sti.print_text(printerparam.FONT_BOLD,printerparam.ALIGN_LEFT,"Jumpa lagi",false);
        sti.printer_linefeed();
        sti.print_text(printerparam.FONT_BOLD,printerparam.ALIGN_RIGHT,"Jumpa lagi",false);
        sti.printer_linefeed();
        sti.print_text(printerparam.FONT_BOLD,printerparam.ALIGN_CENTER,"Jumpa lagi",true);
        sti.print_text(printerparam.FONT_BOLD,printerparam.ALIGN_CENTER,"Jumpa lagi",true);
        sti.print_text(printerparam.FONT_BOLD,printerparam.ALIGN_CENTER,"Jumpa lagi",true);
        sti.print_text(printerparam.FONT_BOLD,printerparam.ALIGN_CENTER,"Jumpa lagi",true);
        sti.printer_linefeed();
        sti.printer_linefeed();
        sti.printer_linefeed();
        sti.printer_linefeed();
        sti.printer_cutpaper();*/
        Log.e(TAG,"PAPER STATUS : "+sti.printer_paperstatus());


    }
int initialize_bankparam()
    {
        int code;
        int res;
        res=sti.initSAMVar_Mandiri(BytesUtil.bytes2HexString(PIN),BytesUtil.bytes2HexString(ins_id),BytesUtil.bytes2HexString(tid));//PIN,insID,TID
        if(res!=OK)
        {
            tvTeks.setText("initSAMVar_Mandiri, code "+res);
            return -2;
        }


        res=sti.initSAMVar_BNI(dummy_BNI_MID,dummy_BNI_TID,bnisam7050000000000210_mrgcode);//MID,TID,MarriedCode
        //res=sti.initSAMVar_BNI(dummy_BNI_MID,dummy_BNI_TID,bnisam1100000000599891_mrgcode);//MID,TID,MarriedCode

        if(res!=OK)
        {
            tvTeks.setText("initSAMVar_BNI, code "+res);
            return -4;
        }

        res=sti.initSAMVar_BRI(dummy_BRI_Procode,dummy_BRI_Batch,dummy_BRI_MID,dummy_BRI_TID);//procode,batchno,merchantID,TID
        if(res!=OK)
        {
            tvTeks.setText("initSAMVar_BRI, code "+res);
            return -3;
        }
        tvTeks.setText("Bank params init successful");
        return OK;
    }
    void InitScrollableTextView()
    {
        tvTeks = findViewById(R.id.tvTitle);
        tvTeks.setMovementMethod(new ScrollingMovementMethod());
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        InitScrollableTextView();
        initlib = findViewById(R.id.bInitLib);
        initpower = findViewById(R.id.bInitPower);
        clearscreen=findViewById(R.id.bClearScreen);

        checkbalance = findViewById(R.id.bCheckBalance);
        deduct = findViewById(R.id.bDeduct);
        msr = findViewById(R.id.bMSR);
        ctl = findViewById(R.id.bCTL);

        samtype = findViewById(R.id.etSAM);
        samslot = findViewById(R.id.etSAMSlot);
        nominal = findViewById(R.id.etPaymentDeduct);


        findcontact = findViewById(R.id.bDetectContact);
        print = findViewById(R.id.bPrint);
        initdevice = findViewById(R.id.bInitDevice);
        trxcounter=0;
        aposversion = findViewById(R.id.valueDeviceLibVersion);
        passtiversion = findViewById(R.id.valuePasstiVersion);
        
        
        
        devicetype=findViewById(R.id.valueDeviceType);
        //devinfo.others = new byte[2];//rfu
        login = findViewById(R.id.bLogin);
        post = findViewById(R.id.bPost);
        trxcounter=0;
        brirefno=0;
        //callback=this;
        ctx = getApplicationContext();
        String path=ctx.getApplicationInfo().nativeLibraryDir;
        spinner = (Spinner) findViewById(R.id.DropDownDevice);
        spinner.setOnItemSelectedListener(this);
        device=9;//set default to wizar
/***********************Important***********************/
        findcontact.setOnClickListener(clickfindcontact);
        print.setOnClickListener(clickprint);
        clearscreen.setOnClickListener(clickclear);

        deduct.setOnClickListener(clickdeduct);

        checkbalance.setOnClickListener(clickcheckbalance);

        initdevice.setOnClickListener(clickinitdevice);

        initlib.setOnClickListener(clickinitlib);

        initpower.setOnClickListener(clickinitpower);

        post.setOnClickListener(clickpostdata);

        login.setOnClickListener(clicklogin);

        msr.setOnClickListener(clickmsr);
        ctl.setOnClickListener(clickfindctl);

    }

    View.OnClickListener clickfindctl=new View.OnClickListener() {
        @Override
        public void onClick(View v) {


             int[] code = new int[1];

            showText("Waiting Contactless Card");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    code[0] = sti.FindCTL_EMV();
                    switch(code[0])
                    {
                        case WAITING:
                            showText("Waiting Contactless Card");

                            break;
                        case ERR_LIB_NOTINIT:
                            showText("Lib. not initialized");
                            break;
                        default:
                            showText("Error code : "+code[0]);
                            break;

                    }
                }}).start();

        }

    };
    View.OnClickListener clickfindcontact=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("findcontact", " click");
             int[] code = new int[1];
            new Thread(new Runnable() {
                @Override
                public void run() {
                    code[0] = sti.FindContactCard();
                    Log.d("findcontact", ""+code[0]);
                    switch(code[0])
                    {
                        case WAITING:
                            showText("Waiting Contact Card");
                            break;
                        case ERR_LIB_NOTINIT:
                            showText("Lib. not initialized");
                            break;
                        default:
                            showText("Error code : "+code[0]);
                            break;

                    }
                }}).start();

        }

    };

    View.OnClickListener clickmsr=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
             int[] code = new int[1];

            new Thread(new Runnable() {
                @Override
                public void run() {
                   code[0] = sti.FindMSR();
                   Log.d("findmsr", ""+code[0]);
                    switch(code[0])
                    {
                        case WAITING:
                            showText("Waiting Magnetic Stripe Card");
                            break;
                        case ERR_LIB_NOTINIT:
                            showText("Lib. not initialized");
                            break;
                        default:
                            showText("Error code : "+code[0]);
                            break;

                    }
                }}).start();

        }

    };
    View.OnClickListener clickprint=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showText("Printer test");
            test_printer();

        }
    };
    View.OnClickListener clicklogin=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            /*if(passti == null){
                showToast("Init Lib dahulu");
                return;
            }*/
            byte[] username = "Admin".getBytes();
            //byte[] username = null;
            byte[] password = "Admin".getBytes();
            //byte[] password = null;
            sti.Login(username,password,callback);

        }
    };
    View.OnClickListener clickpostdata=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(sti == null){
                showToast("Init Lib dahulu");
                return;
            }
            if(sti.getPASSTILog().length==1){
                showToast("Deduct dahulu");
                return;
            }

            sti.UploadData(sti.getPASSTILog(),callback);
        }
    };

 View.OnClickListener clickinitdevice=new View.OnClickListener() {
     @Override
     public void onClick(View v) {
         try {

             bindSdkDeviceService();
         } catch (Exception e) {
             e.printStackTrace();
         }
     }
    };
    View.OnClickListener clickclear=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showText("");}
    };
    View.OnClickListener clickdeduct=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int res;
            if (nominal.getText().toString() == null || nominal.getText().toString().matches("")) {
                showText("Fill amount coloumn first!");
                nominal.setError("Can't be blank/empty");
                return;
            }


            initialize_bankparam();
            ShowProgressBar();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if(sti == null){
                        progressBar.dismiss();
                        showToast("Init Lib dahulu");
                        return;
                    }
                    if(!sti.checkInitLib()){
                        progressBar.dismiss();
                        showToast("Init Lib dahulu");
                        return;
                    }
                    int i = 0;

                    try{
                        trxcounter++;
                        brirefno++;
                        sti.SetTrxCounter(trxcounter);
                        String strBRIRef = String.format("%06d",brirefno);
                        Log.d(TAG,"strBRIRef "+strBRIRef);
                        sti.SetBRIRefNo(strBRIRef);
                        i = sti.deduct(nominal.getText().toString());
                    }
                    catch(Exception e)
                    {
                        Log.e(TAG,"sti.deductt "+e.getMessage());
                        i= NOT_OK;
                    }
                    //if (i != OK && sti.getMandiriCorrection() || sti.getBRICorrection() || sti.getBNICorrection()) {
                    int code = sti.getActionCode();
                    //sti.CancelRepurchase();

                    Log.e(TAG,"sti.getAction,code "+code);
                    if (i != OK && code==1) {
                        String text="";


                        final String txt2display = text+"Deduct fail, Need Correction";
                        showText(txt2display);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //tvTeks.setText(txt2display);
                                progressBar.dismiss();
                                Toast.makeText(TestActivity.this, txt2display, Toast.LENGTH_SHORT).show();
                            }
                        });

                    }else if (i != OK && i == -102) {
                        showText("Gagal Deduct - Saldo Kurang\nSaldo: " + sti.getBalance() + "\nPotongan Deduct: " + nominal.getText().toString());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                progressBar.dismiss();
                                Toast.makeText(TestActivity.this, "Gagal Deduct", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    else if (i != OK) {
                        int finalI = i;
                        showText("Gagal Deduct\nResponseCode: " + finalI);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                progressBar.dismiss();
                                Toast.makeText(TestActivity.this, "Gagal Deduct", Toast.LENGTH_SHORT).show();
                            }
                        });

                    } else {

                        final String pesan = "Saldo Awal: " + sti.getBalance() + "\nSaldo Akhir: " + sti.getLastBalance() + "\nCardNum: " + sti.getCardNo() + "\nPASSTILog: " + BytesUtil.bytes2HexString(sti.getPASSTILog());
                        showText(pesan);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG,"Messages: " + pesan);
                                //printing(sti.getBalance(),sti.getLastBalance(),sti.getCardNo());

                                progressBar.dismiss();
                                Toast.makeText(TestActivity.this, "Sukses Deduct", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }).start();
        }};
    
    View.OnClickListener clickinitpower= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //
            //type*
            //Luminos: 1
            //Mandiri: 2
            //BRI: 3
            //BNI: 4
            //for APOS with only 1 slot, fill with: 1
            //
            if(sti == null){
                showToast("Init Device dahulu");
                return;
            }
            if(!sti.checkInitLib()){
                showToast("Init Library dahulu");
                return;
            }
            if (samtype.getText().toString() == null || samtype.getText().toString().matches("")) {
                samtype.setError("Can't be blank/empty");
                return;
            }
            if (samslot.getText().toString() == null || samslot.getText().toString().matches("")) {
                samslot.setError("Can't be blank/empty");
                return;
            }
            String message = "";

                    int iRet;
            int samBankType = Integer.valueOf(samtype.getText().toString());
            int samSlot = Integer.valueOf(samslot.getText().toString());
            Log.d("samBankType",""+samBankType);
            Log.d("samSlot", ""+samSlot);

            iRet = sti.initCTL_SAM(samBankType,samSlot);
                    switch (samBankType) {
                        case 1:
                            message = "Luminos : ";
                            break;
                        case 2:
                            message = "Mandiri : ";
                            break;
                        case 3:
                            message = "BRI : ";
                            break;
                        case 4:
                            message = "BNI : ";
                            break;
                        default:
                            message = "Unknown Issuer : ";
                            break;
                    }
            message+= GetCodeDesc(iRet);


            showText(message);
        }
    };
    View.OnClickListener clickinitlib= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try{


                if(sti == null){
                    showToast("Init Device dahulu");
                    return;
                }
                int res = sti.initLib(passti_kit);
                Log.d("res", ""+res);
                //int res = sti.initLib("4B2AA2F02AEF40C7A4F8EE425BFF4555");

                if(res==0) {
                    sti.initBank();

                    code=sti.SetMID(deviceMID);
                    if(code!=OK)
                    {
                        tvTeks.setText("SetMID fail, code "+code);
                        return;
                    }
                    code=sti.SetTID(deviceTID);
                    if(code!=OK)
                    {
                        tvTeks.setText("SetTID fail, code "+code);
                        return;
                    }

                }
                showText("Init Lib"  + "\nResponseCode: " + res);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };


    public void ShowProgressBar() {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar = new ProgressDialog(TestActivity.this);
                progressBar.setCancelable(false);
                progressBar.setMessage("Process is running\nDO NOT REMOVE CARD ");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();
                progressBarStatus = 0;
            }
        });
    }


void ExecuteCheckBalance ()
{
    Log.e(TAG, tagclass + "." + new Throwable()
            .getStackTrace()[0]
            .getMethodName());

        try {
            if(sti == null){
                progressBar.dismiss();
                showToast("Init Lib dahulu");
                return;
            }
            if(!sti.checkInitLib()){
                progressBar.dismiss();
                showToast("Init Lib dahulu");
                return;
            }
            final int i = sti.checkbalance();

            if (i != OK) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvTeks.setText("Gagal Check Balance\nResponse Code: " + i);
                        if (progressBar != null)
                            progressBar.dismiss();

                        //Toast.makeText(TestActivity.this, "Gagal Check Balance", Toast.LENGTH_SHORT).show();
                    }
                });

            } else {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (progressBar != null) progressBar.dismiss();
                        tvTeks.setText("Saldo: " + sti.getBalance() + "\nCardNo: " + sti.getCardNo());

                        //Toast.makeText(TestActivity.this, "Sukses Check Balance", Toast.LENGTH_SHORT).show();
                    }
                });

            }


        } catch (final Exception e) {
            e.printStackTrace();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressBar != null) progressBar.dismiss();
                    Toast.makeText(TestActivity.this, "Exception : "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }


};
View.OnClickListener clickcheckbalance=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            initialize_bankparam();

            ShowProgressBar();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if(sti == null){
                            progressBar.dismiss();
                            showToast("Init Lib dahulu");
                            return;
                        }
                        if(!sti.checkInitLib()){
                            progressBar.dismiss();
                            showToast("Init Lib dahulu");
                            return;
                        }
                        int i = sti.checkbalance();

                        if (i != OK) {
                            showText("Gagal Check Balance\nResponse Code: " + i);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (progressBar != null)
                                        progressBar.dismiss();

                                    //Toast.makeText(TestActivity.this, "Gagal Check Balance", Toast.LENGTH_SHORT).show();
                                }
                            });

                        } else {
                            showText("Saldo: " + sti.getBalance() + "\nCardNo: " + sti.getCardNo());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progressBar != null) progressBar.dismiss();

                                   // Toast.makeText(TestActivity.this, "Sukses Check Balance", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        showText("Exception : "+e.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (progressBar != null) progressBar.dismiss();

                               // Toast.makeText(TestActivity.this, "Exception : "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }).start();
        }
    };
	
	private void bindSdkDeviceService() {
        Log.d("device2", ""+device);
            String teks;
            int result=ERR_UNKNOWN;
            try{

                Log.d("stii",""+sti+" result"+result);

                if(sti==null)
                {

                    sti = new STIUtility(getApplicationContext(),this);
                    Log.d("stiii",""+sti);

                    result=sti.SetDeviceService(this,device);//10=pax,9=wizar

                    Log.d(" resultt", String.valueOf(result));

                    passtiversion.setText(sti.getLibVersion());
                    aposversion.setText(sti.DeviceLibVersion());
                    devicetype.setText(Integer.toString(sti.DeviceType()));
                }

                //teks = "Serial #: " + posTerminal. + "\n"+ "OSVersion: " + deviceInfo.getAndroidOSVersion() + "\n" + "IMEI: " + deviceInfo.getIMEI() + "\n";
               // Log.d(TAG, teks);
                Log.d(" resulttt", String.valueOf(result));
                if(result!=OK)
                {
                    teks="Device init. fail, code : "+GetCodeDesc(result);
                }
                else {
                    teks = "Init device successful";
                    //nanti on lagi
//                    test_LED();
//                    test_LED_Morefun();
                }

            }catch (Exception e){
                e.printStackTrace();
                teks="Device init. fail, message "+e.getMessage();

            }
            showText(teks);
    }
    /**
     * Service connection.
     */


    public byte[] convertASCIIToBin(String ascii) {
        byte[] bin = new byte[ascii.length() / 2];
        for (int x = 0; x < bin.length; x++) {
            bin[x] = (byte) Integer.parseInt(ascii.substring(x * 2, x * 2 + 2), 10);

        }
        return bin;
    }
    byte[] bcd_ddmmyyyyhhmmss2hex_yymmddhhmmss(String in)
    {
        byte[] out = new byte[6];
        Log.e(TAG,"in "+in);
        byte[] ddmmyyyyhhmmss =convertASCIIToBin(in);
        int iValue;
        Log.e(TAG,"ddmmyyyyhhmmss "+ Hex.bytesToHexString(ddmmyyyyhhmmss));

        out[0]= ddmmyyyyhhmmss[3];
        out[1]=ddmmyyyyhhmmss[1];
        out[2]=ddmmyyyyhhmmss[0];
        out[3]=ddmmyyyyhhmmss[4];
        out[4]=ddmmyyyyhhmmss[5];
        out[5]=ddmmyyyyhhmmss[6];
        Log.e(TAG,"out "+ Hex.bytesToHexString(out));
        return out;

    }
    long julianSec(int baseyear,byte[] DateTime)
    {
        int i = 0;
        long val = 0;
        int Feb = 0;
        int yearLocal;
        int monthLocal;
        int dayLocal;
        int hourLocal = 0;
        int minuteLocal = 0;
        int secLocal = 0;


        Log.d(TAG,"DateTime "+Hex.bytesToHexString(DateTime));
        dayLocal = DateTime[2];
        monthLocal = DateTime[1];
        yearLocal = 2000+ DateTime[0];
        hourLocal = DateTime[3];
        minuteLocal = DateTime[4];
        secLocal = DateTime[5];
        Log.i(TAG,"dayLocal "+dayLocal);
        Log.i(TAG,"monthLocal "+monthLocal);
        Log.i(TAG,"yearLocal "+yearLocal);
        Log.i(TAG,"hourLocal "+hourLocal);
        Log.i(TAG,"minuteLocal "+minuteLocal);
        Log.i(TAG,"secLocal "+secLocal);


//printf("dd/mm/yyyy = %2d/%2d/%4d hh:nn:ss = %2d:%2d:%2d \r\n", dayLocal,monthLocal,yearLocal,hourLocal,minuteLocal,secLocal);


        for( i = baseyear ;i<=yearLocal - 1;i++)		//i = based tahun
        {
            if (i % 4 == 0) val = val + 366;
            else val = val + 365;
        }

        if (yearLocal % 4 == 0) Feb = 29;
        else Feb = 28;

        for( i = 1 ; i <= monthLocal - 1;i++)		//i = based bulan
        {
            switch(i)
            {
                case 1 : val += 31; break;
                case 3 : val += 31;break;
                case 5 : val += 31;break;
                case 7 : val += 31;break;
                case 8 : val += 31;break;
                case 10: val += 31;break;
                case 12: val += 31;break;
                case 4 : val += 30;break;
                case 6 : val += 30;break;
                case 9 : val += 30;break;
                case 11: val += 30;break;
                case 2 : val += Feb;break;
            }
        }

        val += dayLocal-1;
        val = val * 24;
        val += hourLocal;

        val = val * 60;
        val += minuteLocal;

        val = val * 60;
        val += secLocal;


        return val;
    }
    void test_conversion()
    {

        //byte[] ddmmyyyyhhmmss = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
        byte[] yymmddhhmmss= new byte[6];
        long ljulianSec;
        //Log.d(TAG,"ddmmyyyyhhmmss "+Hex.bytesToHexString(ddmmyyyyhhmmss));
        yymmddhhmmss=bcd_ddmmyyyyhhmmss2hex_yymmddhhmmss(util.Datenow("ddMMyyyyHHmmss"));

        Log.e(TAG,"yymmddhhmmss "+Hex.bytesToHexString(yymmddhhmmss));
        ljulianSec=julianSec(1995,yymmddhhmmss);
        Log.e(TAG,"ljulianSec "+ljulianSec);
        Log.e(TAG,"JULIAN "+ Hex.bytesToHexString(converter.LongtoByteArray(ljulianSec,converter.BIG_ENDIAN)));


    }



    @Override
    protected void onDestroy() {
        Log.d(TAG,".onDestroy");
        super.onDestroy();
        if(sti!=null)
        {
            sti.CloseDevice();
        }

    }



    public void showToast(int redId) {
        showToastOnUI(getString(redId));
    }


    public void showToast(String msg) {
        showToastOnUI(msg);
    }

    private void showToastOnUI(final String msg) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    }
                });

    }

            private void showText(final String text)
            {
                new Thread() {
                    public void run() {

                            try {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        tvTeks.setText(text);
                                        tvTeks.scrollTo(0, 0);
                                        tvTeks.invalidate();
                                    }
                                });
                                Thread.sleep(300);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                }.start();

            }

        String GetCodeDesc(int code) {
            switch (code) {
                default:
                    return Integer.toString(code);
            }
        }

    @Override
    public void onRFDetected(byte[] uid, int mode, ArrayList[] appdata) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        if(uid==null)
        {
            showText("RF card not found");
            return;

        }
        showText("UID : "+Hex.bytesToHexString(uid));
        showCardInformation( uid,  appdata);


    }


    private void showCardInformation(byte[] uid, ArrayList[]  appdata)
    {
        ArrayList<tag> taglist;
        tag currenttag;
        Iterator itr;
        String text="";
        int offset;
        text+="atr/uid : "+Hex.bytesToHexString(uid);
        if(appdata==null)
        {
            text+="\nNo application found";
        }
        else {
            text += "\ntotal apps : " + appdata.length + "\n";

            for (offset = 0; offset < appdata.length; offset++) {
                text += "=======================================================\n";
                text += "Application no : " + (offset + 1) + "\n";
                taglist = (ArrayList<tag>) appdata[offset];


                itr = taglist.iterator();
                while (itr.hasNext()) {
                    currenttag = (tag) itr.next();
                    if (currenttag.Value() != null) {
                        text += "tag : " + Hex.bytesToHexString(currenttag.Key()) + ", value : \n" + Hex.bytesToHexString(currenttag.Value()) + "\n";
                    }
                }
            }
        }
        showText(text);
    }
    @Override
    public void onContactDetected(byte[] uid, ArrayList[]  appdata) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());




        if(uid==null && appdata==null)
        {
            showText("No card detected.");
            return;
        }
        if(uid!=null && appdata==null)
        {
            showText("Incomplete process, please retry");
            return;
        }

        showCardInformation( uid,  appdata);

    }

    @Override
    public void onMSRDetected(byte[][] trackdata) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int track=0;
        String text="";
        if(trackdata==null)
        {
            showText("no msr swiped");
            return;

        }
        for(track=0;track<trackdata.length;track++)
        {
            text+="track "+(track+1)+" : "+Hex.bytesToHexString(trackdata[track])+"\n";


        }
        showText(text);

    }

    @Override
    public void onSAMDetected(int slot, byte[] atr) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());
    }




    @Override
    public void onPinPadHandler(byte[] data) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());
    }

    void FindContactCard()
    {
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {

                sti.FindContactCard();
            }
        };

        handler.post(r);

    }


    }