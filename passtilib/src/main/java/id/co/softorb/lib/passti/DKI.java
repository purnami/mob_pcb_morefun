package id.co.softorb.lib.passti;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

import java.util.Arrays;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.helper.ByteUtils;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.passti.helper.converter;
import id.co.softorb.lib.passti.helper.util;
import id.co.softorb.lib.smartcard.posdevice.params;
import id.co.softorb.lib.smartcard.posdevice.reader;


import static id.co.softorb.lib.passti.devinfo.*;
import static id.co.softorb.lib.passti.helper.ErrorCode.*;

//import com.example.helper.wificonfig;

 class DKI {

	//Command
	String TAG;
	String tagclass;

	//Var
	private String CardNo;
	private byte[] cardNo = new byte[8];
	private String Balance;
	private String Amount;
	private String ExpDate;
	private String Deposit;
	private byte[] RAPDU = new byte[255];
	private String[][] Hist = new String[11][5];
	//private Activity currentAct;
	private int histrec;
	private byte[] logDeductHex = new byte[255];
	String lasttrxBalance="0";
	String lastcardno="0";
	String lastamount="0";


	String saldoAwal,saldoAkhir = "";
 	boolean doCorrection = false;
	byte[] prevAmount;
	byte[] bBalance,prevBalance = new byte[4];
	byte[] selectapp_dki_prod ={(byte)0x00,(byte)0xA4,(byte)0x04,(byte)0x00,(byte)0x08,(byte)0xA0,(byte)0x00,(byte)0x00,(byte)0x05,(byte)0x71,(byte)0x4E,(byte)0x4A,(byte)0x43};
	PASSTI passti;
	 reader device;
	private byte[] reportRAPDU = new byte[255];
	 int samslot;
	 void SetSamPort(int slot)
	 {
		 samslot=slot;
	 }

	//public Luminos(Activity activity, Contactless ctl, SAM sam){
	 //DKI(Context ctx, UDeviceService deviceService, DeviceInfo deviceInfo,PASSTI passtiInstance,APOSUtility aposUtilityInstance) throws RemoteException, UnsupportedEncodingException {
	 DKI(Context ctx, PASSTI passtiInstance, reader reader)  {
		tagclass = this.getClass().getSimpleName();

		 passti = passtiInstance;
		device=reader;
		 samslot=255;//default for unassigned sam slot
		 TAG= passti.dbghelper.TAG;
	}

	static byte[] trim(byte[] bytes)
	{
		int i = bytes.length - 1;
		while (i >= 0 && bytes[i] == 0)
		{
			if(bytes[i-1]== (byte)0x90 && bytes[i] == (byte)0x00)
			{
				break;
			}

			--i;

		}

		return Arrays.copyOf(bytes, i + 1);
	}
    private int DKI_SAM_SelectAID() throws RemoteException {
        Log.d(TAG, "DKI_SAM_SelectAID");
        byte[] AID_DKI_PROD={(byte)0xD3,(byte)0x60,(byte)0x01,(byte)0x00,(byte)0x02,(byte)0x00,(byte)0x01};
        byte[] AID_DEV={(byte)0x57,(byte)0x10,(byte)0x0D,(byte)0xEF,(byte)0x00,(byte)0x00,(byte)0x01};
        byte[] selectAID={(byte)0x00,(byte)0xA4,(byte)0x04,(byte)0x00,(byte)0x07,(byte)0xD3,(byte)0x60,(byte)0x01,(byte)0x00,(byte)0x02,(byte)0x00,(byte)0x01};
        byte[] selectAID2={(byte)0x00,(byte)0xA4,(byte)0x04,(byte)0x00,(byte)0x07,(byte)0xD3,(byte)0x60,(byte)0x01,(byte)0x00,(byte)0x02,(byte)0x00,(byte)0x01};
        //BYTE selectAID[]={0x00,0xA4,0x04,0x00,0x07,0x57,0x10,0x0D,0xEF,0x00,0x00,0x01};
        int idx=0;
        int result=0;
        byte[] command = new byte[AID_DKI_PROD.length + selectAID.length];
        //memcpy(&selectAID[5],AID_DKI_PROD,sizeof(AID_DKI_PROD));
        System.arraycopy(AID_DKI_PROD,0,selectAID,5,AID_DKI_PROD.length);
        //memcpy(&cmdSendToSAM[idx],selectAID,sizeof(selectAID));
        System.arraycopy(selectAID,0,command,idx,selectAID.length);
        idx+=selectAID.length;
        byte[] command1 = new byte[idx];
        System.arraycopy(command,0,command1,0,command1.length);
        //result=SAM_SendCMD(port,cmdSendToSAM,idx,resp,resplen,SW_9000,&iErrorCode,errorMessage);
        byte[] RAPDU;
        //RAPDU = aposUtility.sendSAM(command1);
        RAPDU = device.sendSAM(samslot,command1);
        if(RAPDU == null){
            return DKI_SAM_ERR_SELECTAID;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            Log.d(TAG,"SAM_SelectAppDKIError - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            idx=0;
            command = new byte[AID_DEV.length + selectAID2.length];
            //memcpy(&selectAID[5],AID_DKI_PROD,sizeof(AID_DKI_PROD));
            System.arraycopy(AID_DEV,0,selectAID2,5,AID_DEV.length);
            //memcpy(&cmdSendToSAM[idx],selectAID,sizeof(selectAID));
            System.arraycopy(selectAID2,0,command,idx,selectAID2.length);
            idx+=selectAID2.length;
            command1 = new byte[idx];
            System.arraycopy(command,0,command1,0,command1.length);

            RAPDU = device.sendSAM(samslot,command1);
            if(RAPDU==null){
                return DKI_SAM_ERR_SELECTAID;
            }
            if(APDUHelper.CheckResult(RAPDU)!=OK){
                Log.d(TAG,"SAM_SelectAppDKIErrorDev - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
                return DKI_SAM_ERR_SELECTAID;
            }
            //return DKI_SAM_ERR_SELECTAID;
        }
        return OK;
    }
	
	public int CTL_SelectApp() throws RemoteException {
		Log.d(TAG,"CTLSelectApp");
		lastcardno=CardNo;

//		String command = luminos.ctl_selectapp();
		String command = BytesUtil.bytes2HexString(selectapp_dki_prod);
		passti.dbghelper.DebugPrintString(command);
		/*RAPDU=SmartCard.ISO_SEND(SmartCard.tagIso, ByteUtils.hexStringToByteArray(command));
		if(RAPDU==null)
			return ErrorCode.DKI_CTL_ERR_SELECTAPP;
			*/
		//RAPDU=ctl.SendAPDU(ByteUtils.hexStringToByteArray(command));
		RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(command));
		if(RAPDU==null)
		{
			return DKI_CTL_ERR_SELECTAPP;
		}
		if(APDUHelper.CheckResult(RAPDU)!=0)
		{
			Log.d(TAG,"CTL_SelectApp2");

			command = "00A4040007D3600000030003";

			RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(command));
			if(RAPDU==null)
			{
				return DKI_CTL_ERR_SELECTAPP;
			}
			if(APDUHelper.CheckResult(RAPDU)!=0)
			{
				return DKI_CTL_ERR_SELECTAPP;
			}
		}
		CardNo= ByteUtils.bytesToHex(RAPDU).substring(16, 32);
		cardNo = BytesUtil.hexString2Bytes(CardNo);
		ExpDate = ByteUtils.bytesToHex(RAPDU).substring(50, 58);
		Deposit = ByteUtils.bytesToHex(RAPDU).substring(74, 82);
		return OK;
	}
	
	private int CTL_GetBalance() throws RemoteException {
		Log.d(TAG,"CTL_GetBalance");
		/*aposUtility.searchAndActive(this);
		try{
			Thread.sleep(100);
		}catch (Exception e){
			e.printStackTrace();
		}*/
		CTL_SelectApp();
		lasttrxBalance=Balance;

		RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray("904C000004"));
		if(RAPDU==null)
		{
			Log.d(TAG,"RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
			return DKI_CTL_ERR_GETBALANCE;
		}
		if(APDUHelper.CheckResult(RAPDU) != 0){
			Log.d(TAG,"RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
			return DKI_CTL_ERR_GETBALANCE;
		}
		Log.d(TAG,"RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
		System.arraycopy(RAPDU,0,prevBalance,0,4);
		Balance= String.valueOf(converter.ByteArrayToInt(RAPDU, (byte)0, (byte)4, converter.BIG_ENDIAN));
		return OK;
	}

	private int SAM_InitPurchase() throws RemoteException {
		Log.d(TAG,"SAM_InitPurchase");
		byte[] bAmt = new byte[4];

		String send = "800200001BYYXXXXXXXX";
		byte[] sendRAPDU = new byte[RAPDU.length-2];
		System.arraycopy(RAPDU, 0, sendRAPDU, 0, RAPDU.length-2);
		bAmt= converter.IntegerToByteArray(Integer.valueOf(Amount));
		Log.d(TAG,"sendInitPurchase1 - " + send);
		send=send.replaceAll("XXXXXXXX", ByteUtils.bytesToHex(bAmt));
		Log.d(TAG,"sendInitPurchase2 - " + send);
		send=send.replaceAll("YY", ByteUtils.bytesToHex(sendRAPDU));
		Log.d(TAG,"sendInitPurchase3 - " + send);

		byte[] cmd = ByteUtils.hexStringToByteArray(send);

		RAPDU = device.sendSAM(samslot,cmd);
		if(RAPDU==null)
			return DKI_SAM_ERR_INITPSAM;
		if (RAPDU.length< 2) return DKI_SAM_ERR_INITPSAM;
		if(APDUHelper.CheckResult(RAPDU)!=0)	return DKI_SAM_ERR_INITPSAM;

		return OK;
	}

	private int SAM_DebitLSAM() throws RemoteException {
		Log.d(TAG,"SAM_DebitLSAM");
		byte[] bAmt = new byte[4];

		//String send =  luminos.lsam_debit()+Deposit+ExpDate;
		String send ="8056000122YYXXXXXXXX"+Deposit+ExpDate;

		byte[] sendRAPDU = new byte[RAPDU.length-2];
		System.arraycopy(RAPDU, 0, sendRAPDU, 0, RAPDU.length-2);
		bAmt= converter.IntegerToByteArray(Integer.valueOf(Amount));
		send=send.replaceAll("XXXXXXXX", ByteUtils.bytesToHex(bAmt));
		send=send.replaceAll("YY", ByteUtils.bytesToHex(sendRAPDU));
		Log.d(TAG,send);

		byte[] cmd = ByteUtils.hexStringToByteArray(send);

		RAPDU = device.sendSAM(samslot,cmd);
		if(RAPDU==null)
			return DKI_SAM_ERR_DEBITLSAM;
		if (RAPDU.length < 2) return DKI_SAM_ERR_DEBITLSAM;
		if (APDUHelper.CheckResult(RAPDU) != 0) return DKI_SAM_ERR_DEBITLSAM;


		return OK;
	}

	private int CTL_InitPurchase() throws RemoteException {
		Log.d(TAG,"CTL_InitPurchase");
		byte[] bAmt = new byte[4];

		//String send = luminos.ctl_initpurchase();
		String send = "9040030004XXXXXXXX17";
		bAmt= converter.IntegerToByteArray(Integer.valueOf(Amount));
		send=send.replaceAll("XXXXXXXX", ByteUtils.bytesToHex(bAmt));

		RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(send));
		if(RAPDU==null)
		{
			return DKI_CTL_ERR_INITPURCHASE;
		}
		if(APDUHelper.CheckResult(RAPDU)!=0)
			return DKI_CTL_ERR_INITPURCHASE;
		return OK;
	}

	private int CTL_InitTopup() throws RemoteException {
		byte[] bAmt = new byte[4];
		//String send = initTopUp;
		String send ="9040000004XXXXXXXX";
		bAmt= converter.IntegerToByteArray(Integer.valueOf(Amount));
		send=send.replaceAll("XXXXXXXX", ByteUtils.bytesToHex(bAmt));

		RAPDU = device.sendSAM(samslot,BytesUtil.hexString2ByteArray(send));
		if(RAPDU==null)
		{
			return DKI_CTL_ERR_INITTOPUP;
		}
		if(APDUHelper.CheckResult(RAPDU)!=0)	return DKI_CTL_ERR_INITTOPUP;
		return OK;
	}

	private int CTL_Debit() throws RemoteException {
		byte[] sendRAPDU = new byte[RAPDU.length-2];

		//String send = luminos.ctl_debitpurse();
		String send = "9046000012YY04";
		System.arraycopy(RAPDU, 0, sendRAPDU, 0, RAPDU.length-2);
		send=send.replaceAll("YY", ByteUtils.bytesToHex(sendRAPDU));

		RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(send));
		if(RAPDU==null)
		{
			SetCorrectionFlag();
			return DKI_CTL_ERR_DEBIT;
		}
		if(APDUHelper.CheckResult(RAPDU)!=0){SetCorrectionFlag();	return DKI_CTL_ERR_DEBIT;}
		return OK;
	}

	private int CTL_Topup() throws RemoteException {
		byte[] sendRAPDU = new byte[RAPDU.length-2];

		//String send = luminos.ctl_creditpurse();
		String send = "9042000010YY";
		System.arraycopy(RAPDU, 0, sendRAPDU, 0, RAPDU.length-2);
		send=send.replaceAll("YY", ByteUtils.bytesToHex(sendRAPDU));

		RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(send));
		if(RAPDU==null)
		{
			return DKI_CTL_ERR_TOPUP;
		}
		if(APDUHelper.CheckResult(RAPDU)!=0)	return DKI_CTL_ERR_TOPUP;
		return OK;
	}

	private boolean parseLog(int idx, String Data){
		int inc=0;
		String TrxType = Data.substring(0,  2);
		byte []bAmount = new byte[4];
		byte []bCTC = new byte[4];
		byte []bSTC = new byte[4];

		//trx type
		if(TrxType.compareTo("01")==0)			Hist[idx][inc++]="SALE";
		else if(TrxType.compareTo("02")==0)		Hist[idx][inc++]="TOP UP";
		else									return false;
		//amount
		bAmount= ByteUtils.hexStringToByteArray(Data.substring(20, 28));
		Hist[idx][inc++]= String.valueOf(converter.ByteArrayToInt(bAmount, (byte)0, (byte)4, converter.BIG_ENDIAN));
		//CTC
		bCTC= ByteUtils.hexStringToByteArray(Data.substring(44, 52));
		Hist[idx][inc++]= String.valueOf(converter.ByteArrayToInt(bCTC, (byte)0, (byte)4, converter.BIG_ENDIAN));
		//sam id
		Hist[idx][inc++]=Data.substring(28, 44);
		//STC
		bSTC= ByteUtils.hexStringToByteArray(Data.substring(12, 20));
		Hist[idx][inc++]= String.valueOf(converter.ByteArrayToInt(bSTC, (byte)0, (byte)4, converter.BIG_ENDIAN));

		return true;
	}

	private String getLog(int idx){

		//String Send = luminos.ctl_getlogidx();
		String Send = "00B2XX242E";
		byte []bIdx= converter.IntegerToByteArray(idx);
		String sIdx= ByteUtils.bytesToHex(bIdx);
		Send=Send.replaceAll("XX", sIdx.substring(6, 8));
		return Send;
	}

	private Boolean cGetTrxLog() throws RemoteException {
		int i,j;
		
		for(i=0;i<Hist.length;i++)
			for(j=0;j<Hist[i].length;j++)
				Hist[i][j]=null;
		
		for(i=1,j=1;i<11;i++){

			RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(getLog(i)));
			if(RAPDU==null)
			{
				histrec=j-1;
				return true;
			}
			if(APDUHelper.CheckResult(RAPDU)!=0)
			{
				histrec=j-1;
				return true;
			}
			if(parseLog(j, ByteUtils.bytesToHex(RAPDU))) j++;
		}
		histrec=j-1;
		return true;
	}
	
	private int GetHistoryNumber()
	{
		return histrec;
	}
	
	
	private int getCardInfo() throws RemoteException {
		int result;
		result =CTL_GetBalance();
		if(result!= OK)
			return result;

		if(!cGetTrxLog()) return DKI_CTL_ERR_GETTRXLOG;
		return OK;
	}
	
	 int checkbalance() throws RemoteException {
		int result;
		saldoAwal = "";
		result =CTL_GetBalance();
		if(result!= OK) {
//            aposUtility.rfReader.stopSearch();
            //aposUtility.RFStopSearch();
            return result;
        }
		saldoAwal = Balance;
        //aposUtility.rfReader.stopSearch();
		 //aposUtility.RFStopSearch();
		return OK;
	}
	
	private int CheckHistory() throws RemoteException {

		if(!cGetTrxLog()) return DKI_CTL_ERR_GETTRXLOG;
		return OK;
	}

	 int deduct(String amount) throws RemoteException {
	    saldoAwal = "";
	    saldoAkhir = "";
	    //Arrays.fill(reportRAPDU,(byte)0x00);
		reportRAPDU = new byte[255];
		int i = Payment(amount);
        //aposUtility.rfReader.stopSearch();
		 //aposUtility.RFStopSearch();
		return i;
	}

     String getCardNo(){
        return CardNo;
    }

     String getBalance(){return saldoAwal;}
     String getLastBalance(){
        return saldoAkhir;
    }

     byte[] getLogPASSTIDKI()
    {
        return reportRAPDU;
    }

	private int Payment(String sAmount) throws RemoteException {

		Amount=sAmount;

		int result;
		if(doCorrection){
			int x = DKI_CTL_ReadEFPurse();
			if(x!=OK){
				return x;
			}
			if(BytesUtil.bytecmp(prevBalance,bBalance,4)!=OK) {
				int c = DKI_DeductCorrection(prevAmount);
				return c;
			}
		}
		prevAmount = converter.IntegerToByteArray(Integer.valueOf(Amount));
		result = CTL_GetBalance();
		if(result!= OK)
			return result;
		saldoAwal = Balance;
		if(Integer.valueOf(Amount)> Integer.valueOf(Balance))
			return CTL_ERR_INSUFFICIENTBALANCE;


		result =CTL_InitPurchase();
		if(result!= OK)
			return result;
		result = DKI_SAM_SelectAID();
		if(result != OK)
			return result;
		result =SAM_InitPurchase();
		if(result!= OK)
			return result;

		result =CTL_Debit();
		if(result!= OK)
			return result;

		result =SAM_CreditPSAM();
		if(result!= OK)
			return result;

		//memcpy(ctlinfo.bBalanceKartu,&apdubuf[15],4);
		byte[] balancebyte = new byte[4];
		System.arraycopy(RAPDU,15,balancebyte,0,4);
		//ctlinfo.lBalanceKartu=dword2int(ctlinfo.bBalanceKartu);
		Balance= String.valueOf(converter.ByteArrayToInt(balancebyte, (byte)0, (byte)4, converter.BIG_ENDIAN));
		doCorrection = false;

		/*result = CTL_GetBalance();
		if(result!= OK)
			return result;*/
		saldoAkhir = Balance;
		byte[] curBalance = new byte[4];
		System.arraycopy(RAPDU,0,curBalance,0,4);
		byte[] devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
		byte[] amount = converter.IntegerToByteArray(Integer.valueOf(sAmount));
		RAPDU = passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value, params.ctype_dki,devicetime,cardNo,curBalance,amount,trxno,logDeductHex,others);
		if(RAPDU != null){
			Log.d(TAG,"PASSTI: " + BytesUtil.bytes2HexString(RAPDU));
		}
		/*RAPDU = new byte[logDeductHex.length];
		System.arraycopy(logDeductHex,0,RAPDU,0,logDeductHex.length);*/
		//reportRAPDU = RAPDU;
        reportRAPDU = new byte[RAPDU.length];
        System.arraycopy(RAPDU,0,reportRAPDU,0,RAPDU.length);
		return OK;
	}
	
	private int TopUp(String sAmount) throws RemoteException {
		Log.d(TAG,"TopUp");
		Amount=sAmount;
		int result;
		result = CTL_GetBalance();
		if(result!= OK)
			return result;
		if(Integer.valueOf(Amount)+ Integer.valueOf(Balance)>1000000)
								return CTL_ERR_TOPUPEXCEEDMAXBALANCE;

		result = CTL_InitTopup();
		if(result!= OK)
			return result;

		result = SAM_DebitLSAM();
		if(result!= OK)
			return result;
		result = CTL_Topup();
		if(result!= OK)
			return result;
		result = CTL_InitTopup();
		if(result!= OK)
			return result;
		result = SAM_ConfirmLSAM();
		if(result!= OK)
			return result;

		
		return OK;
	}


	
	private String[][]getHistory(){
		int inc=0;
		Hist[0][inc++]="Transaction Type";
		Hist[0][inc++]="Amount";
		Hist[0][inc++]="Card Trans Counter";
		Hist[0][inc++]="SAM ID";
		Hist[0][inc++]="SAM Trans Counter";		
		return Hist;
	}
	


	private int SAM_ConfirmLSAM() throws RemoteException {
		Log.d(TAG,"SAM_ConfirmLSAM");
		byte[] sendRAPDU = new byte[RAPDU.length-2];
		System.arraycopy(RAPDU, 0, sendRAPDU, 0, RAPDU.length-2);
		String send = "8058000008"+ ByteUtils.bytesToHex(sendRAPDU);

		//RAPDU=util.WebServiceExecuteAndGetResult(soapTopUp, send, currentAct);
		byte[] cmd = ByteUtils.hexStringToByteArray(send);
		//RAPDU=sam.SendAPDU(port_luminos,cmd);
		RAPDU = device.sendSAM(samslot,cmd);
		if(RAPDU==null)
			return DKI_SAM_ERR_CONFIRMLSAM;

		if (RAPDU.length< 2) return DKI_SAM_ERR_CONFIRMLSAM;
		if(APDUHelper.CheckResult(RAPDU)!=0)	return DKI_SAM_ERR_CONFIRMLSAM;

		return OK;
	}

	private int SAM_SelectAID() throws RemoteException {

		Log.d(TAG,TAG+".SAM_SelectAID");
		if(RAPDU==null)
		{
			return DKI_SAM_ERR_SELECTAID;
		}

		byte apdu_samaid []= {(byte)0x00,(byte)0xA4,(byte)0x04,(byte)0x00,(byte)0x07,(byte)0x57,(byte)0x10,(byte)0x0D,(byte)0xEF,(byte)0x00,(byte)0x00,(byte)0x01};
		//byte[] res = sam.SendAPDU(port, ByteUtils.hexStringToByteArray(card.ctl_selectapp()));
		//byte[] res = sam.SendAPDU(port_luminos, apdu_samaid);
		RAPDU = device.sendSAM(samslot,apdu_samaid);
		if(RAPDU==null)
		{
			return DKI_SAM_ERR_SELECTAID;
		}
		if(APDUHelper.CheckResult(RAPDU)!=0)
			return DKI_SAM_ERR_SELECTAID;
		return OK;

	}


	void SaveLog(byte[] resp)
	{
		logDeductHex=new byte[resp.length-2];
		System.arraycopy(resp,0,logDeductHex,0,logDeductHex.length);
		//sharedata.logDeduct = logDeductHex;
	}

	void SetCorrectionFlag(){
	 	doCorrection = true;
	}

	private int SAM_CreditPSAM() throws RemoteException {

		Log.d(TAG,"SAM_CreditPSAM");
		if(RAPDU==null)
		{
			return DKI_SAM_ERR_CREDITPSAM;
		}
		String send= "8004000004";
		byte[] cmd = Hex.hexStringToBytes(send);
		byte[] sendRAPDU = new byte[cmd.length+RAPDU.length-2];
		System.arraycopy(cmd, 0, sendRAPDU, 0, cmd.length);
		System.arraycopy(RAPDU, 0, sendRAPDU, cmd.length, RAPDU.length-2);
		Log.d(TAG, "APP -> SAM : "+ Hex.bytesToHexString(sendRAPDU));
		//RAPDU= util.WebServiceExecuteAndGetResult(soapPayment, Hex.bytesToHexString(sendRAPDU), currentAct);
		cmd = sendRAPDU;
		//RAPDU=sam.SendAPDU(port_luminos,cmd);
		RAPDU = device.sendSAM(samslot,cmd);
		if(RAPDU==null) {
			SetCorrectionFlag();
			return DKI_SAM_ERR_CREDITPSAM;
		}
		Log.d(TAG, "SAM -> APP : "+ Hex.bytesToHexString(RAPDU));
		if (RAPDU.length< 2){SetCorrectionFlag(); return DKI_SAM_ERR_CREDITPSAM;}
		if(APDUHelper.CheckResult(RAPDU)!=0) {
			SetCorrectionFlag();
			return DKI_SAM_ERR_CREDITPSAM;
		}
		else
		{
			//process trxlog here
			if((RAPDU[RAPDU.length-2]==(byte)0x90)&&(RAPDU[RAPDU.length-1]==(byte)0x00))
			{

				SaveLog(RAPDU);


			}
			if(RAPDU[RAPDU.length-2]==(byte)0x61)
			{
				sendRAPDU = new byte[5];
				sendRAPDU[0]=(byte)0x00;
				sendRAPDU[1]=(byte)0xC0;
				sendRAPDU[2]=(byte)0x00;
				sendRAPDU[3]=(byte)0x00;
				sendRAPDU[4]=RAPDU[RAPDU.length-1];
				Log.d(TAG, "APP -> SAM : "+ Hex.bytesToHexString(sendRAPDU));

				cmd = sendRAPDU;
				//RAPDU=sam.SendAPDU(port_luminos,cmd);
				RAPDU = device.sendSAM(samslot,cmd);
				if(RAPDU==null) {
					SetCorrectionFlag();
					return DKI_SAM_ERR_CREDITPSAM;
				}
				Log.d(TAG, "SAM -> APP : "+ Hex.bytesToHexString(RAPDU));
				if(RAPDU==null) {
					SetCorrectionFlag();
					return DKI_SAM_ERR_CREDITPSAM;
				}

				SaveLog(RAPDU);

			}
		}
		return OK;
	}



	int DKI_CTL_InitRepurchase(byte[]  data) throws RemoteException {
		//debug_print(TAG, "DKI_CTL_InitRepurchase");
		byte[] apdu={(byte)0x90,(byte)0x40,(byte)0x04,(byte)0x00,(byte)0x04};
		int idx=0;
		int result;
		byte[] sStatusCode = new byte[3];

		byte[] cmdSendToCL = new byte[255];
		System.arraycopy(apdu,0,cmdSendToCL,idx,apdu.length);
		idx+=apdu.length;

		System.arraycopy(data,0,cmdSendToCL,idx,data.length);

		idx+=data.length;
		byte[] command = new byte[idx];
		System.arraycopy(cmdSendToCL,0,command,0,idx);


		RAPDU = device.sendCTL(command);

		if(RAPDU == null){
			return DKI_CTL_ERR_INITREPURCHASE;
		}
		if(APDUHelper.CheckResult(RAPDU)!=OK){
			return DKI_CTL_ERR_INITREPURCHASE;
		}

		return OK;
	}

	int DKI_SAM_ReinitPurchase(byte[] pursedata,byte[] amount) throws RemoteException {
		//debug_print(DEBUG_CHNL, "DKI_SAM_ReinitPurchase");
		byte[] apdu={(byte)0x80,(byte)0x22,(byte)0x00,(byte)0x00,(byte)0x2B};
		byte[] cmd = new byte[100];
		int result=0;
		int offset=0;
		byte[] sStatusCode = new byte[3];

		System.arraycopy(apdu,0,cmd,offset,apdu.length);
		offset+=apdu.length;
		//memcpy(&cmd[offset],pursedata,pursedatalen);
		System.arraycopy(pursedata,0,cmd,offset,pursedata.length);
		offset+=pursedata.length;
		//memcpy(&cmd[offset],amount,lenAmt);
		System.arraycopy(amount,0,cmd,offset,amount.length);
		offset+=amount.length;

		byte[] command = new byte[offset];
		System.arraycopy(cmd,0,command,0,offset);
		//result=SendSAM(port_dki,cmd,offset,resp,resplen);
		RAPDU = device.sendSAM(samslot,command);
		if(RAPDU == null){
			return DKI_SAM_ERR_INITREPURCHASE;
		}
		if(APDUHelper.CheckResult(RAPDU)!=OK){
			return DKI_SAM_ERR_INITREPURCHASE;
		}


		return OK;
	}

	int DKI_CTL_RePurchase(byte[] datain) throws RemoteException {
		//debug_print(DEBUG_CHNL, "DKI_CTL_RePurchase");
		byte[] repurchasePurse ={(byte)0x90,(byte)0x48,(byte)0x00,(byte)0x00,(byte)0x12};
		int idx=0;
		int result;
		byte[] sStatusCode = new byte[3];
		//memset(sStatusCode,0x00,3);
		byte[] cmdSendToCL = new byte[255];

		System.arraycopy(repurchasePurse,0,cmdSendToCL,idx,repurchasePurse.length);
		idx+=repurchasePurse.length;

		System.arraycopy(datain,0,cmdSendToCL,idx,datain.length);
		idx+=datain.length;
		cmdSendToCL[idx++]=0x04;

		byte[] command = new byte[idx];
		System.arraycopy(cmdSendToCL,0,command,0,idx);

		RAPDU = device.sendCTL(command);
		if(RAPDU == null){
			return DKI_CTL_ERR_REPURCHASE;
		}
		if(APDUHelper.CheckResult(RAPDU)!=OK){
			return DKI_CTL_ERR_REPURCHASE;
		}

		return OK;


	}

	int DKI_SAMConfirmRepurchase(byte[] inData) throws RemoteException {
		//debug_print(DEBUG_CHNL, "DKI_SAMConfirmRepurchase");
		byte[] apdu={(byte)0x80,(byte)0x44,(byte)0x00,(byte)0x00,(byte)0x04};
		byte[] cmd = new byte[100];
		int result=0;
		int offset=0;
		byte[] sStatusCode = new byte[3];

		System.arraycopy(apdu,0,cmd,offset,apdu.length);
		offset+=apdu.length;

		System.arraycopy(inData,0,cmd,offset,inData.length);
		offset+=inData.length;
		cmd[offset++]=0x31;//Le

		byte[] command = new byte[offset];
		System.arraycopy(cmd,0,command,0,offset);

		RAPDU = device.sendSAM(samslot,command);
		if(RAPDU == null){
			return DKI_SAM_ERR_REPURCHASE;
		}
		if(APDUHelper.CheckResult(RAPDU)!=OK){
			return DKI_SAM_ERR_REPURCHASE;
		}

		return OK;
	}


	int DKI_DeductCorrection(byte[] amount) throws RemoteException {

		int result;


		result = DKI_CTL_InitRepurchase(amount);
		if(result!=OK)
		{
			return result;
		}
		byte[] purseData = new byte[RAPDU.length-2];
		System.arraycopy(RAPDU,0,purseData,0,purseData.length);
		result = DKI_SAM_ReinitPurchase(purseData,amount);
		if(result!=OK)
		{

			return result;
		}
		purseData = new byte[RAPDU.length-2];
		System.arraycopy(RAPDU,0,purseData,0,purseData.length);
		result = DKI_CTL_RePurchase(purseData);
		if(result!=OK)
		{

			return result;
		}
		purseData = new byte[RAPDU.length-2];
		System.arraycopy(RAPDU,0,purseData,0,purseData.length);
		result = DKI_SAMConfirmRepurchase(purseData);
		if(result!=OK)
		{

			return result;
		}

		SaveLog(RAPDU);
		byte[] balancebyte = new byte[4];
		System.arraycopy(RAPDU,15,balancebyte,0,4);

		Balance= String.valueOf(converter.ByteArrayToInt(balancebyte, (byte)0, (byte)4, converter.BIG_ENDIAN));
		doCorrection = false;

		saldoAkhir = Balance;
		byte[] curBalance = new byte[4];
		System.arraycopy(RAPDU,0,curBalance,0,4);
		byte[] devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
		byte[] bamount = converter.IntegerToByteArray(Integer.valueOf(Amount));

		RAPDU = new byte[logDeductHex.length];
		System.arraycopy(logDeductHex,0,RAPDU,0,logDeductHex.length);

		reportRAPDU = new byte[RAPDU.length];
		System.arraycopy(RAPDU,0,reportRAPDU,0,RAPDU.length);


		return OK;

	}

	int DKI_CTL_ReadEFPurse() throws RemoteException {
		//debug_print(DEBUG_CHNL, "DKI_CTL_ReadEFPurse");
		byte[] apdu={(byte)0x00,(byte)0xB2,(byte)0x01,(byte)0x24,(byte)0x2E};
		int result=0;

		RAPDU = device.sendCTL(apdu);
		if(RAPDU == null)
		{
			return ERR_NO_RESP;
		}
		if(APDUHelper.CheckResult(RAPDU)!=OK){
			return ERR_SW1SW2;
		}

		 System.arraycopy(RAPDU,2,bBalance,0,4);
		return OK;
	}

}
