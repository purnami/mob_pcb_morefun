package id.co.softorb.lib.passti;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.CountDownTimer;
import android.os.RemoteException;
import android.util.Log;

/*
import com.cloudpos.DeviceException;
import com.cloudpos.OperationListener;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.TimeConstants;
import com.cloudpos.card.Card;
import com.cloudpos.printer.Format;
import com.cloudpos.rfcardreader.RFCardReaderDevice;
import com.cloudpos.rfcardreader.RFCardReaderOperationResult;
import com.cloudpos.sdk.impl.POSTerminalImpl;
*/
import com.wizarpos.jni.PinPadCallbackHandler;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.Arrays;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.passti.helper.converter;
import id.co.softorb.lib.passti.sampledata.TestEMV;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.PASSTIListener;
import id.co.softorb.lib.smartcard.posdevice.DeviceFunction;
import id.co.softorb.lib.smartcard.posdevice.params;
import id.co.softorb.lib.smartcard.posdevice.reader;

import static id.co.softorb.lib.helper.ErrorCode.*;
//import static id.co.softorb.lib.passti.helper.ErrorCode.*;

import static id.co.softorb.lib.smartcard.posdevice.params.*;

/**
 * Created by softorb.co.id on 5/22/2019.
 */

public class STIUtility implements POSListener {


    Mandiri mandiri;
    BRI bri;
    Luminos luminos;

    BNI bni;

    Context ctx;
    String TAG=getClass().getSimpleName();
    String tagclass= this.getClass().getSimpleName();

    int card = 0;

    boolean countdownfinish;
    boolean enable,boolLuminos,boolMdr,boolBRI,boolDKI,boolBNI = false;

    boolean initMdr,initbni,initbri,initcug = false;
    DKI dki;
    PASSTI passti;


    int iRet;
    reader device;
    String clientkey;
    String encryptkey;
    PASSTIListener callback;
    emv emvcard;
    TestEMV test;
    public STIUtility(Context ctx, PASSTIListener callback) throws RemoteException {

        device = new reader(ctx,this);
        this.ctx = ctx;
        clientkey=ctx.getResources().getString(R.string.clientKey_dev);
        encryptkey=ctx.getResources().getString(R.string.encryptKey_dev);
        devinfo.others= new byte[2];
        this.callback=callback;
    }
    public int printer_paperstatus()
    {
        return device.printer_paperstatus();

    }
    public void printer_cutpaper()
    {
        device.printer_cutpaper();

    }
    public void print_text(int fonttype,int alignment,String text,boolean newline)
    {
        device.print_text(fonttype,alignment,text,newline);
    }
    /*public void print_text(Format format,String text, boolean newline)
    {
        device.print_text(format,text,newline);
    }*/


    public void printer_linefeed()
    {
        device.printer_linefeed();
    }

    public void CloseDevice()
    {
        device.CloseDevice();
    }
    public int SetDeviceService(Context ctx,int devicetype)
    {

        return device.SetDeviceService(ctx,devicetype);


    }
    public reader getDeviceReader()
    {
        return device;
    }
    public PASSTI getPASSTIInstance()
    {
        return passti;
    }

	public int DeviceType()
    {
        return device.DeviceType();
    }
    public void SetDeviceType(int type)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        device.SetDeviceType(type);
    }
    public void SetTrxCounter(int counter)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
            .getStackTrace()[0]
            .getMethodName());
        devinfo.trxno= converter.InttoByteArray(counter,converter.BIG_ENDIAN);

    }
    public void SetBRIRefNo(String refno)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        bri.SetRefNo(refno);
    }
    public void print_bitmap(int alignment, Bitmap data)
    {
        device.print_bitmap(alignment,data);

    }
    public int SetMID(byte[] MID)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(MID.length!=8)
            return ERR_PARAM_MID;
		if(passti==null)
        {
            Log.e(TAG,"passti is null");
            return NOT_OK;
        }
        passti.MID=MID;
        return OK;
    }
    public int SetTID(byte[] TID)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(TID.length!=4)
            return ERR_PARAM_TID;
        if(passti==null)
        {
            Log.e(TAG,"passti is null");
            return NOT_OK;
        }
        passti.TID=TID;
        return OK;
    }
    public int initBank() throws UnsupportedEncodingException, RemoteException {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(!enable){
            return Integer.valueOf(ERR_LIB_NOTINIT);
        }

            //passti = new PASSTI(ctx, device);
            emvcard = new emv(ctx,passti,device);
            //test = new TestEMV(emvcard);
            TAG=passti.dbghelper.TAG;
			if(passti==null)
            {
                Log.e(TAG,"passti is null");
            }
            passti.SetLibVersion(getLibVersion());
            passti.SetEncryptKey(encryptkey);
            mandiri = new Mandiri(ctx, passti, device);
            if(mandiri==null)
            {
                Log.e(TAG,"mandiri is null");
            }

            luminos = new Luminos(ctx, passti, device);
            if(luminos==null)
            {
                Log.e(TAG,"luminos is null");
            }
            bni = new BNI(ctx, passti, device);
            if(bni==null)
            {
                Log.e(TAG,"bni is null");
            }
            bri = new BRI(ctx, passti, device);
            if(bri==null)
            {
                Log.e(TAG,"bri is null");


            }
            dki = new DKI(ctx, passti, device);
            if(dki==null)
            {
                Log.e(TAG,"dki is null");
            }
        return OK;
    }

public int checkbalance() throws RemoteException {
    Log.i(TAG, tagclass+"."+new Throwable()
            .getStackTrace()[0]
            .getMethodName());
    if(!enable){
        Log.d("masuksiniii","masuk3");
            return Integer.valueOf(ERR_LIB_NOTINIT);
    }
    Log.d("masuksiniii","masuk4");
        //int i = CTL_ERR_READFAIL;
        int i = ERR_CARDTYPEINACTIVE;//2020-05-28,vivo, return ERR_CARDTYPEINACTIVE by default this
    Log.d("masuksiniii","masuk5  "+i);
        card = checkMyCard();

        Log.d("cardd",""+card);
    switch (card) {
        case params.ctype_mdr:
            if(!boolMdr){
                device.RFHalt();
                return ERR_CARDTYPEINACTIVE;
            }
            i = mandiri.checkBalance();
            break;


        case params.ctype_bri:
            if(!boolBRI){
                device.RFHalt();
                return ERR_CARDTYPEINACTIVE;
            }
            i = bri.checkbalance();
            break;


        case params.ctype_bni:
            Log.d("boolbni",""+boolBNI);
            if(!boolBNI){
                device.RFHalt();
                return ERR_CARDTYPEINACTIVE;
            }
            i = bni.checkbalance();
            break;

        case params.ctype_luminos:
            Log.d("boolluminos",""+boolLuminos);
            if(!boolLuminos){
                device.RFHalt();
                return ERR_CARDTYPEINACTIVE;
            }
            i = luminos.checkbalance();
            Log.d("masuk habis cek balance", "ya");
            break;
        case params.ctype_dki:
            if(!boolDKI){
                device.RFHalt();
                return ERR_CARDTYPEINACTIVE;
            }
            i = dki.checkbalance();

            break;
            case CTL_ERR_NOTFOUND:
                device.RFHalt();
                return CTL_ERR_NOTFOUND;
            case CTL_ERR_READFAIL:
                device.RFHalt();
                return CTL_ERR_READFAIL;
            case ERR_SW1SW2:
                device.RFHalt();
                return ERR_CARDTYPEINACTIVE;

    }
    Log.d("masuk rfhalt", "ya");
        //device.RFStopSearch();;
    device.RFHalt();
    return i;
}

    public int initSAMVar_Mandiri(String PIN,String Ins_ID,String TID){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(PIN.length()!=16)
            return ERR_PARAM_PIN;
        if(Ins_ID.length()!=4)
            return ERR_PARAM_MID;
        if(TID.length()!=8)
            return ERR_PARAM_TID;

        System.arraycopy(BytesUtil.hexString2Bytes(PIN),0,devinfo.issuer.mdrpin,0,BytesUtil.hexString2Bytes(PIN).length);
        System.arraycopy(BytesUtil.hexString2Bytes(Ins_ID),0,devinfo.issuer.mdrInstID,0,BytesUtil.hexString2Bytes(Ins_ID).length);
        System.arraycopy(BytesUtil.hexString2Bytes(TID),0,devinfo.issuer.mdrTid,0,BytesUtil.hexString2Bytes(TID).length);


        initMdr = true;
        return OK;
    }

    public int initSAMVar_BRI(String procode,String batchno, String MID, String TID){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());


        if(batchno.length()!=2)
            return ERR_PARAM_BATCH;
        if(procode.length()!=6)
            return ERR_PARAM_PROCODE;
        if(MID.length()!=16)
            return ERR_PARAM_MID;
        if(TID.length()!=8)
            return ERR_PARAM_TID;

        devinfo.issuer.briTid=BytesUtil.hexString2Bytes(TID);
        devinfo.issuer.str_briTid=TID;
        devinfo.issuer.briMid=BytesUtil.hexString2Bytes(MID);
        devinfo.issuer.str_briMid=MID;
        System.arraycopy(procode.getBytes(),0,devinfo.issuer.briProcode,0,devinfo.issuer.briProcode.length);
        devinfo.issuer.briBatch=BytesUtil.hexString2Bytes(batchno);


        passti.dbghelper.DebugHex("devinfo.issuer.briTid",devinfo.issuer.briTid,devinfo.issuer.briTid.length);
        passti.dbghelper.DebugHex("devinfo.issuer.briMid",devinfo.issuer.briMid,devinfo.issuer.briMid.length);
        passti.dbghelper.DebugHex("devinfo.issuer.briProcode",devinfo.issuer.briProcode,devinfo.issuer.briProcode.length);
        passti.dbghelper.DebugHex("devinfo.issuer.briBatch",devinfo.issuer.briBatch,devinfo.issuer.briBatch.length);

        initbri = true;
        return OK;
    }

    public int initSAMVar_BNI(String MID, String TID, String MarriedCode){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        if(MarriedCode.length()!=32)
            return ERR_PARAM_PIN;
        if(MID.length()!=16)
            return ERR_PARAM_MID;
        if(TID.length()!=8)
            return ERR_PARAM_TID;

        devinfo.issuer.bniTid=BytesUtil.hexString2Bytes(TID);
        passti.dbghelper.DebugHex("devinfo.issuer.bniTid",devinfo.issuer.bniTid,devinfo.issuer.bniTid.length);
        devinfo.issuer.bniMid=BytesUtil.hexString2Bytes(MID);
        passti.dbghelper.DebugHex("devinfo.issuer.bniMid",devinfo.issuer.bniMid,devinfo.issuer.bniMid.length);
        devinfo.issuer.bnimrgcode=BytesUtil.hexString2Bytes(MarriedCode);
        passti.dbghelper.DebugHex("devinfo.issuer.bnimrgcode",devinfo.issuer.bnimrgcode,devinfo.issuer.bnimrgcode.length);
        initbni = true;
        return OK;
    }

    public int getActionCode(){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return passti.GetAction();
    }
    public void CancelRepurchase()
    {
        passti.SetAction(NORMAL);
    }

    public Object getPinPad()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return device.getPinPad();
    }
    public Object getLED()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return device.getLED();
    }
    public int initCTL_SAM(int type,int slot){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int iRet=CTL_ERR_UNKNOWNCARD;
        if(!enable){
            return ERR_LIB_NOTINIT;
        }
        switch(slot)
        {
            //valid number for sam slot is 1
            case 1:
                break;
            default:
                return ERR_INCORRECT_SAMSLOT;
        }

        switch(type)
        {
            //valid number for sam slot is 1
            case ctype_mdr:
            case ctype_bni:
            case ctype_bri:
            case ctype_luminos:
                break;
            default:
                return ERR_INCORRECT_CARDTYPE;
        }

        devinfo.operation="";

        iRet=device.SAMFindCard(type,slot);
        switch(type)
        {
            case params.ctype_mdr:


                if(iRet==OK)
                {
                    if(mandiri==null)
                    {
                        mandiri = new Mandiri(ctx,passti,device);
                    }
                    mandiri.SetSamPort(slot);
                    boolMdr = true;
                    Log.d(TAG,"boolMdr true");
                }
                else
                {
                    boolMdr = false;
                    Log.e(TAG,"boolMdr false");
                }


                break;
            case params.ctype_luminos:


                if(iRet==OK)
                {
                    if(luminos==null)
                    {
                        luminos = new Luminos(ctx,passti,device);
                    }
                    luminos.SetSamPort(slot);
                    boolLuminos = true;
                    Log.d(TAG,"boolLuminos true");
                }
                    else
                {
                    boolLuminos = false;
                    Log.e(TAG,"boolLuminos false");
                }

                break;
            case params.ctype_bri:


                if(iRet==OK)
                {
                    if(bri==null)
                    {
                        bri = new BRI(ctx,passti,device);
                    }
                    bri.SetSamPort(slot);
                    boolBRI = true;
                    Log.d(TAG,"boolBRI true");
                }
                        else
                {
                    boolBRI = false;
                    Log.e(TAG,"boolBRI false");
                }

                break;
            case params.ctype_bni:


                if(iRet==OK)
                {
                    if(bni==null)
                    {
                        bni = new BNI(ctx,passti,device);
                    }
                    bni.SetSamPort(slot);
                    boolBNI = true;
                    Log.d(TAG,"boolBNI true");
                }
                            else
                {
                    boolBNI = false;
                    Log.e(TAG,"boolBNI false");
                }

                break;
            case params.ctype_dki:


                if(iRet==OK)
                {
                    if(dki==null)
                    {
                        dki = new DKI(ctx,passti,device);
                    }
                    dki.SetSamPort(slot);
                    boolDKI = true;
                    Log.d(TAG,"boolDKI true");
                }
                                else
                {
                    boolDKI = false;
                    Log.e(TAG,"boolBNI false");
                }

                break;
            default:

                iRet=ERR_INCORRECT_CARDTYPE;
                break;

        }


        return iRet;
    }

    public String getBalance(){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(!enable){
            return String.valueOf(ERR_LIB_NOTINIT);
        }
        String saldoAwal = "";

        switch(card)
        {

            case params.ctype_mdr:
                saldoAwal = mandiri.getBalance();
                break;
            case params.ctype_luminos:
                saldoAwal = luminos.getBalance();
                break;
            case params.ctype_bri:
                saldoAwal = bri.getBalance();
                break;
            case params.ctype_bni:
                saldoAwal = bni.getBalance();
                break;
            case params.ctype_dki:
                saldoAwal = dki.getBalance();
                break;
            default:
                break;

        }

        return saldoAwal;
    }

    public String getLastBalance(){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(!enable){
            return String.valueOf(ERR_LIB_NOTINIT);
        }
        String saldoAkhir = "";
        switch(card)
        {

            case params.ctype_mdr:
                saldoAkhir = mandiri.getLastBalance();
                break;
            case params.ctype_luminos:
                saldoAkhir = luminos.getLastBalance();
                break;
            case params.ctype_bri:
                saldoAkhir = bri.getLastBalance();
                break;
            case params.ctype_bni:
                saldoAkhir = bni.getLastBalance();
                break;
            case params.ctype_dki:
                saldoAkhir = dki.getLastBalance();
                break;
            default:
                break;

        }
        return saldoAkhir;
    }

        public String getCardNo(){
            Log.i(TAG, tagclass+"."+new Throwable()
                    .getStackTrace()[0]
                    .getMethodName());
            if(!enable){
                return String.valueOf(ERR_LIB_NOTINIT);
            }

            String cardNo;

            passti.dbghelper.DebugHex("passti.ctlinfo.cardno",passti.ctlinfo.cardno);
            if(passti.ctlinfo.cardno != null) {
                cardNo = Hex.bytesToHexString(passti.ctlinfo.cardno);
            }else cardNo = "Card Not Found";
            return cardNo;


        }



    public int deduct(String amount) throws RemoteException {
        Log.d("deduct masuk", "");
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(!enable){
            Log.d("enable", ""+enable);
            return Integer.valueOf(ERR_LIB_NOTINIT);
        }
		if(passti.MID==null)
        {
            return ERR_PARAM_MID;
        }
        if(passti.TID==null)
        {
            return ERR_PARAM_TID;
        }
        if(passti.MID.length!=8)
        {
            return ERR_PARAM_MID;
        }
        if(passti.TID.length!=4)
        {
            return ERR_PARAM_TID;
        }
        int i = ERR_CARDTYPEINACTIVE;//2020-05-28,vivo, return ERR_CARDTYPEINACTIVE by default this
        //device.RFFindCard(listener);
        //devinfo.operation="";//reset flag, indicate no card found
        card = checkMyCard();
        Log.d("deduct cardd", ""+card);
        switch(card)
        {

            case params.ctype_mdr:
                if(!boolMdr){
                    device.RFHalt();
                    return ERR_CARDTYPEINACTIVE;
                }
                if(!initMdr){
                    device.RFHalt();
                    return MDR_SAM_ERR_REINIT;
                }
                i = mandiri.deduct(amount);
                Log.e(TAG,"mandiri.deduct "+i);
                initMdr = false;
                break;
            case params.ctype_luminos:
                Log.d("boolluminos123",""+boolLuminos);
                if(!boolLuminos){
                    Log.d("boolluminos",""+boolLuminos);
                    device.RFHalt();
                    return ERR_CARDTYPEINACTIVE;
                }
                i = luminos.deduct(amount);
                Log.e(TAG,"luminos.deduct "+i);

                break;
            case params.ctype_bri:
                if(!boolBRI){
                    device.RFHalt();
                    return ERR_CARDTYPEINACTIVE;
                }
                if(!initbri){
                    device.RFHalt();
                    return BRI_SAM_ERR_REINIT;
                }
                i = bri.deduct(amount);
                Log.e(TAG,"bri.deduct "+i);
                initbri = false;
                break;
            case params.ctype_bni:
                if(!boolBNI){
                    device.RFHalt();
                    return ERR_CARDTYPEINACTIVE;
                }
                if(!initbni){
                    device.RFHalt();
                    return BNI_SAM_ERR_REINIT;
                }
                i= bni.deduct(amount);
                Log.e(TAG,"bni.deduct "+i);
                initbni = false;
                break;
            case params.ctype_dki:
                if(!boolDKI){
                    device.RFHalt();
                    return ERR_CARDTYPEINACTIVE;
                }
                i = dki.deduct(amount);
                Log.e(TAG,"dki.deduct "+i);
                break;
             case CTL_ERR_NOTFOUND:
                 device.RFHalt();
                return CTL_ERR_NOTFOUND;
            case CTL_ERR_READFAIL:
                device.RFHalt();
                return CTL_ERR_READFAIL;
            case ERR_SW1SW2:
                device.RFHalt();
                return ERR_CARDTYPEINACTIVE;

        }
        //device.RFStopSearch();
        device.RFHalt();

        return i;
    }

    public byte[] getPASSTILog(){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] log = new byte[255];
        if(!enable){
            log = new byte[1];
            log[0] = (byte)ERR_LIB_NOTINIT;
            return log;
        }
        log= passti.log;


        return log;
    }
    public boolean checkInitLib(){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return enable;
    }
    /*
    public byte[] getCardUID(){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] empty = new byte[8];
        if(!enable){
            return empty;
        }
        return passti.ctlinfo.uid.value;
    }*/

    public int FindMSR(){

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(checkInitLib()==false)
            return ERR_LIB_NOTINIT;

        device.FindMSR();

        return WAITING;
    }
    public int FindCTL_EMV()
    {
        if(checkInitLib()==false)
            return ERR_LIB_NOTINIT;
        FindRFCard(devinfo.emv);//find contactless
        return WAITING;
    }
    public int FindContactCard()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(!checkInitLib())
            return ERR_LIB_NOTINIT;

        devinfo.operation="";//reset flag, indicate no card found
        device.ContactFindCard();
        countdownfinish = false;
        Log.d("masuk contact", "0");
        while(devinfo.operation.matches("")){
            if(devinfo.operation.matches(ctx.getString(R.string.contactdetected))){
                break;
            }
            if(countdownfinish){
                return CB_ERR_NOTFOUND;

            }
        }
        Log.d("masuk contact", "ok");
        return OK;

    }
    private int FindRFCard(int mode)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        Log.d("masuksiniii","masuk8  "+mode);

        devinfo.operation="";//reset flag, indicate no card found
        device.FindRFCard(mode);

        countdownfinish = false;
        Log.d("masuksiniii","masuk9");
//        while(devinfo.operation.matches("")){
//            if(devinfo.operation.matches(ctx.getString(R.string.carddetected))){
//                break;
//            }
//            if(countdownfinish==true){
//
//                return CTL_ERR_NOTFOUND;
//
//            }
//        }
        return OK;
    }
    private int checkMyCard() throws RemoteException {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        Log.d("masuksiniii","masuk6");
        iRet= FindRFCard(devinfo.prepaid);
        Log.d("masuksiniii","masuk7  "+iRet);
        if(iRet!=OK)
            return iRet;

        int i = ctype_unknown;

        i= mandiri.Mandiri_CTL_Initialization();
        Log.e(TAG,"Mandiri_CTL_Initialization = "+i);
        if(i==OK) {
            card=params.ctype_mdr;
            return params.ctype_mdr;
        }

        i = bri.BRI_CTL_SelectAIDDesfire();
        Log.e(TAG,"bri.BRI_CTL_SelectAIDDesfire = "+i);
        if(i==OK){card=params.ctype_bri;
            return params.ctype_bri;
        }

        i = bni.BNI_CTL_SelectAID();
        Log.e(TAG,"bni.BNI_CTL_SelectAID = "+i);
        if(i==OK) {
            Log.d(TAG,"bni sukses");
            card=params.ctype_bni;
            return params.ctype_bni;
        }

        //i=luminos.CTL_SelectApp(ctx.getString(R.string.ctl_aid_passtikit));
        i=luminos.CTL_SelectApp(ctx.getString(R.string.ctl_aid_bali));
        Log.e(TAG,"luminos.CTL_SelectApp = "+i);
        if(i==OK)
        {
               card=params.ctype_luminos;
            return params.ctype_luminos;
        }
        switch(i)
        {
            case ERR_NO_RESP:
                return CTL_ERR_READFAIL;
            case ERR_SW1SW2:
                return ERR_SW1SW2;

        }

        return i;
    }


    //Declare loopPost
    CountDownTimer cTimer = null;

    //start loopPost function
    void startTimer() {
        cTimer = new CountDownTimer(7000, 10) {
            public void onTick(long millisUntilFinished) {
                //Log.d(TAG,"Time left: " + millisUntilFinished);

            }
            public void onFinish() {

                countdownfinish=true;

            }
        };
        cTimer.start();
    }

    void cancelTimer() {
        if(cTimer!=null)
            cTimer.cancel();

    }
    public String DeviceLibVersion()
    {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        String version="unknown";
        version=device.LibVersion();
        return version;
    }
    public String getLibVersion()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        //format must be in xx.yy.zz, otherwise cause index out of bound

        return "02.00.00";

    }


    public int initLib(String clientID)
    {
        Log.i(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());
    if(clientID.length() != 32){
            Log.e(TAG, "Invalid length");
            return Integer.valueOf(ERR_INVALID_KEY);
        }
        Log.d(TAG,"Clientkey "+clientkey);
        Log.d(TAG, "clientID " + clientID);

        if(clientkey.matches(clientID)){
            enable = true;
            try {
                passti = new PASSTI(ctx, device);
            } catch (RemoteException e) {
                e.printStackTrace();
                return NOT_OK;
            }
            return OK;
        } else enable = false;
        return Integer.valueOf(ERR_INVALID_KEY);
    }

    public void Login(byte[] username,byte[] password,ClientServerListener callback)
    {


        if(username==null) {
            callback.onResponseReceive(ERR_NAME,GetCodeDesc(ERR_NAME));
            return;
        }
        if(password==null)
        {
            callback.onResponseReceive(ERR_PW,GetCodeDesc(ERR_PW));
            return;
        }

        if (passti==null)
        {
            callback.onResponseReceive(NOT_OK,GetCodeDesc(NOT_OK));
            return;
        }

        byte[] result=passti.pcd_cmd_login(username,password);
        SendData(result,callback);

    }
    public void CheckRespCode(byte[] resp,int resplen,ClientServerListener callback)
    {
        byte code;
        String desc="no response";
        try {
            code = passti.pcd_checkresponsecode(resp, resplen);
            desc=passti.GetDescription(code);
        }catch(Exception e)
        {
            code=(byte)0xFF;

        }

        callback.onResponseReceive(code,desc);
    }
    public void UploadData(byte[] banklog,ClientServerListener callback)
    {
        

        if (banklog==null)
        {
            callback.onResponseReceive(ERR_NORECORD,GetCodeDesc(ERR_NORECORD));
            return;
        }
        if (passti==null)
        {
            callback.onResponseReceive(NOT_OK,GetCodeDesc(NOT_OK));
            return;
        }
        byte[] result=passti.pcd_cmd_dataupload(passti.token,passti.MID,passti.TID,banklog);
        SendData(result,callback);
    }
    public int SendData(byte[] data,ClientServerListener callback) {

        new Thread(new SocketThread(data,callback)).start();
        return OK;
    }



    class SocketThread implements Runnable
    {
        private byte[] data;
        ClientServerListener callback;
        SocketThread(byte[] message,ClientServerListener callback) {
            this.data = message;
            this.callback=callback;
        }
        @Override
        public void run() {
            OutputStream os=null;
            Socket socket=null;

            byte[] buffer=new byte[1000];
            int offset=0;
            int avail;
            byte[] tempbuffer=null;
            int port;
            port = Integer.parseInt(ctx.getResources().getString(R.string.passti_port_dev));
            //1. Create Client
            try {
                socket = new Socket(ctx.getResources().getString(R.string.passti_ip_dev), port);
                socket.setSoTimeout(5000);
            }catch (Exception e)
            {
                Log.e(TAG,e.getMessage());
                callback.onResponseReceive(ERR_SOCKET_NOTCONNECT,ctx.getString(R.string.err_socketnotconnect_msg));
                return;
            }
            //2. output stream
            try {
                os = socket.getOutputStream();
            }catch (Exception e)
            {
                Log.e(TAG,e.getMessage());
                callback.onResponseReceive(ERR_SOCKET,ctx.getString(R.string.err_socket_msg));
                return;
            }
//3. Send data
            try {
                os.write(data);

            }catch (Exception e)
            {
                Log.e(TAG,e.getMessage());
                callback.onResponseReceive(ERR_SOCKET_WRITE,ctx.getString(R.string.err_socketwrite_msg));
                return;
            }
            try {
                Thread.sleep(1000);
            }
            catch (Exception e)
            {
                Log.e(TAG,e.getMessage());
            }
            try {
                //BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                avail=socket.getInputStream().available();
                while(avail>0)
                {
                    tempbuffer=new byte[avail];
                    socket.getInputStream().read(tempbuffer);
                    System.arraycopy(tempbuffer,0,buffer,offset,tempbuffer.length);
                    offset+=tempbuffer.length;
                    avail=socket.getInputStream().available();
                }

                Log.d(TAG,"resp : "+Hex.bytesToHexString(buffer,0,offset));
            }
            catch (Exception e)
            {
                Log.e(TAG,e.getMessage());
                callback.onResponseReceive(ERR_SOCKET_READ,ctx.getString(R.string.err_socketread_msg));
            }
            try{
                os.flush();
                socket.shutdownOutput();
                os.close();
                socket.close();
            }catch (Exception e)
            {
                Log.e(TAG,e.getMessage());
                callback.onResponseReceive(ERR_SOCKET_NOTDISCONNECT,ctx.getString(R.string.err_socketnotdisconnect_msg));
            }
            tempbuffer=new byte[offset];
            System.arraycopy(buffer,0,tempbuffer,0,tempbuffer.length);
            //callback.onResponseReceive(OK,tempbuffer,GetCodeDesc(OK));
            CheckRespCode(tempbuffer, tempbuffer.length, callback);
        }

    }
    String GetCodeDesc(int code)

    {
        switch (code)
        {
            default : return Integer.toString(code);
        }
    }

    @Override
    public void onRFDetected(byte[] uid,int mode) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName() + " uid "+Hex.bytesToHexString(uid));
        if(uid==null)
        {
            passti.ctlinfo.uid.len=0;
            Arrays.fill(passti.ctlinfo.uid.value,(byte)0x00);
            devinfo.operation="";
            countdownfinish=true;
            callback.onRFDetected(uid,mode,null);
            return;

        }
        passti.ctlinfo.uid.value = new byte[uid.length];
        System.arraycopy(uid, 0, passti.ctlinfo.uid.value, 0, passti.ctlinfo.uid.value.length);
        passti.ctlinfo.uid.len = passti.ctlinfo.uid.value.length;

        devinfo.operation=ctx.getString(R.string.carddetected);
        Log.d("uid1",Hex.bytesToHexString(uid)+" mode="+mode+" "+devinfo.emv);
        if(mode==devinfo.emv)
        {
            //callemv function here
            if(emvcard!=null)
            {

                emvcard.setMode(emv.CONTACTLESS);
                if(emvcard.ProcessContact()==OK) {

                    callback.onRFDetected(uid, mode,emvcard.getContactCardData());
                }
                else
                {
                    callback.onRFDetected(uid, mode,null);
                }
            }
            else
            {
                Log.e(TAG,"EMV NOT READY");
            }
        }
        else {
            Log.d(TAG, "mode!=emv");
            callback.onRFDetected(uid, mode,null);
        }
    }



    @Override
    public void onSAMDetected(int slot,byte[] atr) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName()+ " slot "+slot+", ATR "+Hex.bytesToHexString(atr));
        devinfo.operation=ctx.getString(R.string.samdetected);


    }

    @Override
    public void onPinPadHandler(byte[] bytes) {
        callback.onPinPadHandler(bytes);
    }


    @Override
    public void onContactDetected(byte[] uid) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName() + " "+Hex.bytesToHexString(uid));
        if(uid==null)
        {

            devinfo.operation="";
            countdownfinish=true;
            callback.onContactDetected(null,null);
            return;

        }
        devinfo.operation=ctx.getString(R.string.contactdetected);

        if(emvcard!=null)
        {
            emvcard.setMode(emv.CONTACT);
            if(emvcard.ProcessContact()==OK) {
                callback.onContactDetected(uid, emvcard.getContactCardData());
            }
            else
            {
                callback.onContactDetected(uid, null);
            }
        }
        else
        {
            Log.e(TAG,"EMV NOT READY");
        }


    }
    @Override
    public void onMSRDetected(byte[][] trackdata) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        devinfo.operation=ctx.getString(R.string.msrdetected);

        callback.onMSRDetected(trackdata);
    }

/*
    public String PinPad_SN() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        String status="";

                status= device.PinPad_SN();

        return status;

    }


    public byte[] PinPad_GenerateRandom(int length) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status;
                status= device.PinPad_GenerateRandom(length);

        return status;
    }


    public int PinPad_SetPINLength(int min, int max) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status;
                status= device.PinPad_SetPINLength(min,max);

        return status;
    }


    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status;
                status= device.PinPad_UpdateUserKey(mkid,userkeyid,value);

        return status;
    }


    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value, byte[] checksum) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status;
                status= device.PinPad_UpdateUserKey(mkid,userkeyid,value,checksum);

        return status;
    }


    public byte[] PinPad_GetSessionKey(int mkid, int userkeyid, int algo) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status;
                status= device.PinPad_GetSessionKey(mkid,userkeyid,algo);

        return status;
    }


    public byte[] PinPad_Encrypt(int mkid, int userkeyid, int algo, byte[] data) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status;
                status= device.PinPad_Encrypt(mkid,userkeyid,algo,data);

        return status;
    }


    public byte[] PinPad_Calculatemac(int mkid, int userkeyid, int keyalgo, int macalgo, byte[] data) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status;
                status= device.PinPad_Calculatemac(mkid,userkeyid,keyalgo,macalgo,data);

        return status;
    }


    public byte[] PinPad_Calculatedukptmac(byte[] data) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status;
                status= device.PinPad_Calculatedukptmac(data);

        return status;
    }

    public int PinPad_Verifymac(int mkid, int keyid, byte[] data, byte[] mac) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status;
                status= device.PinPad_Verifymac(mkid,keyid,data,mac);

        return status;
    }


    public int PinPad_async_pinblock(int mkid, int keyid, byte[] data) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status;
                status= device.PinPad_async_pinblock(mkid,keyid,data);

        return status;
    }


    public byte[] PinPad_sync_pinblock(int mkid, int keyid, byte[] data) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status;
                status= device.PinPad_sync_pinblock(mkid,keyid,data);

        return status;
    }

    public int PinPad_getMKStatus(int mkid) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status;
        status= device.PinPad_getMKStatus(mkid);

        return status;
    }


    public int PinPad_getSKStatus(int mkid, int skid) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status;

                status= device.PinPad_getSKStatus(mkid,skid);

        return status;
    }

    public int PinPad_getDUKPTStatus(int mkid, byte[] uniquekey) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status;

        status= device.PinPad_getDUKPTStatus(mkid,uniquekey);

        return status;
    }

 */
}
