package id.co.softorb.lib.smartcard.posdevice;

import android.graphics.Bitmap;

public abstract class DeviceFunction extends DeviceBase{
    public abstract int FindMSR();
    public abstract int FindCTL(int mode);
    public abstract int FindCB();
    public abstract int FindSAM(int type,int slot);
    public abstract byte[] sendCTL(byte[] apdu);
    public abstract byte[] sendCB(byte[] apdu);
    public abstract byte[] sendSAM(int slot,byte[] apdu);
    public abstract int printercutpaper();
    public abstract int printerlinefeed();
    public abstract int printerstatus();
    public abstract int printbitmap(int alignment, Bitmap bitmap);
    public abstract int print_text(int fonttype,int alignment,String text,boolean newline);
    public abstract void setcardwaitingtime(int timeout);
    public abstract String PinPad_SN();
    //public abstract byte[] PinPad_GenerateRandom(int length);
    public abstract int PinPad_SetPINLength(int min,int max);
    public abstract int PinPad_UpdateUserKey(int mkid,int userkeyid,byte[] value);
    public abstract int PinPad_UpdateUserKey(int mkid,int userkeyid,byte[] value,byte[] checksum);
    public abstract byte[] PinPad_GetSessionKey(int mkid,int userkeyid,int algo);
    public abstract byte[] PinPad_Encrypt(int mkid,int userkeyid,int algo,byte[] data);
    public abstract byte[] PinPad_Calculatemac(int mkid,int userkeyid,int keyalgo,int macalgo,byte[] data);
    public abstract byte[] PinPad_Calculatedukptmac(byte[] data);
    public abstract int PinPad_Verifymac(int mkid,int keyid,byte[] data,byte[] mac);
    public abstract int PinPad_async_pinblock(int mkid,int keyid,byte[] data);
    public abstract byte[] PinPad_sync_pinblock(int mkid,int keyid,byte[] data);
    public abstract int PinPad_getMKStatus(int mkid);
    public abstract int PinPad_getSKStatus(int mkid,int skid);
    public abstract int PinPad_getDUKPTStatus(int mkid,byte[] uniquekey);


}
