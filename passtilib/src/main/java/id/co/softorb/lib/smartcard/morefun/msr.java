package id.co.softorb.lib.smartcard.morefun;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.morefun.yapi.ServiceResult;
import com.morefun.yapi.device.reader.icc.IccCardReader;
import com.morefun.yapi.device.reader.icc.IccReaderSlot;
import com.morefun.yapi.device.reader.mag.MagCardInfoEntity;
import com.morefun.yapi.device.reader.mag.MagCardReader;
import com.morefun.yapi.device.reader.mag.OnSearchMagCardListener;
import com.morefun.yapi.engine.DeviceServiceEngine;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.Base;

import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INITREADER;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;

public class msr extends Base {
    String tagclass= this.getClass().getSimpleName();
    private DeviceServiceEngine deviceService;
    private MagCardReader magCardReader;
    private POSListener detectListener;

    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            deviceService=(DeviceServiceEngine) devInstance;
//            Log.d("deviceService2", ""+deviceService);
            magCardReader = deviceService.getMagCardReader();

            if (magCardReader != null) {
                detectListener = listener;
                return OK;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());

        }
        return NOT_OK;
    }

    @Override
    public int open() {
        //Tidak ada fungsi open
        return OK;
    }

    @Override
    public int close() {
        return OK;
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            magCardReader.setIsCheckLrc(true);
            magCardReader.searchCard(listener,0,new Bundle());
        } catch (RemoteException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return WAITING;
    }

    @Override
    public void stopfind() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            magCardReader.stopSearch();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    OnSearchMagCardListener.Stub listener=new OnSearchMagCardListener.Stub() {
        @Override
        public void onSearchResult(int i, MagCardInfoEntity magCardInfoEntity) throws RemoteException {
            byte[][] trackData = new byte[3][];
            byte[] bytee={0x00};
            if (i == ServiceResult.Success) {
                Log.d(TAG, "----- onSuccess -----");
                Log.d("Track1", ""+ magCardInfoEntity.getTk1()+" ");
                Log.d("Track2", ""+magCardInfoEntity.getTk2()+" ");
                Log.d("Track3", ""+magCardInfoEntity.getTk3()+" ");
                trackData[0]= magCardInfoEntity.getTk1().getBytes();
                if(magCardInfoEntity.getTk2()==null){
                    trackData[1]=bytee;
                }else {
                    trackData[1]=magCardInfoEntity.getTk2().getBytes();
                }
                trackData[2]=magCardInfoEntity.getTk3().getBytes();
                detectListener.onMSRDetected(trackData);
                Log.d("Track1", ""+ Hex.bytesToHexString(trackData[0])+" ");
                Log.d("Track2", ""+Hex.bytesToHexString(trackData[1])+" ");
                Log.d("Track3", ""+Hex.bytesToHexString(trackData[2])+" ");
            } else {
                Log.d(TAG, "----- onError -----");
                stopfind();
            }

        }
    };
}
