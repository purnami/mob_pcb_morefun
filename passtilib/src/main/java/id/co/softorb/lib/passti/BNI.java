package id.co.softorb.lib.passti;


import android.content.Context;
import android.os.RemoteException;
import android.util.Log;


import java.util.Arrays;


import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.helper.converter;
import id.co.softorb.lib.passti.helper.util;
import id.co.softorb.lib.smartcard.posdevice.params;
import id.co.softorb.lib.smartcard.posdevice.reader;


import static id.co.softorb.lib.passti.helper.ErrorCode.*;
import static id.co.softorb.lib.passti.devinfo.*;
import static id.co.softorb.lib.smartcard.posdevice.params.*;

class BNI {
	// Command
/*	private final String selectApp = "00A4040008A000424E4910000100";
	private final String getChallenge = "0084000008";
	private final String getPurseData = "903203000A1201";
	private final String verifySecureReadPurse= "8012XX0132";
	private final String samGenerateDebit = "8010XX0138";
	private final String debitPurse = "90340000250312011403";
	private final String verifyDebitReceipt = "8011XX0138";
*/

     class saminfo
     {
         byte[] id;
         int idlen;
         public saminfo()
         {
             id = new byte[32];
             idlen=0;
         }
     }

     class bni_cardinfo
     {
         byte[] CRN;
         int lenCRN;
         byte[] CSN;
         int lenCSN;
         byte Version;
         byte PurseStatus;
         byte KeySetIdx;
         byte[] TRH;
         int lenTRH;
         byte[] eData_CRCB ;
         byte[] expiry;
         byte[] lastCreditTRP;
         byte[] lastCreditHeader;
         byte[] lastTxnTRP;
         byte[] lastTxnRecord;
         byte[] lastTxnSignCert;
         byte[] counterdata;
         byte[] lastTxnCounter;
         byte[] SignCert;
         saminfo sam;
         int leneData_CRCB;
         byte DO;
         byte[] lastTRH;
         byte lenlastTRH ;
         byte[] BackupTRH;
         byte lenBackupTRH ;
         long julian_card;
         byte[] anonym;

         public bni_cardinfo()
         {
             sam= new saminfo();
             CRN =new byte[8];
             lenCRN=0;
             CSN=new byte [8];
             lenCSN=0;
             Version=0;
             PurseStatus=0;
             KeySetIdx=0;
             TRH =new byte[8];
             lenTRH=0;
             eData_CRCB =new byte[18];
             expiry=new byte[2];
             lastCreditTRP=new byte[4];
             lastCreditHeader=new byte[8];
             lastTxnTRP=new byte[4];
             lastTxnRecord=new byte[16];
             lastTxnSignCert=new byte[8];
             counterdata=new byte[8];
             lastTxnCounter=new byte[8];
             SignCert=new byte[8];
             saminfo sam;
             leneData_CRCB=0;
             DO=0;
             lastTRH =new byte[8];
             lenlastTRH=0;
             BackupTRH =new byte[8];
             lenBackupTRH=0 ;
             julian_card=0;
             anonym=new byte[27];
         }

     }
	// global var

	private byte[]  logDeductHex=new byte[200];
	private byte[] OldBalance = new byte[4];
	private byte[] DebitRecord=new byte[16];
	private byte[] DebitReceipt=new byte[24];
    private byte[] invoiceReference;
	private byte[] TRU_BNI={(byte)0x50,(byte)0x55,(byte)0x52,(byte)0x43,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00}; //Trx USer Data
	private byte[] termRndNumBNI={(byte)0x44,(byte)0x5D,(byte)0x30,(byte)0x86,(byte)0x8F,(byte)0x58,(byte)0x6A,(byte)0xA0};


    byte DO;
	byte debitoption=0x00;
	byte PF = 0x03;
	byte SKF=0x14;
	byte SKN = 0x03;
	private byte[] RAPDU = new byte[255];


	String TAG;

	private byte TrxDebit=0x01;
	private byte TrxBlacklist=0x02;
    String tagclass;

    private byte[] backuplog;

    //private byte [] dev_marriage_code={(byte)0x50,(byte)0x0A,(byte)0x0A,(byte)0x70,(byte)0xB9,(byte)0x4D,(byte)0xB7,(byte)0x65,(byte)0xF6,(byte)0x8E,(byte)0xC1,(byte)0xB4,(byte)0x86,(byte)0x35,(byte)0x0B,(byte)0x6F};
    PASSTI passti;
	String saldoAwal,saldoAkhir = "";
	String marriedBNIKey ;
	 int samslot;
     bni_cardinfo ctl_bni;
    reader device;
     void SetSamPort(int slot)
     {
         samslot=slot;
     }
//	 BNI(Context ctx, UDeviceService deviceService, DeviceInfo deviceInfo,PASSTI passtiInstance,APOSUtility aposUtilityInstance) throws RemoteException, UnsupportedEncodingException {
	  BNI(Context ctx, PASSTI passtiInstance, reader devicereader)  {
        tagclass = this.getClass().getSimpleName();
        OldBalance=new byte[4];
        ctl_bni=new bni_cardinfo();

        passti=passtiInstance;
         device=devicereader;
        samslot=255;//default for unassigned sam slot
         TAG= passti.dbghelper.TAG;
	}


	 int checkbalance() throws RemoteException {
		int i = bni_checkbalance();

		return i;
	}

	 int deduct(String amount) throws RemoteException {
		saldoAwal = "";
		saldoAkhir = "";

		int i = bni_deduct(amount);

		return i;
	}

	 String getBalance(){

		return saldoAwal;
	}

	 String getLastBalance(){
		return saldoAkhir;
	}
/*
	 String getCardNo(){
	Log.d(TAG,tagclass+".getCardNo");
		//return Hex.bytesToHexString(CardNo);
         String cardNo;

         passti.dbghelper.DebugHex("passti.ctlinfo.cardno",passti.ctlinfo.cardno);
         if(passti.ctlinfo.cardno != null) {
             cardNo = Hex.bytesToHexString(passti.ctlinfo.cardno);
         }else cardNo = "Card Not Found";
         return cardNo;
	}
*/
     int BNI_SAM_GetInfo() throws RemoteException {
    passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
            .getStackTrace()[0]
            .getMethodName());
         int result;
         byte[] getsaminfo={(byte)0x80,(byte)0x06,(byte)0x00,(byte)0x00,(byte)0x1D};
//         RAPDU = device.sendSAM_Le(samslot,getsaminfo);
         RAPDU = device.sendSAM(samslot,getsaminfo);
         if(RAPDU == null){
             return ERR_NO_RESP;
         }
         if(APDUHelper.CheckResult(RAPDU) != 0){

             return ERR_SW1SW2;
         }
         System.arraycopy(RAPDU,0,ctl_bni.sam.id,0,8);
         return OK;
     }

    public int  BNI_SAM_Marriage()
    {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int offset=0;
        byte[] saminit={(byte)0x80,(byte)0x18,(byte)0x00,(byte)0x00,(byte)0x08 };
        byte[] cmd = new byte[saminit.length+termRndNumBNI.length];
        System.arraycopy(saminit,0,cmd,offset,saminit.length);
        offset+=saminit.length;
        System.arraycopy(termRndNumBNI,0,cmd,offset,termRndNumBNI.length);
        offset+=saminit.length;

        //RAPDU=sam.SendAPDU(port_bni,cmd);
        RAPDU = device.sendSAM(samslot,cmd);
        if(RAPDU==null)
            return ERR_NO_RESP;

        if (RAPDU.length< 2)
            return ERR_NO_RESP;
        if(APDUHelper.CheckResult(RAPDU)!=0)
            return ERR_SW1SW2;

        //save samid and marriage code, use shared preference
        //byte[] marriagekey = new byte[RAPDU.length-2];
        //System.arraycopy(RAPDU,0,marriagekey,0,marriagekey.length);

        //marriage_code_bni = new byte[marriagekey.length];
        System.arraycopy(RAPDU,0,devinfo.issuer.bnimrgcode,0,devinfo.issuer.bnimrgcode.length);

        return OK;

    }

	private int SAM_Initialization() throws RemoteException {
         passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                 .getStackTrace()[0]
                 .getMethodName());


		int result;

        result = BNI_SAM_SelectAID();
        if(result!=OK)
        {
            return BNI_SAM_ERR_SELECTAID;
        }

        result = BNI_SAM_GetInfo();
        if(result!=OK)
        {
            return BNI_SAM_ERR_GETINFO;
        }

		if (RAPDU[8] == 0x01)
		{
			Log.d(TAG,"SAM is married");
			return OK;
		}
		else
		{
            passti.dbghelper.DebugPrintString("SAM not married, initialize marriage code");
           	return BNI_SAM_ERR_NOTMARRIED;

		}
	}

	public byte[] Get_SAMMariage()
    {
        Log.d(TAG,"BNI_SAM_InitMarriage");
        return devinfo.issuer.bnimrgcode;
    }

     public int BNI_SAM_InitMarriage()
     {
         passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                 .getStackTrace()[0]
                 .getMethodName());
         int idx=0;
         int result;


         int offset=0;
         byte[] saminit={(byte)0x80,(byte)0x18,(byte)0x00,(byte)0x00,(byte)0x08 };
         byte[] cmd = new byte[saminit.length+termRndNumBNI.length];
         System.arraycopy(saminit,0,cmd,offset,saminit.length);
         offset+=saminit.length;
         System.arraycopy(termRndNumBNI,0,cmd,offset,termRndNumBNI.length);
         offset+=saminit.length;

         //RAPDU=sam.SendAPDU(port_bni,cmd);
         RAPDU = device.sendSAM(samslot,cmd);
         if(RAPDU==null)
             return ERR_NO_RESP;

         if (RAPDU.length< 2) return ERR_NO_RESP;
         if(APDUHelper.CheckResult(RAPDU)!=0)	return ERR_SW1SW2;

         System.arraycopy(RAPDU,0,devinfo.issuer.bnimrgcode,0,RAPDU.length-2);


         return OK;


     }




	/*
	private int sGenerateDebitRecord() {
		LogD(TAG,"sGenerateDebitRecord");

		String send = bnicard.cmdsamgeneratedebit();
		byte []b3Amt = new byte[3];
		byte []b4Amt = new byte[4];
		byte []b8Julian = new byte[8];
		byte []b4Julian = new byte[4];
		b4Amt=converter.InttoByteArray(0xFFFFFFFF- Integer.valueOf(Amount)+1, converter.BIG_ENDIAN);
		System.arraycopy(b4Amt, 1, b3Amt, 0, 3);
		b8Julian=converter.LongtoByteArray(util.calcJulianSecondNow("19950101"), converter.BIG_ENDIAN);
		System.arraycopy(b8Julian, 4, b4Julian, 0, 4);

		send=send.replaceAll("XX", KeySetIdx) + CSN +CardNo+RRN+CRN+TID+"00031403"+"01"+ByteUtils.bytesToHex(b3Amt)+ByteUtils.bytesToHex(b4Julian)+TRU_BNI;
		//RAPDU=util.WebServiceExecuteAndGetResult(soapDebit, send, currentAct);

		byte[] cmd = ByteUtils.hexStringToByteArray(send);
		RAPDU=sam.SendAPDU(port_bni,cmd);
		if(RAPDU==null)
			return ErrorCode.BNI_SAM_ERR_GENERATEDEBIT;

		if (RAPDU.length< 2) return ErrorCode.BNI_SAM_ERR_GENERATEDEBIT;
		if(APDUHelper.CheckResult(RAPDU)!=0)	return ErrorCode.BNI_SAM_ERR_GENERATEDEBIT;
		DebitRecord=ByteUtils.bytesToHex(RAPDU).substring(0, 32);
		return ErrorCode.OK;
	}
	*/




	private byte[] marriedTRN= new byte[8];
	private byte[] marriedCRN= new byte[8];







    long Convert_to_2Complement(long value)
    {
        long result;
        if (value==0)
            return 0;

        if (value > 0) 	result = 0xFFFFFF - (value-1);
        else result = 0xFFFFFF;
        return result;
    }




   private int bni_deduct(String strAmount) throws RemoteException {
       passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
               .getStackTrace()[0]
               .getMethodName());
        //byte[] amount = strAmount.getBytes();
       Log.d("masuk bni deduct", "1");
       byte[] amount= new byte[4];
        int result;
        long deductAmount;
        long ldeductAmount;
        byte[] jdevtime = new byte[4];
        String timestamp;
        int y = bni_checkbalance();
        if(y != OK){
            return y;
        }
        amount= converter.IntegerToByteArray(Integer.valueOf(strAmount));
       System.arraycopy(amount,0,passti.deduct.baAmount,0,4);
       timestamp= util.Datenow("ddMMyyyyHHmmss");
        //iBalanceBefore = iBalance;


        //Log.d(TAG,"start deduct process");
        //printf("incoming data : ");printhex(amount,datalen);
        //ldeductAmount = dword2int(amount);
        //ldeductAmount = Integer.valueOf(strAmount);

        ldeductAmount =  Long.parseLong(strAmount);

        //printf("2's complement from amount %i : ",ldeductAmount);
        //deductAmount=Convert_to_2Complement(ldeductAmount);
        deductAmount = Convert_to_2Complement(ldeductAmount);

        if (ctl_bni.PurseStatus==0x00||ctl_bni.PurseStatus==0x02|ctl_bni.PurseStatus==0x04|ctl_bni.PurseStatus==0x06)
        {

            result=BNI_CTL_ERR_PURSENOTENABLED;
            return result;
        }

        result = BNI_SAM_VerifySecureReadPurse();
        if(result!=OK)
        {
          //  memcpy(outLog,apdubuf,apdulen);

            return BNI_SAM_ERR_VERIFYSECUREREADPURSE;
        }


        if(passti.ctlinfo.lBalanceKartu<ldeductAmount)
        {

            return CTL_ERR_INSUFFICIENTBALANCE;
        }

        //compose transaction header
       ctl_bni.TRH[0]=TrxDebit;
       ctl_bni.TRH[1]=(byte)((deductAmount & 0xFF0000)>>16);
       ctl_bni.TRH[2]=(byte)((deductAmount & 0x00FF00)>>8);
       ctl_bni.TRH[3]=(byte)(deductAmount & 0x0000FF);

        amount = new byte[4];
        System.arraycopy(ctl_bni.TRH,0,amount,0,4);


       byte[] yymmddhhmmss= new byte[6];
       long ljulianSec;

       yymmddhhmmss=util.bcd_ddmmyyyyhhmmss2hex_yymmddhhmmss(timestamp);
       passti.dbghelper.DebugHex("yymmddhhmmss",yymmddhhmmss,yymmddhhmmss.length);
       ljulianSec=util.julianSec(1995,yymmddhhmmss);
       byte[] julianSec = converter.LongtoByteArray(ljulianSec, converter.BIG_ENDIAN);
        System.arraycopy(julianSec,4,ctl_bni.TRH,4,4);
       if(passti.GetAction()== REPURCHASE){
           Log.e(TAG,"Autocompletion required");
           passti.dbghelper.DebugHex("passti.ctlinfo.cardno",passti.ctlinfo.cardno,passti.ctlinfo.cardno.length);
           passti.dbghelper.DebugHex("passti.deduct.prevCardNo",passti.deduct.prevCardNo,passti.deduct.prevCardNo.length);
           if(Hex.bytesToHexString(passti.ctlinfo.cardno).matches(Hex.bytesToHexString(passti.deduct.prevCardNo)))
           {
               passti.dbghelper.DebugHex("ctl_bni.lastTxnRecord",ctl_bni.lastTxnRecord,ctl_bni.lastTxnRecord.length);
               passti.dbghelper.DebugHex("ctl_bni.BackupTRH",ctl_bni.BackupTRH,ctl_bni.BackupTRH.length);

               //if(Arrays.e(ctl_bni.lastTxnRecord,ctl_bni.BackupTRH)==true)
               byte[] lastTRH = Arrays.copyOfRange(ctl_bni.lastTxnRecord,0,8);
               passti.dbghelper.DebugHex("lastTRH",lastTRH,lastTRH.length);
               if(Arrays.equals(lastTRH,ctl_bni.BackupTRH)==true)
               {
                   //LED_CardProcessOngoing();
                   passti.dbghelper.DebugPrintString("Same TRH, prepare log and exit");
                   passti.dbghelper.DebugHex("backuplog",backuplog,backuplog.length);
                   ctl_bni.TRH[0]=TrxDebit;
                   ctl_bni.TRH[1]=(byte)((deductAmount & 0xFF0000)>>16);
                   ctl_bni.TRH[2]=(byte)((deductAmount & 0x00FF00)>>8);
                   ctl_bni.TRH[3]=(byte)(deductAmount & 0x0000FF);
                   System.arraycopy(julianSec,0,ctl_bni.TRH,4,4);
                   System.arraycopy(ctl_bni.TRH,0,backuplog,10,8);
                   System.arraycopy(passti.deduct.prevBalance,1,backuplog,23,3);
                   System.arraycopy(passti.ctlinfo.bBalanceKartu,1,backuplog,26,3);
                   System.arraycopy(ctl_bni.lastTxnCounter,0,backuplog,29,8);
                   System.arraycopy(ctl_bni.lastTxnSignCert,0,backuplog,37,8);

                   //byte[] devicetime = Hex.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
                   byte[] devicetime = Hex.hexStringToByteArray(timestamp);
                   saldoAwal= String.valueOf(converter.ByteArrayToInt(passti.deduct.prevBalance,0,4,converter.BIG_ENDIAN));
                   saldoAkhir= String.valueOf(converter.ByteArrayToInt(passti.ctlinfo.bBalanceKartu,0,4,converter.BIG_ENDIAN));

                   passti.log = passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value, params.ctype_bni,devicetime,passti.ctlinfo.cardno,passti.ctlinfo.bBalanceKartu,passti.deduct.prevAmt,trxno,backuplog,others);

                   passti.SetAction(NORMAL);
                   return OK;
               }
               else
               {
                   //deduct.doCorrection=0;
                   passti.SetAction(NORMAL);
                   passti.dbghelper.DebugPrintString("Different TRH, process with normal tap");
               }

           }else return ERR_WRONGCARDNO;
       }
        //Log.d(TAG,"TRH_BNI: " + BytesUtil.bytes2HexString(TRH_BNI));
        result = BNI_SAM_GenerateDebitRecord();
        if(result!=OK)
        {

            return BNI_SAM_ERR_GENERATEDEBIT;
        }

        result = BNI_CTL_DebitPurse();
        //result= BNI_TestAutoCorrection();
        if(result!=OK)
        {
            if(result==ERR_NO_RESP)
            {
                System.arraycopy(ctl_bni.TRH,0,ctl_bni.BackupTRH,0,8);
                passti.SetAction(REPURCHASE);	//2020-03-26,vivo, ask user to tap card again after lost contact
                backuplog=BNI_ComposeLog();
                passti.dbghelper.DebugHex("backuplog",backuplog,backuplog.length);
            }
            return BNI_CTL_ERR_DEBITPURSE;
        }




        result = BNI_SAM_VerifyDebit();
        if(result!=OK)
        {
            System.arraycopy(ctl_bni.TRH,0,ctl_bni.BackupTRH,0,8);
            passti.SetAction(REPURCHASE);	//2020-03-26,vivo, ask user to tap card again after lost contact
            backuplog=BNI_ComposeLog();
            passti.dbghelper.DebugHex("backuplog",backuplog,backuplog.length);
            return BNI_SAM_ERR_VERIFYDEBITRECEIPT;
        }
        saldoAkhir = String.valueOf(passti.ctlinfo.lBalanceKartu);
       RAPDU =BNI_ComposeLog();
       //byte[] devicetime = Hex.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
       byte[] devicetime = Hex.hexStringToByteArray(timestamp);

       passti.log = passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value, params.ctype_bni,devicetime,passti.ctlinfo.cardno,passti.ctlinfo.bBalanceKartu,passti.deduct.baAmount,trxno,RAPDU,others);

        return OK;
    }

    int BNI_SAM_VerifySecureReadPurse() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx=0;
        int i;
        int result;

        byte[] verifysecurereadpurse={(byte)0x80,(byte)0x12,(byte)0x00,(byte)0x01,(byte)0x32};
        byte[] cmdSendToCL = new byte[verifysecurereadpurse.length+ctl_bni.CSN.length+passti.ctlinfo.cardno.length+termRndNumBNI.length+ctl_bni.CRN.length+ctl_bni.eData_CRCB.length];

        System.arraycopy(verifysecurereadpurse,0,cmdSendToCL,idx,verifysecurereadpurse.length);
        idx+=verifysecurereadpurse.length;
        cmdSendToCL[2]=ctl_bni.KeySetIdx;
        System.arraycopy(ctl_bni.CSN,0,cmdSendToCL,idx,ctl_bni.CSN.length);
        idx+=ctl_bni.CSN.length;
        System.arraycopy(passti.ctlinfo.cardno,0,cmdSendToCL,idx,passti.ctlinfo.cardno.length);
        idx+=passti.ctlinfo.cardno.length;

        for (i=0; i<8; i++)
            marriedTRN[i] = (byte)(termRndNumBNI[i] ^ devinfo.issuer.bnimrgcode[i]);	//Terminal rand num xor first 8Byte marriage code

        //memcpy(&cmdSendToCL[idx],marriedTRN,sizeof(marriedTRN));
        System.arraycopy(marriedTRN,0,cmdSendToCL,idx,marriedTRN.length);
        idx+=marriedTRN.length;

        for (i=0; i<8; i++)
            marriedCRN[i] = (byte)(ctl_bni.CRN[i] ^ devinfo.issuer.bnimrgcode[i+8]);	//Terminal rand num xor 2ND 8Byte marriage code


        System.arraycopy(marriedCRN,0,cmdSendToCL,idx,marriedCRN.length);
        idx+=marriedCRN.length;



        System.arraycopy(ctl_bni.eData_CRCB,0,cmdSendToCL,idx,ctl_bni.eData_CRCB.length);
        idx+=ctl_bni.eData_CRCB.length;

        RAPDU = device.sendSAM(samslot,cmdSendToCL);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            return ERR_SW1SW2;
        }

		System.arraycopy(RAPDU,0,ctl_bni.lastTxnSignCert,0,8);
        System.arraycopy(RAPDU,8,ctl_bni.lastTxnCounter,0,8);
        return OK;

    }


    int BNI_SAM_GenerateDebitRecord() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int i =0;
        int idx=0;
        int result;
        byte[] samgeneratedebitrecord ={(byte)0x80,(byte)0x10,(byte)0x00,(byte)0x01,(byte)0x38};
        byte[] cmdSendToCL = new byte[samgeneratedebitrecord.length+ctl_bni.CSN.length+passti.ctlinfo.cardno.length+termRndNumBNI.length+ctl_bni.CRN.length+ devinfo.issuer.bniTid.length+4+ctl_bni.TRH.length+TRU_BNI.length];

        System.arraycopy(samgeneratedebitrecord,0,cmdSendToCL,idx,samgeneratedebitrecord.length);
        idx+=samgeneratedebitrecord.length;
        cmdSendToCL[2]=ctl_bni.KeySetIdx;
        System.arraycopy(ctl_bni.CSN,0,cmdSendToCL,idx,ctl_bni.CSN.length);
        idx+=ctl_bni.CSN.length;
        System.arraycopy(passti.ctlinfo.cardno,0,cmdSendToCL,idx,passti.ctlinfo.cardno.length);
        idx+=passti.ctlinfo.cardno.length;

        System.arraycopy(termRndNumBNI,0,cmdSendToCL,idx,termRndNumBNI.length);
        idx+=termRndNumBNI.length;

        System.arraycopy(ctl_bni.CRN,0,cmdSendToCL,idx,ctl_bni.CRN.length);
        idx+=ctl_bni.CRN.length;

        System.arraycopy(devinfo.issuer.bniTid,0,cmdSendToCL,idx, devinfo.issuer.bniTid.length);
        idx+= devinfo.issuer.bniTid.length;


        cmdSendToCL[idx++]=debitoption;
        cmdSendToCL[idx++]=PF;
        cmdSendToCL[idx++]=SKF;
        cmdSendToCL[idx++]=SKN;

        System.arraycopy(ctl_bni.TRH,0,cmdSendToCL,idx,ctl_bni.TRH.length);
        idx+=ctl_bni.TRH.length;

        System.arraycopy(TRU_BNI,0,cmdSendToCL,idx,TRU_BNI.length);
        idx+=TRU_BNI.length;

        RAPDU = device.sendSAM(samslot,cmdSendToCL);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){

            return  ERR_SW1SW2;
        }
        for (i=0; i<16; i++)
        {
            DebitRecord[i] = (byte) (RAPDU[i] ^ devinfo.issuer.bnimrgcode[i]);
        }
        return OK;

    }


    int BNI_CTL_DebitPurse() throws RemoteException {

        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx=0;
        int result;
        byte[] debitPurse = {(byte)0x90,(byte)0x34,(byte)0x00,(byte)0x00,(byte)0x25,(byte)0x03,(byte)0x12,(byte)0x01,(byte)0x14,(byte)0x03};
        byte[] cmdSendToCL = new byte[debitPurse.length+termRndNumBNI.length+DebitRecord.length+TRU_BNI.length+1];

        System.arraycopy(debitPurse,0,cmdSendToCL,idx,debitPurse.length);
        idx+=debitPurse.length;

        System.arraycopy(termRndNumBNI,0,cmdSendToCL,idx,termRndNumBNI.length);
        idx+=termRndNumBNI.length;

        System.arraycopy(DebitRecord,0,cmdSendToCL,idx,DebitRecord.length);
        idx+=DebitRecord.length;

        System.arraycopy(TRU_BNI,0,cmdSendToCL,idx,TRU_BNI.length);
        idx+=TRU_BNI.length;
        cmdSendToCL[idx++]=0x18;

        RAPDU = device.sendCTL(cmdSendToCL);
        if(RAPDU == null){

            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            return ERR_SW1SW2;
        }
        System.arraycopy(RAPDU,0,DebitReceipt,0,24);
        return OK;
    }


    int BNI_SAM_VerifyDebit() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        int idx = 0;
        int result;
        byte[] verifydebitreceipt ={(byte) 0x80,(byte)0x11,(byte)0x00,(byte)0x01,(byte)0x38};
        byte[] cmdSendToCL = new byte[verifydebitreceipt.length+ctl_bni.CSN.length+passti.ctlinfo.cardno.length+termRndNumBNI.length+ctl_bni.CRN.length+DebitReceipt.length+1];

        System.arraycopy(verifydebitreceipt,0,cmdSendToCL,idx,verifydebitreceipt.length);
        idx+=verifydebitreceipt.length;
        cmdSendToCL[2]=ctl_bni.KeySetIdx;
        System.arraycopy(ctl_bni.CSN,0,cmdSendToCL,idx,ctl_bni.CSN.length);
        idx+=ctl_bni.CSN.length;
        System.arraycopy(passti.ctlinfo.cardno,0,cmdSendToCL,idx,passti.ctlinfo.cardno.length);
        idx+=passti.ctlinfo.cardno.length;
        System.arraycopy(marriedTRN,0,cmdSendToCL,idx,marriedTRN.length);
        idx+=marriedTRN.length;
        System.arraycopy(marriedCRN,0,cmdSendToCL,idx,marriedCRN.length);
        idx+=marriedCRN.length;
        System.arraycopy(DebitReceipt,0,cmdSendToCL,idx,DebitReceipt.length);
        idx+=DebitReceipt.length;
        cmdSendToCL[idx++]=0x13;
        RAPDU = device.sendSAM(samslot,cmdSendToCL);
        if(RAPDU == null){

            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) !=0){

            return ERR_SW1SW2;
        }
        Arrays.fill(passti.ctlinfo.bBalanceKartu,(byte)0x00);
        passti.ctlinfo.bBalanceKartu[0] = (byte)0x00;
        System.arraycopy(RAPDU,0,passti.ctlinfo.bBalanceKartu,1,3);
        passti.ctlinfo.lBalanceKartu = converter.ByteArrayToInt(passti.ctlinfo.bBalanceKartu,0,passti.ctlinfo.bBalanceKartu.length, converter.BIG_ENDIAN);

        System.arraycopy(RAPDU,3,ctl_bni.SignCert,0,8);
        System.arraycopy(RAPDU,11,ctl_bni.counterdata,0,8);
        System.arraycopy(RAPDU,3,logDeductHex,27,3);
        System.arraycopy(RAPDU,11,logDeductHex,30,8);
        System.arraycopy(RAPDU,3,logDeductHex,38,8);
        return OK;
    }


    private int bni_checkbalance() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int len;
        int result=ERR_NO_RESP;

        result = SAM_Initialization();
        if(result!=OK)

        {
            return result;
        }
        Log.d("saldo awal1", ""+saldoAwal);
        result = BNI_CTL_GetChallenge();
        if(result!=OK)

        {
            return BNI_CTL_ERR_GETCHALLENGE;
        }
        System.arraycopy(RAPDU,0,ctl_bni.CRN,0,8);
        Log.d("saldo awal2", ""+saldoAwal);
        result = BNI_CTL_SecureReadPurse();
        if(result!=OK)
        {
            return BNI_CTL_ERR_GETPURSEDATA;
        }
		saldoAwal = String.valueOf(passti.ctlinfo.lBalanceKartu);
        Log.d("saldo awal3", ""+saldoAwal);

        return OK;
    }

    int BNI_SAM_SelectAID() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] selectsamaid			= {(byte)0x00,(byte)0xA4,(byte)0x04,(byte)0x00,(byte)0x08,(byte)0xA0,(byte)0x00,(byte)0x42,(byte)0x4E,(byte)0x49,(byte)0x20,(byte)0x00,(byte)0x01};
        RAPDU = device.sendSAM(samslot,selectsamaid);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){

            return ERR_SW1SW2;
        }

        return OK;
    }

    int BNI_CTL_SelectAID() throws RemoteException {
               passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        byte[] cmdSelectAIDBNI   =  {(byte) 0x00,(byte) 0xA4,(byte) 0x04,(byte) 0x00,(byte) 0x08,(byte) 0xA0,(byte) 0x00,(byte) 0x42,(byte) 0x4E,(byte) 0x49,(byte) 0x10,(byte) 0x00,(byte) 0x01,(byte) 0x00};

//        RAPDU = device.sendCTLString(cmdSelectAIDBNI, "bni");
        RAPDU = device.sendCTL(cmdSelectAIDBNI);
        Log.d("RAPDUUUU bni", Hex.bytesToHexString(RAPDU));
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){

            return ERR_SW1SW2;
        }
        return OK;
    }


    int BNI_CTL_GetChallenge() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmdGetChallangeBNI =  {(byte) 0x00,(byte) 0x84,(byte) 0x00,(byte) 0x00,(byte) 0x08};
        RAPDU = device.sendCTL(cmdGetChallangeBNI);
        Log.d("RAPDU bni 2", ""+Hex.bytesToHexString(RAPDU));
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){

            return ERR_NO_RESP;
        }
        return OK;

    }

    int BNI_CTL_SecureReadPurse() throws RemoteException {
passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        int idx=0;
        byte[] cmdGetCardDataBNI =  {(byte) 0x90,(byte) 0x32,(byte) 0x03,(byte) 0x00,(byte) 0x0A,(byte) 0x12,(byte) 0x01};
        byte[] cmdSendToCL = new byte[cmdGetCardDataBNI.length+termRndNumBNI.length+1];

        System.arraycopy(cmdGetCardDataBNI,0,cmdSendToCL,idx,cmdGetCardDataBNI.length);
        Log.d("cmdSendToCL", ""+Hex.bytesToHexString(cmdSendToCL));

        idx+=cmdGetCardDataBNI.length;
        System.arraycopy(termRndNumBNI,0,cmdSendToCL,idx,termRndNumBNI.length);
        Log.d("cmdSendToCL2", ""+Hex.bytesToHexString(cmdSendToCL));
        idx+=termRndNumBNI.length;
        cmdSendToCL[idx++]=0;
        Log.d("cmdSendToCL3", ""+Hex.bytesToHexString(cmdSendToCL));
        RAPDU = device.sendCTL(cmdSendToCL);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            //Log.d(TAG,"FailedSecureReadPurse - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }


        ctl_bni.PurseStatus=RAPDU[1];
        Log.d("rapduuu", ""+Hex.bytesToHexString(RAPDU));
        Arrays.fill(passti.ctlinfo.bBalanceKartu,(byte)0x00);
        Log.d("balancekartu", ""+Hex.bytesToHexString(passti.ctlinfo.bBalanceKartu));
        System.arraycopy(RAPDU,2,passti.ctlinfo.bBalanceKartu,1,3);
        Log.d("balancekartu", ""+Hex.bytesToHexString(passti.ctlinfo.bBalanceKartu));
        System.arraycopy(passti.ctlinfo.bBalanceKartu,0,OldBalance,0,4);


        passti.ctlinfo.lBalanceKartu= converter.ByteArrayToInt(passti.ctlinfo.bBalanceKartu,0,4, converter.BIG_ENDIAN);

        System.arraycopy(RAPDU,8,passti.ctlinfo.cardno,0,8);

        System.arraycopy(RAPDU,16,ctl_bni.CSN,0,8);
        System.arraycopy(RAPDU,24,ctl_bni.expiry,0,2);
        System.arraycopy(RAPDU,28,ctl_bni.lastCreditTRP,0,4);
        System.arraycopy(RAPDU,32,ctl_bni.lastCreditHeader,0,8);
        System.arraycopy(RAPDU,42,ctl_bni.lastTxnTRP,0,4);
        System.arraycopy(RAPDU,46,ctl_bni.lastTxnRecord,0,16);
        ctl_bni.KeySetIdx=RAPDU[71];
        DO = RAPDU[94];
        System.arraycopy(RAPDU,95,ctl_bni.eData_CRCB,0,18);




        return OK;

    }



	byte[] BNI_ComposeLog()
	{

        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] temp=new byte[200];
        int idx=0;
        temp[idx++]='D';
        temp[idx++]=TrxDebit;

        System.arraycopy(passti.ctlinfo.cardno,0,temp,idx,8);
        idx+=8;

        System.arraycopy(ctl_bni.TRH,0,temp,idx,8);
        idx+=8;
        System.arraycopy(devinfo.issuer.bniTid,0,temp,idx,devinfo.issuer.bniTid.length);
        idx+= devinfo.issuer.bniTid.length;

        temp[idx++]=00;//DEBIT OPTION :NORMAL
        System.arraycopy(OldBalance,1,temp,idx,3);
        idx+=3;

        //purse BALANCE

        System.arraycopy(passti.ctlinfo.bBalanceKartu,1,temp,idx,3);
        idx+=3;

        //coUNTER DATA

        System.arraycopy(ctl_bni.counterdata,0,temp,idx,8);
        idx+=8;

        //Signed Cert
        //Log.d(TAG,"SignCert "+ Hex.bytesToHexString(SignCert));
        System.arraycopy(ctl_bni.SignCert,0,temp,idx,8);
        idx+=8;

        //lastCreditTRP

        System.arraycopy(ctl_bni.lastCreditTRP,0,temp,idx,ctl_bni.lastCreditTRP.length);
        idx+=ctl_bni.lastCreditTRP.length;

        //lastCreditHeader

        System.arraycopy(ctl_bni.lastCreditHeader,0,temp,idx,ctl_bni.lastCreditHeader.length);
        idx+=ctl_bni.lastCreditHeader.length;

        //lastTxnTRP

        System.arraycopy(ctl_bni.lastTxnTRP,0,temp,idx,ctl_bni.lastTxnTRP.length);
        idx+=ctl_bni.lastTxnTRP.length;

        //lastTxnRecord

        System.arraycopy(ctl_bni.lastTxnRecord,0,temp,idx,ctl_bni.lastTxnRecord.length);
        idx+=ctl_bni.lastTxnRecord.length;

        //last DO
        temp[idx++]=DO;
        //last lastTxnSignCert

        System.arraycopy(ctl_bni.lastTxnSignCert,0,temp,idx,ctl_bni.lastTxnSignCert.length);
        idx+=ctl_bni.lastTxnSignCert.length;

        //last lastTxnCounterData


        System.arraycopy(ctl_bni.lastTxnCounter,0,temp,idx,ctl_bni.lastTxnCounter.length);
        idx+=ctl_bni.lastTxnCounter.length;

        //SAMID

        System.arraycopy(ctl_bni.sam.id,0,temp,idx,ctl_bni.sam.id.length);
        idx+=ctl_bni.sam.id.length;

        invoiceReference = "                               ".getBytes();
        System.arraycopy(invoiceReference,0,temp,idx,15);
        idx+=15;

        System.arraycopy(devinfo.issuer.bniMid,0,temp,idx,devinfo.issuer.bniMid.length);
        idx+=devinfo.issuer.bniMid.length;


        byte[] out = new byte[idx];
        System.arraycopy(temp,0,out,0,out.length);
        Log.d(TAG,"BNI Log " + Hex.bytesToHexString(out));
        return out;
	}

     int BNI_TestAutoCorrection()
     {
         passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                 .getStackTrace()[0]
                 .getMethodName());
         if(passti.GetAction()==NORMAL)
         {
             return ERR_NO_RESP;
         }
         if(passti.GetAction()==REPURCHASE)
         {
             return OK;
         }
         return OK;
      }
}