package id.co.softorb.lib.passti.helper;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


public class converter {
	
	public final static byte BIG_ENDIAN=0;
	public final static byte LITTLE_ENDIAN=1;
	final static String TAG="converter";
	
	public static byte[] HexToBCD(int value)
	{
		int counter;
		byte[] result = new byte[2];
		int threshold;
		byte high=0;
		byte low;
		low =0;
		for(counter=0;counter<value;counter+=100)
		{
			threshold = value-counter;
			if(threshold>=100)
			{
				high++;
			}
			else
			{
				low=(byte) threshold;
			}
		}
		result[0]=HexToBCD(high);
		result[1]=HexToBCD(low);
		return result;
	}
	
	public static byte HexToBCD(byte value)
	{

		int counter;
		byte high=0;
		byte low;
		byte result;
		int threshold;
		low =0;
		for(counter=0;counter<value;counter+=10)
		{
			threshold = value-counter;
			if(threshold>=10)
			{
				threshold=10;
				high+=(byte)(0x10);
			}
			else
			low=(byte) threshold;
		}
		result = (byte) (high|low);
		
		return result;
		
		
	}
	public static byte[] IntegerToByteArray(int value)
	{
		ByteBuffer buffer = ByteBuffer.allocate(4) ;
		try{
		buffer.putInt(value); //amount of topup/deduct in hex value is stored in 4 bytes array
		}
		catch(BufferOverflowException e1)
		{
			return null;
		}
		return buffer.array();
	}
	
	public static int StringToInteger(String sValue)
	{
		try
		{
		return Integer.valueOf(sValue).intValue();//value is stored in integer variable
		}
		catch(NumberFormatException e1)
		{
			return -1;
		}
	}
	
	public static int ByteArrayToInt(byte[]bArray,int offset,int arraylength, byte endian_mode) {
		byte[] aTemp=new byte[10];
		//aTemp = Arrays.copyOfRange(bArray, (int)offset,(int) offset+arraylength);
		System.arraycopy(bArray, offset, aTemp, 0, arraylength);
		ByteBuffer buffer = ByteBuffer.wrap(aTemp);
		switch(endian_mode) {
			case LITTLE_ENDIAN:
				buffer.order(ByteOrder.LITTLE_ENDIAN);  // if you want little-endian
				break;
			case BIG_ENDIAN:
				buffer.order(ByteOrder.BIG_ENDIAN);  // if you want little-endian
				break;
		}
		return buffer.getInt();
	}
	
	public static long ByteArrayToLong(byte[]bArray,byte offset,byte arraylength, byte endian_mode)
	{
		long result=0;
		
		ByteBuffer buffer ;
		byte[] aTemp=new byte[10];
		//aTemp = Arrays.copyOfRange(bArray, (int)offset,(int) offset+arraylength);
		System.arraycopy(bArray, offset, aTemp, 0, arraylength);
		buffer = ByteBuffer.wrap(aTemp);
		switch(endian_mode)
		{
		case LITTLE_ENDIAN:
			buffer.order(ByteOrder.LITTLE_ENDIAN);  // if you want little-endian
			break;
		case BIG_ENDIAN:
			buffer.order(ByteOrder.BIG_ENDIAN);  // if you want little-endian
			break;
		
		}
		result= buffer.getLong();
		return result;

	}
	
	public static  byte[] InttoByteArray(int myInteger, byte endian_mode){
		
		byte[] aTemp=new byte[4];
		ByteBuffer buffer ;
		buffer = ByteBuffer.wrap(aTemp);
		switch(endian_mode)
		{
		case LITTLE_ENDIAN: 
		{
			buffer.order(ByteOrder.LITTLE_ENDIAN).putInt(myInteger);
			break;
		}
		case BIG_ENDIAN:
			buffer.order(ByteOrder.BIG_ENDIAN).putInt(myInteger);
			break;
		}
		return buffer.array();
	}
	
public static  byte[] LongtoByteArray(long myInteger, byte endian_mode){
		
		byte[] aTemp=new byte[8];
		ByteBuffer buffer ;
		buffer = ByteBuffer.wrap(aTemp);
		switch(endian_mode)
		{
		case LITTLE_ENDIAN: 
		{
			buffer.order(ByteOrder.LITTLE_ENDIAN).putLong(myInteger);
			break;
		}
		case BIG_ENDIAN:
			buffer.order(ByteOrder.BIG_ENDIAN).putLong(myInteger);
			break;
		}
		return buffer.array();
	}

	 public static String asciiToHex(String asciiValue)
	   {
	      char[] chars = asciiValue.toCharArray();
	      StringBuffer hex = new StringBuffer();
	      for (int i = 0; i < chars.length; i++)
	      {
	         hex.append(Integer.toHexString((int) chars[i]));
	      }
	      return hex.toString();
	   }

	public static byte[]  ChangeEndian(byte[] data,int len)
	{
		byte[] output = new byte[4];
		if(len<4)
			return null;

		output[0]=data[3];
		output[1]=data[2];
		output[2]=data[1];
		output[3]=data[0];

		return output;
	}
}