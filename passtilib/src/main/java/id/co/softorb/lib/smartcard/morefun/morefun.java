package id.co.softorb.lib.smartcard.morefun;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.cloudpos.POSTerminal;
import com.morefun.yapi.ServiceResult;
import com.morefun.yapi.card.cpu.APDUCmd;
import com.morefun.yapi.card.cpu.CPUCardHandler;
import com.morefun.yapi.device.reader.icc.ICCSearchResult;
import com.morefun.yapi.device.reader.icc.IccCardReader;
import com.morefun.yapi.device.reader.icc.IccCardType;
import com.morefun.yapi.device.reader.icc.IccReaderSlot;
import com.morefun.yapi.device.reader.icc.OnSearchIccCardListener;
import com.morefun.yapi.engine.DeviceInfoConstrants;
import com.morefun.yapi.engine.DeviceServiceEngine;
import com.usdk.apiservice.aidl.UDeviceService;

import java.lang.reflect.Method;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.apos.apos;
import id.co.softorb.lib.smartcard.posdevice.DeviceFunction;

import static id.co.softorb.lib.helper.ErrorCode.ERR_INITMSR;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;

public class morefun extends DeviceFunction {
    String tagclass= this.getClass().getSimpleName();
    Context ctx;
    private led posled;
    private contactless ctlReader;
    private msr msrReader;
    private contact cbReader;
    private printer posprinter;
    private buzzer buzzer;
    private static int result;

    private MorefunViewModel viewModel;

    public DeviceServiceEngine getDeviceService() {
        return deviceService;
    }

    public void setDeviceService(DeviceServiceEngine deviceService) {
        this.deviceService = deviceService;
    }

    private DeviceServiceEngine deviceService;
    private IccCardType iccCardType;
    private POSTerminal posTerminal;
    private sam samReader;
    private final String SERVICE_ACTION = "com.morefun.ysdk.service";
    private final String SERVICE_PACKAGE = "com.morefun.ysdk";

    public morefun(Context ctx) {
        this.ctx=ctx;
        ctlReader = new contactless();
        samReader=new sam();
        posled=new led();
        cbReader=new contact();
        msrReader=new msr();
        posprinter=new printer();
        buzzer=new buzzer();
    }

    @Override
    public int init(POSListener listener, Object instance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        if(checkAppInstalled("com.morefun.ysdk")){
            viewModel=new MorefunViewModel(this.ctx);
            viewModel.createService();
            Bundle bundle = new Bundle();

            viewModel.getmService().observeForever(service->{
                deviceService = DeviceServiceEngine.Stub.asInterface(service);
                Log.d("deviceService1", ""+deviceService);
                try {
                    deviceService.login(bundle,"09000000");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                result=ctlReader.init(listener,deviceService);
                Log.d("result ctl", ""+result);
                result=cbReader.init(listener,deviceService);
                Log.d("result contact", ""+result);
                result=msrReader.init(listener,deviceService);
                Log.d("result msr", ""+result);
                result=samReader.init(listener,deviceService);
                Log.d("result sam", ""+result);
                result=posprinter.init(listener,deviceService);
                Log.d("result printer", ""+result);
                result=posled.init(listener,deviceService);
                Log.d("result led", ""+result);
                result=buzzer.init(listener,deviceService);
                Log.d("result buzzer", ""+result);
            });
        }

        return OK;
    }

//    public void bindDeviceService() {
//        if (null != deviceService) {
//            return;
//        }
//
//        Intent intent = new Intent();
//        intent.setAction(SERVICE_ACTION);
//        intent.setPackage(SERVICE_PACKAGE);
//
//        this.ctx.bindService(intent, connection, Context.BIND_AUTO_CREATE);
//        new android.os.Handler().postDelayed( new Runnable() { public void run() {
//            setDeviceService(deviceService);
//
//
//        }}, 100);
//    }

    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
            Log.e(TAG, "======onServiceDisconnected======");
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            deviceService = DeviceServiceEngine.Stub.asInterface(service);
            Log.d(TAG, "======onServiceConnected======");
        }
    };

    @Override
    public int open() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        result=msrReader.open();
        if(result!=OK)
            return result;
        result=ctlReader.open();
        if(result!=OK)
            return result;
        result=posprinter.open();
        if(result!=OK)
            return result;
        result=cbReader.open();
        if(result!=OK)
            return result;
        result=samReader.open();
        if(result!=OK)
            return result;
        result= buzzer.open();
        if(result!=OK){
            return result;
        }
        result= posled.open();
        if(result!=OK){
            return result;
        }
        return result;
    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        result=msrReader.close();
        result=ctlReader.close();
        result=posprinter.close();
        result=cbReader.close();
        result=samReader.close();
        result=posled.close();
        result=buzzer.close();
        return result;
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return WAITING;
    }

    @Override
    public void stopfind() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        ctlReader.stopfind();
        msrReader.stopfind();
        samReader.stopfind();
        posprinter.stopfind();
        posled.stopfind();
        buzzer.stopfind();
        cbReader.stopfind();
    }

    @Override
    public int FindMSR() {
        if(msrReader==null)
            return NOT_OK;
        msrReader.find();
        return WAITING;
    }

    @Override
    public int FindCTL(int mode) {
        ctlReader.setMode(mode);
        if(ctlReader==null)
            return NOT_OK;
        ctlReader.find();
        return WAITING;
    }

    @Override
    public int FindCB() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        if(cbReader==null)
            return NOT_OK;
        result=cbReader.find();
        return result;
    }

    @Override
    public int FindSAM(int type, int slot) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=samReader.find();
        return result;
    }

    @Override
    public byte[] sendCTL(byte[] apdu) {
        byte[] res;
        res=ctlReader.sendCTL(apdu);

        return res;
    }

    @Override
    public byte[] sendCB(byte[] apdu) {
        return cbReader.sendCB(1,apdu);
    }

    @Override
    public byte[] sendSAM(int slot, byte[] apdu) {
        Log.d("sendsam", ""+slot+" "+toReversedHex(apdu));
        return samReader.sendCB(slot,apdu);
    }

    @Override
    public int printercutpaper() {
        return posprinter.cutpaper();
    }

    @Override
    public int printerlinefeed() {
        return posprinter.linefeed();
    }

    @Override
    public int printerstatus() {
        return posprinter.status();
    }

    @Override
    public int printbitmap(int alignment, Bitmap bitmap) {
        return posprinter.bitmap(alignment, bitmap);
    }

    @Override
    public int print_text(int fonttype, int alignment, String text, boolean newline) {
        return posprinter.text(fonttype,alignment,text,newline);
    }

    @Override
    public void setcardwaitingtime(int timeout) {
        waitingtime=timeout;
    }

    @Override
    public String PinPad_SN() {
        return null;
    }

    @Override
    public int PinPad_SetPINLength(int min, int max) {
        return 0;
    }

    @Override
    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value) {
        return 0;
    }

    @Override
    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value, byte[] checksum) {
        return 0;
    }

    @Override
    public byte[] PinPad_GetSessionKey(int mkid, int userkeyid, int algo) {
        return new byte[0];
    }

    @Override
    public byte[] PinPad_Encrypt(int mkid, int userkeyid, int algo, byte[] data) {
        return new byte[0];
    }

    @Override
    public byte[] PinPad_Calculatemac(int mkid, int userkeyid, int keyalgo, int macalgo, byte[] data) {
        return new byte[0];
    }

    @Override
    public byte[] PinPad_Calculatedukptmac(byte[] data) {
        return new byte[0];
    }

    @Override
    public int PinPad_Verifymac(int mkid, int keyid, byte[] data, byte[] mac) {
        return 0;
    }

    @Override
    public int PinPad_async_pinblock(int mkid, int keyid, byte[] data) {
        return 0;
    }

    @Override
    public byte[] PinPad_sync_pinblock(int mkid, int keyid, byte[] data) {
        return new byte[0];
    }

    @Override
    public int PinPad_getMKStatus(int mkid) {
        return 0;
    }

    @Override
    public int PinPad_getSKStatus(int mkid, int skid) {
        return 0;
    }

    @Override
    public int PinPad_getDUKPTStatus(int mkid, byte[] uniquekey) {
        return 0;
    }

    @Override
    public String DeviceSN() {
        iccCardType=new IccCardType();
        Log.d("cardtype", iccCardType.AT24CXX);
        String sn = null;
        sn = getSerialNumber();
        Log.d("serialnumbermorefun1", sn);
        return sn;
    }

    private static String getSerialNumber(){
        String serialNumber;
        try{
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get= c.getMethod("get", String.class);
            serialNumber=(String) get.invoke(c, "gsm.sn1");
            if(serialNumber.equals(""))
                serialNumber=(String) get.invoke(c, "ril.serialnumber");
            if(serialNumber.equals(""))
                serialNumber=(String) get.invoke(c, "ro.serialno");
            if(serialNumber.equals(""))
                serialNumber=(String) get.invoke(c, "sys.serialnumber");
            if(serialNumber.equals(""))
                serialNumber= Build.SERIAL;
            if(serialNumber.equals(Build.UNKNOWN))
                serialNumber=null;
        } catch (Exception e) {
            e.printStackTrace();
            serialNumber=null;
        }
        return serialNumber;
    }
    private String toReversedHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; ++i) {
            if (i > 0) {
                sb.append(" ");
            }
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
        }
        return sb.toString();
    }

    @Override
    public String LibVersion() {
        return null;
    }

    public Object getlED() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return posled;
    }

    private boolean checkAppInstalled(String pkgName) {
        if (pkgName == null || pkgName.isEmpty()) {
            return false;
        }
        PackageInfo packageInfo;
        try {
            packageInfo = ctx.getPackageManager().getPackageInfo(pkgName, 0);
            return packageInfo != null;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public class MorefunViewModel extends ViewModel {
        public Context context;
        private MutableLiveData<Boolean> mServiceBound=new MutableLiveData<>();
        private MutableLiveData<IBinder> mService=new MutableLiveData<>();

        public LiveData<IBinder> getmService() {
            return mService;
        }

        public MorefunViewModel(Context context){
            this.context=context;
        }

        public final void createService(){
            Intent intent = new Intent();
            intent.setAction(SERVICE_ACTION);
            intent.setPackage(SERVICE_PACKAGE);

            this.context.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        }

        private ServiceConnection mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                mServiceBound.setValue(false);
            }
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mServiceBound.setValue(true);
                Log.d("mServiceBound10", ""+mServiceBound.getValue());
                mService.setValue(service);
            }
        };

    }
}
