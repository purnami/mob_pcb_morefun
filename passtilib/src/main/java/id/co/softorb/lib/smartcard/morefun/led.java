package id.co.softorb.lib.smartcard.morefun;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.morefun.yapi.device.led.LEDDriver;
import com.morefun.yapi.engine.DeviceServiceEngine;

import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.LED;

import static id.co.softorb.lib.helper.ErrorCode.ERR_INITLED;
import static id.co.softorb.lib.helper.ErrorCode.OK;

public class led extends LED {
    String tagclass= this.getClass().getSimpleName();
    private DeviceServiceEngine deviceService;
    private LEDDriver ledDriver;

    public static final int YELLOW = 1;
    public static final int GREEN = 2;
    public static final int RED = 3;
    public static final int BLUE = 4;
    @Override
    public int blink() {
        return 0;
    }

    @Override
    public int open(int color) {
        try {
            ledDriver.setLed(color, true);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int on() {
        try {
            ledDriver.PowerLed(true, true, true, true);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int off() {
        try {
            ledDriver.PowerLed(false, false, false, false);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean ison() {
        return false;
    }

    @Override
    public int init(POSListener detectListener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
//            Bundle bundle = new Bundle();
            deviceService=(DeviceServiceEngine) devInstance;
//            deviceService.login(bundle,"09000000");
            ledDriver = deviceService.getLEDDriver();
            open(BLUE);
//            on();
//            off();
            if (ledDriver != null) {
                Log.d("masuk init led", "abc");
                return OK;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return ERR_INITLED;
    }

    @Override
    public int open() {
        //Tidak ada fungsi open()
        return OK;
    }

    @Override
    public int close() {
        //Tidak ada fungsi close()
        return OK;
    }

    @Override
    public int find() {
        //Tidak ada fungsi find()
        return 0;
    }

    @Override
    public void stopfind() {
        //Tidak ada fungsi stopfind()
    }
}
