package id.co.softorb.lib.passti;


import android.util.Log;

import id.co.softorb.lib.helper.Hex;

import static id.co.softorb.lib.smartcard.posdevice.params.*;


public class debughelper {

    String TAG = "STIUtility";


    void DebugDeductAction(String label,int action)
    {
        String output="";
        switch(action)
        {
            case NORMAL:  String.format("%s : NORMAL",label); break;
            case REPURCHASE:    String.format("%s : NORMAL",label); break;
            case REWRITEPARTNERDATA:    String.format("%s : NORMAL",label); break;
            default :

                String.format("%s : i",label,action); break;
        }
        Log.d(TAG,output);
    }


    void DebugPrintString(String label)
    {
        Log.d(TAG,label);

    }
    void DebugCardType(String label, int cardtype)
    {
        String output="";
        switch(cardtype)
        {
            case ctype_luminos:output = String.format("%s : ctype_luminos",label); break;
            case ctype_mdr:output = String.format("%s : ctype_mdr",label); break;
            case ctype_bri:output = String.format("%s : ctype_bri",label); break;
            case ctype_bni:output = String.format("%s : ctype_bni",label); break;
            case ctype_bca:output = String.format("%s : ctype_bca",label); break;
            case ctype_nobu:output = String.format("%s : ctype_nobu",label); break;
            case ctype_mega:output = String.format("%s : ctype_mega",label); break;
            case ctype_dki:output = String.format("%s : ctype_dki",label); break;
            default:

                String.format("%s : unknown, code %i",label,cardtype); break;

        }
        Log.d(TAG,output);
    }


    void DebugHex(String srcMsg, byte[] srcHex)
    {
        String output="";
        output = String.format("%s : %s",srcMsg, Hex.bytesToHexString(srcHex));
        Log.d(TAG,output);

    }

    void DebugHex(String srcMsg, byte[] srcHex,int offset,int length)
    {
        String output="";
        output = String.format("%s : %s",srcMsg,Hex.bytesToHexString(srcHex,offset,length));
        Log.d(TAG,output);

    }
    void DebugHex(String srcMsg, byte[] srcHex,int length)
    {
        String output="";
        output = String.format("%s : %s",srcMsg,Hex.bytesToHexString(srcHex,0,length));
        Log.d(TAG,output);

    }

    void DebugASCII(String srcMsg, String srcASCII)
    {
        String output="";
        output = String.format("%s : %s",srcMsg,srcASCII);
        Log.d(TAG,output);

    }


    void DebugLong(String srcMsg, long value)
    {

        String output="";
        output = String.format("%s : %d",srcMsg,value);
        Log.d(TAG,output);
    }


    void DebugInteger(String srcMsg, int value)
    {
        String output="";
        output = String.format("%s : %d",srcMsg,value);
        Log.d(TAG,output);
    }


    void DebugBoolean(String srcMsg, boolean value)
    {
        String output="";
        //output = String.format("%s : %b",srcMsg,value? "true" : "false");
        output = String.format("%s : %b",srcMsg,value);
        Log.d(TAG,output);

    }

    void DebugString(String label,String srcMsg)
    {
        String output="";
        output = String.format("%s : %s",label,srcMsg);
        Log.d(TAG,output);
    }




}
