package id.co.softorb.lib.smartcard.apos;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.usdk.apiservice.aidl.UDeviceService;
import com.usdk.apiservice.aidl.constants.RFDeviceName;
import com.usdk.apiservice.aidl.data.BytesValue;
import com.usdk.apiservice.aidl.data.IntValue;
import com.usdk.apiservice.aidl.icreader.DriverID;
import com.usdk.apiservice.aidl.icreader.ICError;
import com.usdk.apiservice.aidl.icreader.PowerMode;
import com.usdk.apiservice.aidl.icreader.UICCpuReader;
import com.usdk.apiservice.aidl.icreader.Voltage;
import com.usdk.apiservice.aidl.magreader.OnSwipeListener;
import com.usdk.apiservice.aidl.magreader.UMagReader;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.Base;

import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INITREADER;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;

public class msr extends Base {
    String tagclass= this.getClass().getSimpleName();
    private UDeviceService deviceService;
    private UMagReader magReader;
    private POSListener detectListener;
    byte[][] trackData = new byte[3][];
    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            deviceService = (UDeviceService) devInstance;
            Log.d("device service",""+deviceService.getVersion());
//            buzzer.init(listener,deviceService);
            Bundle param = new Bundle();
            param.putString("rfDeviceName", RFDeviceName.INNER);
            BytesValue responseData = new BytesValue();
            magReader=(UMagReader) UMagReader.Stub.asInterface(deviceService.getMagReader());
            if (magReader!=null) {
                Log.d("result init magcard","ok");
                detectListener = listener;
                return OK;

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());

        }
        return NOT_OK;
    }

    @Override
    public int open() {
        return 0;
    }

    @Override
    public int close() {
        return 0;
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
//        int result=NOT_OK;
        try {
            magReader.enableTrack(1);
            magReader.enableTrack(2);
            magReader.enableTrack(3);
            magReader.setTrackType(0);
            magReader.searchCard(waitingtime,listener);

        } catch (RemoteException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return WAITING;
    }

    @Override
    public void stopfind() {

    }

    OnSwipeListener.Stub listener =new OnSwipeListener.Stub() {
        @Override
        public void onSuccess(Bundle bundle) throws RemoteException {
            Log.d(TAG, "----- onSuccess -----");
            trackData = new byte[3][];

            Log.d("Track1", ""+bundle.getString("TRACK1"));
            Log.d("Track2", ""+bundle.getString("TRACK2"));
            Log.d("Track3", ""+bundle.getString("TRACK3"));
            trackData[0]= bundle.getString("TRACK1").getBytes();
            trackData[1]=bundle.getString("TRACK2").getBytes();
            trackData[2]=bundle.getString("TRACK3").getBytes();
            detectListener.onMSRDetected(trackData);
            Log.d("Track1", ""+ Hex.bytesToHexString(trackData[0])+" ");
            Log.d("Track2", ""+Hex.bytesToHexString(trackData[1])+" ");
            Log.d("Track3", ""+Hex.bytesToHexString(trackData[2])+" ");
        }

        @Override
        public void onError(int i) throws RemoteException {
            Log.d(TAG, "----- onError -----");
        }

        @Override
        public void onTimeout() throws RemoteException {
            Log.d(TAG, "----- onTimeout -----");
        }
    };
}
