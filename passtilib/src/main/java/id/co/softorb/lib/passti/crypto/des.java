package id.co.softorb.lib.passti.crypto;

import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import id.co.softorb.lib.helper.Hex;


public class des {
	private final int block_size =8;
	private static final String THREEDES_CBC_NO_PAD = "DESede/CBC/NoPadding";
	private static final String THREEDES_CBC_PKCS5_PAD = "DESede/CBC/PKCS5Padding";

	private byte[] IV = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};// initialization vector
	 byte[] vector = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};// initialization vector

	private Cipher c3des;

	private SecretKeySpec sks = null;
	private IvParameterSpec ivSpec;
	private String algo;

	  public des() {
		ivSpec = new IvParameterSpec(IV);
		SetCryptoAlgo(THREEDES_CBC_NO_PAD);
	}

	private void SetCryptoAlgo(String CryptoAlgo) {
		algo = CryptoAlgo;
		try {
			c3des = Cipher.getInstance(algo);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		}
	}

	  public void LoadKey(byte[] Key) {
		if(Key.length<24) return;
		sks = new SecretKeySpec(Key, "DESede");
	}

	public void InitEncryption(byte[] iv) {
		if(sks==null) return;
		try {
			ivSpec = new IvParameterSpec(iv);
			c3des.init(Cipher.ENCRYPT_MODE, sks,ivSpec);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void InitDecryption(byte[] iv) {
		if(sks==null) return;
		try {
			ivSpec = new IvParameterSpec(iv);
			c3des.init(Cipher.DECRYPT_MODE, sks,ivSpec);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	  public byte[] doEncrypt(byte[] data2encrypt) {
		byte[] cipherText;
		try {
			c3des = Cipher.getInstance(algo);
			c3des.init(Cipher.ENCRYPT_MODE, sks, ivSpec);
			cipherText = c3des.doFinal(data2encrypt);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return cipherText;
	}

	  public byte[] doDecrypt(byte[] data2decrypt) {
		byte[] plainText;
		try {
			c3des = Cipher.getInstance(algo);
			c3des.init(Cipher.DECRYPT_MODE, sks, ivSpec);
			plainText = c3des.doFinal(data2decrypt);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return plainText;
	}
	
	 byte[] doConcatEncrypt(boolean lastBlock, byte[] data2encrypt) {
		byte[] result;
		int dataLen;
		dataLen=data2encrypt.length;

		if((dataLen%block_size)!=0) SetCryptoAlgo(THREEDES_CBC_PKCS5_PAD);
		else SetCryptoAlgo(THREEDES_CBC_NO_PAD);

		InitEncryption(IV);

		if(lastBlock) result=finalizeCipher(data2encrypt);
		else result=doCipher(data2encrypt);

		return result;
	}

	 byte[] doConcatDecrypt(boolean lastBlock, byte[] data2decrypt) {
		byte[] result;
		int dataLen;
		dataLen=data2decrypt.length;

		if((dataLen%block_size)!=0) SetCryptoAlgo(THREEDES_CBC_PKCS5_PAD);
		else SetCryptoAlgo(THREEDES_CBC_NO_PAD);

		InitDecryption(IV);

		if(lastBlock) result=finalizeCipher(data2decrypt);
		else result=doCipher(data2decrypt);

		return result;
	}

	private byte[] doCipher(byte[] dataToCipher) {
		return c3des.update(dataToCipher,0,dataToCipher.length);
	}

	private byte[] finalizeCipher(byte[] dataToCipher) {
		byte[] cipherBytes = null;
		try {
			cipherBytes = c3des.doFinal(dataToCipher,0,dataToCipher.length);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
		return cipherBytes;
	}

     byte[] encrypt(String value) {
        byte[] encrypted = null;
        try {

            //byte[] raw = new byte[]{'T', 'h', 'i', 's', 'I', 's', 'A', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};
            byte[] raw = new byte[24];
            Key skeySpec = new SecretKeySpec(raw, "DESede");
            Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            byte[] iv = new byte[cipher.getBlockSize()];

            IvParameterSpec ivParams = new IvParameterSpec(iv);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec,ivParams);
            encrypted  = cipher.doFinal(Hex.hexStringToByteArray(value));

            System.out.println("encrypted string length: " + encrypted.length);

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return encrypted;
    }

      byte[]  decrypt(byte[] encrypted) {
        byte[] original = null;
        Cipher cipher = null;
        try {
            //byte[] raw = new byte[]{'T', 'h', 'i', 's', 'I', 's', 'A', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};
            byte[] raw = new byte[24];
            Key key = new SecretKeySpec(raw, "DESede");
            cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            //the block size (in bytes), or 0 if the underlying algorithm is not a block cipher
            byte[] ivByte = new byte[cipher.getBlockSize()];
            //This class specifies an initialization vector (IV). Examples which use
            //IVs are ciphers in feedback mode, e.g., DES in CBC mode and RSA ciphers with OAEP encoding operation.
            IvParameterSpec ivParamsSpec = new IvParameterSpec(ivByte);
            cipher.init(Cipher.DECRYPT_MODE, key, ivParamsSpec);
            original= cipher.doFinal(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return original;
    }

}