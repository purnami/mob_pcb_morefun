package id.co.softorb.lib.smartcard.morefun;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.POSTerminal;
import com.cloudpos.card.CPUCard;
import com.cloudpos.rfcardreader.RFCardReaderDevice;
import com.morefun.yapi.ServiceResult;
import com.morefun.yapi.card.cpu.APDUCmd;
import com.morefun.yapi.card.cpu.CPUCardHandler;
import com.morefun.yapi.card.mifare.M1CardHandler;
import com.morefun.yapi.device.reader.icc.ICCSearchResult;
import com.morefun.yapi.device.reader.icc.IccCardReader;
import com.morefun.yapi.device.reader.icc.IccCardType;
import com.morefun.yapi.device.reader.icc.IccReaderSlot;
import com.morefun.yapi.device.reader.icc.OnSearchIccCardListener;
import com.morefun.yapi.engine.DeviceServiceEngine;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.ContactlessBase;

import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INITREADER;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;

public class contactless extends ContactlessBase {
    String tagclass= this.getClass().getSimpleName();
    private DeviceServiceEngine deviceService;
    public void setSlot(int slot) {
        this.slot = slot;
    }

    public int getSlot() {
        return slot;
    }

    int slot;
    private POSListener detectListener;
    IccCardReader rfReader;

    private CPUCardHandler cpuCardHandler;

    public contactless() {
    }

    @Override
    public byte[] sendCTL(final byte[] apdu) {
        byte[] result=new byte[0];
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] tmp = new byte[256];
        int ret;
        try {
            Thread.sleep(500);
            ret = cpuCardHandler.exchangeCmd(tmp, apdu, apdu.length);
            result=Hex.subByte(tmp, 0, ret);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public void setMode(int mode) {
        this.mode=mode;
    }

    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
//            Bundle bundle = new Bundle();
            deviceService=(DeviceServiceEngine) devInstance;
//            deviceService.login(bundle,"09000000");
            rfReader = deviceService.getIccCardReader(IccReaderSlot.RFSlOT);
            Log.d("rfreader", ""+rfReader.isCardExists());
//            cpuCardHandler = deviceService.getCPUCardHandler(rfReader);
//            cpuCardHandler.active();

            if (rfReader != null) {
                detectListener = listener;
                return OK;
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());

        }
        return CTL_ERR_INITREADER;
    }

    @Override
    public int open() {
        //tidak fungsi untuk open
        return OK;
    }

    @Override
    public int close() {
        if(cpuCardHandler!=null) {
            try {
                cpuCardHandler.setPowerOff();
            } catch (RemoteException e) {
                e.printStackTrace();
                return NOT_OK;
            }
        }
        return OK;
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            rfReader.searchCard(listener, 0, new String[]{IccCardType.CPUCARD});
            Log.d("ICcard2", String.valueOf(rfReader.isCardExists()));
            Log.d("SLOTTTT", ""+getSlot());
        } catch (RemoteException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return WAITING;
    }

    @Override
    public void stopfind() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            rfReader.stopSearch();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    OnSearchIccCardListener.Stub listener = new OnSearchIccCardListener.Stub() {
        @Override
        public void onSearchResult(final int retCode, Bundle bundle) throws RemoteException {
            byte[] tempuid=null;

            if (ServiceResult.Success == retCode) {
                String cardType = bundle.getString(ICCSearchResult.CARDTYPE);
                Log.d("cardType", cardType);
                int slot = bundle.getInt(ICCSearchResult.CARDOTHER);
                String uid = bundle.getString(ICCSearchResult.M1SN);
                tempuid=Hex.hexStringToByteArray(uid);
//                tempuid=Arrays.copyOfRange(tempuid,1,tempuid.length);
                Log.d("newuid", ""+Hex.bytesToHexString(tempuid));
                setSlot(slot);

                cpuCardHandler = deviceService.getCPUCardHandler(rfReader);
                cpuCardHandler.setPowerOn(new byte[]{0x00, 0x00});
                cpuCardHandler.active();
                Log.d("slot",""+slot+" uid="+uid+" tempuid="+Hex.bytesToHexString(tempuid));

            }
            else {
                stopfind();
            }
            detectListener.onRFDetected(tempuid, mode);
        }
    };

}
