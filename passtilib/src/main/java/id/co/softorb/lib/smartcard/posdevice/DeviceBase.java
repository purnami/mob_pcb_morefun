package id.co.softorb.lib.smartcard.posdevice;


public abstract class DeviceBase extends Base {


    public abstract String DeviceSN();

    public abstract String LibVersion();






    public abstract class SAM extends Base
    {
        public abstract int init();
        public abstract int open();
        public abstract int close();
        public abstract int find();
        public abstract void stopfind();
        public abstract byte[] sendapdu(int port,byte[] apdu);
    }



}
