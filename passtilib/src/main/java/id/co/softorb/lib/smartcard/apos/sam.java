package id.co.softorb.lib.smartcard.apos;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.usdk.apiservice.aidl.UDeviceService;
import com.usdk.apiservice.aidl.data.ApduResponse;
import com.usdk.apiservice.aidl.data.BytesValue;
import com.usdk.apiservice.aidl.data.IntValue;
import com.usdk.apiservice.aidl.icreader.DriverID;
import com.usdk.apiservice.aidl.icreader.ICError;
import com.usdk.apiservice.aidl.icreader.PowerMode;
import com.usdk.apiservice.aidl.icreader.UPSamReader;
import com.usdk.apiservice.aidl.icreader.Voltage;
import com.usdk.apiservice.aidl.rfreader.RFError;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.ContactBase;

import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INITREADER;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.SAM_ERR_POWERONFAIL;
import static id.co.softorb.lib.helper.ErrorCode.SAM_ERR_RELEASEFAIL;

public class sam extends ContactBase {
    String tagclass= this.getClass().getSimpleName();
    private UPSamReader psamReader;
    private UDeviceService deviceService;
    private static final String SLOT = "slot";
    @Override
    public byte[] sendCB(int slot, byte[] apdu) {
        byte[] rapdu=new byte[0];
        try {
            Log.d("device service",""+deviceService.getVersion());
            Log.d("apdu", Hex.bytesToHexString(apdu));
            ApduResponse apduResponse = psamReader.exchangeApdu(apdu);
            Log.d("rapdu sam apos", ""+ Hex.bytesToHexString(apduResponse.getData())+" "+ Hex.byteToHexString(apduResponse.getSW1())+" "+ Hex.byteToHexString(apduResponse.getSW2()));
            byte[] data=apduResponse.getData();
            byte[] sw={apduResponse.getSW1(),apduResponse.getSW2()};
            if(Hex.bytesToHexString(sw).equals("9000")){
                Log.d("sw", "9000");
                if(data!=null){
                    Log.d("data", "tidak null");
                    rapdu=new byte[data.length+ sw.length];
                    rapdu= BytesUtil.merge(data, sw, new byte[]{});
                }else{
                    Log.d("data", "null");
                    rapdu=new byte[sw.length];
                    rapdu=sw;
                }

            }else{
                Log.d("sw", "tidak 9000");
                rapdu=new byte[sw.length];
                rapdu=sw;
            }
            Log.d("new rapdu apos1", ""+ Hex.bytesToHexString(rapdu)+" ");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Log.d("new rapdu apos2", ""+ Hex.bytesToHexString(rapdu)+" ");
        return rapdu;
    }

    @Override
    public int init(POSListener detectListener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            deviceService = (UDeviceService) devInstance;
            Log.d("device service",""+deviceService.getVersion());
            Bundle bundle = new Bundle();
            bundle.putInt(SLOT, 1);
            psamReader=(UPSamReader) UPSamReader.Stub.asInterface(deviceService.getICReader(DriverID.PSAMCARD, bundle));
            int ret = psamReader.initModule(Voltage.ICCpuCard.VOL_DEFAULT, PowerMode.DEFAULT);
            if (ret == ICError.SUCCESS) {
                Log.d("result init sam apos","ok");
                return OK;

            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return SAM_ERR_RELEASEFAIL;
    }

    @Override
    public int open() {
        return 0;
    }

    @Override
    public int close() {
        return 0;
    }

    @Override
    public int find() {

        try {
            BytesValue atr = new BytesValue();
            IntValue protocol = new IntValue();
            int powerup=psamReader.powerUp(atr, protocol);
            if (powerup == ICError.SUCCESS) {
                Log.d("result find sam apos","ok");
                return OK;

            }
            else
                return SAM_ERR_POWERONFAIL;
        } catch (RemoteException e) {
            e.printStackTrace();
            return SAM_ERR_POWERONFAIL;
        }

    }

    @Override
    public void stopfind() {

    }
}
