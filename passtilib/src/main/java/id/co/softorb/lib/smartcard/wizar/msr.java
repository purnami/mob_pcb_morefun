package id.co.softorb.lib.smartcard.wizar;

import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationListener;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.msr.MSRDevice;
import com.cloudpos.msr.MSROperationResult;
import com.cloudpos.msr.MSRTrackData;

import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.Base;
import id.co.softorb.lib.smartcard.posdevice.DeviceBase;

import static id.co.softorb.lib.helper.ErrorCode.*;


class msr extends Base
{
    String tagclass= this.getClass().getSimpleName();

    private MSRDevice MSR;
    private POSTerminal posTerminal;

    private MSRTrackData msrTrackData;
    private POSListener detectListener;
    byte[][] trackData = new byte[3][];
    OperationListener msrlistener = new OperationListener() {

        @Override
        public void handleResult(OperationResult arg0) {


            int trackError = 0;
            trackData = new byte[3][];

            Log.e(TAG,"arg0.getResultCode() = "+arg0.getResultCode());
            if (arg0.getResultCode() == OperationResult.SUCCESS) {

                msrTrackData = ((MSROperationResult) arg0).getMSRTrackData();
                for (int trackNo = 0; trackNo < 3; trackNo++) {
                    trackError = msrTrackData.getTrackError(trackNo);
                    if (trackError == MSRTrackData.NO_ERROR) {
                        trackData[trackNo] = msrTrackData.getTrackData(trackNo);

                    } else {

                    }
                }
                detectListener.onMSRDetected(trackData);

            }
            else
            {
                detectListener.onMSRDetected(null);
            }



        }
    };
    /*
            public msr()
            {

            }
            @Override
            public int init() {
                Log.i(TAG, tagclass+"."+new Throwable()
                        .getStackTrace()[0]
                        .getMethodName());
                MSR =(MSRDevice) posTerminal.getDevice("cloudpos.device.msr");
                if(MSR!=null) {
                    return OK;
                }
                else
                    return ERR_INITMSR;
            }*/
    @Override
    public int init(POSListener listener,Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=ERR_INITMSR;

        try {
            posTerminal=(POSTerminal) devInstance;
            MSR = (MSRDevice) posTerminal.getDevice("cloudpos.device.msr");
            if (MSR != null) {
                detectListener = listener;
                return OK;
            } else
                return ERR_INITMSR;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());

        }
        return result;
    }

    @Override
    public int open() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {

            MSR.open();
        } catch (DeviceException e) {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());
            return ERR_OPENMSR;
        }
        return OK;
    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            MSR.close();

        }
        catch (DeviceException e)
        {
            Log.e(TAG,e.toString());
            return NOT_OK;
        }
        return OK;
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            MSR.listenForSwipe(msrlistener, waitingtime);
        }
        catch (DeviceException e)
        {
            return NOT_OK;

        }
        return WAITING;
    }

    @Override
    public void stopfind() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {

            MSR.cancelRequest();
        }
        catch (DeviceException e)
        {
            Log.e(TAG,e.toString());

        }


    }


}
