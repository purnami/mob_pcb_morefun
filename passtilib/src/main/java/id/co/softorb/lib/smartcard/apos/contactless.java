package id.co.softorb.lib.smartcard.apos;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.cloudpos.rfcardreader.RFCardReaderDevice;
import com.morefun.yapi.card.cpu.CPUCardHandler;
import com.usdk.apiservice.aidl.UDeviceService;
import com.usdk.apiservice.aidl.beeper.UBeeper;
import com.usdk.apiservice.aidl.constants.RFDeviceName;
import com.usdk.apiservice.aidl.data.ApduResponse;
import com.usdk.apiservice.aidl.data.BytesValue;
import com.usdk.apiservice.aidl.icreader.UICCpuReader;
import com.usdk.apiservice.aidl.led.ULed;
import com.usdk.apiservice.aidl.rfreader.CardType;
import com.usdk.apiservice.aidl.rfreader.OnPassListener;
import com.usdk.apiservice.aidl.rfreader.RFError;
import com.usdk.apiservice.aidl.rfreader.URFReader;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.smartcard.POSListener;

import id.co.softorb.lib.smartcard.posdevice.ContactlessBase;

import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INITREADER;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;

public class contactless extends ContactlessBase {
    String tagclass= this.getClass().getSimpleName();
    private URFReader rfReader;
    private POSListener detectListener;

    private UDeviceService deviceService;
    private IBinder service;
    private buzzer buzzer;
    private Context context;
    private UBeeper beeper;
    private ULed led;

    public byte[] getTempuid() {
        return tempuid;
    }

    public void setTempuid(byte[] tempuid) {
        this.tempuid = tempuid;
    }

    byte[] tempuid=null;
    public int getCardType() {
        return cardType;
    }

    public void setCardType(int cardType) {
        this.cardType = cardType;
    }

    int cardType;

    ApduResponse apduResponse;
    @Override
    public byte[] sendCTL(byte[] apdu) {

        Log.d("cardtype sendctl",""+getCardType());
        byte[] rapdu=new byte[0];
        try {
//            beeper.startBeep(200);
//            led.turnOn(1);
//            buzzer.startBeep(100);
            Log.d("device service22",""+deviceService.getVersion());
            Log.d("apdu", Hex.bytesToHexString(apdu));
            rfReader.isExist();

            BytesValue responseData = new BytesValue();
            rfReader.activate(getCardType(), responseData);
//            Log.d("cardtype222",""+getCardType()+" isexist "+rfReader.isExist());
            if(apduResponse!=null){
                Log.d("rapdu apos", ""+ Hex.bytesToHexString(apduResponse.getData())+" "+ Hex.byteToHexString(apduResponse.getSW1())+" "+ Hex.byteToHexString(apduResponse.getSW2()));
            }

            apduResponse=rfReader.exchangeApdu(apdu);
//            if(apduResponse==null){
//                Log.d("rapdu null","null");
//            }
            Log.d("rapdu apos", ""+ Hex.bytesToHexString(apduResponse.getData())+" "+ Hex.byteToHexString(apduResponse.getSW1())+" "+ Hex.byteToHexString(apduResponse.getSW2()));
            byte[] data=apduResponse.getData();
            byte[] sw={apduResponse.getSW1(),apduResponse.getSW2()};
            if(Hex.bytesToHexString(sw).equals("9000")){
                if(data!=null){
                    rapdu=new byte[data.length+ sw.length];
                    rapdu=BytesUtil.merge(data, sw, new byte[]{});
                }else{
                    rapdu=new byte[sw.length];
                    rapdu=sw;
                }

            }else{
                rapdu=new byte[sw.length];
                rapdu=sw;
            }
            Log.d("new rapdu apos1", ""+ Hex.bytesToHexString(rapdu)+" ");
//            rapdu=cekapdu(rapdu, apdu);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Log.d("new rapdu apos2", ""+ Hex.bytesToHexString(rapdu)+" ");
        return rapdu;
    }
    private byte[] cekapdu(byte[] rapdu, byte[] apdu) {
//        byte[] result=new byte[0];
        if(Hex.bytesToHexString(apdu).equals("904C000004")){
            Log.d("masukcekbal","");
            byte[] dataArray=Hex.hexStringToByteArray("00A4040008A000000571504805");
            try {
                rfReader.exchangeApdu(dataArray);
                ApduResponse apduResponse=rfReader.exchangeApdu(apdu);
                byte[] data=apduResponse.getData();
                byte[] sw={apduResponse.getSW1(),apduResponse.getSW2()};
                rapdu=new byte[data.length+ sw.length];
                rapdu=BytesUtil.merge(data, sw, new byte[]{});
                Log.d("new rapdu", ""+ Hex.bytesToHexString(rapdu)+" ");
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        if(Hex.bytesToHexString(apdu).equals("90400300040000000117")){
            byte[] dataArray=Hex.hexStringToByteArray("00A4040008A000000571504805");
            byte[] dataArray2=Hex.hexStringToByteArray("904C000004");
            try {
                rfReader.exchangeApdu(dataArray);
                rfReader.exchangeApdu(dataArray2);
                ApduResponse apduResponse=rfReader.exchangeApdu(apdu);
                byte[] data=apduResponse.getData();
                byte[] sw={apduResponse.getSW1(),apduResponse.getSW2()};
                rapdu=new byte[data.length+ sw.length];
                rapdu=BytesUtil.merge(data, sw, new byte[]{});
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        if(Hex.bytesToHexString(apdu).contains("90460000123015")){
            byte[] dataArray=Hex.hexStringToByteArray("00A4040008A000000571504805");
            byte[] dataArray2=Hex.hexStringToByteArray("904C000004");
            byte[] dataArray3=Hex.hexStringToByteArray("90400300040000000117");
            try {
                rfReader.exchangeApdu(dataArray);
                rfReader.exchangeApdu(dataArray2);
                rfReader.exchangeApdu(dataArray3);
                ApduResponse apduResponse=rfReader.exchangeApdu(apdu);
                byte[] data=apduResponse.getData();
                byte[] sw={apduResponse.getSW1(),apduResponse.getSW2()};
                rapdu=new byte[data.length+ sw.length];
                rapdu=BytesUtil.merge(data, sw, new byte[]{});
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return rapdu;
    }

    @Override
    public void setMode(int mode) {
        this.mode=mode;
    }

    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            deviceService = (UDeviceService) devInstance;
            Log.d("device service",""+deviceService.getVersion());
//            buzzer.init(listener,deviceService);
            Bundle param = new Bundle();
            param.putString("rfDeviceName", RFDeviceName.INNER);
            BytesValue responseData = new BytesValue();
            rfReader = (URFReader) URFReader.Stub.asInterface(deviceService.getRFReader(param));
//            beeper = (UBeeper) UBeeper.Stub.asInterface(deviceService.getBeeper());
//            led=(ULed) ULed.Stub.asInterface(deviceService.getLed(param));

//            rfReader.activate(getCardType(), responseData);

            int ret = rfReader.activate(cardType, responseData);
            if (ret == RFError.SUCCESS) {
                Log.d("result init ctl apos","ok");
                detectListener = listener;
                return OK;

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());
        }
        return CTL_ERR_INITREADER;
    }

    @Override
    public int open() {
        return 0;
    }

    @Override
    public int close() {
        return 0;
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            rfReader.searchCard(listener);
//            rfReader.isExist();
//            BytesValue responseData = new BytesValue();
//            rfReader.activate(getCardType(), responseData);
//            Log.d("cardtypeee",""+getCardType());
//            Log.d("apos cardin", rfReader.isExist()+"   "+Hex.bytesToHexString(getTempuid()));
        } catch (RemoteException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return WAITING;
    }

    @Override
    public void stopfind() {

    }

    OnPassListener.Stub listener=new OnPassListener.Stub(){

        @Override
        public void onCardPass(int i) throws RemoteException {
            setCardType(i);
//            rfReader.isExist();
//                    Log.d("cardtype222",""+type+" isexist "+rfReader.isExist());
            BytesValue responseData = new BytesValue();
//            rfReader.activate(CardType.PRO_CARD, responseData);
//            // Exchange APDU
//            byte[] cmd2={0x00, (byte) 0xA4,0x04,0x00,0x07, (byte) 0xD3,0x57,0x10,0x00,0x03,0x00,0x03};
//            ApduResponse data = rfReader.exchangeApdu(cmd2);
//            Log.d("RAPDU1", Hex.bytesToHexString(data.getData())+" | "+Hex.byteToHexString(data.getSW1())+" | "+Hex.byteToHexString(data.getSW2()));

            tempuid=rfReader.getCardSerialNo(responseData.getData());
            Log.d("  cardtype",""+getCardType());
            Log.d("gettempuid",""+tempuid);
            detectListener.onRFDetected(tempuid, mode);
        }

        @Override
        public void onFail(int i) throws RemoteException {

        }
    };


}
