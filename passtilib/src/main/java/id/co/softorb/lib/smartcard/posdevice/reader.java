package id.co.softorb.lib.smartcard.posdevice;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.RemoteException;
import android.util.Log;

//import com.cloudpos.DeviceException;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.apos.apos;
import id.co.softorb.lib.smartcard.morefun.morefun;
import id.co.softorb.lib.smartcard.pax.pax;
import id.co.softorb.lib.smartcard.wizar.wizarpos;

import static id.co.softorb.lib.helper.ErrorCode.*;

import static id.co.softorb.lib.smartcard.posdevice.params.DEV_APOS;
import static id.co.softorb.lib.smartcard.posdevice.params.DEV_MOREFUN;
import static id.co.softorb.lib.smartcard.posdevice.params.DEV_PAX;
import static id.co.softorb.lib.smartcard.posdevice.params.DEV_WIZAR;
import static id.co.softorb.lib.smartcard.posdevice.params.UNKNOWN;


/**
 * <h1>reader</h1>
 * class providing interface between device hardware service and PASSTI commands
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author  Vivo
 * @version 1.0
 * @since   2020-10-19
 */
public class reader {

    DeviceUtility deviceUtility;
    Context ctx;
    String TAG="PasstiMultiDevice";
    String tagclass;
    wizarpos wizarpos;
    pax paxpos;
    morefun morefunpos;
    apos apos;
    private POSListener detectListener;
    byte[] UID;
    int devicetype;

    public void SetDeviceType(int type)
    {
        devicetype=type;

    }
    public reader(Context ctx,POSListener callback)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        tagclass= this.getClass().getSimpleName();
        try {

            this.ctx = ctx;

            devicetype=UNKNOWN;


            detectListener=callback;
            wizarpos=new wizarpos(ctx);
            paxpos = new pax(ctx);
            morefunpos=new morefun(ctx);
            apos= new apos(ctx);

        }catch (Exception e)
        {
            Log.e(TAG,e.toString());
        }
    }
    public int CloseDevice()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        switch(devicetype)
        {
            case DEV_PAX:
                return paxpos.close();

            case DEV_WIZAR:
                return wizarpos.close();

            case DEV_MOREFUN:
                return morefunpos.close();
            case DEV_APOS:
                return apos.close();
        }
        return NOT_OK;
    }

    public Object getPinPad()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        switch(devicetype)
        {
            case DEV_PAX:
                return null;

            case DEV_WIZAR:
                return wizarpos.getPinPad();
        }
        return null;
    }

    public Object getLED()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        switch(devicetype)
        {
            case DEV_PAX:
                return null;

            case DEV_WIZAR:
                return wizarpos.getLED();
            case DEV_MOREFUN:
                return morefunpos.getlED();
            case DEV_APOS:
                return apos.getlED();
        }
        return null;
    }
    /**
     * method to set instance retrieved from SDK service binding process
     * @param ctx application context
     * @return Nothing
     */
    public int SetDeviceService(Context ctx,int devicetype)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        Log.d("devicetype",""+devicetype);
        this.devicetype=devicetype;
        int result;
        result=initPOSDevices();
        Log.d(TAG,"initPOSDevices result "+result);
        return result;
    }

    private int
    initPOSDevices()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=NOT_OK;
        Log.d("devicetype2",""+devicetype);
        switch(devicetype)
        {
            case DEV_WIZAR:
                result=wizarpos.init(detectListener,null);
                if(result!=OK) return result;
                result=wizarpos.open();//incluode open msr,contactless
                break;
            case DEV_PAX:
                result=paxpos.init(detectListener,null);
                if(result!=OK) return result;
                result=paxpos.open();

                break;

            case DEV_MOREFUN:
                result=morefunpos.init(detectListener,null);
//                result=OK;
                System.out.println("result1="+result);
                if(result!=OK) return result;
                result=morefunpos.open();
//                System.out.println("result2="+result);
                break;

            case DEV_APOS:
                result=apos.init(detectListener, null);
                if(result!=OK) return result;
                break;
        }

        System.out.println("result3="+result);
        return result;

    }
    /**
     * get contactless UID from received byte array during SDK service binding
     * @param data byte array retrieved during SDK service binding
     * @return contactless card UID
     */
    public byte[] parseUID(byte[] data)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        Log.d(TAG,"data "+ Hex.bytesToHexString(data));
        if(data==null)
            return null;
        UID= new byte[data.length];
        System.arraycopy(data,0,UID,0,data.length);
        return UID;

    }
    /**
     * send APDU command to contactless card
     * @param apdu APDU command
     * @return APDU response (data +SW1SW2)
     */
    public byte[] sendCTL(byte[]apdu)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        Log.d(TAG,"RDR-->CTL : "+ Hex.bytesToHexString(apdu));
        byte[] apduResult=null;
        switch(devicetype)
        {
            case DEV_PAX:

                apduResult=paxpos.sendCTL(apdu);
                break;
            case DEV_WIZAR:
                apduResult=wizarpos.sendCTL(apdu);

                break;
            case DEV_MOREFUN:
//                apduResult=wizarpos.sendCTL(apdu);
//                if(Hex.bytesToHexString(apdu).equals("904C000004")){
//                    morefunpos.sendCTL(Hex.hexStringToByteArray("00A4040008A000000571504805"));
//                    apduResult= morefunpos.sendCTL(apdu);
//                }else{
//                    apduResult= morefunpos.sendCTL(apdu);
//                }
                apduResult= morefunpos.sendCTL(apdu);
                Log.d(TAG,"apduresult : "+ Hex.bytesToHexString(apduResult));
                break;
            case DEV_APOS:
                apduResult= apos.sendCTL(apdu);
                Log.d(TAG,"apduresult : "+ Hex.bytesToHexString(apduResult));
                break;
        }
        Log.d(TAG,"RDR<--CTL : "+ Hex.bytesToHexString(apduResult));
        return apduResult;

    }

    public byte[] sendCTLString(byte[]apdu, String card)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        Log.d(TAG,"RDR-->CTL "+card+": "+ Hex.bytesToHexString(apdu));
        byte[] apduResult=null;
        switch(devicetype)
        {
            case DEV_PAX:

                apduResult=paxpos.sendCTL(apdu);
                break;
            case DEV_WIZAR:
                apduResult=wizarpos.sendCTL(apdu);

                break;
            case DEV_MOREFUN:
                apduResult= morefunpos.sendCTL(apdu);
                break;
        }
        Log.d(TAG,"RDR<--CTL "+card+" : "+ Hex.bytesToHexString(apduResult));
        return apduResult;

    }
    /**
     * receive device type supported by library
     * @return device code
     */
    public int DeviceType()
    {
        return devicetype;
    }
    /**
     * send APDU command to SAM card
     * @param slot target SAM slot
     * @param cmd APDU command
     * @return APDU response (data +SW1SW2)
     */
    public byte[] sendSAM(int slot,byte[]apdu)
    {
        //byte[] data=deviceUtility.sendSAM(slot,cmd);
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        Log.d(TAG,"RDR-->SAM : "+ Hex.bytesToHexString(apdu));
        byte[] apduResult=null;
        Log.d("sendsam devicetype", ""+devicetype);
        switch(devicetype)
        {
            case DEV_PAX:

                //apduResult=paxpos.sendCB(apdu);
                break;
            case DEV_WIZAR:
                apduResult=wizarpos.sendSAM(slot,apdu);

                break;

            case DEV_MOREFUN:
                apduResult=morefunpos.sendSAM(slot, apdu);
                break;
            case DEV_APOS:
                apduResult=apos.sendSAM(slot, apdu);
                break;
        }
        Log.d(TAG,"RDR<--SAM : "+ Hex.bytesToHexString(apduResult));
        return apduResult;

    }
    public byte[] sendContact(byte[]apdu)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] apduResult=null;

        Log.d(TAG,"RDR-->ICC : "+ Hex.bytesToHexString(apdu));
        switch(devicetype)
        {
            case DEV_PAX:

                apduResult=paxpos.sendCB(apdu);
                break;
            case DEV_WIZAR:
                apduResult=wizarpos.sendCB(apdu);

                break;
            case DEV_MOREFUN:
                apduResult=morefunpos.sendCB(apdu);
                break;
            case DEV_APOS:
                apduResult=apos.sendCB(apdu);
                break;

        }
        Log.d(TAG,"RDR<--ICC : "+ Hex.bytesToHexString(apduResult));

        return apduResult;
    }
    /**
     * get SDK library version
     * @return library version
     */
    public String LibVersion()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        String version="unknown";

        switch(devicetype)
        {
            case DEV_PAX:
                version = paxpos.LibVersion();
                break;
            case DEV_WIZAR:
                version = wizarpos.LibVersion();
                break;
        }



        return version;
    }

    /**
     * send APDU command to SAM card with expected length of response
     * @param slot target SAM slot
     * @param cmd APDU command
     * @return APDU response (data +SW1SW2)
     */
    public byte[] sendSAM_Le(int slot,byte[]cmd)
    {
        byte[] data=deviceUtility.sendSAM(slot,cmd);
        return data;
    }
    /**
     * get device serial number
     * @return device serial number
     */
    public byte[] SerialNumber()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        switch(devicetype)
        {
            case DEV_PAX:
                return paxpos.DeviceSN().getBytes();

            case DEV_WIZAR:
                return wizarpos.DeviceSN().getBytes();
            case DEV_MOREFUN:
                return morefunpos.DeviceSN().getBytes();
            case DEV_APOS:
                return apos.DeviceSN().getBytes();
        }
        return null;
    }
    /**
     * Detect contactless card

     * @return Nothing
     */
    public void FindRFCard(int mode)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        //detectListener =obj;
        switch(devicetype) {
            case DEV_WIZAR:
                wizarpos.FindCTL(mode);
                break;
            case DEV_PAX:
                paxpos.FindCTL(mode);

                break;
            case DEV_MOREFUN:
                morefunpos.FindCTL(mode);
                break;
            case DEV_APOS:
                apos.FindCTL(mode);
                break;
        }
    }
    public void FindMSR()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());


        switch(devicetype)
        {
            case DEV_PAX:

                paxpos.FindMSR();
                break;
            case DEV_WIZAR:
                wizarpos.FindMSR();

                break;
            case DEV_MOREFUN:
                morefunpos.FindMSR();
                break;
            case DEV_APOS:
                apos.FindMSR();
                break;

        }
    }
    public void ContactFindCard()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        switch(devicetype)
        {
            case DEV_PAX:

                 paxpos.FindCB();
                break;
            case DEV_WIZAR:
                wizarpos.FindCB();

                break;
            case DEV_MOREFUN:
                morefunpos.FindCB();
                break;
            case DEV_APOS:
                apos.FindCB();
                break;

        }
    }

    /**
     * Halt RF reader
     * @return Nothing
     */
    public void RFHalt()
    {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        new Thread(new Runnable() {
            @Override
            public void run() {

                switch(devicetype)
                {
                    case DEV_PAX:
                        paxpos.stopfind();
                        break;
                    case DEV_WIZAR:
                        wizarpos.stopfind();

                        break;
                    case DEV_MOREFUN:
                        morefunpos.stopfind();
                        break;
                    case DEV_APOS:
                        apos.stopfind();
                        break;

                }
            }}).start();

    }
   /* public int RF_Init()
    {
        return deviceUtility.initCTL();
    }*/
    /**
     * Find SAM card in targeted slot
     * @param type card type code based on PASSTI specification
     * @param slot targeted sam slot
     * @return Nothing
     */
    public int SAMFindCard(int type,int slot)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        String message = "";
        int result;
        switch(devicetype)
        {
            case DEV_PAX:
                result=paxpos.FindSAM(type,slot);
                break;
            case DEV_WIZAR:
                result=wizarpos.FindSAM(type,slot);

                break;
            case DEV_MOREFUN:
                result=morefunpos.FindSAM(type, slot);
                break;
            case DEV_APOS:
//                result=apos.FindSAM(type, slot);
                break;

        }

        /*
         * valid valud : 0 - contactcard
         * */
   /*     samreader =(SmartCardReaderDevice) POSTerminal.getInstance(ctx).getDevice("cloudpos.device.smartcardreader",slot);
        contact[slot].setReader(samreader);
        if(contact[slot].Open(samlistener,TimeConstants.FOREVER)!=OK)
        {

            return ERR_INITREADER;
        }
*/

        return OK;
    }

    /**
     * Set detected contactless card type based on device SDK specification
     * @param type card type code based on device SDK specification
     * @return Nothing
     */
    public void RFType(int type)
    {
        //deviceUtility.setCardType(type);
    }
    public int print_text(int fonttype,int alignment,String text,boolean newline) {
        switch(devicetype)
        {
            case DEV_WIZAR:
                return wizarpos.print_text( fonttype, alignment, text, newline);

            case DEV_PAX:
                return paxpos.print_text( fonttype, alignment, text, newline);
            case DEV_MOREFUN:
                return morefunpos.print_text(fonttype, alignment, text, newline);
            case DEV_APOS:
                return apos.print_text(fonttype, alignment, text, newline);
        }
        return NOT_OK;
    }


    public int print_bitmap(int alignment, Bitmap data)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        switch(devicetype)
        {
            case DEV_WIZAR:
                return wizarpos.printbitmap(alignment,data);
            case DEV_PAX:
                return paxpos.printbitmap(alignment,data);
            case DEV_MOREFUN:
                return morefunpos.printbitmap(alignment, data);
            case DEV_APOS:
                return apos.printbitmap(alignment, data);

        }
        return NOT_OK;

    }
    public int printer_cutpaper()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        switch(devicetype)
        {
            case DEV_WIZAR:
                return wizarpos.printercutpaper();
            case DEV_PAX:
                return paxpos.printercutpaper();
            case DEV_MOREFUN:
                return morefunpos.printercutpaper();

        }
        return NOT_OK;

    }
    public int printer_linefeed()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        switch(devicetype)
        {
            case DEV_WIZAR:
                return wizarpos.printerlinefeed();

            case DEV_PAX:
                return paxpos.printerlinefeed();
            case DEV_MOREFUN:
                return morefunpos.printerlinefeed();
            case DEV_APOS:
                return apos.printerlinefeed();
        }
        return NOT_OK;
    }



    public int printer_paperstatus()
    {



        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status=0;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.printerstatus();
                break;
            case DEV_PAX:
                status=  paxpos.printerstatus();
                break;
            case DEV_MOREFUN:
                status=morefunpos.printerstatus();
                break;
            case DEV_APOS:
                status=apos.printerstatus();
                break;
        }
        return status;

    }

    public String PinPad_SN() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        String status="";
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_SN();
                break;
            case DEV_PAX:

                break;
        }
        return status;

    }

/*
    public byte[] PinPad_GenerateRandom(int length) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status=null;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_GenerateRandom(length);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }
*/

    public int PinPad_SetPINLength(int min, int max) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status=NOT_OK;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_SetPINLength(min,max);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }


    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status=NOT_OK;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_UpdateUserKey(mkid,userkeyid,value);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }


    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value, byte[] checksum) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status=NOT_OK;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_UpdateUserKey(mkid,userkeyid,value,checksum);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }


    public byte[] PinPad_GetSessionKey(int mkid, int userkeyid, int algo) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status=null;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_GetSessionKey(mkid,userkeyid,algo);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }


    public byte[] PinPad_Encrypt(int mkid, int userkeyid, int algo, byte[] data) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status=null;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_Encrypt(mkid,userkeyid,algo,data);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }


    public byte[] PinPad_Calculatemac(int mkid, int userkeyid, int keyalgo, int macalgo, byte[] data) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status=null;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_Calculatemac(mkid,userkeyid,keyalgo,macalgo,data);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }


    public byte[] PinPad_Calculatedukptmac(byte[] data) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status=null;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_Calculatedukptmac(data);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }

    public int PinPad_Verifymac(int mkid, int keyid, byte[] data, byte[] mac) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status=ERR_CRC;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_Verifymac(mkid,keyid,data,mac);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }


    public int PinPad_async_pinblock(int mkid, int keyid, byte[] data) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status=ERR_NO_RESP;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_async_pinblock(mkid,keyid,data);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }


    public byte[] PinPad_sync_pinblock(int mkid, int keyid, byte[] data) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] status=null;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_sync_pinblock(mkid,keyid,data);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }

    public int PinPad_getMKStatus(int mkid) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status=ERR_NO_RESP;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_getMKStatus(mkid);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }


    public int PinPad_getSKStatus(int mkid, int skid) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status=ERR_NO_RESP;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_getSKStatus(mkid,skid);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }

    public int PinPad_getDUKPTStatus(int mkid, byte[] uniquekey) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status=ERR_NO_RESP;
        switch(devicetype)
        {
            case DEV_WIZAR:
                status= wizarpos.PinPad_getDUKPTStatus(mkid,uniquekey);
                break;
            case DEV_PAX:

                break;
        }
        return status;
    }

}
