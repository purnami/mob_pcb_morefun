package id.co.softorb.lib.smartcard.apos;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.usdk.apiservice.aidl.UDeviceService;

import java.lang.reflect.Method;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.DeviceFunction;

import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;

public class apos extends DeviceFunction {
    String tagclass= this.getClass().getSimpleName();
    Context ctx;
    private contactless ctlReader;
    private sam samReader;
    private static int result;
    boolean mBound = false;
    private printer posprinter;
    private led posled;
    private buzzer buzzer;
    private contact cbReader;
    private msr msrReader;

//    private POSTerminal posTerminal;
    private static final String USDK_ACTION_NAME = "com.usdk.apiservice";
    private static final String USDK_PACKAGE_NAME = "com.usdk.apiservice";
    private static final String EMV_LOG = "emvLog";
    private static final String COMMON_LOG = "commonLog";
    private UDeviceService deviceService;

    AsyncTask<String, String, String> sendTask;

    private AposViewModel viewModel;

    public void setDeviceService(UDeviceService deviceService) {
        this.deviceService = deviceService;
    }

    public apos(Context ctx) {
        this.ctx = ctx;
        ctlReader = new contactless();
        samReader=new sam();
        posprinter=new printer();
        posled=new led();
        buzzer=new buzzer();
        cbReader=new contact();
        msrReader=new msr();
//        bindSdkDeviceService();

//        Intent intent = new Intent();
//        intent.setAction(USDK_ACTION_NAME);
//        intent.setPackage(USDK_PACKAGE_NAME);
//
//        this.ctx.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }
    @Override
    public int FindMSR() {
        if(msrReader==null)
            return NOT_OK;
        msrReader.find();
        return WAITING;
    }

    @Override
    public int FindCTL(int mode) {
        ctlReader.setMode(mode);
        if(ctlReader==null)
            return NOT_OK;
        ctlReader.find();
        return WAITING;
    }

    @Override
    public int FindCB() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        if(cbReader==null)
            return NOT_OK;
        result=cbReader.find();


        return result;
    }

    @Override
    public int FindSAM(int type, int slot) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(samReader==null)
            return NOT_OK;
        int result=samReader.find();
        return result;
    }

    public Object getlED() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return posled;
    }


    @Override
    public byte[] sendCTL(byte[] apdu) {
//        ctlReader.find();
        return ctlReader.sendCTL(apdu);
    }

    @Override
    public byte[] sendCB(byte[] apdu) {
        return cbReader.sendCB(0,apdu);
    }

    @Override
    public byte[] sendSAM(int slot, byte[] apdu) {
        return samReader.sendCB(slot,apdu);
    }

    @Override
    public int printercutpaper() {
        return 0;
    }

    @Override
    public int printerlinefeed() {
        return posprinter.linefeed();
    }

    @Override
    public int printerstatus() {
        return 0;
    }

    @Override
    public int printbitmap(int alignment, Bitmap bitmap) {
        return posprinter.bitmap(alignment, bitmap);
    }

    @Override
    public int print_text(int fonttype, int alignment, String text, boolean newline) {
        return posprinter.text(fonttype,alignment,text,newline);
    }

    @Override
    public void setcardwaitingtime(int timeout) {

    }

    @Override
    public String PinPad_SN() {
        return null;
    }

    @Override
    public int PinPad_SetPINLength(int min, int max) {
        return 0;
    }

    @Override
    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value) {
        return 0;
    }

    @Override
    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value, byte[] checksum) {
        return 0;
    }

    @Override
    public byte[] PinPad_GetSessionKey(int mkid, int userkeyid, int algo) {
        return new byte[0];
    }

    @Override
    public byte[] PinPad_Encrypt(int mkid, int userkeyid, int algo, byte[] data) {
        return new byte[0];
    }

    @Override
    public byte[] PinPad_Calculatemac(int mkid, int userkeyid, int keyalgo, int macalgo, byte[] data) {
        return new byte[0];
    }

    @Override
    public byte[] PinPad_Calculatedukptmac(byte[] data) {
        return new byte[0];
    }

    @Override
    public int PinPad_Verifymac(int mkid, int keyid, byte[] data, byte[] mac) {
        return 0;
    }

    @Override
    public int PinPad_async_pinblock(int mkid, int keyid, byte[] data) {
        return 0;
    }

    @Override
    public byte[] PinPad_sync_pinblock(int mkid, int keyid, byte[] data) {
        return new byte[0];
    }

    @Override
    public int PinPad_getMKStatus(int mkid) {
        return 0;
    }

    @Override
    public int PinPad_getSKStatus(int mkid, int skid) {
        return 0;
    }

    @Override
    public int PinPad_getDUKPTStatus(int mkid, byte[] uniquekey) {
        return 0;
    }

    @Override
    public String DeviceSN() {
        Log.d("Apos SerialNo", getSerialNumber());
        return getSerialNumber();
    }
    private static String getSerialNumber(){
        String serialNumber;
        try{
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get= c.getMethod("get", String.class);
            serialNumber=(String) get.invoke(c, "gsm.sn1");
            if(serialNumber.equals(""))
                serialNumber=(String) get.invoke(c, "ril.serialnumber");
            if(serialNumber.equals(""))
                serialNumber=(String) get.invoke(c, "ro.serialno");
            if(serialNumber.equals(""))
                serialNumber=(String) get.invoke(c, "sys.serialnumber");
            if(serialNumber.equals(""))
                serialNumber= Build.SERIAL;
            if(serialNumber.equals(Build.UNKNOWN))
                serialNumber=null;
        } catch (Exception e) {
            e.printStackTrace();
            serialNumber=null;
        }
        return serialNumber;
    }
    @Override
    public String LibVersion() {
        return null;
    }

    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        viewModel=new AposViewModel(this.ctx);
//        viewModel.setContext(this.ctx);
        viewModel.createService();

        final Boolean[] boundservis = new Boolean[1];
        Log.d("viewmodelboundservice3", ""+viewModel.getmServiceBound2());
        viewModel.getmServiceBound().observeForever(bounds->{
            Log.d("viewmodelboundservice", ""+bounds);
            boundservis[0] =bounds;
            Log.d("viewmodelboundservice2", ""+boundservis[0]);
        });

        viewModel.getmService().observeForever(service->{
            deviceService = UDeviceService.Stub.asInterface(service);
            try { deviceService.register(null, new Binder()); } catch (RemoteException e) { e.printStackTrace(); }
            result=ctlReader.init(listener,deviceService);
            Log.d("result ctl", ""+result);
        });

//        result=ctlReader.init(listener,deviceService);
//        Log.d("result ctl", ""+result);
//        if(result!=OK)
//            return result;

        Boolean a = getValue(viewModel.getmServiceBound());
        Log.d("getvalue", ""+a);

//        bindSdkDeviceService();
//
//        new android.os.Handler().postDelayed( new Runnable() { public void run() {
//            try {
//                Log.d(TAG, "SDK deviceService initiated version 123:" + deviceService.getVersion() + ".");
//                if(deviceService!=null){
//                    result=ctlReader.init(listener,deviceService);
//                    Log.d("result ctl", ""+result);
//                    result=samReader.init(listener, deviceService);
//                    Log.d("result sam", ""+result);
//                    result=posprinter.init(listener, deviceService);
//                    Log.d("result printer", ""+result);
//                    result=posled.init(listener, deviceService);
//                    Log.d("result led", ""+result);
//                    result=buzzer.init(listener, deviceService);
//                    Log.d("result buzzer", ""+result);
//                    result=cbReader.init(listener, deviceService);
//                    Log.d("result cbreader", ""+result);
//                    result=msrReader.init(listener, deviceService);
//                    Log.d("result msrReader", ""+result);
//                }
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            }
//
//        }}, 100);
        Log.d("resultttt", ""+result);
        return result;
    }

    @Override
    public int open() {
        return 0;
    }

    @Override
    public int close() {
        return 0;
    }

    @Override
    public int find() {
        return 0;
    }

    @Override
    public void stopfind() {
        ctlReader.stopfind();
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "SDK service disconnected.");
            deviceService = null;
            mBound=false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "SDK service connected.");

            try {
                deviceService = UDeviceService.Stub.asInterface(service);
                deviceService.register(null, new Binder());
                Bundle bundle = new Bundle();
                bundle.putBoolean(EMV_LOG, true);
                bundle.putBoolean(COMMON_LOG, true);
                deviceService.debugLog(bundle);
//                sendTask = new InBack();
//                sendTask.execute(new String[]{""});
                Log.d(TAG, "SDK deviceService initiated version:" + deviceService.getVersion() + ".");
                setDeviceService(deviceService);
            } catch (RemoteException e) {
                throw new RuntimeException("SDK deviceService initiating failed.", e);
            }

        }

    };

    private void bindSdkDeviceService() {
        Intent intent = new Intent();
        intent.setAction(USDK_ACTION_NAME);
        intent.setPackage(USDK_PACKAGE_NAME);
        Log.d(TAG, "binding sdk device service...");
        boolean flag = this.ctx.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
//        new android.os.Handler().postDelayed( new Runnable() { public void run() {
//            if (!flag) {
//                Log.d(TAG, "SDK service binding failed.");
//                return;
//            }else{
//                try {
//                    Log.d(TAG, "SDK deviceService initiated version abcd:" + deviceService.getVersion() + ".");
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
//                Log.d(TAG, "SDK service binding successfully.");
////            this.ctx.unbindService(serviceConnection);
//            }
//        }}, 100);


    }


//    private final class InBack extends AsyncTask<String, String, String> {
//
//        @Override
//        protected String doInBackground(String... strings) {
//            Intent intent = new Intent();
//            intent.setAction(USDK_ACTION_NAME);
//            intent.setPackage(USDK_PACKAGE_NAME);
//
//            ctx.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
////            Log.d("mServiceBound", ""+mServiceBound);
//
//            try {
//                Log.d(TAG, "SDK deviceService initiated version223345:" + deviceService.getVersion() + ".");
//                result=ctlReader.init(listener,deviceService);
//                Log.d("result ctl", ""+result);
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            }
//
//            return "Executed";
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
////            TextView txt = (TextView) findViewById(R.id.output);
////            txt.setText("Executed"); // txt.setText(result);
//            // You might want to change "executed" for the returned string
//            // passed into onPostExecute(), but that is up to you
//        }
//    }

    public class AposViewModel extends ViewModel {
        public Context context;
        private MutableLiveData<Boolean> mServiceBound=new MutableLiveData<>();
        private MutableLiveData<IBinder> mService=new MutableLiveData<>();

        public LiveData<Boolean> getmServiceBound() {
            return mServiceBound;
        }
        public Boolean getmServiceBound2() {
            final Boolean[] a = new Boolean[1];
            mServiceBound.observeForever(bounds->{
                a[0] =bounds;
                Log.d("mServiceBound11", ""+bounds+"  "+ a[0]);
            });
            Log.d("mServiceBound112", ""+ a[0]);
            return a[0];
        }

        public LiveData<IBinder> getmService() {
            return mService;
        }

        public AposViewModel(Context context){
            this.context=context;
        }

//        public void setContext(Context context) {
//            this.context = context;
//        }

        public final void createService(){
            Intent intent = new Intent();
            intent.setAction(USDK_ACTION_NAME);
            intent.setPackage(USDK_PACKAGE_NAME);

            this.context.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        }

        private ServiceConnection mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                mServiceBound.setValue(false);
            }
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mServiceBound.setValue(true);
                Log.d("mServiceBound10", ""+mServiceBound.getValue());
//                Log.d("mServiceBound11", ""+mServiceBound.);
                mService.setValue(service);
                deviceService = UDeviceService.Stub.asInterface(service);
                try {
//                    deviceService.register(null, new Binder());
//                    Bundle bundle = new Bundle();
//                    bundle.putBoolean(EMV_LOG, true);
//                    bundle.putBoolean(COMMON_LOG, true);
//                    deviceService.debugLog(bundle);
                    Log.d(TAG, "SDK deviceService initiated versionxxx:" + deviceService.getVersion() + ".");
                    setDeviceService(deviceService);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
//            runningTask = new LongOperation();
//            runningTask.execute(new String[]{""});

//                mServiceBound.observeForever(bounds->{
//                    Log.d("mServiceBound11", ""+bounds);
//                });
            }
        };

    }

    public static <T> T getValue(LiveData<T> liveData) {
        final Object[] objects = new Object[1];
        final CountDownLatch latch = new CountDownLatch(1);

        Observer<T> observer = new Observer<T>() {
            @Override
            public void onChanged(@Nullable Object o) {
                objects[0] = o;
                latch.countDown();
                liveData.removeObserver(this);
                Log.d("objectt", ""+objects[0]);
            }
        };
        liveData.observeForever(observer);
        try {
            latch.await(5, TimeUnit.SECONDS);
            Log.d("objectt2", ""+objects[0]);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d("objectt3", ""+objects[0]);
        return (T) objects[0];
    }
}
