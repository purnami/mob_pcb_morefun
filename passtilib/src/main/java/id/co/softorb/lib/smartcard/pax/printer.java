package id.co.softorb.lib.smartcard.pax;

import android.graphics.Bitmap;
import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.POSTerminal;
import com.cloudpos.printer.Format;
import com.cloudpos.printer.PrinterDevice;
import com.pax.dal.IDAL;
import com.pax.dal.IMag;
import com.pax.dal.IPrinter;
import com.pax.dal.exceptions.PrinterDevException;

import id.co.softorb.lib.helper.printerparam;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.POSPrinter;

import static id.co.softorb.lib.helper.ErrorCode.ERR_INITPRINTER;
import static id.co.softorb.lib.helper.ErrorCode.ERR_OPENPRINTER;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;

public class printer extends POSPrinter
{
    String TAG= "PAX";
    String tagclass= this.getClass().getSimpleName();
    private IPrinter printer;
    private IDAL dal;

    private POSListener detectListener;
    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;


        this.detectListener=listener;
        dal = (IDAL)devInstance;
        if(dal==null)
            return NOT_OK;
        printer = dal.getPrinter();


        if (printer != null) {
            try {
                printer.init();



            } catch (PrinterDevException e) {
                e.printStackTrace();
                Log.e(TAG,e.getMessage());
                return ERR_OPENPRINTER;
            }
            return OK;
        } else
            return ERR_INITPRINTER;




    }
/*
        @Override
        public int init() {
            return ERR_INITPRINTER;
        }*/

    @Override
    public int open() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
       /*try {
            printer.start();



        } catch (PrinterDevException e) {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());
            return ERR_OPENPRINTER;
        }*/
        return OK;
    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        /*try {
            printer.();



        } catch (PrinterDevException e)
        {
            Log.e(TAG,e.toString());
            return NOT_OK;
        }*/
        return OK;
    }

    @Override
    public int find() {
        return OK;
    }

    @Override
    public void stopfind() {

    }


    public int text(int fonttype,int alignment,String text,boolean newline)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        /*Format defaultformat = new Format();


        switch(fonttype)
        {
            case printerparam.FONT_BOLD:
                defaultformat.setParameter(Format.FORMAT_FONT_BOLD, Format.FORMAT_FONT_VAL_TRUE);
                defaultformat.setParameter(Format.FORMAT_FONT_ITALIC, Format.FORMAT_FONT_VAL_FALSE);
                break;
            case printerparam.FONT_ITALIC:
                defaultformat.setParameter(Format.FORMAT_FONT_ITALIC, Format.FORMAT_FONT_VAL_TRUE);
                defaultformat.setParameter(Format.FORMAT_FONT_BOLD, Format.FORMAT_FONT_VAL_FALSE);
                break;

            default:
                defaultformat.setParameter(Format.FORMAT_FONT_ITALIC, Format.FORMAT_FONT_VAL_FALSE);
                defaultformat.setParameter(Format.FORMAT_FONT_BOLD, Format.FORMAT_FONT_VAL_FALSE);
                break;
        }
        switch(alignment)
        {
            case printerparam.ALIGN_LEFT:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                break;
            case printerparam.ALIGN_CENTER:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                break;
            case printerparam.ALIGN_RIGHT:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_RIGHT);
                break;
            default:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                break;
        }*/
        try {
            printer.init();
            if (newline) {
                printer.printStr(text,null);
            } else {
                printer.printStr(text,null);
            }
            printer.start();
        }
        catch (PrinterDevException e)
        {
            Log.e(TAG,e.toString());
            return NOT_OK;
        }
        return OK;
    }

    @Override
    public int cutpaper() {
        return NOT_OK;
    }

    public int linefeed()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmd= {(byte)0x0A};
        try{
           // printer.step(10);
            printer.init();
        }
        catch (PrinterDevException e)
        {
            Log.e(TAG,e.toString());
        }
        return OK;
    }

    @Override
    public int status() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int status;
        String strStatus;
        try {
            status=printer.getStatus();
            strStatus=statusCode2Str(status);
            Log.e(TAG,"printer "+strStatus);
            return status;

    } catch (PrinterDevException e) {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());
            return NOT_OK;
        }

    }

    public int bitmap(int alignment, Bitmap bitmap)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        /*Format defaultformat = new Format();
        switch(alignment)
        {
            case printerparam.ALIGN_LEFT:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                break;
            case printerparam.ALIGN_CENTER:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                break;
            case printerparam.ALIGN_RIGHT:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_RIGHT);
                break;
            default:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                break;
        }*/
        try {
            printer.init();
            printer.printBitmap(bitmap);
            printer.start();

        }
        catch (PrinterDevException e)
        {
            Log.e(TAG,e.toString());
            return NOT_OK;
        }
        return OK;
    }
    public String statusCode2Str(int status) {
        String res = "";
        switch (status) {
            case 0:
                res = "Success ";
                break;
            case 1:
                res = "Printer is busy ";
                break;
            case 2:
                res = "Out of paper ";
                break;
            case 3:
                res = "The format of print data packet error ";
                break;
            case 4:
                res = "Printer malfunctions ";
                break;
            case 8:
                res = "Printer over heats ";
                break;
            case 9:
                res = "Printer voltage is too low";
                break;
            case 240:
                res = "Printing is unfinished ";
                break;
            case 252:
                res = " The printer has not installed font library ";
                break;
            case 254:
                res = "Data package is too long ";
                break;
            default:
                break;
        }
        return res;
    }

}