package id.co.softorb.lib.smartcard.apos;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.usdk.apiservice.aidl.UDeviceService;
import com.usdk.apiservice.aidl.icreader.DriverID;
import com.usdk.apiservice.aidl.icreader.ICError;
import com.usdk.apiservice.aidl.icreader.PowerMode;
import com.usdk.apiservice.aidl.icreader.UPSamReader;
import com.usdk.apiservice.aidl.icreader.Voltage;
import com.usdk.apiservice.aidl.printer.AlignMode;
import com.usdk.apiservice.aidl.printer.OnPrintListener;
import com.usdk.apiservice.aidl.printer.PrinterError;
import com.usdk.apiservice.aidl.printer.UPrinter;

import java.io.ByteArrayOutputStream;

import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.POSPrinter;

import static id.co.softorb.lib.helper.ErrorCode.ERR_INITPRINTER;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.SAM_ERR_RELEASEFAIL;

public class printer extends POSPrinter {
    String tagclass= this.getClass().getSimpleName();
    private UDeviceService deviceService;
    private UPrinter printer;
    @Override
    public int cutpaper() {
        try {
            printer.autoCutPaper();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int linefeed() {
        try {
            printer.feedLine(4);
            printer.startPrint(listener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int status() {
        return 0;
    }

    @Override
    public int bitmap(int alignment, Bitmap bitmap) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] image=bitmap2bytearray(bitmap);
        try {
            switch (alignment){
                case 4: alignment=0; break;
                case 5: alignment=1; break;
                case 6: alignment=2; break;
            };
            printer.addImage(alignment,image);
            printer.feedLine(5);
            printer.startPrint(listener);
//            printer.cutPaper();
//            printer.autoCutPaper();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private byte[] bitmap2bytearray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    public int text(int fonttype, int alignment, String text, boolean newline) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            switch (alignment){
                case 4: alignment=0; break;
                case 5: alignment=1; break;
                case 6: alignment=2; break;
            };
            printer.addText(alignment, text);
//            printer.feedLine(3);
            printer.startPrint(listener);
            printer.cutPaper();
            printer.autoCutPaper();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int init(POSListener detectListener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            deviceService = (UDeviceService) devInstance;
            Log.d("device service",""+deviceService.getVersion());
            printer=(UPrinter) UPrinter.Stub.asInterface(deviceService.getPrinter());
            int ret = printer.getStatus();
            Log.d("printer status", ""+ret);
            if (ret == PrinterError.SUCCESS) {
                Log.d("result init printer","ok");
                return OK;

            }
//            else
//                return ERR_INITPRINTER;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return ERR_INITPRINTER;
    }

    @Override
    public int open() {
        return 0;
    }

    @Override
    public int close() {
        return 0;
    }

    @Override
    public int find() {
        return 0;
    }

    @Override
    public void stopfind() {

    }

    OnPrintListener.Stub listener=new OnPrintListener.Stub() {
        @Override
        public void onFinish() throws RemoteException {
            Log.d("print", "----- onFinish -----");
        }

        @Override
        public void onError(int i) throws RemoteException {
            Log.d("print", "----- onError ----");
        }
    };
}
