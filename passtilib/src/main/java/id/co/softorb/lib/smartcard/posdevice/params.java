package id.co.softorb.lib.smartcard.posdevice;

public class params {


    public final static byte NORMAL=0; /*transaction run in normal*/
    public final static byte REPURCHASE=1; /*lost contact during purchase process*/
    public final static byte REWRITEPARTNERDATA =2; /*lost contact during writing partner data*/


    public final static byte ctype_unknown=0;
    public final static byte ctype_luminos= 1;
    public  final static byte ctype_mdr= 2;
    public final static byte ctype_bri= 3;
    public final static byte ctype_bni= 4;
    public final static byte ctype_bca= 5;
    public final static byte ctype_dki= 6;
    public final static byte ctype_nobu= 7;
    public final static byte ctype_mega= 8;



    public final static byte UNKNOWN=0;
    public final static byte DEV_JELLIES=2;
    public final static byte DEV_APOS=3;
    public final static byte DEV_SUNMI=7;
    public final static byte DEV_NEWLAND=10;
    public final static byte DEV_WIZAR=9;
    public final static byte DEV_PAX=10;
    public final static byte DEV_MOREFUN=11;


    public byte cardtype;
    public byte prevcardtype;
    public int deductAction;//describe deduct action : NORMAL,REPURCHASE,REWRITEPARTNERDATA


    public byte[] prevCardNo;
    public byte[] prevUID;
    public boolean fSaveSaldo;
    public byte[] prevAmt;

    public byte[] prevTRH;
    public long LastlBalanceKartu;
    public byte[] prevBalance;
    public byte[] ExpctBalance;
    public byte[] baAmount;


    public params()
    {
        cardtype=0;
        prevcardtype=0;
        deductAction=NORMAL;//describe deduct action : NORMAL,REPURCHASE,REWRITEPARTNERDATA


        prevCardNo=new byte[8];
        prevUID=new byte[7];
        fSaveSaldo=false;
        prevAmt=new byte[4];
        prevTRH=new byte[8];
        LastlBalanceKartu=0;
        prevBalance=new byte[4];
        ExpctBalance=new byte[4];
        baAmount=new byte[4];

    }

}
