package id.co.softorb.lib.smartcard.wizar;

import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.POSTerminal;
import com.cloudpos.led.LEDDevice;


import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.LED;

import static id.co.softorb.lib.helper.ErrorCode.*;

public class led extends LED {
    private LEDDevice device = null;
    String tagclass= this.getClass().getSimpleName();

    private POSTerminal posTerminal;

    @Override
    public int blink() {
        int result =ERR_BLINKLED;
        try {
            device.startBlink(100, 100, 10);
            return OK;
        } catch (DeviceException e) {
            e.printStackTrace();
        }
        return result;
    }

    int getLogicalID(int color)
    {
        switch(color)
        {
            case BLUE: return LEDDevice.ID_BLUE;
            case YELLOW: return LEDDevice.ID_YELLOW;
            case GREEN: return LEDDevice.ID_GREEN;
            case RED: return LEDDevice.ID_RED;
            default:
                return LEDDevice.ID_RED;
        }
    }
    @Override
    public int open(int color) {
        int result =ERR_OPENLED;
        selectedColor=getLogicalID(color);
        try {
            device.open(selectedColor);
            result=OK;
        } catch (DeviceException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int on() {
        int result =ERR_TURNONLED;

        try {
            device.turnOn();
            result=OK;
        } catch (DeviceException e) {
            e.printStackTrace();
        }
        return result;

    }

    @Override
    public int off() {
        int result =ERR_TURNOFFLED;

        try {
            device.turnOff();
            result=OK;
        } catch (DeviceException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean ison() {
        int status ;


        try {
            status = device.getStatus();
            if(status==LEDDevice.STATUS_ON)
                return true;
            if(status==LEDDevice.STATUS_OFF)
                return true;
        } catch (DeviceException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public int init(POSListener detectListener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=ERR_INITLED;

        try {
        posTerminal=(POSTerminal) devInstance;
        device =(LEDDevice) posTerminal.getDevice("cloudpos.device.led");
            if (device != null) {

                return OK;
            } else
                return result;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());

        }

        return result;
    }

    @Override
    public int open() {
        return ERR_OPENLED;
    }

    @Override
    public int close() {
        int result =ERR_CLOSELED;

        try {
            device.close();
            result=OK;
        } catch (DeviceException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int find() {
        return OK;
    }

    @Override
    public void stopfind() {

    }
}
