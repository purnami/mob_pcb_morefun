package id.co.softorb.lib.smartcard.morefun;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.Gravity;

import com.morefun.yapi.device.printer.FeedUnit;
import com.morefun.yapi.device.printer.FontFamily;
import com.morefun.yapi.device.printer.MulPrintStrEntity;
import com.morefun.yapi.device.printer.MultipleAppPrinter;
import com.morefun.yapi.device.printer.OnPrintListener;
import com.morefun.yapi.device.printer.Printer;
import com.morefun.yapi.device.printer.PrinterConfig;
import com.morefun.yapi.engine.DeviceServiceEngine;
import com.usdk.apiservice.aidl.UDeviceService;
import com.usdk.apiservice.aidl.printer.PrinterError;
import com.usdk.apiservice.aidl.printer.UPrinter;

import java.util.ArrayList;
import java.util.List;

import id.co.softorb.lib.helper.printerparam;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.POSPrinter;

import static id.co.softorb.lib.helper.ErrorCode.ERR_INITPRINTER;
import static id.co.softorb.lib.helper.ErrorCode.OK;

public class printer extends POSPrinter {
    String tagclass= this.getClass().getSimpleName();
    private DeviceServiceEngine deviceService;
    private MultipleAppPrinter mulPrinter;
    private Printer printer;

    public static final int CENTER = 17;
    public static final int LEFT = 3;
    public static final int RIGHT = 5;
    @Override
    public int cutpaper() {
        //Kertas tidak mau terpotong
        try {
            printer.cutPaper();
            printer.startPrint(listener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int linefeed() {
        try {
            printer.feedPaper(5, 1);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return OK;
    }

    @Override
    public int status() {
        int status = 0;
        try {
            status=printer.getStatus();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public int bitmap(int alignment, Bitmap bitmap) {
        try {
            Log.i(TAG, tagclass+"."+new Throwable()
                    .getStackTrace()[0]
                    .getMethodName());
//            printer.appendImage(bitmap);
//            printer.feedPaper(3,);
//            printer.startPrint(listener);
            mulPrinter=deviceService.getMultipleAppPrinter();
            Bundle config = new Bundle();
            List<MulPrintStrEntity> list = new ArrayList<>();
            MulPrintStrEntity entity = new MulPrintStrEntity();
//            entity.setFontsize(FontFamily.BIG);
            entity.setText(" ");
            entity.setYspace(60);

            if(alignment==5){
                entity.setGravity(Gravity.CENTER);
            }else if(alignment==4){
                entity.setGravity(Gravity.START);
            }else if(alignment==6){
                Log.d("masuk sini end", "hh");
                entity.setGravity(Gravity.END);
            }

//            entity.setMarginX(50);
            entity.setBitmap(bitmap);

            list.add(entity);
            mulPrinter.printStr(list,listener,config);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int text(int fonttype, int alignment, String text, boolean newline) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        try {
            mulPrinter=deviceService.getMultipleAppPrinter();
            Bundle config = new Bundle();
            List<MulPrintStrEntity> list = new ArrayList<>();
            MulPrintStrEntity entity = new MulPrintStrEntity();
//            entity.setBitmap(bitmap);
            entity.setFontsize(FontFamily.MIDDLE);
            entity.setText(text);
            entity.setYspace(30);

            if(alignment==5){
                entity.setGravity(Gravity.CENTER);
            }else if(alignment==4){
                entity.setGravity(Gravity.START);
            }else if(alignment==6){
                entity.setGravity(Gravity.END);
            }

            if(fonttype== printerparam.FONT_BOLD){
                entity.setIsBold(0);
            }
            list.add(entity);
            mulPrinter.printStr(list,listener,config);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int init(POSListener detectListener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            deviceService = (DeviceServiceEngine) devInstance;
            mulPrinter=deviceService.getMultipleAppPrinter();
            printer=deviceService.getPrinter();

            if (mulPrinter!=null) {
                Log.d("result init printer","ok");
                return OK;

            }
            
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return ERR_INITPRINTER;
    }

    @Override
    public int open() {
        //Tidak ada fungsi open()
        return OK;
    }

    @Override
    public int close() {
        //Tidak ada fungsi close()
        return OK;
    }

    @Override
    public int find() {
        //Tidak ada fungsi find()
        return OK;
    }

    @Override
    public void stopfind() {
        //Tidak ada fungsi stopfind()
    }

    OnPrintListener.Stub listener=new OnPrintListener.Stub() {
        @Override
        public void onPrintResult(int i) throws RemoteException {
            Log.d("print", "----- onFinish -----"+i);
        }
    };
}
