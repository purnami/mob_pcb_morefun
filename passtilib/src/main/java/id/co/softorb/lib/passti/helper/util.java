
package id.co.softorb.lib.passti.helper;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;




public class util {
	public static String BinaryDate_to_Date(String binaryDateTime){
		String DateTime;
		long iyear, imonth, iday;
		long iHour, iMinute, iSecond;
		
		iyear= Integer.parseInt(binaryDateTime.substring(0, 7), 2)+2000;
		imonth= Integer.parseInt(binaryDateTime.substring(7, 11), 2);
		iday= Integer.parseInt(binaryDateTime.substring(11, 16), 2);
		iHour= Integer.parseInt(binaryDateTime.substring(16, 21), 2);
		iMinute= Integer.parseInt(binaryDateTime.substring(21, 27), 2);
		iSecond= Integer.parseInt(binaryDateTime.substring(27, 33), 2);
	
		DateTime= String.format("%02d/%02d/%d %02d:%02d:%02d", iday, imonth, iyear, iHour, iMinute, iSecond);
		
		return DateTime;
	}
	
	public static String Date_to_BinaryDate(String DateTime){
		String binDateTime;
		int iyear, imonth, iday;
		int iHour, iMinute, iSecond;
		
		iyear= Integer.parseInt(DateTime.substring(0, 2));
		imonth= Integer.parseInt(DateTime.substring(2, 4));
		iday= Integer.parseInt(DateTime.substring(4, 6));
		iHour= Integer.parseInt(DateTime.substring(6, 8));
		iMinute= Integer.parseInt(DateTime.substring(8, 10));
		iSecond= Integer.parseInt(DateTime.substring(10, 12));
		
		binDateTime= String.format("%7s", Integer.toBinaryString(iyear)).replaceAll(" ", "0")+
				String.format("%4s", Integer.toBinaryString(imonth)).replaceAll(" ", "0")+
				String.format("%5s", Integer.toBinaryString(iday)).replaceAll(" ", "0")+
				String.format("%5s", Integer.toBinaryString(iHour)).replaceAll(" ", "0")+
				String.format("%6s", Integer.toBinaryString(iMinute)).replaceAll(" ", "0")+
				String.format("%6s", Integer.toBinaryString(iSecond)).replaceAll(" ", "0")+
				"000000000000000";
				
		return binDateTime;
	}	

	public static String Datenow(String dateFormat) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(cal.getTime());
	}
	public static byte[] bcd_ddmmyyhhmmss_to_bcd_ddmmyyyyhhmmss(byte[] data)
	{
		byte[] output=new byte[7];
		output[0]=data[0];
		output[1]=data[1];
		output[2]=0x20;
		output[3]=data[2];
		output[4]=data[3];
		output[5]=data[4];
		output[6]=data[5];
		return output;
	}
	public static byte[] bcd_ddmmyyyyhhmmss2hex_yymmddhhmmss(String in)
	{
		byte[] out = new byte[6];
		//Log.e(TAG,"in "+in);
		byte[] ddmmyyyyhhmmss =ByteUtils.convertASCIIToBin(in);
		int iValue;

		//Log.e(TAG,"ddmmyyyyhhmmss "+Hex.bytesToHexString(ddmmyyyyhhmmss));

		out[0]= ddmmyyyyhhmmss[3];
		out[1]=ddmmyyyyhhmmss[1];
		out[2]=ddmmyyyyhhmmss[0];
		out[3]=ddmmyyyyhhmmss[4];
		out[4]=ddmmyyyyhhmmss[5];
		out[5]=ddmmyyyyhhmmss[6];
		//Log.e(TAG,"out "+Hex.bytesToHexString(out));
		return out;

	}


	public static long julianSec(int baseyear,byte[] DateTime)
	{
		int i = 0;
		long val = 0;
		int Feb = 0;
		int yearLocal;
		int monthLocal;
		int dayLocal;
		int hourLocal = 0;
		int minuteLocal = 0;
		int secLocal = 0;

//ddmmyyyyhhnnss
//convert BCD to decimal
//dayLocal = (((getDateTime[0] & 0xF0) >> 4) * 10) + (getDateTime[0] & 0x0F);
//incoming datetime format yymmddhhmmss
//printf("Device Time : ");printhex(getDateTime,6);

		//Log.d(TAG,"DateTime "+Hex.bytesToHexString(DateTime));
		dayLocal = DateTime[2];
		monthLocal = DateTime[1];
		yearLocal = 2000+ DateTime[0];
		hourLocal = DateTime[3];
		minuteLocal = DateTime[4];
		secLocal = DateTime[5];
		/*Log.i(TAG,"dayLocal "+dayLocal);
		Log.i(TAG,"monthLocal "+monthLocal);
		Log.i(TAG,"yearLocal "+yearLocal);
		Log.i(TAG,"hourLocal "+hourLocal);
		Log.i(TAG,"minuteLocal "+minuteLocal);
		Log.i(TAG,"secLocal "+secLocal);*/


//printf("dd/mm/yyyy = %2d/%2d/%4d hh:nn:ss = %2d:%2d:%2d \r\n", dayLocal,monthLocal,yearLocal,hourLocal,minuteLocal,secLocal);


		for( i = baseyear ;i<=yearLocal - 1;i++)		//i = based tahun
		{
			if (i % 4 == 0) val = val + 366;
			else val = val + 365;
		}

		if (yearLocal % 4 == 0) Feb = 29;
		else Feb = 28;

		for( i = 1 ; i <= monthLocal - 1;i++)		//i = based bulan
		{
			switch(i)
			{
				case 1 : val += 31; break;
				case 3 : val += 31;break;
				case 5 : val += 31;break;
				case 7 : val += 31;break;
				case 8 : val += 31;break;
				case 10: val += 31;break;
				case 12: val += 31;break;
				case 4 : val += 30;break;
				case 6 : val += 30;break;
				case 9 : val += 30;break;
				case 11: val += 30;break;
				case 2 : val += Feb;break;
			}
		}

		val += dayLocal-1;
		val = val * 24;
		val += hourLocal;

		val = val * 60;
		val += minuteLocal;

		val = val * 60;
		val += secLocal;


		return val;
	}


	public static long calcJulianSecondNow(String baseYYYYMMDD) {
		Calendar cal = Calendar.getInstance();
		Calendar calStart = Calendar.getInstance();
		int iyear = Integer.valueOf(baseYYYYMMDD.substring(0, 4));
		int imonth = Integer.valueOf(baseYYYYMMDD.substring(4, 6));
		int iday = Integer.valueOf(baseYYYYMMDD.substring(6, 8));
		calStart.set(iyear, imonth-1, iday,0,0,0);			
		return (cal.getTimeInMillis()-calStart.getTimeInMillis())/1000;
	}
	
	public static String convJulianSecond_to_Date2(String baseYYYYMMDD, long lTimesecond){
		Calendar cal = Calendar.getInstance();
		Calendar calStart = Calendar.getInstance();
		int iyear= Integer.valueOf(baseYYYYMMDD.substring(0, 4));
		int imonth= Integer.valueOf(baseYYYYMMDD.substring(4, 6));
		int iday= Integer.valueOf(baseYYYYMMDD.substring(6, 8));
		calStart.set(iyear, imonth-1, iday,0,0,0);		
		cal.setTimeInMillis(calStart.getTimeInMillis()+lTimesecond*1000);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		return sdf.format(cal.getTime());
	}
	
	public static String[][] HistSort(String[][] Data, int DateColumn, int DataRow)
			throws java.text.ParseException {
		String[] Temp = new String[Data[0].length];
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		DateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		for (int i = 1; i < DataRow; i++) {
			for (int j = 1; j < DataRow-1; j++) {	
				
				if (df.parse(Data[j][DateColumn]).compareTo(df2.parse(Data[j + 1][DateColumn]))< 0) {
					Temp = Data[j + 1];
					Data[j + 1] = Data[j];
					Data[j] = Temp;
				}
			}
		}

		return Data;
	}
	
	public static String FormatCardNumber(String RawData, int digit)
	{
		StringBuilder edit;
		int counter=0;
		
		if(RawData!=null)
		{
		edit = new StringBuilder(RawData);
		for(counter=digit;counter<RawData.length();counter+=digit)
		{
			
			edit.insert(counter, "-");
			counter++;
		}
		
		return edit.toString();
		}
		else
			return  null;
		
	}


	
	public static String FormatAmount(String RawData, boolean withcurrency)
	{
		StringBuilder edit;
		int counter=0;
		
		if(RawData!=null)
		{
			if(RawData.contains("-"))
			{
				String temp;
				temp=RawData.substring(1);
				edit = new StringBuilder(temp);
				for(counter=temp.length()-3;counter>0;counter-=3)
				{
					
					edit.insert(counter, ".");
				}
				edit.insert(0, "-");
				if(withcurrency)
				{
				return "Rp. "+edit.toString();
				}
				else
				{
					return edit.toString();
				}
			}
			else
			{
				edit = new StringBuilder(RawData);
				for(counter=RawData.length()-3;counter>0;counter-=3)
				{
					
					edit.insert(counter, ".");
				}
				if(withcurrency)
				{
				return "Rp. "+edit.toString();
				}
				else
				{
					return edit.toString();
				}
				
			}
		}return null;
	}
	
	public static String FormatCepas(String RawData, boolean withcurrency)
	{
		StringBuilder edit;
		int counter=0;
		int loop=0;
		
		if(RawData!=null)
		{
			if(RawData.contains("-"))
			{
				String temp;
				temp=RawData.substring(1);
				edit = new StringBuilder(temp);
				for(counter=temp.length()-2;counter>=0;counter-=2)
				{
					if(counter==0)
					{
						if(loop==0)
						edit.insert(counter, "0.");
					}
					else
					{
					edit.insert(counter, ".");
					}
					loop++;
				}
				//edit.insert(0, "-");
				if(withcurrency)
				{
				return "S$ "+edit.toString();
				}
				else
				{
					return edit.toString();
				}
			}
			else
			{
				edit = new StringBuilder(RawData);
				for(counter=RawData.length()-2;counter>=0;counter-=2)
				{
					
					if(counter==0)
					{
						if(loop==0)
						edit.insert(counter, "0.");
					}
					else
					{
					edit.insert(counter, ".");
					}
					loop++;
				}
				if(withcurrency)
				{
					return "S$ "+edit.toString();
				}
				else
				{
					return edit.toString();
				}
				
			}
		}return null;
	}
	
	
/*
	public static String FormatAmount(String RawData,boolean withcurrency)
	{
		StringBuilder edit;
		int counter=0;
		
		if(RawData!=null)
		{
		edit = new StringBuilder(RawData);
		for(counter=RawData.length()-3;counter>0;counter-=3)
		{
			
			edit.insert(counter, ".");
		}
		if(withcurrency)
		{
		return "Rp. "+edit.toString();
		}
		else
		{
			return edit.toString();
		}
		}return null;
		
	}
	*/
	public static byte[] convertDrawabletoByteArray(Drawable draw)
	{
		Drawable d; // the drawable (Captain Obvious, to the rescue!!!)
		byte[] bitmapdata;
		d=draw;
		Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		bitmapdata = stream.toByteArray();
		return bitmapdata;
	}

    public static String GetAppVersion(Context ctx)
    {

        PackageManager pm = ctx.getPackageManager();
        String vName="";
        try {
            vName=pm.getPackageInfo(ctx.getPackageName(), 0).versionName;
            vName +="."+(pm.getPackageInfo(ctx.getPackageName(), 0)).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return vName;
    }
	public static String twosCompliment(String bin) {
		String twos = "", ones = "";

		for (int i = 0; i < bin.length(); i++) {
			ones += flip(bin.charAt(i));
		}
		int number0 = Integer.parseInt(ones, 2);
		StringBuilder builder = new StringBuilder(ones);
		boolean b = false;
		for (int i = ones.length() - 1; i > 0; i--) {
			if (ones.charAt(i) == '1') {
				builder.setCharAt(i, '0');
			} else {
				builder.setCharAt(i, '1');
				b = true;
				break;
			}
		}
		if (!b)
			builder.append("1", 0, 7);


		twos = builder.toString();

		return twos;
	}

	// Returns '0' for '1' and '1' for '0'
	static char flip(char c) {
		return (c == '0') ? '1' : '0';
	}
}
