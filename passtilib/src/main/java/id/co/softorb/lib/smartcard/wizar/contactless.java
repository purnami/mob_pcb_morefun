package id.co.softorb.lib.smartcard.wizar;

import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationListener;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.card.ATR;
import com.cloudpos.card.CPUCard;
import com.cloudpos.card.Card;
import com.cloudpos.rfcardreader.RFCardReaderDevice;
import com.cloudpos.rfcardreader.RFCardReaderOperationResult;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.ContactlessBase;

import static id.co.softorb.lib.helper.ErrorCode.*;


public class contactless extends ContactlessBase
{

    String tagclass= this.getClass().getSimpleName();
    private RFCardReaderDevice rfReader;
    private POSListener detectListener;
    private POSTerminal posTerminal;

    public contactless()
    {

    }
    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=CTL_ERR_INITREADER;

        try {
            posTerminal=(POSTerminal) devInstance;

            rfReader =(RFCardReaderDevice) posTerminal.getDevice("cloudpos.device.rfcardreader");


            if (rfReader != null) {
                detectListener = listener;
                return OK;
            } else
                return CTL_ERR_INITREADER;

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());

        }
        return result;
    }

        /*@Override
        public int init() {
            Log.i(TAG, tagclass+"."+new Throwable()
                    .getStackTrace()[0]
                    .getMethodName());
            int result=CTL_ERR_INITREADER;


            return result;
        }*/

    @Override
    public int open() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=ERR_OPENCTL;
        try {
            rfReader.open();
            result=OK;
        } catch (DeviceException e) {
            e.printStackTrace();
            Log.e(TAG,e.toString());

        }
        return result;
    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            rfReader.close();



        }catch (DeviceException e)
        {
            Log.e(TAG,e.toString());
            return NOT_OK;

        }
        return OK;
    }

    @Override
    public int find() {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {

            rfReader.listenForCardPresent(listener, waitingtime);
        }
        catch (DeviceException e)
        {
            return NOT_OK;

        }
        return WAITING;
    }

    @Override
    public void stopfind() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            if(rfCard!=null) {

                ((CPUCard) rfCard).disconnect();
            }

        }
        catch (DeviceException e)
        {
            Log.e(TAG, e.toString());
        }
    }



    @Override
    public byte[] sendCTL(byte[] apdu) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] apduResult=null;
        try {
            apduResult =((CPUCard)rfCard).transmit(apdu);

        } catch (DeviceException e) {
            e.printStackTrace();
            Log.e(TAG,e.toString());


        }
        return apduResult;
    }

    @Override
    public void setMode(int mode) {
        this.mode=mode;
    }


    private Card rfCard;
    ATR atr;
    OperationListener listener = new OperationListener() {

        @Override
        public void handleResult(OperationResult arg0) {
            byte[] tempuid=null;

            Log.e(TAG,"arg0.getResultCode() = "+arg0.getResultCode());
            if (arg0.getResultCode() == OperationResult.SUCCESS) {
                rfCard = ((RFCardReaderOperationResult) arg0).getCard();
                try {
                    tempuid=rfCard.getID();

                    atr = ((CPUCard) rfCard).connect();
                    Log.e(TAG,"ATR "+ Hex.bytesToHexString(atr.getBytes()));
                }
                catch (DeviceException e)
                {
                    Log.d(TAG, e.toString());
                    stopfind();
                }

            }
            detectListener.onRFDetected(tempuid,mode);


        }
    };
}
