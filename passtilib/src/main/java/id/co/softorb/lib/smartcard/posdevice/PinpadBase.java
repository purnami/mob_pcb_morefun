package id.co.softorb.lib.smartcard.posdevice;

public abstract class PinpadBase extends Base{
    public abstract int setPinpadCallback(Object obj);
    public abstract String SN();
    //public abstract byte[] GenerateRandom(int length);
    public abstract int SetPINLength(int min,int max);
    public abstract int UpdateUserKey(int mkid,int userkeyid,byte[] value);
    public abstract int UpdateUserKey(int mkid,int userkeyid,byte[] value,byte[] checksum);
    public abstract byte[] getSessionKey(int mkid,int userkeyid,int algo);
    public abstract byte[] encrypt(int mkid,int userkeyid,int algo,byte[] data);
    public abstract byte[] calculatemac(int mkid,int userkeyid,int keyalgo,int macalgo,byte[] data);
    public abstract byte[] calculatedukptmac(byte[] data);
    public abstract int verifymac(int mkid,int keyid,byte[] data,byte[] mac);
    public abstract int async_pinblock(int mkid,int keyid,byte[] data);
    public abstract byte[] sync_pinblock(int mkid,int keyid,byte[] data);
    public abstract int getMKStatus(int mkid);
    public abstract int getSKStatus(int mkid,int skid);
    public abstract int getDUKPTStatus(int mkid,byte[] uniquekey);
    public abstract int cancel();
    protected abstract int SelectedKeyAlgo(int algo);
    protected abstract int SelectedMACAlgo(int algo);
    //public abstract int UpdateUserKey(int );
}
