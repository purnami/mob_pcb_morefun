package id.co.softorb.lib.smartcard.wizar;

import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationListener;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.card.ATR;
import com.cloudpos.card.CPUCard;
import com.cloudpos.card.Card;
import com.cloudpos.smartcardreader.SmartCardReaderDevice;
import com.cloudpos.smartcardreader.SmartCardReaderOperationResult;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.ContactBase;
import static id.co.softorb.lib.helper.ErrorCode.*;

public class sam extends ContactBase {
String tagclass= this.getClass().getSimpleName();
    private SmartCardReaderDevice contactreader;
    private POSListener detectListener;
    private POSTerminal posTerminal;
    private Card ContactCard;
    private final int samslot=1;
    private contactcard[] cb;


    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        cb = new contactcard[1];
        cb[0] = new contactcard();
        posTerminal=(POSTerminal) devInstance;
        contactreader =(SmartCardReaderDevice) posTerminal.getDevice("cloudpos.device.smartcardreader",samslot);
        cb[0].setReader(contactreader);
        detectListener=listener;

        return OK;
    }

    @Override
    public int open() {
        return cb[0].Open();
    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return cb[0].Close();
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=NOT_OK;
        if(!cb[0].isOpen())
        {

            open();
        }


        result=cb[0].findcard(contactlistener, waitingtime);


        return result;
    }

    @Override
    public void stopfind() {
        cb[0].Close();

    }
 @Override
    public byte[] sendCB( int slot,byte[] apdu) {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] apduResult=null;

        Log.d(TAG,"RDR-->SAM : "+ Hex.bytesToHexString(apdu));
        try {
            apduResult = ((CPUCard) (cb[0].getCard())).transmit(apdu);

        } catch (DeviceException e) {
            e.printStackTrace();
            return null;
        }
        Log.d(TAG,"RDR<--SAM : "+ Hex.bytesToHexString(apduResult));

        return apduResult;
    }
    OperationListener contactlistener = new OperationListener() {

        @Override
        public void handleResult(OperationResult arg0) {
            byte[] tempuid=null;
            ATR atr;
            Log.e(TAG,"arg0.getResultCode() = "+arg0.getResultCode());
            int result=arg0.getResultCode();
            switch(result)
            {
                case OperationResult.SUCCESS:
                    ContactCard = ((SmartCardReaderOperationResult) arg0).getCard();
                    try {
                        cb[0].setCard(ContactCard);
                        tempuid=ContactCard.getID();
                        atr = ((CPUCard) ContactCard).connect();

                    }
                    catch (DeviceException e)
                    {
                        Log.d(TAG, e.toString());

                    }
                    break;
                default:
                    break;
            }
            detectListener.onSAMDetected(samslot,tempuid);



        }
    };
}
