package id.co.softorb.lib.helper;

 public class ErrorCode
{
    public final  static int ERR_NOTSUPPORTED = -109;
    public final  static int ERR_INVALID_KEY = -108;
    public final  static int ERR_PARAM_PROCODE=-33;
    public final  static int ERR_PARAM_BATCH=-32;
    public final  static int ERR_PARAM_TID=-31;
    public final  static int ERR_PARAM_MID=-30;
    public final  static int ERR_PARAM_PIN=-29;

    public final  static int ERR_LIB_NOTINIT=-13;
    public final  static int ERR_INCORRECT_CARDTYPE =-12;
    public final  static int ERR_INCORRECT_SAMSLOT =-11;

    public final static int  ERR_UPDATE_FAIL= -28;
    public final static int  ERR_INCORRECT_UPDATEFILE= -27;
    public final static int  ERR_LAYANANGRATIS_NOTSUPPORTED= -26;
    public final static int  ERR_LOGON= -25;

    public final static int  ERR_OPENGPS= -23;
    public final static int  ERR_NORECORD= -24;

    public final static int  OFFICER_CARD =200;
    public final static int  UPDATE_CARD =201;

    public final static int  NOT_OK= -10;
    public final static int  ERR_SW1SW2= -2;
    public final static int  ERR_TIMEOUT= -1;

    public final static int  OK =0;
    public final static int  WAITING =1;
    public final static int  NO_TAG_ERROR= 2;
    public final static int  ERR_NO_RESP =3;
    public final static int  ERR_CRC =4;
    public final static int  ERR_STORAGE =5;
    public final static int  ERR_MARRIAGE =6;
    public final static int  ERR_GETMARRIAGECODE =7;
    //public final static int  ERR_INSUFFICIENTBALANCE 8
    public final static int  ERR_CARDINACTIVE =9;
    public final static int  ERR_SOCKET_NOTCONNECT =10;
    public final static int  ERR_SOCKET_WRITE =11;
    public final static int  ERR_SOCKET_READ =12;
    public final static int  ERR_SOCKET_NOTDISCONNECT =13;
    public final static int  ERR_GPS_NODATA =14;
    public final static int  ERR_IP =15;
    public final static int  ERR_SOCKET =16;
    //public final static int  ERR_NETWORK_TIME 17
    public final static int  ERR_APN =18;
    public final static int  ERR_PARTIAL =19;
    //public final static int  ERR_NOTOKOTRIP 20
    public final static int  ERR_GPS_DATANOTVALID =21;
    public final static int  ERR_CLOSEGPS =22;
    public final static int  ERR_SN_NOTMATCH =23;
    public final static int  ERR_WRONGCARDNO =24;
    public final static int  ERR_MF_EMU_CONNECT_FAIL =25;
    public final static int  ERR_MF_EMU_READBLOCK =26;
    public final static int  ERR_MF_EMU_WRITEBLOCK =27;
    public final static int  ERR_MF_EMU_SWITCH2SC =28;
    public final static int  ERR_MF_EMU_AUTH =29;

    public final static int  OKOTRIP_INCORRECTLENGTH =45;

    public final static int  ERR_CONTROLCARD_INVALID =46;
    public final static int  ERR_WIFI_TCPCONNECT =47;
    public final static int  ERR_WIFI_TCPWRITE =48;
    public final static int  ERR_WIFI_TCPREAD =49;
    public final static int  ERR_WIFI_TCPDISCONNECT =50;
    public final static int  ERR_WIFI_STATUSNOTREADY =51;
    public final static int  ERR_NAME =52;
    public final static int  ERR_PW =53;
    public final static int  ERR_TRX_PROFILE =54;
    public final static int  ERR_OPERATIONMODE =55;
    public final static int  PRINT_RECEIPT =56;
    public final static int  ERR_WIFI_CONFIGURE_IP =57;
    public final static int  ERR_WIFI_CONFIGURE_DNS =58;
    public final static int  ERR_WIFI_GET_SIGNAL_STRENGTH =59;
    public final static int  ERR_CONFIGCARD_INVALID =60;
    public final static int  ERR_KODEBUS_INVALID =61;

    public final static int  ERR_REPURCHASEREQUIRED =64;

    public final static int  ERR_EXPIRED =66;
        public final static int  ERR_CONFIGNOTFOUND =69;
    public final static int  ERR_UNKNOWNPARAM =70;
    public final static int  ERR_UNKNOWN_FRAME =71;
    public final static int  ERR_INITREADER =72;
    public final static int  ERR_READERNOTINIT =73;
    public final static int  ERR_SAMNOTREADY =74;
    public final static int  ERR_CHECKCARDINFO =75;
    public final static int  ERR_INACTIVEREADER =76;
    public final static int  ERR_CARDTYPEINACTIVE =77;

    public final static int  ERR_MID =78;
    public final static int  ERR_TID =79;
    public final static int  ERR_APPKEY =81;
    public final static int  ERR_ACTTARGET =82;
    public final static int  ERR_ACTION =83;
    public final static int  ERR_ISSUERCONFIGLEN =84;
    public final static int  ERR_DISPLAY =85;

    public final static int  ERR_TAGLEN_TAPIN =86;
    public final static int  ERR_TAGLEN_TAPOUT =87;
    public final static int  ERR_TAGLEN_PETUGAS =88;
    public final static int  ERR_TAGLEN_UPLOAD =89;
    public final static int  ERR_TAGLEN_ARMADA =90;
    public final static int  ERR_TAGLEN_APPVER =91;
    public final static int  ERR_TAGLEN_DEVTID =92;
    public final static int  ERR_TAGLEN_TITLE =93;
    public final static int  ERR_TAGLEN_DATETIME =94;
    public final static int  ERR_TAGLEN_TARIF =95;
    public final static int  ERR_TAGLEN_ICONDATA=96;
    public final static int  ERR_TAGLEN_ICONGPS=97;
    public final static int  ERR_TAGLEN_ICONCTL=98;
    public final static int  ERR_TAGLEN_ICONSYNC=99;
    public final static int  ERR_TAGLEN_AMT=100;
    public final static int  ERR_TAGLEN_TAPCODE=101;
    public final static int  ERR_TAGLEN_KODEPRODUK=102;
    public final static int  ERR_TAGLEN_BNIMRG=103;
    public final static int  ERR_TAGLEN_DISPID=104;
    public final static int  ERR_TAGLEN_ERRORCODE=105;
    public final static int  ERR_TAGLEN_BALANCE=106;
    public final static int  ERR_TAGLEN_ROUTE=107;

    public final static int  ERR_TAGLEN_ICONDOWNLOAD=109;
    public final static int  ERR_TAGLEN_LED=110;
    public final static int  ERR_TAGLEN_ZEROTARIFF=111;
    public final static int  ERR_TAGLEN_LAYANANGRATIS=112;
    public final static int  ERR_TAGLEN_ECOTARIFF=113;


    public final static int  ERR_TAGNOTFOUND_STATUSCODE=150;
    public final static int  ERR_TAGNOTFOUND_CARDTYPE=151;
    public final static int  ERR_TAGNOTFOUND_INTEROP=152;
    public final static int  ERR_TAGNOTFOUND_CARDNO=153;
    public final static int  ERR_TAGNOTFOUND_BALANCE=154;
    public final static int  ERR_TAGNOTFOUND_CARDDATA=155;
    public final static int  ERR_TAGNOTFOUND_LAYANANGRATIS=156;
    public final static int  ERR_TAGNOTFOUND_BANKLOG=157;
    public final static int  ERR_TAGNOTFOUND_UID=158;
    public final static int  ERR_TAGNOTFOUND_EXP=159;
    public final static int  ERR_TAGLEN_CARDDATA=160;

    public final static int  ERR_TAGNOTFOUND_LOGCHKSUM=161;
    public final static int  ERR_LOGCHKSUM=162;
    public final static int  ERR_TAGNOTFOUND_CHKSUM=163;

    public final static int  ERR_FS_CREATE= -2001;
    public final static int  ERR_FS_WRITE = -2002;
    public final static int  ERR_FS_READ  = -2003;
    public final static int  ERR_FS_APPEND= -2004;
    public final static int  ERR_FS_DELETE= -2005;
    public final static int  ERR_FS_NOTFOUND= -2006;
    public final static int  ERR_FS_CLEAR_FAIL= -2007;

    public final static int  SAM_ERR_POWERONFAIL = -1001;
    public final static int  SAM_ERR_RELEASEFAIL = -1002;
    public final static int  ERR_INITPRINTER = -1003;
    public final static int  ERR_INITMSR = -1004;
    public final static int  ERR_OPENMSR = -1005;
    public final static int  ERR_OPENPRINTER = -1006;
    public final static int  ERR_OPENCTL = -1007;
    public final static int  ERR_OPENCONTACT = -1008;
    public final static int  ERR_CONTACTCLOSED = -1009;
    public final static int  ERR_INITLED = -1010;
    public final static int  ERR_OPENLED = -1011;
    public final static int  ERR_CLOSELED = -1012;
    public final static int  ERR_TURNONLED = -1013;
    public final static int  ERR_TURNOFFLED = -1014;
    public final static int  ERR_BLINKLED = -1015;
    public final static int  ERR_OPENPINPAD = -1016;
   // public final static int  ERR_BLINKLED = -1015;

    public final static int  CTL_ERR_MANAGERNOTFOUND= -1201;
    public final static int  CTL_ERR_INITREADER= -1202;
    public final static int  CTL_ERR_NOTFOUND= -1203;
    public final static int  CTL_ERR_READFAIL= -1204;
    public final static int  CTL_ERR_APDUNOTSENT= -1205;
    public final static int  CTL_ERR_DESELECT= -1206;
    public final static int  CTL_ERR_GETUID = -106;
    public final static int  CTL_ERR_UNKNOWNCARD= -107;
    public final static int  CTL_ERR_TIMEWINDOW= -109;
    public final static int  CB_ERR_NOTFOUND= -110;
    public final static int  ERR_UNKNOWN= -111;
    public final static int  ERR_LENGTH= -112;


    public final static int  DKI_CTL_ERR_SELECTAPP= -608;
    public final static int  DKI_SAM_ERR_SELECTAID= -609;
    public final static int  DKI_CTL_ERR_INITTOPUP=-610;
    public final static int  DKI_SAM_ERR_DEBITLSAM=-611;
    public final static int  DKI_CTL_ERR_TOPUP=-612;
    public final static int  DKI_SAM_ERR_CONFIRMLSAM=-613;
    public final static int  DKI_CTL_ERR_INITPURCHASE=-620;
    public final static int  DKI_SAM_ERR_INITPSAM=-621;
    public final static int  DKI_CTL_ERR_DEBIT=-622;
    public final static int  DKI_SAM_ERR_CREDITPSAM=-623;
    public final static int  DKI_CTL_ERR_INITREPURCHASE=-630;
    public final static int  DKI_SAM_ERR_INITREPURCHASE=-631;
    public final static int  DKI_CTL_ERR_REPURCHASE=-632;
    public final static int  DKI_SAM_ERR_REPURCHASE=-633;
    public final static int  DKI_CTL_ERR_INITCANCELPURCHASE=-640;
    public final static int  DKI_PSAM_ERR_INITCANCELPURCHASE=-641;
    public final static int  DKI_CTL_ERR_CANCELPURCHASE=-642;
    public final static int  DKI_PSAM_ERR_CANCELPURCHASE=-643;
    public final static int  DKI_CTL_ERR_READOKOTRIP=-651;
    public final static int  DKI_CTL_ERR_WRITEOKOTRIP=-652;
    public final static int  DKI_CTL_ERR_READINTEROP= -653;
    public final static int  DKI_CTL_ERR_READEFPURSE= -654;
    public final static int  DKI_CTL_ERR_GETBALANCE= -655;
    public final static int  DKI_CTL_ERR_READMRT= -656;
    public final static int  DKI_CTL_ERR_WRITEMRT= -657;
    public final static int  DKI_CTL_ERR_GETCHALLENGE= -658;
    public final static int  DKI_SAM_ERR_GETMAC = -659;
    public final static int  DKI_SAM_ERR_INITPURCHASE= -660;
    public final static int  DKI_CTL_ERR_GETTRXLOG= -661;


    public final static int  DKI_MF_ERR_READWALLETDATA=-670;
    public final static int  DKI_MF_ERR_SAMPROCESSWALLETDATA=-671;
    public final static int  DKI_MF_ERR_SAMCHECKBALANCE=-672;
    public final static int  DKI_MF_ERR_SAMDEDUCT=-673;
    public final static int  DKI_MF_ERR_SAMCOMMIT=-674;
    public final static int  DKI_MF_ERR_WRITEWALLETDATA=-675;
    public final static int  DKI_LAYANANGRATIS_UNKNOWN_ED=-676;
    public final static int  DKI_MF_ERR_SAMSTATUS=-677;
    public final static int  DKI_MF_ERR_SAMAUTH=-678;
    public final static int  DKI_MF_ERR_READOKOTRIP=-679;
    public final static int  DKI_MF_ERR_WRITEOKOTRIP=-680;

    public final static int  LUMINOS_CTL_ERR_SELECTAPP= -100;
    public final static int  LUMINOS_CTL_ERR_GETBALANCE= -101;
    public final static int  CTL_ERR_INSUFFICIENTBALANCE = -102;
    public final static int  CTL_ERR_TOPUPEXCEEDMAXBALANCE = -103;
    public final static int  LUMINOS_CTL_ERR_GETTRXLOG = -104;
    public final static int  LUMINOS_CTL_ERR_INITTOPUP=-110;
    public final static int  LUMINOS_SAM_ERR_DEBITLSAM=-111;
    public final static int  LUMINOS_CTL_ERR_TOPUP=-112;
    public final static int  LUMINOS_SAM_ERR_CONFIRMLSAM=-113;
    public final static int  LUMINOS_CTL_ERR_INITPURCHASE=-120;
    public final static int  LUMINOS_SAM_ERR_INITPURCHASE=-121;
    public final static int  LUMINOS_CTL_ERR_DEBIT=-122;
    public final static int  LUMINOS_SAM_ERR_CREDITPSAM=-123;
    public final  static int LUMINOS_SAM_ERR_SELECTAID= -124;
    public final static int  LUMINOS_CTL_ERR_INITREPURCHASE=-130;
    public final static int  LUMINOS_SAM_ERR_INITREPURCHASE=-131;
    public final static int  LUMINOS_CTL_ERR_REPURCHASE=-132;
    public final static int  LUMINOS_SAM_ERR_REPURCHASE=-133;
    public final static int  LUMINOS_CTL_ERR_INITCANCELPURCHASE=-140;
    public final static int  LUMINOS_PSAM_ERR_INITCANCELPURCHASE=-141;
    public final static int  LUMINOS_CTL_ERR_CANCELPURCHASE=-142;
    public final static int  LUMINOS_PSAM_ERR_CANCELPURCHASE=-143;
    public final static int  LUMINOS_CTL_ERR_READEFPURSE= -144;

    public final static int  MDR_SAM_ERR_GETPIN= -208;
    public final static int  MDR_SAM_ERR_GETINSID= -2081;
    public final static int  MDR_CTL_NOTSUPPORTED= -209;
    public final static int  MDR_CTL_ERR_SELECTAPP= -210;
    public final static int  MDR_CTL_ERR_READDATA= -211;
    public final static int  MDR_CTL_ERR_READCARDINFO= -212;
    public final static int  MDR_CTL_ERR_GETBALANCE= -213;
    public final static int  MDR_SAM_ERR_INITPURCHASE= -214;
    public final static int  MDR_CTL_ERR_DEBITPURSE= -215;
    public final static int  MDR_SAM_ERR_GETLOG= -216;
    public final static int  MDR_SAM_ERR_SELECT= -217;
    public final static int  MDR_SAM_ERR_SELECTAID1 = -218;
    public final static int  MDR_SAM_ERR_LOGIN1 = -219;
    public final static int  MDR_SAM_ERR_UPDATETERMINALINFO = -220;
    public final static int  MDR_SAM_ERR_SELECTAID2 = -221;
    public final static int  MDR_SAM_ERR_GETUID = -222;
    public final static int  MDR_SAM_ERR_GETINFO = -223;
    public final static int  MDR_SAM_ERR_GETJCOPBM = -224;
    public final static int  MDR_SAM_ERR_STARTPAYMENT = -225;
    public final static int  MDR_SAM_ERR_GETTRXREPORT = -226;
    public final static int  MDR_CTL_ERR_SELECTAPPID1 = -227;
    public final static int  MDR_CTL_ERR_READBINAPPID1 = -228;
    public final static int  MDR_CTL_ERR_SELECTAPPID2 = -229;
    public final static int  MDR_CTL_ERR_GETBINAPPID2 = -230;
    public final static int  MDR_SAM_ERR_INITCHECKBALANCE = -231;
    public final static int  MDR_CTL_ERR_INITCHECKBALANCE= -232;
    public final static int  MDR_SAM_ERR_INITCHECKBALANCEHIST= -234;
    public final static int  MDR_SAM_ERR_DEBITUNKNOWNTAG= -235;
    public final static int  MDR_CTL_ERR_SELECTAIDKTME= -237;
    public final static int  MDR_CTL_ERR_CHANGEPINKTME= -238;
    public final static int  MDR_CTL_ERR_CHANGELOGINPINKTME= -239;
    public final static int  MDR_SAM_ERR_STARTCORRECTION= -240;
    public final static int  MDR_CTL_ERR_CORRECTION = -2410;
    public final static int  MDR_SAM_ERR_STARTVOID= -242;
    public final static int  MDR_CTL_ERR_VOID = -2430;
    public final static int  MDR_SAM_ERR_CTLRSP= -244;
    public final static int  MDR_SAM_ERR_GENREPORT= -245;
    public final static int  MDR_CTL_ERR_READHISTORY= -246;
    public final static int  MDR_SAM_ERR_COPYCARDHISTORY= -247;
    public final static int  MDR_SAM_ERR_PURPOSE_SAMMESSAGE= -2330;
    public final static int  MDR_SAM_ERR_PURPOSE_BALANCE = -2440;
    public final static int  MDR_SAM_ERR_PURPOSE_ENDSEQ = -2450;
    public final static int  MDR_SAM_ERR_PURPOSE_AUTOGENTRXREPORT= -2460;
    public final static int  MDR_SAM_ERR_LOGIN2 = -248;
    public final static int  MDR_SAM_ERR_UPDATETERMINALINFO2= -249;
    public final static int  MDR_CTL_ERR_READOKOTRIP= -250;
    public final static int  MDR_CTL_ERR_WRITEOKOTRIP= -251;
    public final static int  MDR_CTL_ERR_UNKNOWNAPPID= -252;
    public final static int  MDR_SAM_ERR_STARTCHECKBALANCE = -253;
    public final static int  MDR_INVALID_DATA = -254;
    public final  static int MDR_SAM_ERR_REINIT = -255;
    public final  static int MDR_GRACEPERIOD = -256;

    public final static int  BRI_SAM_ERR_SELECTAID = -310;
    public final static int  BRI_CTL_ERR_DESFIRESELECTAID1 = -311;
    public final static int  BRI_CTL_ERR_DESFIREGETCARDNO = -312;
    public final static int  BRI_CTL_ERR_DESFIREGETCARDSTATUS = -313;
    public final static int  BRI_CTL_ERR_DESFIRESELECTAID3 = -314;
    public final static int  BRI_CTL_ERR_DESFIREGETKEYCARD = -315;
    public final static int  BRI_SAM_ERR_DESFIREAUTHKEY = -316;
    public final static int  BRI_CTL_ERR_DESFIREAUTHCARD  = -317;
    public final static int  BRI_CTL_ERR_DESFIREGETLASTTRXDATE = -318;
    public final static int  BRI_CTL_ERR_DESFIREGETBALANCE = -319;
    public final static int  BRI_CTL_ERR_DESFIREDEBIT  = -320;
    public final static int  BRI_SAM_ERR_DESFIRECREATEHASH = -321;
    public final static int  BRI_CTL_ERR_DESFIREWRITELOG = -322;
    public final static int  BRI_CTL_ERR_DESFIREWRITELASTTRX = -323;
    public final static int  BRI_CTL_ERR_DESFIRECOMMITTRX = -324;
    public final static int  BRI_CTL_ERR_DESFIREABORTTRX = -325;
    public final static int  BRI_CTL_ERR_GETPARTNERDATA = -326;
    public final static int  BRI_CTL_ERR_WRITEPARTNERDATA = -327;
    public final static int  BRI_CTL_ERR_AUTHPARTNERDATA = -328;
    public final static int  BRI_SAM_ERR_GETRANDOM = -329;
    public final static int  BRI_CTL_ERR_SELECTAID6 = -330;
    public final static int  BRI_CTL_ERR_GETKEYCARD8 = -331;
    public final static int  BRI_CTL_ERR_GETKEYCARD2 = -332;
    public final static int  BRI_CTL_ERR_SELECTAID5 = -333;
    public final static int  BRI_CTL_ERR_APPNOTACTIVE= -334;
    public final  static int BRI_CTL_ERR_SELECTAID = -335;
    public final  static int BRI_SAM_ERR_REINIT = -336;

    public final static int  BNI_CTL_ERR_SELECTAPP= -410;
    public final static int  BNI_CTL_ERR_GETCHALLENGE= -411;
    public final static int  BNI_CTL_ERR_GETPURSEDATA= -412;
    public final static int  BNI_SAM_ERR_VERIFYSECUREREADPURSE= -413;
    public final static int  BNI_SAM_ERR_GENERATEDEBIT= -414;
    public final static int  BNI_CTL_ERR_DEBITPURSE= -415;
    public final static int  BNI_SAM_ERR_VERIFYDEBITRECEIPT= -416;
    public final static int  BNI_SAM_ERR_SELECTAID= -417;
    public final static int  BNI_SAM_ERR_GETINFO= -418;
    public final static int  BNI_SAM_ERR_NOTMARRIED= -419;
    public final static int  BNI_SAM_ERR_INIT= -420;
    public final static int  BNI_CTL_ERR_PURSENOTENABLED = -421;
    public final static int  BNI_SAM_ERR_STOREMARRIAGECODE = -422;
    public final static int  BNI_CTL_ERR_READOKOTRIP= -423;
    public final static int  BNI_CTL_ERR_WRITEOKOTRIP= -424;
    public final static int  BNI_SAM_ERR_MARRIAGE= -425;
    public final static int  BNI_SAM_ERR_GETMARRIAGECODE= -426;
    public final static int  BNI_CTL_ERR_READMRT= -427;
    public final static int  BNI_CTL_ERR_WRITEMRT= -428;
    public final static  int BNI_CTL_ERR_GETBALANCE = -429;
    public final static  int BNI_SAM_ERR_MARRIED = -430;
    public final static  int BNI_SAM_ERR_REINIT = -431;

    public final static int  BCA_CTL_ERR_SELECTAPP= -510;
    public final static int  BCA_CTL_ERR_SELECTDF= -511;
    public final static int  BCA_CTL_ERR_READBIN= -512;
    public final static int  BCA_CTL_ERR_READTERM= -513;
    public final static int  BCA_CTL_ERR_GETCCARD_CTC= -514;
    public final static int  BCA_SAM_ERR_GETCTERM= -515;
    public final static int  BCA_SAM_ERR_GETRTERM= -516;
    public final static int  BCA_SAM_ERR_GETTTC= -517;
    public final static int  BCA_SAM_ERR_VERIFYCRYPTO= -518;
    public final static int  BCA_CTL_ERR_VERIFYCTERM= -519;
    public final static int  BCA_CTL_ERR_DEBITPURSE= -520;
    public final static int  BCA_CTL_ERR_GETBALANCE= -521;
    public final static int  BCA_CTL_ERR_WRITELOG= -522;
    public final static int  BCA_CTL_ERR_GETLOG= -523;
    public final static int  BCA_CTL_ERR_READOKOTRIP= -524;
    public final static int  BCA_CTL_ERR_WRITEOKOTRIP= -525;
    public final static int  BCA_SAM_ERR_VERIFYDEBIT= -526;
    public final static int  BCA_SAM_ERR_INIT= -527;
    public final static int  BCA_AUTH_MRT= -528;
    public final static int  BCA_READ_MRT= -529;
    public final static int  BCA_WRITE_MRT= -530;
    public final static int  BCA_DEDUCT_ERR= -531;

    public final static int  MEGA_CTL_ERR_SELECTAID= -801;
    public final static int  MEGA_SAM_ERR_SELECTAID= -802;
    public final static int  MEGA_CTL_ERR_READLOG= -803;
    public final static int  MEGA_SAM_ERR_CHALLENGE= -804;
    public final static int  MEGA_SAM_ERR_GETSESKEY= -805;
    public final static int  MEGA_SAM_ERR_VERIFYPIN= -806;
    public final static int  MEGA_CTL_ERR_CHALLENGE= -807;
    public final static int  MEGA_CTL_ERR_SECUREREADPURSE= -808;
    public final static int  MEGA_SAM_ERR_GETDEBITCRGM= -810;
    public final static int  MEGA_CTL_ERR_DEBIT= -811;
    public final static int  MEGA_SAM_ERR_GETTRXRPT= -812;
    public final static int  MEGA_CTL_ERR_GETOKOTRIP= -813;
    public final static int  MEGA_CTL_ERR_WRITEOKOTRIP= -814;
    public final static int  MEGA_CTL_ERR_READPURSE= -815;
    public final static int  MEGA_SAM_ERR_GETRANDOM= -816;
    public final static int  MEGA_SAM_ERR_VALIDATEDEBIT= -817;
    public final static int  MEGA_SAM_ERR_STARTVALIDATEDEBIT= -818;

    public final static int  NOBU_SAM_ERR_SELECTAID= -701;
    public final static int  NOBU_SAM_ERR_GETCHALLENGE= -702;
    public final static int  NOBU_SAM_ERR_TAU= -703;
    public final static int  NOBU_SAM_ERR_VERIFYSECUREREAD= -704;
    public final static int  NOBU_SAM_ERR_GEN_DEBIT_CRGRAM= -705;
    public final static int  NOBU_SAM_ERR_VERIFY_TRX_RCPT= -706;
    public final static int  NOBU_CTL_ERR_SELECTAID= -707;
    public final static int  NOBU_CTL_ERR_SECUREREADPURSE= -708;
    public final static int  NOBU_CTL_ERR_DEBIT= -709;
    public final static int  NOBU_SAM_ERR_GETINFO= -710;
    public final static int  NOBU_CTL_BLACKLISTED= -711;
    public final static int  NOBU_CTL_DISABLED= -712;

     ErrorCode()
    {

    }

}