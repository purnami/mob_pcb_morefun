package id.co.softorb.lib.smartcard.pax;

import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationListener;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.card.ATR;
import com.cloudpos.card.CPUCard;
import com.cloudpos.card.Card;
import com.cloudpos.rfcardreader.RFCardReaderDevice;
import com.cloudpos.rfcardreader.RFCardReaderOperationResult;
import com.pax.dal.IDAL;
import com.pax.dal.IPicc;
import com.pax.dal.ISys;
import com.pax.dal.entity.EBeepMode;
import com.pax.dal.entity.EDetectMode;
import com.pax.dal.entity.EPiccRemoveMode;
import com.pax.dal.entity.EPiccType;
import com.pax.dal.entity.PiccCardInfo;
import com.pax.dal.exceptions.EPiccDevException;
import com.pax.dal.exceptions.PiccDevException;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.ContactlessBase;

import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INITREADER;
import static id.co.softorb.lib.helper.ErrorCode.ERR_OPENCTL;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;


public class contactless extends ContactlessBase
{
    String tagclass= this.getClass().getSimpleName();
    private IDAL dal;
    private ISys iSys = null;
    private EPiccType piccType;
    private IPicc picc;

    public  DetectABThread detectABThread;
    private POSListener detectListener;
    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=CTL_ERR_INITREADER;
        dal = (IDAL)devInstance;
        if(dal==null)
            return result;
        iSys = dal.getSys();
        piccType=EPiccType.INTERNAL;
        picc = dal.getPicc(piccType);
        if(picc==null)
            return result;
        detectListener = listener;

        return OK;
    }



    @Override
    public int open() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=ERR_OPENCTL;
        if(picc==null) {
            return result;
        }
        try {
            picc.open();
            Log.e(TAG,"open picc success");
        } catch (PiccDevException e) {
            e.printStackTrace();
            Log.e(TAG,"error open picc "+e.getMessage());
        }
        return OK;
    }

    @Override
    public int close() {
        try {
            picc.close();
        }
        catch (PiccDevException e)
        {
            Log.e(TAG,e.toString());
            return NOT_OK;
        }
        return OK;
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        detectABThread=null;
        if (detectABThread == null) {
            detectABThread = new DetectABThread();
            detectABThread.start();
        }
        return WAITING;
    }

    @Override
    public void stopfind() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        while (true) {
            try {
                picc.remove(EPiccRemoveMode.REMOVE, (byte) 0);
            } catch (PiccDevException e) {

                if (e.getErrCode() == EPiccDevException.PICC_ERR_CARD_SENSE.getErrCodeFromBasement()) {
                    continue;
                } else {
                    break;
                }
            }

            break;
        }
    }



    @Override
    public byte[] sendCTL(byte[] apdu) {
        byte[] ret = null;
        try {
            ret = picc.isoCommand((byte) 0,apdu);
            return ret;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    class DetectABThread extends Thread {
        @Override
        public void run() {
            try {
                picc.setLed((byte)0x00);
                picc.setLed((byte)0x01);
            } catch (PiccDevException e) {
                e.printStackTrace();
                Log.e(TAG,e.getMessage());
            }
            while (!Thread.interrupted()) {
                int i = detectAorBandCommand();
                if (i == 1) {
                    break;
                }
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    // Log.i("ss", "exit thread" + getId());
                    break;
                }
            }
        }
    }
    public PiccCardInfo detect(EDetectMode mode) {
        try {
            PiccCardInfo cardInfo = picc.detect(mode);

            Log.e(TAG,"detect picc success");
            return cardInfo;
        } catch (PiccDevException e) {
            e.printStackTrace();

            Log.e(TAG,"detect picc fail "+e.toString());
            return null;
        }
    }
    @Override
    public void setMode(int mode) {
        this.mode=mode;
    }
    public int detectAorBandCommand() {
        // light init
        // setLed((byte) 0x00);
        // blue light
        // setLed((byte) 0x08);
        byte[] tempuid=null;
        PiccCardInfo cardInfo;
        if (null != (cardInfo = detect(EDetectMode.ISO14443_AB))) {
            // open blue and yellow light


            iSys.beep(EBeepMode.FREQUENCE_LEVEL_1, 500);
            // light init

            tempuid = cardInfo.getSerialInfo();
            detectListener.onRFDetected(tempuid,mode);
            return 1;
        }
        return 0;
    }
}
