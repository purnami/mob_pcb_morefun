package id.co.softorb.lib.helper;

public class printerparam {
    final public static int FONT_BOLD=1;
    final public static int FONT_ITALIC=2;
    final public static int FONT_NORMAL=3;
    final public static int ALIGN_LEFT=4;
    final public static int ALIGN_CENTER=5;
    final public static int ALIGN_RIGHT=6;

    final public static int PRINTER_NOPAPER=0;
    final public static int PRINTER_PAPEREXIST=1;
}
