  package id.co.softorb.lib.passti;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

import java.util.Arrays;
import static id.co.softorb.lib.passti.devinfo.*;
import static id.co.softorb.lib.passti.helper.ErrorCode.*;
import static id.co.softorb.lib.passti.helper.util.*;
import static id.co.softorb.lib.smartcard.posdevice.params.*;

import id.co.softorb.lib.helper.Hex;


import id.co.softorb.lib.passti.helper.ByteUtils;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.passti.helper.converter;
import id.co.softorb.lib.passti.helper.util;
import id.co.softorb.lib.smartcard.posdevice.params;
import id.co.softorb.lib.smartcard.posdevice.reader;


class BRI {
	// Command
	/*
	private final String selectApp = "00A404000D4252495A5A495F655075727365";	
	private final String getData = "801B000000";	
	private final String initPurchaseSam= "903100001F";
	private final String debitPurse= "802B01002E";
	private final String samGenerateLog= "9033010010";
	*/
    class bri_trx
     {
         class  bri_cardinfo
         {


             byte[] lasttrxdate;
             byte[] akumdebet;
             long lAkumDebet;
             byte[] expiry;

             public bri_cardinfo()
             {
                 lasttrxdate = new byte[3];
                 akumdebet = new byte[4];
                 expiry = new byte[2];
                 lAkumDebet=0;
             }

             public int setExpiry(byte[] expiry)
             {
                 if(expiry.length!=2)
                     return NOT_OK;
                 this.expiry=expiry;
                 return OK;
             }
             public byte[] getExpiry()
             {
                 return expiry;
             }

             public int setAkumDebet(byte[] akumdebet)
             {
                 if(akumdebet.length!=4)
                     return NOT_OK;
                 this.akumdebet=akumdebet;
                 lAkumDebet=converter.ByteArrayToInt(akumdebet,0,4,converter.BIG_ENDIAN);
                 return OK;
             }
             public byte[] getAkumDebet()
             {
                 return akumdebet;
             }

             public long AkumDebet()
             {
                 return lAkumDebet;
             }

             public int setLastTrxDate(byte[] lasttrxdate)
             {
                 if(lasttrxdate.length!=3)
                     return NOT_OK;
                 this.lasttrxdate=lasttrxdate;
                 return OK;
             }
             public byte[] getLasttrxdate()
             {
                 return lasttrxdate;
             }

         }

         byte[] random;
         byte[] authdata;
         byte[] samhash;
         String refno;
         long lRefNo;
         bri_cardinfo cardinfo;
         public bri_trx()
         {
             authdata = new byte[8];
             random = new byte[16];
             samhash = new byte[4];

             lRefNo=0;
             cardinfo=new bri_cardinfo();
         }



         public int setAuthData(byte[] authdata)
         {
             if(authdata.length!=8)
                 return NOT_OK;
             this.authdata=authdata;
             return OK;
         }

         public byte[] getAuthdata()
         {
             return authdata;
         }

         public int setRandom(byte[] random)
         {
             if(random==null)
                 return NOT_OK;
             this.random=random;
             return OK;
         }
         public byte[] getRandom()
         {
             return random;
         }

         public int setSamHash(byte[] hash)
         {
             if(samhash.length!=4)
                 return NOT_OK;
             this.samhash=hash;
             return OK;
         }


         public String getSamhash()
         {
             return Hex.bytesToHexString(samhash);
         }
         public int setRefno(String refno)
         {

             this.refno=refno;
             return OK;
         }
         public String getRefNo()
         {

             return refno;
         }

     }



	    //byte[] cmdGetUIDBRIDesfireStep1		=  {(byte) 0x90,(byte) 0x60,(byte) 0x00,(byte) 0x00,(byte) 0x00 };
    //byte[] cmdGetUIDBRIDesfireStep2		=  {(byte) 0x90,(byte) 0xAF,(byte) 0x00,(byte) 0x00,(byte) 0x00 };

    //byte[] cmdReadFileDataBRIDesfire	=  {(byte) 0x90,(byte) 0xBD,(byte) 0x00,(byte) 0x00,(byte) 0x07,(byte) 0x01,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x30,(byte) 0x00,(byte) 0x00,(byte)0x00};


    byte[] cmddata = {(byte)0xFF,(byte)0x00,(byte)0x00,(byte)0x03,(byte)0x00,(byte)0x80,(byte)0x00,(byte)0x00,(byte)0x00};


	// global var

	private final long akumDebitLimit = 20000000;

	private String Balance;
	private byte[] RAPDU = new byte[255];
    private String saldoAwal,saldoAkhir;


     String TAG;
	//APOSUtility aposUtility;

	PASSTI passti;
reader device;
	private String hash_cardno,timestamp,hash_amount,hash_refno,hash_batchno="";
     String tagclass;
     int samslot;
     bri_trx ctl_bri;
     void SetSamPort(int slot)
     {
         samslot=slot;
     }

	//public BRI(Activity activity, Contactless ctl, SAM sam){
	 //BRI(Context ctx, UDeviceService deviceService, DeviceInfo deviceInfo,PASSTI passtiInstance,APOSUtility aposUtilityInstance) throws RemoteException, UnsupportedEncodingException {

	BRI(Context ctx, PASSTI passtiInstance, reader reader) {

         ctl_bri = new bri_trx();

         passti=passtiInstance;
        device=reader;
         samslot=255;//default for unassigned sam slot
         TAG= passti.dbghelper.TAG;
         tagclass = this.getClass().getSimpleName();
	}

     int checkbalance() throws RemoteException {
        int i = bridesfire_checkbalance_v2();

        return i;
    }
     String getBalance(){
        return saldoAwal;
    }
     String getLastBalance(){
        return saldoAkhir;
    }

     int deduct(String amount) throws RemoteException {
        saldoAwal = "";
        saldoAkhir = "";

        int i = bridesfire_deduct(amount);

        return i;
    }

    private int bridesfire_checkbalance_v2() throws RemoteException {

        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx;
        int result;

        result=CTL_ReadCardHolderDesfire();
        if(result!=OK)
        {
            return BRI_CTL_ERR_DESFIREGETCARDNO;
        }

        // Response : Data 3B + CardNo 8B + Data 21B + 9100


        result =CTL_ReadCIStatus();
        if(result!=OK)
        {
            return BRI_CTL_ERR_DESFIREGETCARDSTATUS;
        }
        result =CTL_SelectAID_500000();
        if(result!=OK)
        {
            return BRI_CTL_ERR_DESFIRESELECTAID3;
        }

        result =CTL_GetKeyCard();
        // result = BRI_CTL_GetKeyCard02();
        if(result!=OK)
        {
            return BRI_CTL_ERR_DESFIREGETKEYCARD;
        }

        result=BRI_SAM_SelectAID();
        if(result!=OK)
        {
            return BRI_SAM_ERR_SELECTAID;
        }
        result = BRI_SAM_GetRandom();
        if(result!=OK)
        {
            return BRI_SAM_ERR_GETRANDOM;
        }
        result =CTL_AuthDesfire(ctl_bri.getRandom());
        if(result!=OK)
        {
            return BRI_CTL_ERR_DESFIREAUTHCARD;
        }

        result=CTL_GetPartnerData();
        if(result!=OK)
        {
            return BRI_CTL_ERR_GETPARTNERDATA;
        }

        result=BRI_CTL_SelectAIDDesfire();
        if(result!=OK)
        {
            return BRI_CTL_ERR_SELECTAID;
        }
        result=BRI_SAM_SelectAID();
        if(result!=OK)
        {
            return BRI_SAM_ERR_SELECTAID;
        }

        result =CTL_SelectAID_300000();
        if(result!=OK)
        {
            return BRI_CTL_ERR_DESFIRESELECTAID3;
        }

        result =CTL_GetKeyCard();
        if(result!=OK)
        {
            return BRI_CTL_ERR_DESFIREGETKEYCARD;
        }

        result = BRI_SAM_GetRandom2();
        if(result!=OK)
        {
            return BRI_SAM_ERR_GETRANDOM;
        }

        result =CTL_AuthDesfire(ctl_bri.getRandom());
        if(result!=OK)
        {
            return BRI_CTL_ERR_DESFIREAUTHCARD;
        }

        result =CTL_GetLastTrx();
        if(result!=OK)
        {
            return BRI_CTL_ERR_DESFIREGETLASTTRXDATE;
        }
        result =CTL_GetBalanceDesfire();
        if(result!=OK)
            return BRI_CTL_ERR_DESFIREGETBALANCE;

        saldoAwal = Balance;



        return OK;
    }

    private int CTL_GetPartnerData() {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
//        byte[] cmd=  {(byte) 0x90,(byte) 0xBD,(byte) 0x00,(byte) 0x00,(byte) 0x07,(byte) 0x03,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x07,(byte) 0x00,(byte) 0x00,(byte)0x00};
        byte[] cmd=  {(byte) 0x90,(byte) 0xBD,(byte) 0x00,(byte) 0x00,(byte) 0x07,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x20,(byte) 0x00,(byte) 0x00,(byte)0x00};

        RAPDU = device.sendCTL(cmd);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != OK){
            //Log.d(TAG,"BRI_CTL_ERR_GETLASTTRX - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }
        cmd= new byte[]{(byte) 0x90, (byte) 0xBD, (byte) 0x00, (byte) 0x00, (byte) 0x07, (byte) 0x00, (byte) 0x20, (byte) 0x00, (byte) 0x00, (byte) 0x20, (byte) 0x00, (byte) 0x00, (byte) 0x00};

        RAPDU = device.sendCTL(cmd);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != OK){
            //Log.d(TAG,"BRI_CTL_ERR_GETLASTTRX - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }
        return OK;
    }


    int BRI_SAM_GetRandom()
     {

         passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                 .getStackTrace()[0]
                 .getMethodName());
         int idx;
         int result;
         byte[] cmd = new byte[50];
         byte[] command ;
         byte[] random = new byte[16];
         idx=0;
         cmd[idx++] = (byte)0x80;
         cmd[idx++] = (byte)0xB0;
         cmd[idx++] = 0x00;
         cmd[idx++] = 0x00;
         cmd[idx++] = 0x20;
         //card number 8B
         System.arraycopy(passti.ctlinfo.cardno,0,cmd,idx,passti.ctlinfo.len_cardno);
         idx+=passti.ctlinfo.len_cardno;
         //UID 7B + FF
         //memcpy(&cmdSendToSAM[idx],&uid[0],7);
         //idx+=7;
         byte[] tempuid = Arrays.copyOfRange(passti.ctlinfo.uid.value, 1, passti.ctlinfo.uid.len);
         System.arraycopy(tempuid,0,cmd,idx,7);//2020-06-15,device will return 7 bytes UID, save in 7 bytes in passti.ctlinfo.uid.value
         idx+=7;
         cmd[idx++] = (byte)0xFF;
         cmd[idx++] = 0x00;
         cmd[idx++] = 0x00;
         cmd[idx++] = 0x05;
         cmd[idx++] = 0x00;
         cmd[idx++] = (byte)0x80;
         cmd[idx++] = 0x00;
         cmd[idx++] = 0x00;
         cmd[idx++] = 0x00;
         //Random desfire 8B

         System.arraycopy(ctl_bri.getRandom(),0,cmd,29,ctl_bri.getRandom().length);
         idx+=ctl_bri.getRandom().length;

         command = new byte[idx];
         System.arraycopy(cmd,0,command,0,idx);
		 RAPDU = device.sendSAM(samslot,command);
         if(RAPDU == null){
             return ERR_NO_RESP;
         }
         if(APDUHelper.CheckResult(RAPDU) != 0){
             //Log.d(TAG,"BRI_SAM_ERR_SELECTAID - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
             return ERR_SW1SW2;
         }

         System.arraycopy(RAPDU,16,random,0,random.length);
         ctl_bri.setRandom(random);
         return OK;
     }

    int BRI_SAM_GetRandom2()
    {

        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx;
        int result;
        byte[] cmd = new byte[50];
        byte[] command ;
        byte[] random = new byte[16];
        idx=0;
        cmd[idx++] = (byte)0x80;
        cmd[idx++] = (byte)0xB0;
        cmd[idx++] = 0x00;
        cmd[idx++] = 0x00;
        cmd[idx++] = 0x20;
        //card number 8B
        System.arraycopy(passti.ctlinfo.cardno,0,cmd,idx,passti.ctlinfo.len_cardno);
        idx+=passti.ctlinfo.len_cardno;
        //UID 7B + FF
        //memcpy(&cmdSendToSAM[idx],&uid[0],7);
        //idx+=7;
        byte[] tempuid = Arrays.copyOfRange(passti.ctlinfo.uid.value, 1, passti.ctlinfo.uid.len);
        System.arraycopy(tempuid,0,cmd,idx,7);//2020-06-15,device will return 7 bytes UID, save in 7 bytes in passti.ctlinfo.uid.value
        idx+=7;
        cmd[idx++] = (byte)0xFF;
        cmd[idx++] = 0x00;
        cmd[idx++] = 0x00;
        cmd[idx++] = 0x03;
        cmd[idx++] = 0x00;
        cmd[idx++] = (byte)0x80;
        cmd[idx++] = 0x00;
        cmd[idx++] = 0x00;
        cmd[idx++] = 0x00;
        //Random desfire 8B

        System.arraycopy(ctl_bri.getRandom(),0,cmd,29,ctl_bri.getRandom().length);
        idx+=ctl_bri.getRandom().length;

        command = new byte[idx];
        System.arraycopy(cmd,0,command,0,idx);
        RAPDU = device.sendSAM(samslot,command);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            //Log.d(TAG,"BRI_SAM_ERR_SELECTAID - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }

        System.arraycopy(RAPDU,16,random,0,random.length);
        ctl_bri.setRandom(random);
        return OK;
    }

    int BRI_SAM_SelectAID() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx;
        int result;
        idx=0;

        byte[] command = {(byte) 0x00,(byte) 0xA4,(byte) 0x04,(byte) 0x00,(byte) 0x09,(byte) 0xA0,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x11};
        //result=SendSAM(port,cmdSendToSAM,idx,resp,resplen);
        RAPDU = device.sendSAM(samslot,command);
        if(RAPDU == null){
            return BRI_SAM_ERR_SELECTAID;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            return BRI_SAM_ERR_SELECTAID;
        }
        return OK;
    }


    int CTL_ReadCardHolderDesfire() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        //Log.d(TAG,"CTL_ReadCardHolderDesfire");
        byte[] cmdReadCardHolderBRIDesfire	=  { (byte) 0x90,(byte) 0xBD,(byte) 0x00,(byte) 0x00,(byte) 0x07,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte)0x00};

        //result = SendCTL(cmdReadCardHolderBRIDesfire,sizeof(cmdReadCardHolderBRIDesfire),apdubuf,&apdulen/*,SW_9100*/);
        RAPDU = device.sendCTL(cmdReadCardHolderBRIDesfire);
        Log.d("rapducardholder", ""+Hex.bytesToHexString(RAPDU));
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            //Log.d(TAG,"BRI_CTL_ERR_READCARDHOLDER - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }
        System.arraycopy(RAPDU,3,passti.ctlinfo.cardno,0,8);
        passti.ctlinfo.len_cardno=8;
        Log.d("cardnocardholder", ""+Hex.bytesToHexString(passti.ctlinfo.cardno));
        //Log.d(TAG,"CardNo: " + BytesUtil.bytes2HexString(cardNo));
        //CardNo = BytesUtil.bytes2HexString(passti.ctlinfo.cardno);

        return OK;
    }

    int CTL_ReadCIStatus() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        int result;
        byte[] cmdReadCIStatusBRIDesfire	=  { (byte) 0x90,(byte) 0xBD,(byte) 0x00,(byte) 0x00,(byte) 0x07,(byte) 0x01,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte)0x00};
        //result = SendCTL(cmdReadCIStatusBRIDesfire,sizeof(cmdReadCIStatusBRIDesfire),apdubuf,&apdulen/*,SW_9100*/);
        RAPDU = device.sendCTL(cmdReadCIStatusBRIDesfire);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            //Log.d(TAG,"BRI_CTL_ERR_READCISTATUS - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }
        if(!(RAPDU[3] == 0x61 && RAPDU[4] == 0x61))
            return BRI_CTL_ERR_APPNOTACTIVE;

        return OK;
    }

    int CTL_SelectAID_500000() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx=0;
        int result;
        byte[] cmd = {(byte) 0x90,(byte) 0x5A,(byte) 0x00,(byte) 0x00,(byte) 0x03,(byte) 0x05,(byte) 0x00,(byte) 0x00,(byte) 0x00};


        RAPDU = device.sendCTL(cmd);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            //Log.d(TAG,"BRI_CTL_ERR_SELECTAID - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }
        return OK;
    }

    int CTL_SelectAID_300000() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx=0;
        int result;
        byte[] cmd = {(byte) 0x90,(byte) 0x5A,(byte) 0x00,(byte) 0x00,(byte) 0x03,(byte) 0x03,(byte) 0x00,(byte) 0x00,(byte) 0x00};


        RAPDU = device.sendCTL(cmd);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            //Log.d(TAG,"BRI_CTL_ERR_SELECTAID - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }
        return OK;
    }

    int CTL_GetKeyCard() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx=0;
        int result;
        byte[] cmd = {(byte) 0x90,(byte) 0x0A,(byte) 0x00,(byte) 0x00,(byte) 0x01,(byte) 0x00,(byte) 0x00};
        byte[] random = new byte[8];

        RAPDU = device.sendCTL(cmd);
        Log.d("rapdukeycard", ""+Hex.bytesToHexString(RAPDU));
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != OK){
            return ERR_SW1SW2;
        }
        System.arraycopy(RAPDU,0,random,0,random.length);
        Log.d("randomm", ""+Hex.bytesToHexString(random));
        ctl_bri.setRandom(random);
        return OK;
    }

    int CTL_AuthDesfire(byte[] random) throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        byte[] apdu ={(byte) 0x90,(byte)0xAF,(byte)0x00,(byte)0x00,(byte)0x10};
        int idx=0;
        int result;

        byte[] authdata=new byte[8];
        byte[] command = new byte[apdu.length+random.length+1];

        //memcpy(&cmdSendToCL[idx],AuthDesFire,sizeof(AuthDesFire));
        System.arraycopy(apdu,0,command,idx,apdu.length);
        idx+=apdu.length;
        //memcpy(&cmdSendToCL[idx],random,inLen);
        System.arraycopy(random,0,command,idx,random.length);
        idx+=random.length;
//        command[idx++] = 0x00;
        //byte[] command = new byte[idx];
        //System.arraycopy(cmdSendToCL,0,command,0,command.length);
        RAPDU = device.sendCTL(command);
        //result = SendCTL(cmdSendToCL,idx,apdubuf,&apdulen/*,SW_9100*/);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != OK){
            //Log.d(TAG,"BRI_ERR_AUTHDESFIRE - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }

        System.arraycopy(RAPDU,0,authdata,0,authdata.length);
        ctl_bri.setAuthData(authdata);

        return OK;
    }

    int CTL_GetLastTrx() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] cmd=  {(byte) 0x90,(byte) 0xBD,(byte) 0x00,(byte) 0x00,(byte) 0x07,(byte) 0x03,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x07,(byte) 0x00,(byte) 0x00,(byte)0x00};
//        byte[] cmd=  {(byte) 0x90,(byte) 0xBD,(byte) 0x00,(byte) 0x00,(byte) 0x07,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x20,(byte) 0x00,(byte) 0x00,(byte)0x00};
        byte[] lasttrxdate=new byte[3];
        byte[] akumdebet;


        RAPDU = device.sendCTL(cmd);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != OK){
            //Log.d(TAG,"BRI_CTL_ERR_GETLASTTRX - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }
        akumdebet = new byte[4];
        System.arraycopy(RAPDU,0,lasttrxdate,0,3);
        ctl_bri.cardinfo.setLastTrxDate(lasttrxdate);
        System.arraycopy(RAPDU,3,akumdebet,0,4);
        ctl_bri.cardinfo.setAkumDebet(akumdebet);
        //Log.d(TAG,"akumDebit: " + BytesUtil.bytes2HexString(akumdebet));
        //lAkumDebet = converter.ByteArrayToInt(akumdebet,0,akumdebet.length, converter.LITTLE_ENDIAN);


        return OK;
    }

    int CTL_GetBalanceDesfire() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] cmdGetBalanceBRIDesfire		=  {(byte) 0x90,(byte) 0x6C,(byte) 0x00,(byte) 0x00,(byte) 0x01,(byte) 0x00,(byte)0x00};

        //result = SendCTL(cmdGetBalanceBRIDesfire,sizeof(cmdGetBalanceBRIDesfire),apdubuf,&apdulen/*,SW_9100*/);
        RAPDU = device.sendCTL(cmdGetBalanceBRIDesfire);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != OK){
            //Log.d(TAG,"BRI_ERR_GETBALANCEDESFIRE - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return NOT_OK;
        }

        passti.ctlinfo.bBalanceKartu=converter.ChangeEndian(RAPDU,4);
        passti.ctlinfo.lBalanceKartu=converter.ByteArrayToInt(passti.ctlinfo.bBalanceKartu,0,passti.ctlinfo.bBalanceKartu.length, converter.BIG_ENDIAN);
        Balance = String.valueOf(passti.ctlinfo.lBalanceKartu);
        //saldoAwal = Balance;
        return OK;
    }

    public int BRI_CTL_SelectAIDDesfire() throws RemoteException {
        int result;
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmdSelectAIDBRIDesfire		=  { (byte) 0x90, (byte) 0x5A, (byte) 0x00,(byte) 0x00,(byte) 0x03,(byte) 0x01,(byte) 0x00,(byte) 0x00,(byte)0x00};

        //result = SendCTL(cmdSelectAIDBRIDesfire,sizeof(cmdSelectAIDBRIDesfire),apdubuf,&apdulen/*,SW_9100*/);
        RAPDU = device.sendCTL(cmdSelectAIDBRIDesfire);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != OK){
            //Log.d(TAG,"BRI_CTL_ERR_SELECTAID - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }

        return OK;
    }

    int BRI_CTL_GetKeyCard02() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx=0;
        int result;
        byte[] command = {(byte) 0x90,(byte) 0x0A,(byte) 0x00,(byte) 0x00,(byte) 0x01,(byte) 0x02,(byte) 0x00};

        RAPDU = device.sendCTL(command);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != OK){
            //Log.d(TAG,"BRI_CTL_ERR_GETKEYCARD - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }

        return OK;

    }

    private int bridesfire_deduct(String sAmount) throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx;
        int result;


        byte[] amount;

        int ldeductAmount;
        byte[] localamount = new byte[5];


        int i = bridesfire_checkbalance_v2();
        if (i != OK) {
            return i;
        }



        if(passti.GetAction()== REPURCHASE)
        {
            if(BytesUtil.bytes2HexString(passti.ctlinfo.cardno).matches(BytesUtil.bytes2HexString(passti.deduct.prevCardNo)))
            {
                //if (BytesUtil.bytecmp(prevCardBalance,balance,balance.length) == OK)
                passti.dbghelper.DebugPrintString("User provide same card no, continue process autocompletion");
                passti.dbghelper.DebugHex("deduct.prevBalance",passti.deduct.prevBalance,4);
                passti.dbghelper.DebugHex("ctlinfo.bBalanceKartu",passti.ctlinfo.bBalanceKartu,4);
                if(Arrays.equals(passti.deduct.prevBalance,passti.ctlinfo.bBalanceKartu)==true)
                {
                    //sAmount = Integer.toString(converter.ByteArrayToInt(prevAmount, 0, prevAmount.length, converter.BIG_ENDIAN));
                    passti.dbghelper.DebugPrintString("Card has not been deducted, deduct with amount=passti.deduct.prevAmt");
                    sAmount = Integer.toString(converter.ByteArrayToInt(passti.deduct.prevAmt, 0, passti.deduct.prevAmt.length, converter.BIG_ENDIAN));
                } else {
                    passti.dbghelper.DebugPrintString("Card has been deducted, deduct with amount=0");
                    sAmount = "0";
                }


            } else {
                //different card
                return ERR_WRONGCARDNO;
            }
        }

        ldeductAmount = Integer.valueOf(sAmount);
        amount= converter.InttoByteArray(ldeductAmount, converter.BIG_ENDIAN);

        //byte[] balance_new = new byte[4];
        if (Integer.valueOf(saldoAwal) < ldeductAmount) {
            return CTL_ERR_INSUFFICIENTBALANCE;}

        System.arraycopy(amount,0,passti.deduct.baAmount,0,4);

//ctldebit


        //result = CTL_Debit(bAmount);\
        /*
        * note on variable amount :
        * variable amount = 0 in CTL_Debit during autocompletion, and then set to previous deduct amount in SAM_CreateHash
        * variable amount = current amount in CTL_Debit and SAM_CreateHash during normal process
        * */
        result = CTL_Debit(amount);
        if(result!=OK)
            return BRI_CTL_ERR_DESFIREDEBIT;
        //create hash
        Log.d(TAG,"Create Hash");
        if((passti.GetAction()==REPURCHASE) && (ldeductAmount==0))//2019-03-26,vivo, if correction,
        {
            passti.dbghelper.DebugPrintString("if in correction mode and deduct amount = 0, use previous amount to create settlement");
            passti.dbghelper.DebugInteger("ldeductAmount",ldeductAmount);
            System.arraycopy(passti.deduct.prevAmt,0,amount,0,4);
            System.arraycopy(amount,0,passti.deduct.baAmount,0,4);
        }
        /*byte[] time = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyHHmmss"));


        result=SAM_CreateHash( Long.toString(ctl_bri.lRefNo),amount,time);*/
        String strTime = util.Datenow("ddMMyyHHmmss");
        byte[] time =ByteUtils.hexStringToByteArray(strTime);
        result=SAM_CreateHash( amount,strTime);
        if(result!=OK)
            return BRI_SAM_ERR_DESFIRECREATEHASH;

        result=CTL_WriteLog(passti.deduct.baAmount,time);
        if(result!=OK) {
            if(result==ERR_SW1SW2) {
                CTL_AbortTransaction();
            }
            return BRI_CTL_ERR_DESFIREWRITELOG;
        }


        //write last transaction


        //Log.d(TAG,"write last transaction");
        result=CTL_WriteLastTrx(passti.deduct.baAmount,time);
        if(result!=OK) {
            if(result==ERR_SW1SW2) {
                CTL_AbortTransaction();
            }
            return BRI_CTL_ERR_DESFIREWRITELASTTRX;
        }


        //commit transaction
        result = CTL_CommitTransaction();
        //result=BRI_TestAutoCorrection();
        if (result != OK) {


            passti.SetAction(params.REPURCHASE);

            return BRI_CTL_ERR_DESFIRECOMMITTRX;
        }


        Log.d(TAG,"calculate new balance");
        if(passti.GetAction()==NORMAL)
        {
            passti.ctlinfo.lBalanceKartu-=ldeductAmount;
            saldoAwal = Balance;
            saldoAkhir=String.valueOf(passti.ctlinfo.lBalanceKartu);
        }
        else
        {
            saldoAwal= String.valueOf(converter.ByteArrayToInt(passti.deduct.prevBalance,0,4,converter.BIG_ENDIAN));
            saldoAkhir=String.valueOf(passti.ctlinfo.lBalanceKartu);
        }



        //Log.d(TAG,"refno "+refno);
        RAPDU = BRI_ComposeLog();
        //byte[] devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
        byte[] ddmmyyhhmmss = ByteUtils.hexStringToByteArray(strTime);
        byte[] devicetime = bcd_ddmmyyhhmmss_to_bcd_ddmmyyyyhhmmss(ddmmyyhhmmss);
        byte[] tempuid = Arrays.copyOfRange(passti.ctlinfo.uid.value, 1, passti.ctlinfo.uid.len);
        passti.log = passti.PASSTI_ComposeTransactionLog(tempuid,params.ctype_bri,devicetime,passti.ctlinfo.cardno,passti.ctlinfo.bBalanceKartu,passti.deduct.baAmount,trxno,RAPDU,others);

        passti.SetAction(NORMAL);



        return OK;
    }


     int CTL_WriteLastTrx(byte[] amount,byte[] time)  throws RemoteException
     {
         passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                 .getStackTrace()[0]
                 .getMethodName());
         int idx = 0;
         byte[] cmdSendToCL = new byte[255];
         byte[] command;
         byte[] cmdWriteLastTrx	=  {(byte) 0x90,(byte) 0x3D,(byte) 0x00,(byte) 0x00,(byte) 0x0E,(byte) 0x03,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x07,(byte) 0x00,(byte) 0x00};
         byte[] akumdebet= new byte[4];
         long total;
         Arrays.fill(cmdSendToCL, (byte) 0x00);
         System.arraycopy(cmdWriteLastTrx, 0, cmdSendToCL, idx, cmdWriteLastTrx.length);
         idx += cmdWriteLastTrx.length;

         System.arraycopy(time, 0, cmdSendToCL, idx, 3);
         idx += 3;
         if (ctl_bri.cardinfo.lasttrxdate[1] != time[1]) {
             //memcpy(ctl_bri.akumdebet,amount,4);
             System.arraycopy(amount, 0, akumdebet, 0, 4);
         } else {
             total = ctl_bri.cardinfo.AkumDebet() + converter.ByteArrayToInt(amount,0,4,converter.BIG_ENDIAN);
             akumdebet = converter.IntegerToByteArray((int)total);
         }

         System.arraycopy(akumdebet, 0, cmdSendToCL, idx, 4);
         idx += 4;

         idx++;//for trailing zero
         command = new byte[idx];
         System.arraycopy(cmdSendToCL, 0, command, 0, command.length);
         RAPDU = device.sendCTL(command);
         if (RAPDU == null) {
             return ERR_NO_RESP;
         }
         if (APDUHelper.CheckResult(RAPDU) != OK) {
             CTL_AbortTransaction();
             return ERR_SW1SW2;
         }
         return OK;
     }
     int CTL_WriteLog(byte[] amount,byte[] time)
     {
         passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                 .getStackTrace()[0]
                 .getMethodName());
         /*
         * amount in bigendian
         * */
         byte[] cmdWriteLog=  {(byte) 0x90,(byte) 0x3B,(byte) 0x00,(byte) 0x00,(byte) 0x27,(byte) 0x01,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x20,(byte) 0x00,(byte) 0x00};
         int idx=0;
         byte[] cmdSendToCL = new byte[255];
         //byte[] merchantid;
         byte[] command;
         byte[] amount_reverse;
         byte[] balance_new = new byte[4];
         byte[] balance_old = new byte[4];
         Arrays.fill(cmdSendToCL, (byte) 0x00);
         System.arraycopy(cmdWriteLog, 0, cmdSendToCL, idx, cmdWriteLog.length);
         idx += cmdWriteLog.length;

         //merchantid = BytesUtil.hexString2Bytes(MID);

         System.arraycopy(devinfo.issuer.briMid, 0, cmdSendToCL, idx, devinfo.issuer.briMid.length);
         idx += devinfo.issuer.briMid.length;
         //memcpy(&cmdSendToCL[idx],&hash_terminalid[0],sizeof(hash_terminalid));
         System.arraycopy(issuer.str_briTid.getBytes(), 0, cmdSendToCL, idx, issuer.str_briTid.getBytes().length);
         idx += issuer.str_briTid.getBytes().length;



         System.arraycopy(time, 0, cmdSendToCL, idx, time.length);
         idx += time.length;
         cmdSendToCL[idx++] = (byte) 0xEB;


         amount_reverse=converter.ChangeEndian(amount,4);
         System.arraycopy(amount_reverse, 0, cmdSendToCL, idx, 3);
         idx += 3;
         //memcpy(&cmdSendToCL[idx],&ctlinfo.bBalanceKartu,3);
         balance_old = converter.ChangeEndian(passti.ctlinfo.bBalanceKartu,4);
         System.arraycopy(balance_old, 0, cmdSendToCL, idx, 3);
         idx += 3;

         //memcpy(&cmdSendToCL[idx],&balance_new,3);
         int newBalance = Integer.valueOf(saldoAwal) - converter.ByteArrayToInt(amount,0,4,converter.BIG_ENDIAN);
         balance_new = converter.InttoByteArray(newBalance, converter.LITTLE_ENDIAN);
         System.arraycopy(balance_new, 0, cmdSendToCL, idx, 3);
         idx += 3;

         idx++;//for trailing zero
         command = new byte[idx];
         System.arraycopy(cmdSendToCL, 0, command, 0, command.length);
         RAPDU = device.sendCTL(command);
         if (RAPDU == null) {
             return ERR_NO_RESP;
         }
         if (APDUHelper.CheckResult(RAPDU) != OK) {

             return ERR_SW1SW2;
         }
         return OK;
     }



     int SAM_CreateHash(byte[] amount,String time)
     {
         passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                 .getStackTrace()[0]
                 .getMethodName());
         byte[] cmdSAMCreateHash=  {(byte) 0x80,(byte) 0xB4,(byte) 0x00,(byte) 0x00,(byte) 0x58};
         int idx=0;
         byte[] hash = new byte[4];
         byte[] cmdSendToSAM = new byte[255];

         byte[] padding ={(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF};
         byte[] command;
         long lAmt;

         hash_cardno = BytesUtil.bytes2HexString(passti.ctlinfo.cardno);

         //timestamp = BytesUtil.bytes2HexString(time);
         timestamp = time;

         //2021-01-21,vivo, convert amount from hex to integer
         lAmt=converter.ByteArrayToInt(amount,0,amount.length,converter.BIG_ENDIAN);
         hash_amount = String.format("%08d", lAmt);
         //hash_amount = BytesUtil.bytes2HexString(amount);
         hash_amount+="00";//add decimal value
         hash_refno = ctl_bri.getRefNo();

         hash_batchno = BytesUtil.byteArray2HexString(issuer.briBatch);

         Arrays.fill(cmdSendToSAM, (byte) 0x00);
         idx = 0;

         System.arraycopy(cmdSAMCreateHash, 0, cmdSendToSAM, idx, cmdSAMCreateHash.length);
         idx += cmdSAMCreateHash.length;

         System.arraycopy(passti.ctlinfo.cardno, 0, cmdSendToSAM, idx, passti.ctlinfo.len_cardno);
         idx += passti.ctlinfo.len_cardno;

         //System.arraycopy(passti.ctlinfo.uid.value, 1, cmdSendToSAM, idx, 7);
         byte[] tempuid = Arrays.copyOfRange(passti.ctlinfo.uid.value, 1, passti.ctlinfo.uid.len);
         System.arraycopy(tempuid, 0, cmdSendToSAM, idx, 7);//device return 7 bytes UID, and passti.ctlinfo.uid.value store 7 bytes
         idx += 7;

         System.arraycopy(cmddata, 0, cmdSendToSAM, idx, cmddata.length);
         idx += cmddata.length;

         System.arraycopy(ctl_bri.getAuthdata(), 0, cmdSendToSAM, idx, 8);
         idx += 8;

         System.arraycopy(hash_cardno.getBytes(), 0, cmdSendToSAM, idx, hash_cardno.getBytes().length);
         idx += hash_cardno.getBytes().length;

         System.arraycopy(hash_amount.getBytes(), 0, cmdSendToSAM, idx, hash_amount.getBytes().length);
         idx += hash_amount.getBytes().length;

         System.arraycopy(timestamp.getBytes(), 0, cmdSendToSAM, idx, timestamp.getBytes().length);
         idx += timestamp.getBytes().length;

         System.arraycopy(issuer.briProcode, 0, cmdSendToSAM, idx, issuer.briProcode.length);
         idx += issuer.briProcode.length;

         System.arraycopy(hash_refno.getBytes(), 0, cmdSendToSAM, idx, hash_refno.getBytes().length);
         idx += hash_refno.getBytes().length;

         System.arraycopy(hash_batchno.getBytes(), 0, cmdSendToSAM, idx, hash_batchno.getBytes().length);
         idx += hash_batchno.getBytes().length;

         System.arraycopy(padding, 0, cmdSendToSAM, idx, padding.length);
         idx += padding.length;



         command = new byte[idx];
         System.arraycopy(cmdSendToSAM, 0, command, 0, command.length);
         RAPDU = device.sendSAM(samslot,command);
         if (RAPDU == null) {

             passti.SetAction(params.REPURCHASE);
             //CTL_AbortTransaction();
             return ERR_NO_RESP;
         }
         if (APDUHelper.CheckResult(RAPDU) != OK) {

             //CTL_AbortTransaction();
             return ERR_NO_RESP;
         }
         System.arraycopy(RAPDU,0,hash,0,4);
         ctl_bri.setSamHash(hash);

         return OK;
     }



     int CTL_Debit(byte[] amount)
     {
         /*
         amount in big endian
         */

         passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                 .getStackTrace()[0]
                 .getMethodName());

         byte[] cmdDebitBalanceBRIDesfire	=  {(byte) 0x90,(byte) 0xDC,(byte) 0x00,(byte) 0x00,(byte) 0x05};
         byte[] command=new byte[cmdDebitBalanceBRIDesfire.length+amount.length+1+1];
         byte[] le_amount;
         int idx=0;
         System.arraycopy(cmdDebitBalanceBRIDesfire, 0, command, idx, cmdDebitBalanceBRIDesfire.length);
         idx += cmdDebitBalanceBRIDesfire.length;
         command[idx++] = 0x00;
         le_amount=converter.ChangeEndian(amount,4);//converto to little endian
         System.arraycopy(le_amount,0,command,idx,le_amount.length);
         idx+=le_amount.length;
         command[idx++] = 0x00;//trailing zero

         RAPDU = device.sendCTL(command);
         if (RAPDU == null) {
             passti.SetAction(params.REPURCHASE);
             return ERR_NO_RESP;
         }
         if (APDUHelper.CheckResult(RAPDU) != OK) {
             return ERR_SW1SW2;
         }

         return OK;
     }


     int CTL_CommitTransaction() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] cmdCommitTrx=  {(byte) 0x90,(byte) 0xC7,(byte) 0x00,(byte) 0x00,(byte) 0x00};

        //result = SendCTL(cmdCommitTrx,sizeof(cmdCommitTrx),apdubuf,&apdulen/*,SW_9100*/);
        RAPDU = device.sendCTL(cmdCommitTrx);
        if(RAPDU == null){
            return BRI_CTL_ERR_DESFIRECOMMITTRX;
        }
        if(APDUHelper.CheckResult(RAPDU) != OK){
            CTL_AbortTransaction();
            //Log.d(TAG,"BRI_CTL_ERR_COMMIT - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return BRI_CTL_ERR_DESFIRECOMMITTRX;
        }


        return OK;
    }



    byte[] BRI_ComposeLog()
    {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] temp = new byte[256];
        int idx=0;

        String hash_terminal = "FFFFFFFF";

        Log.d(TAG,"hash_cardno "+hash_cardno);
        Log.d(TAG,"timestamp "+timestamp);
        Log.d(TAG,"hash_amount "+hash_amount);
        Log.d(TAG,"hash_refno "+hash_refno);
        Log.d(TAG,"hash_batchno "+hash_batchno);
        Log.d(TAG,"str_briTid "+devinfo.issuer.str_briTid);
        Log.d(TAG,"str_briMid "+devinfo.issuer.str_briMid);
        Log.d(TAG,"issuer.briProcode "+Hex.bytesToHexString(issuer.briProcode));
        Log.d(TAG,"ctl_bri.getSamhash() "+ctl_bri.getSamhash());




        System.arraycopy(hash_cardno.getBytes(),0,temp,idx,hash_cardno.getBytes().length);
        idx+=hash_cardno.getBytes().length;
        //memcpy(&temp[idx],timestamp,sizeof(timestamp));
        System.arraycopy(timestamp.getBytes(),0,temp,idx,timestamp.getBytes().length);
        idx+=timestamp.getBytes().length;
        //memcpy(&temp[idx],hash_amount,sizeof(hash_amount));
        System.arraycopy(hash_amount.getBytes(),0,temp,idx,hash_amount.getBytes().length);
        idx+=hash_amount.getBytes().length;

        //memcpy(&temp[idx],hash_refno,sizeof(hash_refno));
        System.arraycopy(hash_refno.getBytes(),0,temp,idx,hash_refno.getBytes().length);
        idx+=hash_refno.getBytes().length;
        //memcpy(&temp[idx],hash_batchno,sizeof(hash_batchno));
        System.arraycopy(hash_batchno.getBytes(),0,temp,idx,hash_batchno.getBytes().length);
        idx+=hash_batchno.getBytes().length;
        //memcpy(&temp[idx],&hash_merchantid[1],15);
        System.arraycopy(devinfo.issuer.str_briMid.getBytes(),1,temp,idx,15);
        idx+=15;
        //memcpy(&temp[idx],hash_terminalid,sizeof(hash_terminalid));

        System.arraycopy(devinfo.issuer.str_briTid.getBytes(),0,temp,idx,devinfo.issuer.str_briTid.getBytes().length);
        idx+=devinfo.issuer.str_briTid.getBytes().length;
        //memcpy(&temp[idx],hash_brizzi,sizeof(hash_brizzi));
        System.arraycopy(ctl_bri.getSamhash().getBytes(),0,temp,idx,ctl_bri.getSamhash().getBytes().length);
        idx+=ctl_bri.getSamhash().getBytes().length;

        //memcpy(&temp[idx],hash_procode,sizeof(hash_procode));
        System.arraycopy(issuer.briProcode,0,temp,idx,issuer.briProcode.length);
        idx+=issuer.briProcode.length;
        //memcpy(out,temp,idx);
        //*outLen=idx;
        byte[] out = new byte[idx];
        System.arraycopy(temp,0,out,0,out.length);
        Log.d(TAG,"BRI Log " + Hex.bytesToHexString(out));
        Log.d(TAG,"BRI Log out " + new String(out));
        return out;
    }

    int CTL_AbortTransaction() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] cmdAbortTrx =  {(byte) 0x90,(byte) 0xA7,(byte) 0x00,(byte) 0x00,(byte) 0x00};


        RAPDU = device.sendCTL(cmdAbortTrx);
        if(RAPDU == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(RAPDU) != OK){
            //Log.d(TAG,"CTL_ERR_DESFIREABORTTRX - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }

        return  OK;
    }

     public void SetRefNo(String trxrefno)
     {
         passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                 .getStackTrace()[0]
                 .getMethodName());
         Log.d(TAG,"trxrefno "+trxrefno);

         int ref=Integer.parseInt(trxrefno);
         Log.d(TAG,"ref "+ref);
         ctl_bri.setRefno(trxrefno);
     }

     int BRI_TestAutoCorrection()
     {
         passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                 .getStackTrace()[0]
                 .getMethodName());
         if(passti.GetAction()==NORMAL)
         {

             return ERR_NO_RESP;
         }
         if(passti.GetAction()==REPURCHASE)
         {
             return OK;
         }
         return OK;

     }
}
