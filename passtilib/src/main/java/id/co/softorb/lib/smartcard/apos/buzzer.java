package id.co.softorb.lib.smartcard.apos;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.usdk.apiservice.aidl.UDeviceService;
import com.usdk.apiservice.aidl.beeper.UBeeper;
import com.usdk.apiservice.aidl.constants.RFDeviceName;
import com.usdk.apiservice.aidl.data.BytesValue;
import com.usdk.apiservice.aidl.rfreader.RFError;
import com.usdk.apiservice.aidl.rfreader.URFReader;

import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.Base;

import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INITREADER;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;

public class buzzer extends Base {
    String tagclass= this.getClass().getSimpleName();
    private UBeeper beeper;
    private UDeviceService deviceService;
    @Override
    public int init(POSListener detectListener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            deviceService = (UDeviceService) devInstance;
            Log.d("device service",""+deviceService.getVersion());
            beeper = (UBeeper) UBeeper.Stub.asInterface(deviceService.getBeeper());

            if (beeper!=null) {
                Log.d("result init buzzer apos","ok");
                return OK;
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());
        }
        return NOT_OK;
    }

    public void startBeep(int milliseconds){
        try {
            beeper.startBeep(milliseconds);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int open() {
        return 0;
    }

    @Override
    public int close() {
        return 0;
    }

    @Override
    public int find() {
        return 0;
    }

    @Override
    public void stopfind() {

    }
}
