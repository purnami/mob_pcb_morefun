package id.co.softorb.lib.smartcard.posdevice;


import id.co.softorb.lib.smartcard.POSListener;

public abstract class Base {
    //public abstract int init();
    public abstract int init(POSListener detectListener, Object devInstance);
    public abstract int open();
    public abstract int close();
    public abstract int find();
    public abstract void stopfind();
    public   int waitingtime=10000;
    protected String TAG = "PasstiMultiDevice";
}
