package id.co.softorb.lib.smartcard.posdevice;

public abstract class ContactlessBase extends Base{
    public abstract byte[] sendCTL(byte[] apdu);
    public abstract void setMode(int mode);
    public  int mode=0;
}
