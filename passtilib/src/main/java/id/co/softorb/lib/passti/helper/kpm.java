package id.co.softorb.lib.passti.helper;

public class kpm {
    /**
     * Search the data byte array for the first occurrence
     * of the byte array pattern.
     */
    public static int indexOf(byte[] rawdata,int offset, byte[] pattern) {
        int[] failure = computeFailure(pattern);

        int j = 0;
        if(offset>rawdata.length)
            return -1;
        byte [] data = new byte[rawdata.length-offset];
        System.arraycopy(rawdata,offset,data,0,data.length);
        for (int i = 0; i < data.length; i++) {
            while (j > 0 && pattern[j] != data[i]) {
                j = failure[j - 1];
            }
            if (pattern[j] == data[i]) {
                j++;
            }
            if (j == pattern.length) {
                return i - pattern.length + 1;
            }
        }
        return -1;
    }

    /**
     * Computes the failure function using a boot-strapping process,
     * where the pattern is matched against itself.
     */
    private static int[] computeFailure(byte[] pattern) {
        int[] failure = new int[pattern.length];

        int j = 0;
        for (int i = 1; i < pattern.length; i++) {
            while (j>0 && pattern[j] != pattern[i]) {
                j = failure[j - 1];
            }
            if (pattern[j] == pattern[i]) {
                j++;
            }
            failure[i] = j;
        }

        return failure;
    }
}
