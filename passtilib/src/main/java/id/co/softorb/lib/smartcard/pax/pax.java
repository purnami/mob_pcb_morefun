package id.co.softorb.lib.smartcard.pax;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.SystemClock;
import android.util.Log;

import com.pax.dal.IDAL;
import com.pax.dal.IMag;
import com.pax.dal.IPicc;
import com.pax.dal.ISys;
import com.pax.dal.entity.EBeepMode;
import com.pax.dal.entity.EDetectMode;
import com.pax.dal.entity.EPiccRemoveMode;
import com.pax.dal.entity.EPiccType;
import com.pax.dal.entity.ETermInfoKey;
import com.pax.dal.entity.PiccCardInfo;
import com.pax.dal.entity.TrackData;
import com.pax.dal.exceptions.EPiccDevException;
import com.pax.dal.exceptions.MagDevException;
import com.pax.dal.exceptions.PiccDevException;
import com.pax.neptunelite.api.NeptuneLiteUser;

import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.ContactlessBase;
import id.co.softorb.lib.smartcard.posdevice.DeviceBase;
import id.co.softorb.lib.smartcard.posdevice.DeviceFunction;

import static id.co.softorb.lib.helper.ErrorCode.*;

public class pax extends DeviceFunction {

    String tagclass= this.getClass().getSimpleName();

    private IDAL dal;
    private ISys iSys = null;
    Context ctx;
    private msr msrReader;
    private contactless ctlReader;
    private contact cbReader;
    private printer posprinter;
    @Override
    public int FindMSR() {
        if(msrReader==null)
            return NOT_OK;
        msrReader.find();
        return WAITING;
    }

    @Override
    public int FindCTL(int mode) {
        ctlReader.setMode(mode);
        if(ctlReader==null)
            return NOT_OK;
        ctlReader.find();
        return WAITING;
    }

    @Override
    public int FindCB() {
        if(cbReader==null)
            return NOT_OK;
        cbReader.find();
        return WAITING;
    }

    @Override
    public int FindSAM(int type, int slot) {
        return ERR_NOTSUPPORTED;
    }

    @Override
    public int printercutpaper() {
        return posprinter.cutpaper();
    }

    @Override
    public int printerlinefeed() {
        return posprinter.linefeed();
    }

    @Override
    public int printerstatus() {
        return posprinter.status();
    }

    @Override
    public int printbitmap(int alignment, Bitmap bitmap) {
        return posprinter.bitmap(alignment,bitmap);
    }

    @Override
    public int print_text(int fonttype, int alignment, String text, boolean newline) {
        return posprinter.text(fonttype,alignment,text,newline);
    }

    @Override
    public void setcardwaitingtime(int timeout) {
        waitingtime=timeout;
    }

    @Override
    public String PinPad_SN() {
        return null;
    }
/*
    @Override
    public byte[] PinPad_GenerateRandom(int length) {
        return new byte[0];
    }
*/
    @Override
    public int PinPad_SetPINLength(int min, int max) {
        return 0;
    }

    @Override
    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value) {
        return 0;
    }

    @Override
    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value, byte[] checksum) {
        return 0;
    }

    @Override
    public byte[] PinPad_GetSessionKey(int mkid, int userkeyid, int algo) {
        return new byte[0];
    }

    @Override
    public byte[] PinPad_Encrypt(int mkid, int userkeyid, int algo, byte[] data) {
        return new byte[0];
    }

    @Override
    public byte[] PinPad_Calculatemac(int mkid, int userkeyid, int keyalgo, int macalgo, byte[] data) {
        return new byte[0];
    }

    @Override
    public byte[] PinPad_Calculatedukptmac(byte[] data) {
        return new byte[0];
    }

    @Override
    public int PinPad_Verifymac(int mkid, int keyid, byte[] data, byte[] mac) {
        return 0;
    }

    @Override
    public int PinPad_async_pinblock(int mkid, int keyid, byte[] data) {
        return 0;
    }

    @Override
    public byte[] PinPad_sync_pinblock(int mkid, int keyid, byte[] data) {
        return new byte[0];
    }

    @Override
    public int PinPad_getMKStatus(int mkid) {
        return 0;
    }

    @Override
    public int PinPad_getSKStatus(int mkid, int skid) {
        return 0;
    }

    @Override
    public int PinPad_getDUKPTStatus(int mkid, byte[] uniquekey) {
        return 0;
    }

    public pax(Context ctx)
    {
        this.ctx=ctx;
        msrReader= new msr();
        ctlReader = new contactless();
        cbReader = new contact();
        posprinter=new printer();
    }


    @Override
    public int init(POSListener listener, Object instance) {
        int result=ERR_INITMSR;
        if(dal == null){
            try {
                long start = System.currentTimeMillis();
                dal = NeptuneLiteUser.getInstance().getDal(ctx);
                Log.i(TAG,"get dal cost:"+(System.currentTimeMillis() - start)+" ms");
                iSys= dal.getSys();
                result=msrReader.init(listener,dal);
                if(result!=OK)
                    return result;
                result=ctlReader.init(listener,dal);
                if(result!=OK)
                    return result;
                result=posprinter.init(listener,dal);
                if(result!=OK)
                    return result;
                result=cbReader.init(listener,dal);
                if(result!=OK)
                    return result;



            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG,"error occurred");

            }
        }
        return result;
    }

    @Override
    public int open() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        result=msrReader.open();
        if(result!=OK)
            return result;
        result=ctlReader.open();
        if(result!=OK)
            return result;
        /*result=cbReader.open(); //cb will be open in find function
        if(result!=OK)
            return result;*/
        result=posprinter.open();
        if(result!=OK)
            return result;
        return result;

    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        result=msrReader.close();
        result=ctlReader.close();
        result=cbReader.close();
        result=posprinter.close();
        return result;
    }

    @Override
    public int find() {
        return WAITING;
    }

    @Override
    public void stopfind() {

    }

    @Override
    public String DeviceSN() {
        if(iSys==null)
            return null;
        return iSys.getTermInfo().get(ETermInfoKey.SN);
    }
    @Override
    public byte[] sendCTL(byte[] apdu) {
        return ctlReader.sendCTL(apdu);
    }

    @Override
    public byte[] sendCB(byte[] apdu) {
        return cbReader.sendCB(0,apdu);
    }

    @Override
    public byte[] sendSAM(int slot, byte[] apdu) {
        return new byte[0];
    }

    @Override
    public String LibVersion() {
        return null;
    }


}

