package id.co.softorb.lib.smartcard.wizar;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.cloudpos.POSTerminal;
import com.cloudpos.TerminalSpec;
import com.cloudpos.smartcardreader.SmartCardReaderDevice;
import com.wizarpos.jni.PinPadCallbackHandler;

import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.DeviceFunction;

import static id.co.softorb.lib.helper.ErrorCode.*;

public class wizarpos extends DeviceFunction  {

    String TAG = "PasstiMultiDevice";
    String tagclass= this.getClass().getSimpleName();
    private msr msrReader;
    private contactless ctlReader;
    private printer posprinter;
    private contact cbReader;
    private pinpad pospinpad;
    private sam samReader;
    private led posled;
    @Override
    public int FindMSR() {
        if(msrReader==null)
            return NOT_OK;
        msrReader.find();
        return WAITING;
    }

    @Override
    public int FindCTL(int mode) {
        ctlReader.setMode(mode);
        if(ctlReader==null)
            return NOT_OK;
        ctlReader.find();
        return WAITING;
    }
    @Override
    public void setcardwaitingtime(int timeout) {
        waitingtime=timeout;
    }

    @Override
    public String PinPad_SN() {
       return pospinpad.SN();
    }
/*
    @Override
    public byte[] PinPad_GenerateRandom(int length) {
        return pospinpad.GenerateRandom(length);
    }
*/
    @Override
    public int PinPad_SetPINLength(int min, int max) {
        return pospinpad.SetPINLength(min,max);
    }

    @Override
    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value) {
        return pospinpad.UpdateUserKey(mkid,userkeyid,value);
    }

    @Override
    public int PinPad_UpdateUserKey(int mkid, int userkeyid, byte[] value, byte[] checksum) {
        return pospinpad.UpdateUserKey(mkid,userkeyid,value,checksum);
    }

    @Override
    public byte[] PinPad_GetSessionKey(int mkid, int userkeyid, int algo) {
        return pospinpad.getSessionKey(mkid,userkeyid,algo);
    }

    @Override
    public byte[] PinPad_Encrypt(int mkid, int userkeyid, int algo, byte[] data) {
        return pospinpad.encrypt(mkid,userkeyid,algo,data);
    }

    @Override
    public byte[] PinPad_Calculatemac(int mkid, int userkeyid, int keyalgo, int macalgo, byte[] data) {
        return pospinpad.calculatemac(mkid,userkeyid,keyalgo,macalgo,data);
    }

    @Override
    public byte[] PinPad_Calculatedukptmac(byte[] data) {
        return pospinpad.calculatedukptmac(data);

    }

    @Override
    public int PinPad_Verifymac(int mkid, int keyid, byte[] data, byte[] mac) {
        return pospinpad.verifymac(mkid,keyid,data,mac);
    }

    @Override
    public int PinPad_async_pinblock(int mkid, int keyid, byte[] data) {
        return pospinpad.async_pinblock(mkid,keyid,data);
    }

    @Override
    public byte[] PinPad_sync_pinblock(int mkid, int keyid, byte[] data) {
        return pospinpad.sync_pinblock(mkid,keyid,data);
    }

    @Override
    public int PinPad_getMKStatus(int mkid) {
        return pospinpad.getMKStatus(mkid);
    }

    @Override
    public int PinPad_getSKStatus(int mkid, int skid) {
        return pospinpad.getSKStatus(mkid,skid);
    }

    @Override
    public int PinPad_getDUKPTStatus(int mkid, byte[] uniquekey) {
        return pospinpad.getDUKPTStatus(mkid,uniquekey);
    }

    @Override
    public int FindCB() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        if(cbReader==null)
            return NOT_OK;
        result=cbReader.find();


        return result;
    }

    @Override
    public int FindSAM(int type, int slot) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(samReader==null)
            return NOT_OK;
        int result=samReader.find();
        return result;
    }

    @Override
    public int printercutpaper() {
        return posprinter.cutpaper();
    }

    @Override
    public int printerlinefeed() {
        return posprinter.linefeed();
    }

    @Override
    public int printerstatus() {
        return posprinter.status();
    }

    @Override
    public int printbitmap(int alignment, Bitmap bitmap) {
        return posprinter.bitmap(alignment,bitmap);
    }

    @Override
    public int print_text(int fonttype, int alignment, String text, boolean newline) {
        return posprinter.text(fonttype,alignment,text,newline);
    }


    Context ctx;
    private POSTerminal posTerminal;
    private TerminalSpec terminalSpec;
   //

    public wizarpos(Context ctx)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        this.ctx=ctx;
        msrReader= new msr();
        ctlReader = new contactless();
        posprinter= new printer();
        cbReader = new contact();
        pospinpad= new pinpad();
        samReader=new sam();
        posled=new led();
    }

    public Object getPinPad()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return pospinpad;
    }
    public Object getLED()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return posled;
    }

    @Override
    public int init(POSListener listener,Object instance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        posTerminal = POSTerminal.getInstance(ctx);
        terminalSpec=posTerminal.getTerminalSpec();
        result=msrReader.init(listener,posTerminal);
        if(result!=OK)
            return result;
        result=ctlReader.init(listener,posTerminal);
        if(result!=OK)
            return result;
        result=posprinter.init(listener,posTerminal);
        if(result!=OK)
            return result;
        result=cbReader.init(listener,posTerminal);
        if(result!=OK)
            return result;
        result=pospinpad.init(listener,posTerminal);
        if(result!=OK)
            return result;
        result=samReader.init(listener,posTerminal);
        if(result!=OK)
            return result;
        result=posled.init(listener,posTerminal);
        if(result!=OK)
            return result;
        return OK;
    }


    @Override
    public int open() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        result=msrReader.open();
        if(result!=OK)
            return result;
        result=ctlReader.open();
        if(result!=OK)
            return result;
        result=posprinter.open();
        if(result!=OK)
            return result;
        result=cbReader.open();
        if(result!=OK)
            return result;
        result=pospinpad.open();
        if(result!=OK)
            return result;
        result=samReader.open();
        if(result!=OK)
            return result;
       /* result=posled.open();
        if(result!=OK)
            return result;*/
        return result;
    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        result=msrReader.close();
        result=ctlReader.close();
        result=posprinter.close();
        result=cbReader.close();
        result=pospinpad.close();
        result=samReader.close();
        //result=posled.close();
        return result;
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return WAITING;
    }

    @Override

    public void stopfind() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        ctlReader.stopfind();
        msrReader.stopfind();
        posprinter.stopfind();
        //cbReader.stopfind();//cb closed only after application destroyed
        pospinpad.stopfind();
        posled.stopfind();

    }

    @Override
    public String DeviceSN() {
        if(posTerminal!=null)
        return posTerminal.getTerminalSpec().getSerialNumber();
        else
            return null;
    }

    @Override
    public byte[] sendCTL(byte[] apdu) {
        return ctlReader.sendCTL(apdu);
    }

    @Override
    public byte[] sendCB(byte[] apdu) {
        return cbReader.sendCB(0,apdu);
    }

    @Override
    public byte[] sendSAM(int slot, byte[] apdu) {
        return samReader.sendCB(slot,apdu);
    }

    @Override
    public String LibVersion() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        String version="unknown";
    if(terminalSpec!=null)
        version = terminalSpec.getAPILevel();


        return version;
    }


}
