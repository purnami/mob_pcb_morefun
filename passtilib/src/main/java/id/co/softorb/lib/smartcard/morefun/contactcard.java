package id.co.softorb.lib.smartcard.morefun;

import android.os.RemoteException;
import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationListener;
import com.cloudpos.card.Card;
import com.cloudpos.smartcardreader.SmartCardReaderDevice;
import com.morefun.yapi.card.cpu.CPUCardHandler;
import com.morefun.yapi.device.reader.icc.IccCardReader;
import com.morefun.yapi.device.reader.icc.IccCardType;
import com.morefun.yapi.device.reader.icc.OnSearchIccCardListener;

import static id.co.softorb.lib.helper.ErrorCode.ERR_INCORRECT_CARDTYPE;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_bca;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_bni;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_bri;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_luminos;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_mdr;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_unknown;

class contactcard
{
    String tagclass= this.getClass().getSimpleName();
    protected String TAG = "PasstiMultiDevice";
    public IccCardReader pSamReader;
    int cardtype;
    boolean state;
    private CPUCardHandler psamCard;
    public contactcard()
    {
        cardtype=ctype_unknown;
        state=false;
    }
    public CPUCardHandler getCard()
    {
        return psamCard;
    }
    public void setCard(CPUCardHandler card)
    {
        psamCard=card;
    }
    public contactcard(int cardtype)
    {
        this.cardtype=cardtype;
    }
    public IccCardReader Reader()
    {
        return pSamReader;
    }
    public int findcard(OnSearchIccCardListener.Stub listener)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            pSamReader.searchCard(listener,0,new String[]{IccCardType.CPUCARD});
        } catch (RemoteException e) {
            e.printStackTrace();
            Log.e(TAG,"error open sam");
            return NOT_OK;
        }

        return WAITING;
    }
    public int setReader(IccCardReader iccCardReader)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(iccCardReader==null)
            return NOT_OK;

        pSamReader=iccCardReader;
        return OK;
    }
    public boolean isOpen()
    {
        return state;
    }
    public int Open()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        state=true;
        return OK;
    }
    public int Close()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        state=false;
        return OK;

    }
    public int setCardType(int type)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        switch(type)
        {
            //valid number for sam slot is 1 and 2
            case ctype_mdr:
            case ctype_bni:
            case ctype_bri:
            case ctype_luminos:
            case ctype_bca:
                break;
            default:
                return ERR_INCORRECT_CARDTYPE;
        }

        this.cardtype=type;
        return OK;
    }


}