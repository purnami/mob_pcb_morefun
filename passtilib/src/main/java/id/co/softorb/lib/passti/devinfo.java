package id.co.softorb.lib.passti;

/**
 * Created by softorb.co.id on 4/24/2019.
 */

public class devinfo {

    static class bank_device
    {
        byte[] TID;
        byte[] MID;
        byte[] bniMid;
        byte[] str_bniMid;
        byte[] bniTid;
        byte[] str_bniTid;
        byte[] bnimrgcode;
        byte[] str_bnimrgcode;
        byte[] lmnsMid;
        byte[] str_lmnsMid;
        byte[] lmnsTid;
        byte[] str_lmnsTid;
        byte[] dkiMid;
        byte[] str_dkiMid;
        byte[] dkiTid;
        byte[] str_dkiTid;
        byte[] dkiBatch;
        byte[] str_dkiBatch;
        byte[] mdrpin;
        byte[] str_mdrpin;
        byte[] mdrInstID;
        byte[] str_mdrInstID;
        byte[] mdrTid;
        byte[] str_mdrTid;
        byte[] briMid;
        String str_briMid;
        byte[] briTid;
        String str_briTid;
        byte[] briBatch;
        String str_briBatch;
        byte[] briProcode;//contains ascii hex
        byte[] bcaMid;
        byte[] bcaPrefixTid;
        byte[] bcaTid;
        byte[] nobuMid;
        byte[] nobuTid;
        byte[] nobuSamToken;
        byte[] nobuAreaKey;
        byte[] megaMid;
        byte[] megaTid;
        byte[] megaTermMK;
        byte[] megaSamPin;
        
        public bank_device()
        {
             TID=new byte[4];
             MID=new byte[8];
             bniMid=new byte[8];
             str_bniMid=new byte[16+1];
             bniTid=new byte[4];
             str_bniTid=new byte[8+1];
             bnimrgcode=new byte[16];
             str_bnimrgcode=new byte[32+1];
             lmnsMid=new byte[8];
             str_lmnsMid=new byte[16+1];
             lmnsTid=new byte[4];
             str_lmnsTid=new byte[8+1];
             dkiMid=new byte[8];
             str_dkiMid=new byte[16+1];
             dkiTid=new byte[4];
             str_dkiTid=new byte[8+1];
             dkiBatch=new byte[3];
             str_dkiBatch=new byte[6+1];
             mdrpin=new byte[8];
             str_mdrpin=new byte[16+1];
             mdrInstID=new byte[2];
             str_mdrInstID=new byte[4+1];
             mdrTid=new byte[4];
             str_mdrTid=new byte[8+1];
             briMid=new byte[8];
             str_briMid="";
             briTid=new byte[4];
             str_briTid="";
             briBatch=new byte[2];
             str_briBatch="";
             briProcode=new byte[6];//contains ascii hex
             bcaMid=new byte[15];
             bcaPrefixTid=new byte[3];
             bcaTid=new byte[8];
             nobuMid=new byte[8];
             nobuTid=new byte[4];
             nobuSamToken=new byte[16];
             nobuAreaKey=new byte[16];
             megaMid=new byte[8];
             megaTid=new byte[4];
             megaTermMK=new byte[16];
             megaSamPin=new byte[8];
        }
        
    }

    protected static byte[] trxno = new byte[4];
    protected static byte[] others = new byte[4];
    protected static String operation = ""; //indicate process execution, valid value "checkbalance","deduct"
    protected static bank_device issuer = new bank_device();
    protected static final int prepaid=0;
    protected static final int emv=1;


}
