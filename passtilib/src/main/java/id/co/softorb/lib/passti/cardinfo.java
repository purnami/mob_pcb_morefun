package id.co.softorb.lib.passti;

public class cardinfo {



        public class UID{
            public byte[] value;
            public int len;
            public byte[] card_type;
            public byte[] data;

            public UID()
            {
                value=new byte[8];
                len=0;
                card_type=new byte[1];
                data=new byte[8];

            }
        }

        UID uid;
        public byte[] value;//store mandiri cardinfo
        public int len_value;//store mandiri cardinfo length
        public byte[] log;//store bank log after successful deduct. used to get bank log if rewrite partner data required
        public int len_log;
        public byte[] cardno;
        public int len_cardno;
        public byte[] bBalanceKartu;
        public int len_BalanceKartu;
        public long lBalanceKartu;
        public byte type;
        public byte[] exp;

        public  cardinfo()
        {
            uid = new UID();
            value=new byte[100];
            len_value=0;
            log=new byte[500];//store bank log after successful deduct. used to get bank log if rewrite partner data required
            len_log=0;
            cardno=new byte[8];
            len_cardno=0;
            bBalanceKartu=new byte[4];
            len_BalanceKartu=0;
            lBalanceKartu=0;
            type=0;
            exp=new byte[2];
        }


}
