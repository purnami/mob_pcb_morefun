package id.co.softorb.lib.passti;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

import java.util.Arrays;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.helper.ByteUtils;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.passti.helper.ErrorCode;
import id.co.softorb.lib.passti.helper.converter;
import id.co.softorb.lib.passti.helper.util;
import id.co.softorb.lib.smartcard.posdevice.params;
import id.co.softorb.lib.smartcard.posdevice.reader;


import static id.co.softorb.lib.passti.devinfo.*;
import static id.co.softorb.lib.passti.helper.ErrorCode.*;
import static id.co.softorb.lib.passti.helper.converter.ChangeEndian;
import static id.co.softorb.lib.passti.helper.util.*;
import static id.co.softorb.lib.smartcard.posdevice.params.*;
/**
 * Created by softorb.co.id on 4/16/2019.
 */


/**
 * Created by softorb.co.id on 4/16/2019.
 */



 class Mandiri {

    public class mdr_cardinfo{
        byte appid;
        boolean JCOP;
    }
    public class  mdr_sam
    {
        public byte[] uid;
        public byte[] info;
        public mdr_sam()
        {
            uid = new byte[16];
            info=new byte[32];
        }
    };
    private byte[] RAPDU = new byte[300];
    byte[] cmdSendToSAM = new byte[255];
    byte[] cmdSendToCL = new byte[255];
    byte[] cmd_to_sam = {(byte) 0x00, (byte) 0xc3, (byte) 0x00, (byte) 0x00, (byte) 0x00};
    byte[] SW1SW2_9000 = {(byte) 0x90, (byte) 0x00};
    //byte[] SAM_UID;
   // byte[] SAM_Info;
    byte[] SAM_GetTrxReport 		= {(byte) 0x00, (byte) 0xC2, (byte) 0x04, (byte) 0x00, (byte) 0x00};
    byte[] SAM_InstitutionNumber 	= {(byte) 0x00, (byte) 0xC2, (byte) 0x06, (byte) 0x00, (byte) 0x00};
    static byte[] SAM_SelectAID_1 	= {(byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x00, (byte) 0x08, (byte) 0x42, (byte) 0x4D, (byte) 0x53, (byte) 0x41, (byte) 0x4D, (byte) 0x5F, (byte) 0x30, (byte) 0x31};



    byte[] SAM_InitVoid		 		= {(byte) 0x00, (byte) 0xc3, (byte) 0x06, (byte) 0x00, (byte) 0x4C};
/*
     byte[] PIN = {(byte) 0x18,(byte) 0xA4,(byte) 0x33,(byte) 0xA5,(byte) 0xCF,(byte) 0x82,(byte) 0x37,(byte) 0xF9};
     byte[] ins_id = {(byte)0x00,(byte) 0x23};
     byte[] tid = {(byte) 0x12,(byte) 0x34,(byte) 0x56,(byte) 0x78,(byte) 0x2F,(byte) 0xA9};
*/
    final byte TAG_TERMINAL  = (byte) 0x02;
    final byte TAG_PICCTCL = (byte) 0x05;
    final byte PURPOSE_BALANCE = (byte)0x00;
    final byte PURPOSE_SAMINFO = (byte)0x01;
    final byte PURPOSE_ERRORMSGFROMSAM =(byte)0x02;
    final byte PURPOSE_SAMAUTOGENTRXREPORT = 0x03;
    final byte PURPOSE_POINTINFO = (byte)0x04;
    final byte PURPOSE_DATAPACKAGE = (byte)0x05;
    final byte PURPOSE_SAMCONFIG = (byte)0x06;
    final byte PURPOSE_ENDOFSEQUENCE = (byte)0x90;
    final byte APPID_03 = (byte) 0x03;
    final byte APPID_02 = (byte)0x02;
    final byte APPID_83 = (byte)0x83;
    final int offset_balance = 27;
    final int offset_appid = 18;
    final byte MDR_ENDOFGRACEPERIOD =(byte)0x6D;
    final byte MDR_CARDEXPIRED =(byte)0x64;

    byte[] SW_9000={(byte)0x90,(byte)0x00};

    //private mandiri mandiricard;


    private static final String MODEL_AECR_C10 = "AECR C10";
    private int iBalance;//store balance value conversion from response APDU
    private String saldoAwal = "";//store balance before deduct
    //private byte[] bsaldoAwal = new byte[4];
    private String saldoAkhir = "";//store balance after deduct
    boolean fSaveSaldo;

    String TAG;
    PASSTI passti;
    mdr_cardinfo ctl_mdr;
    int samslot;
    reader device;
    String tagclass;
    mdr_sam sam_mdr;
    private byte[] banklog = new byte[255];
    void SetSamPort(int slot)
    {
        samslot=slot;
    }
     //Mandiri(Context ctx, UDeviceService deviceService, DeviceInfo deviceInfo,PASSTI passtiInstance,APOSUtility aposUtilityInstance) throws RemoteException, UnsupportedEncodingException {
//     Mandiri(Context ctx, UDeviceService deviceService, DeviceInfo deviceInfo,PASSTI passtiInstance,APOSUtility aposUtilityInstance) throws RemoteException, UnsupportedEncodingException {
    private void SaveLog(byte[] resp)
    {
        banklog =new byte[resp.length-2];
        System.arraycopy(resp,0, banklog,0, banklog.length);
    }
	 Mandiri(Context ctx,PASSTI passtiInstance, reader reader)  {
         Log.d(TAG,TAG+".Mandiri");

        //mandiricard = new mandiri();
        passti=passtiInstance;
         device=reader;
         ctl_mdr= new mdr_cardinfo();
         sam_mdr=new mdr_sam();
         samslot=255;//default for unassigned sam slot
         tagclass = this.getClass().getSimpleName();
         TAG=passti.TAG;
    }
/*
     String getCardNo(){
         Log.d(TAG,TAG+".getCardNo");
        String cardNo;

        passti.dbghelper.DebugHex("passti.ctlinfo.cardno",passti.ctlinfo.cardno);
         if(passti.ctlinfo.cardno != null) {
             cardNo = Hex.bytesToHexString(passti.ctlinfo.cardno);
         }else cardNo = "Card Not Found";
         return cardNo;

    }*/

     String getLastBalance(){
        return saldoAkhir;
    }

     String getBalance() {
        return saldoAwal;
    }

     int checkBalance() throws RemoteException {
        int i = mandiri_checkbalance();

        return i;
    }



    int MDR_TestAutoCorrection()
    {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(passti.GetAction()==NORMAL)
        {
            //return MDR_CTL_ERR_DEBITPURSE;
            return ERR_NO_RESP;
        }
        if(passti.GetAction()==REPURCHASE)
        {
            return OK;
        }
        return OK;
    }



    int deduct(String amount) throws RemoteException {
        //reportRAPDU = new byte[255];
         int intAmount = Integer.parseInt(amount);
         byte[] leAmount = converter.InttoByteArray(intAmount, converter.BIG_ENDIAN);
        //int i = mandiri_deduct(amount.getBytes());
         int i = mandiri_deduct(leAmount);

        return i;
    }


    private int mandiri_deduct(byte[] amount) throws RemoteException {
        /*amount in BIG_ENDIAN*/
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        /*
        * amount = byte array, BIG ENDIAN
        * */
        int result;
        byte[] amount_le;
        //long ldeductAmount;
        int intAmount;


        //cmdSendToSAM = new byte[255];
        Arrays.fill(cmdSendToSAM,(byte)0x00);
        byte[] devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyHHmmss"));
        passti.dbghelper.DebugHex("devicetime",devicetime);


        passti.deduct.baAmount= amount;
        amount_le = ChangeEndian(amount,amount.length);
        intAmount = converter.ByteArrayToInt(amount,0,amount.length,converter.BIG_ENDIAN);
        passti.dbghelper.DebugInteger("intAmount",intAmount);
        passti.dbghelper.DebugHex("passti.deduct.baAmount",passti.deduct.baAmount);
        passti.dbghelper.DebugHex("amount_le",amount_le);

        if(passti.GetAction()== REPURCHASE)
        {
            Log.d(TAG,"Execute autocompletion");

            Log.d(TAG,"passti.ctlinfo.cardno    : " + Hex.bytesToHexString(passti.ctlinfo.cardno));
            passti.dbghelper.DebugHex("passti.deduct.prevCardNo",passti.deduct.prevCardNo);
            passti.dbghelper.DebugHex("passti.ctlinfo.uid.value",passti.ctlinfo.uid.value);
            passti.dbghelper.DebugHex("passti.deduct.prevUID",passti.deduct.prevUID);

            if(BytesUtil.bytes2HexString(passti.ctlinfo.cardno).matches(BytesUtil.bytes2HexString(passti.deduct.prevCardNo))  && BytesUtil.bytes2HexString(passti.ctlinfo.uid.value).matches(BytesUtil.bytes2HexString(passti.deduct.prevUID)))
            {
                int c = mandiri_correction(amount_le,devicetime);
                if(c==OK)
                {
                    passti.SetAction(NORMAL);
                    Arrays.fill(passti.deduct.prevUID,(byte)0x00);
                }
                return c;
            }

            return MDR_CTL_ERR_CORRECTION;
        }
        if(passti.GetAction()== NORMAL)
        {
            int z = mandiri_checkbalance();
            if (z != OK) {
                //Log.d(TAG,"Failed Check Balance");
                return z;
            }

            if (intAmount > Integer.valueOf(saldoAwal)) 
			{

                return CTL_ERR_INSUFFICIENTBALANCE;
            }

            passti.dbghelper.DebugPrintString("Execute deduct process");
            Log.e(TAG,"ctl_mdr.appid "+Hex.byteToHexString(ctl_mdr.appid));
            passti.dbghelper.DebugBoolean("ctl_mdr.JCOP",ctl_mdr.JCOP);
            if (ctl_mdr.appid == APPID_83) 
			{

                //2020-02-06, vivo,no need to call Mandiri_SAMInit_NewApplet, because mandiri_checkbalance has call it
                result = Mandiri_SAMInit_NewApplet();
                if (result != OK)
                    return result;

                result = DebitProcess(devicetime, amount_le);

                if (result != OK) {
                    switch (result) {
                        case ERR_NO_RESP:

                            passti.SetAction(REPURCHASE);
                            break;

                    }
                    return result;
                }

                return result;

            }
            if (ctl_mdr.appid == APPID_03) {
                //2020-02-07,vivo
                int x  = Mandiri_SAMInitialization();
                if(x!=OK){
                    return x;
                }
                if (ctl_mdr.JCOP)
                {


                    saldoAwal = String.valueOf(iBalance);
                    passti.dbghelper.DebugString("saldoAwal", saldoAwal);

                    result = DebitProcessJCOPBM(devicetime, amount_le);

                    if (result != OK) {
                        //MDR_SetCorrectionFlag();
                        passti.SetAction(REPURCHASE);
                        return result;
                    }



                    return result;
                }
                else
                {
                    //Mandiri_SAMInitialization();
                    //result =DebitProcess(devicetime,newAmount);
                    result = DebitProcess(devicetime, amount_le);

                    if (result != OK) {
                        switch (result) {
                            case ERR_NO_RESP:
                                //MDR_SetCorrectionFlag();
                                passti.SetAction(REPURCHASE);
                                break;

                        }
                        return result;

                }
//                    printf("save last successfull card no for time blocking\r");
//                    memcpy(deduct.prevCardNo,ctlinfo.cardno,ctlinfo.len_cardno);

                        return result;



                }


            }

            if (ctl_mdr.appid == APPID_02) {

                int x = Mandiri_SAMInitialization();
                if (x != OK) {
                    return x;
                }
                result = DebitProcess(devicetime, amount_le);
                if (result != OK) {
                    switch (result) {
                        case ERR_NO_RESP:
                            //MDR_SetCorrectionFlag();
                            passti.SetAction(REPURCHASE);
                            break;
                        case MDR_ENDOFGRACEPERIOD:
                             result=   MDR_GRACEPERIOD;
                            break;

                    }
                }
                return result;
            }

            
        }
        passti.SetAction(NORMAL);
        return OK;
    }

    private int Mandiri_SAMInit_NewApplet() throws RemoteException
    {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;

        result=Mandiri_SelectSAMAID1();
        if(result!=OK)
        {
            return MDR_SAM_ERR_SELECTAID1;
        }
        result = Mandiri_SAMGetUID();
        if(result!=OK)
        {
            return MDR_SAM_ERR_GETUID;
        }
        result = Mandiri_SAMGetInfo();
        if(result!=OK)
        {
            return MDR_SAM_ERR_GETINFO;
        }

        result=Mandiri_SAMLogin(devinfo.issuer.mdrpin);
        if(result!=OK)
        {
            return MDR_SAM_ERR_LOGIN1;
        }
        cmdSendToSAM = new byte[255];

        result=SAM_UpdateTerminalInfo(devinfo.issuer.mdrInstID,devinfo.issuer.mdrTid);
        if(result!=OK)
        {
            return MDR_SAM_ERR_UPDATETERMINALINFO;
        }
        return OK;
    }


    /*byte[] ChangeEndian(byte[] data,int datalen)
    {
        if(datalen<4)
            return data;
        byte[] resp = new byte[4];
        resp[0]=data[3];
        resp[1]=data[2];
        resp[2]=data[1];
        resp[3]=data[0];

        return resp;
    }*/


    private int DebitProcess(byte[] date, byte[] amount) throws RemoteException {
        //date in bcd format
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        boolean execute=true;

        int idx;
        int iResult;
        byte[] commit = {(byte) 0x00,(byte) 0xDD,(byte) 0x00,(byte) 0x00,(byte) 0x00};
        byte[] mdr_SAM_InitPayment 		= {(byte) 0x00,(byte)  0xc3,(byte)  0x02,(byte)  0x00,(byte)  0x50};
        byte[] cmd;
        byte[] result;
        int commit_flag = 0;
        idx=0;
        int spc = 0;
        //memset(cmdSendToSAM,0x00,sizeof(cmdSendToSAM));
        //Arrays.fill(cmdSendToSAM,(byte)0x00);
        if(passti.ctlinfo.uid.value.length < 7) {
            spc+=7- passti.ctlinfo.uid.value.length;
            //cmdSendToSAM = new byte[mdr_SAM_InitPayment.length + date.length + spc + aposUtility.UID.length + value.length + amount.length];
            cmdSendToSAM = new byte[mdr_SAM_InitPayment.length + date.length + spc + passti.ctlinfo.uid.value.length + passti.ctlinfo.len_value + amount.length];
        }else{
            //cmdSendToSAM = new byte[mdr_SAM_InitPayment.length + date.length+ aposUtility.UID.length + value.length + amount.length];
            cmdSendToSAM = new byte[mdr_SAM_InitPayment.length + date.length+ passti.ctlinfo.uid.value.length + passti.ctlinfo.len_value + amount.length];

        }
        //memcpy(&cmdSendToSAM[idx],mdr_SAM_InitPayment,sizeof(mdr_SAM_InitPayment));
        System.arraycopy(mdr_SAM_InitPayment,0,cmdSendToSAM,idx,mdr_SAM_InitPayment.length);
        idx+=mdr_SAM_InitPayment.length;
        //memcpy(&cmdSendToSAM[idx],date,datelen);
        System.arraycopy(date,0,cmdSendToSAM,idx,date.length);
        idx+=date.length;
        if(passti.ctlinfo.uid.value.length<7)
        {
            idx+=(7- passti.ctlinfo.uid.value.length);
        }
        //memcpy(&cmdSendToSAM[idx],&ctlinfo.uid.value[0],ctlinfo.uid.len);
        System.arraycopy(passti.ctlinfo.uid.value,0,cmdSendToSAM,idx, passti.ctlinfo.uid.value.length);
        idx+= passti.ctlinfo.uid.value.length;

        //memcpy(&cmdSendToSAM[idx],ctlinfo.value,ctlinfo.len_value);
        passti.dbghelper.DebugHex("passti.ctlinfo.value",passti.ctlinfo.value,passti.ctlinfo.len_value);
        System.arraycopy(passti.ctlinfo.value,0,cmdSendToSAM,idx,passti.ctlinfo.len_value);
        idx+=passti.ctlinfo.len_value;

        //  byte[] resultEndian = ChangeEndian(amount,amount.length);
        // byte[] resultEndian = converter.IntegerToByteArray(intAmount,converter.LITTLE_ENDIAN);

        //memcpy(&cmdSendToSAM[idx],apdubuf,apdulen);

        System.arraycopy(amount,0,cmdSendToSAM,idx,amount.length);//little endian here
        idx+=amount.length;

              //memcpy(data_correction,ctlinfo.value,8);
        //memcpy(&data_correction[8],&ctlinfo.uid.value[0],ctlinfo.uid.len);

        iResult=MDR_SAM_ERR_STARTPAYMENT;

        RAPDU = device.sendSAM(samslot,cmdSendToSAM);
        if(RAPDU == null){
            //Log.d(TAG,"Failed sendSAM DebitProcess");
            return MDR_SAM_ERR_DEBITUNKNOWNTAG;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            //Log.d(TAG,"Failed sendSAM DebitProcess !=9000");
            return MDR_SAM_ERR_DEBITUNKNOWNTAG;
        }
        iResult = OK;
        do
        {
            //Log.d(TAG,"RDR<-SAM : " + BytesUtil.bytes2HexString(RAPDU));//printhex(apdubuf,apdulen);
            if(iResult!=OK)
                return iResult;

            cmd = new byte[300];
            iResult=RAPDU[1];
            passti.dbghelper.DebugInteger("iResult" , iResult);
            result = new byte[iResult];
            //memcpy(result,&apdubuf[2],iResult);
            System.arraycopy(RAPDU,2,result,0,iResult);
            mdrtag("RAPDU[0]",RAPDU[0]);
            switch(RAPDU[0])
            {
                case TAG_PICCTCL:
                    //Log.d(TAG,"TAG_PICCTCL");
                    if(RAPDU[1]==0x05)
                    {


                        passti.dbghelper.DebugHex("result",result);
                        //passti.dbghelper.DebugHex("commit",commit);

                        if(Arrays.equals(commit,result))
                        {
                        Log.e(TAG,"Commit command");
                        commit_flag = 1;
                        }

                    }

                    RAPDU = device.sendCTL(result);
                    if(RAPDU == null){
                        //MDR_SetCorrectionFlag();
                        passti.SetAction(REPURCHASE);
                        //Log.d(TAG,"Failed DebitProcess");
                            return ERR_NO_RESP;
                    }
                    if(APDUHelper.CheckResult(RAPDU) != 0){
                        //Log.d(TAG,"Failed DebitProcess");
                        //MDR_SetCorrectionFlag();
                        passti.SetAction(REPURCHASE);
                        return ERR_NO_RESP;
                    }
/*
					//activate this part for testing autocompletion
                    //start autocompletion test
					if(commit_flag==1)
                    {
                        iResult= MDR_TestAutoCorrection();
                        if(iResult!=OK)
                        {
                            execute=false;
                        }

                    }


                    //finist autocompletion test
*/

                    cmd=SAM_ComposeCMD(RAPDU,RAPDU.length,cmd);
                    cmd = trim(cmd);

                    break;
                case TAG_TERMINAL:

                    mdrtagpurpose("RAPDU[2]",RAPDU[2]);
                    switch(RAPDU[2])
                    {
                        case PURPOSE_BALANCE:
                            cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                            cmd = trim(cmd);
                            iResult=MDR_SAM_ERR_PURPOSE_BALANCE;
                            break;
                        case PURPOSE_ERRORMSGFROMSAM:

                            mdrerrormessage("RAPDU[3]",RAPDU[3]);
                            switch(RAPDU[3])
                            {
                                case MDR_CARDEXPIRED:
                                case MDR_ENDOFGRACEPERIOD : //_ADD_MDR001_
                                    execute=false;
                                    iResult =MDR_ENDOFGRACEPERIOD;

                                    break;
                                case 0x6C:
                                    cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                                    cmd = trim(cmd);

                                    break;
                                case 0x54:

                                    iResult = -5;
                                    execute=false;
                                    cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                                    cmd = trim(cmd);
                                    RAPDU = device.sendSAM(samslot,cmd);
                                    if(RAPDU == null){
                                        //Log.d(TAG,"Failed DebitProcess");
                                        return ERR_NO_RESP;
                                    }
                                    if(APDUHelper.CheckResult(RAPDU) != 0){
                                        //Log.d(TAG,"Failed DebitProcess " + BytesUtil.bytes2HexString(RAPDU));
                                        return ERR_SW1SW2;
                                    }
                                    iResult = OK;

                                    break;
                                case 0x68:
                                    execute=false;
                                    //*outLen=0;
                                    cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                                    //SAM_SendCMD(port_mdr,cmd,idx,apdubuf,&apdulen,SW_9000,&iErrorCode,errorMessage);
                                    cmd = trim(cmd);

                                    RAPDU = device.sendSAM(samslot,cmd);
                                    if(RAPDU == null){
                                        //Log.d(TAG,"Failed DebitProcess");
                                        return ERR_NO_RESP;
                                    }
                                    if(APDUHelper.CheckResult(RAPDU) != 0){
                                        //Log.d(TAG,"Failed DebitProcess " + BytesUtil.bytes2HexString(RAPDU));
                                        return ERR_SW1SW2;
                                    }
                                    iResult=MDR_SAM_ERR_PURPOSE_SAMMESSAGE;

                                    break;
                                case 0x5E:
                                    passti.dbghelper.DebugPrintString("Fail execute correction, reset autocorrection flag");
                                    passti.SetAction(NORMAL);
                                    break;

                                default:

                                    cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                                    RAPDU = device.sendSAM(samslot,cmd);
                                    if(RAPDU == null){
                                        return ERR_NO_RESP;
                                    }
                                    if(APDUHelper.CheckResult(RAPDU) != 0){
                                        return ERR_SW1SW2;
                                    }
                                    execute=false;
                                    //return MDR_SAM_ERR_PURPOSE_SAMMESSAGE;
                                    break;


                            }


                            break;
                        case PURPOSE_SAMAUTOGENTRXREPORT:
                            cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                            cmd = trim(cmd);
                            break;
                        case PURPOSE_ENDOFSEQUENCE:
                            iResult=MDR_SAM_ERR_PURPOSE_ENDSEQ;

                            iResult=SAMCMD_GetTrxReport();
                            if(iResult==OK)
                            {

                                iBalance = converter.ByteArrayToInt(RAPDU,27,4, converter.LITTLE_ENDIAN);
                                saldoAkhir = String.valueOf(iBalance);
                                byte[] curBalance = new byte[4];
                                System.arraycopy(RAPDU,27,curBalance,0,4);
                                //byte[] devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
                                byte[] devicetime = bcd_ddmmyyhhmmss_to_bcd_ddmmyyyyhhmmss(date);
                                byte[] be_amount = new byte[4];
                                byte[] be_curBalance = new byte[4];
                                be_amount=ChangeEndian(amount,4);
                                be_curBalance=ChangeEndian(curBalance,4);

                                //passti.log = passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value, params.ctype_mdr,devicetime,passti.ctlinfo.cardno,be_curBalance,be_amount,trxno,RAPDU,others);
                                SaveLog(RAPDU);
                                passti.log = passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value, params.ctype_mdr,devicetime,passti.ctlinfo.cardno,be_curBalance,be_amount,trxno,banklog,others);

                                Arrays.fill(cmdSendToCL,(byte)0x00);
                                Arrays.fill(cmdSendToSAM,(byte)0x00);
                                Arrays.fill(RAPDU,(byte)0x00);

                            }
							execute=false;
                            return OK;
                        default:
                            //passti.dbghelper.DebugPrintString("default");
                            cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                            cmd = trim(cmd);
                            RAPDU = device.sendSAM(samslot,cmd);
                            if(RAPDU == null){
                                //Log.d(TAG,"Failed DebitProcess");
                                return ERR_NO_RESP;
                            }
                            if(APDUHelper.CheckResult(RAPDU) != 0){
                                //Log.d(TAG,"Failed DebitProcess " + BytesUtil.bytes2HexString(RAPDU));
                                return ERR_SW1SW2;
                            }
                            //SAM_SendCMD(port_mdr,cmd,idx,apdubuf,&apdulen,SW_9000,&iErrorCode,errorMessage);
                            return MDR_SAM_ERR_DEBITUNKNOWNTAG;
                    }
                    break;
            }
            if(execute)
            {
                RAPDU = device.sendSAM(samslot,cmd);
                if(RAPDU == null){
                    return ERR_NO_RESP;
                }
                if(APDUHelper.CheckResult(RAPDU) != 0){
                    return ERR_SW1SW2;
                }
                iResult = OK;
            }



        } while(execute);

        return iResult;

    }

    private  int SAMCMD_GetTrxReport() throws RemoteException {

        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] mdr_SAM_GetTrxReport	= {(byte) 0x00,(byte) 0xC2,(byte) 0x04,(byte) 0x00,(byte) 0x00};
        int result;


        RAPDU = device.sendSAM(samslot,mdr_SAM_GetTrxReport);
        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;


        return OK;
    }

    static byte[] trim(byte[] bytes)
    {
        int i = bytes.length - 1;
        while (i >= 0 && bytes[i] == 0)
        {
            if(bytes[i-1]== (byte)0x90 && bytes[i] == (byte)0x00)
            {
                break;
            }

            --i;

        }

        return Arrays.copyOf(bytes, i + 1);
    }

    private int mandiri_checkbalance() throws RemoteException {
        //Log.d(TAG,"checkbalance");
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        //byte[] mdr_CekAppID			=  {(byte) 0x00,(byte) 0xB3,(byte) 0x00,(byte) 0x00,(byte) 0x3F };//2020-02-06,vivo,commented. modified as in jellies where Le = 0x32

        //byte[] mdr_GetBalance			=  {(byte) 0x00,(byte) 0xB5,(byte) 0x00,(byte) 0x00,(byte) 0x0A };

        //byte[] temp = new byte[4];
        int idx=0;
        int result;
        boolean retry=true;//default to read app 02 balance

        Mandiri_CTL_Initialization();
        byte[] devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyHHmmss"));



            Log.d(TAG,"ctl_mdr.appid "+Hex.byteToHexString(ctl_mdr.appid));
            switch(ctl_mdr.appid)
            {
                case APPID_83:

                    return CTL_GetBalance();
                case APPID_03:
                    if(!ctl_mdr.JCOP) {

                        return CTL_GetBalance();
                    }
                    else
                    {
                        return CTL_GetBalance();
                    }

                case APPID_02:

                    Mandiri_SAMInitialization();

                    while(retry) {
                        result = CheckBalance_app02(devicetime);
                        passti.dbghelper.DebugInteger("CheckBalance_app02 result",result);
                        if (result != OK)
                            {
                                if ((result == MDR_ENDOFGRACEPERIOD) || (result == MDR_CARDEXPIRED)) {
                                    passti.dbghelper.DebugPrintString("Card with grace period, retry deduct");
                                    devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyHHmmss"));
                                }
                                else
                                {
                                    retry=false;
                                    return MDR_CTL_ERR_GETBALANCE;
                                }

                            }
                            else
                            {
                                retry=false;
                            }

                        }



                    if (APDUHelper.CheckResult(RAPDU) != 0){
                        return MDR_CTL_ERR_GETBALANCE;
                    }
                    break;


                default :
                    Arrays.fill(passti.ctlinfo.bBalanceKartu,(byte)0x00);
                    return MDR_CTL_ERR_UNKNOWNAPPID;


            }

            return OK;

    }

    private  byte[] SAM_ComposeCMD(byte[] data,int len, byte[] out)
    {
        int offset=0;
        //memcpy(out+offset,cmd_to_sam,sizeof(cmd_to_sam));

        System.arraycopy(cmd_to_sam,0,out,offset,cmd_to_sam.length);
        offset+=cmd_to_sam.length;

        out[offset-1]=(byte)len;
        //memcpy(out+offset,data,len);
        System.arraycopy(data,0,out,offset,len);
        //RAPDU = out;
        offset+=len;
        //Log.d(TAG,"SAM_ComposeCMD: " + BytesUtil.bytes2HexString(out));
        return out;
    }
/*
    private int Mandiri_SendAPDU(byte CLA,byte INS,byte P1,byte P2,byte Lc,byte[] data,int Len4CTL,int step)  {
        int idx;
        int result;
        byte[] line_msg = new byte[100];
        byte[] msg;
        msg=line_msg;
        //Log.d(TAG,"Step " + step);
        idx=0;
        byte[] cmdSendToSAM = new byte[Len4CTL];
        cmdSendToSAM[idx++] = CLA;
        cmdSendToSAM[idx++] = INS;
        cmdSendToSAM[idx++] = P1;
        cmdSendToSAM[idx++] = P2;
        cmdSendToSAM[idx++] = Lc;

        System.arraycopy(data,0,cmdSendToSAM,idx, Lc);

        idx+=Lc;

        RAPDU = device.sendSAM(samslot,cmdSendToSAM);
        if(APDUHelper.CheckResult(RAPDU) != 0){
            //Log.d(TAG,"Failed Mandiri_SendAPDU-Step: " + step);
            return MDR_CTL_ERR_GETBALANCE;
        }


        return OK;


    }
*/
    private int Mandiri_SAMInitialization() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;


        result=Mandiri_SelectSAMAID1();
        if(result!=OK)
        {
            return MDR_SAM_ERR_SELECTAID1;
        }

        cmdSendToSAM = new byte[255];

        result=Mandiri_SAMLogin(devinfo.issuer.mdrpin);

        if(result!=OK)
        {
            return MDR_SAM_ERR_LOGIN1;
        }
        cmdSendToSAM = new byte[255];
        result=SAM_UpdateTerminalInfo(devinfo.issuer.mdrInstID,devinfo.issuer.mdrTid);
        if(result!=OK)
        {
            return MDR_SAM_ERR_UPDATETERMINALINFO;
        }

        result=Mandiri_SelectSAMAID();
        if(result!=OK)
        {
            return MDR_SAM_ERR_SELECTAID2;
        }
        cmdSendToSAM = new byte[255];

        result=Mandiri_SAMLogin(devinfo.issuer.mdrpin);
        if(result!=OK)
        {
            return MDR_SAM_ERR_LOGIN1;
        }


        cmdSendToSAM = new byte[255];
        result=SAM_UpdateTerminalInfo(devinfo.issuer.mdrInstID,devinfo.issuer.mdrTid);


        if(result!=OK)
        {
            return MDR_SAM_ERR_UPDATETERMINALINFO;
        }
        result=Mandiri_SAMGetUID();
        if(result!=OK)
        {
            return MDR_SAM_ERR_GETUID;
        }
        result=Mandiri_SAMGetInfo();
        if(result!=OK)
        {
            return MDR_SAM_ERR_GETINFO;
        }
        result=Mandiri_SAMGetJCOPBM();
        if(result!=OK)
        {
            return MDR_SAM_ERR_GETJCOPBM;
        }
        return OK;
    }

    private int Mandiri_SelectSAMAID1() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] mdr_SAM_SelectAID_1		= {(byte) 0x00,(byte)  0xA4,(byte)  0x04,(byte)  0x00,(byte)  0x08,(byte)  0x42,(byte)  0x4D,(byte)  0x53,(byte)  0x41,(byte)  0x4D,(byte)  0x5F,(byte)  0x30,(byte)  0x31};

        RAPDU = device.sendSAM(samslot,mdr_SAM_SelectAID_1);

        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;
        return OK;
    }


    private int Mandiri_SAMLogin(byte[] data)  {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx=0;
        int result;
        byte[] mdr_SAM_Login			= {(byte) 0x00,(byte) 0xC2,(byte) 0x0A,(byte) 0x01,(byte) 0x08};
        byte[] apdu=new byte[mdr_SAM_Login.length+data.length];

        Arrays.fill(apdu,(byte)0x00);
        System.arraycopy(mdr_SAM_Login,0,apdu,idx,mdr_SAM_Login.length);
        idx+=mdr_SAM_Login.length;
        System.arraycopy(data,0,apdu,idx,data.length);
        idx+=data.length;
        RAPDU = device.sendSAM(samslot,apdu);
        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;


        return OK;
    }

    private int Mandiri_SelectSAMAID() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] mdr_SAM_Select			= {(byte) 0x00,(byte) 0xA4,(byte) 0x04,(byte) 0x00,(byte) 0x08,(byte) 0x52,(byte) 0x56,(byte) 0x53,(byte) 0x5F,(byte) 0x30,(byte) 0x31,(byte) 0x30,(byte) 0x30 };

        RAPDU = device.sendSAM(samslot,mdr_SAM_Select);

        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;

        return OK;
    }


    private int Mandiri_SAMGetUID() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] mdr_SAM_GetUID 			= {(byte) 0x00,(byte)  0xC2,(byte)  0x10,(byte)  0x00,(byte)  0x00};


        RAPDU = device.sendSAM(samslot,mdr_SAM_GetUID);
        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;
        if(RAPDU.length<sam_mdr.uid.length)
        {
            System.arraycopy(RAPDU, 0, sam_mdr.uid, 0, RAPDU.length);
        }
        else {
            System.arraycopy(RAPDU, 0, sam_mdr.uid, 0, sam_mdr.uid.length);
        }
        passti.dbghelper.DebugHex("sam_mdr.uid.length",sam_mdr.uid,sam_mdr.uid.length);

        return OK;
    }

    private int Mandiri_SAMGetInfo() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] mdr_SAM_GetInfo 			= {(byte) 0x00,(byte)  0xC2,(byte)  0x11,(byte)  0x00,(byte)  0x00};

        RAPDU = device.sendSAM(samslot,mdr_SAM_GetInfo);
        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;

        if(RAPDU.length<sam_mdr.info.length)
        {

            System.arraycopy(RAPDU,0,sam_mdr.info,0,RAPDU.length);
        }
        else {
            System.arraycopy(RAPDU,0,sam_mdr.info,0,sam_mdr.info.length);
        }

        passti.dbghelper.DebugHex("sam_mdr.info.length",sam_mdr.info,sam_mdr.info.length);
        return OK;
    }

    private int Mandiri_SAMGetJCOPBM() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] mdr_SAM_GetJCOPBM 			= {(byte) 0x00,(byte)  0x12,(byte)  0x00,(byte) 0x00,(byte) 0x00};

        RAPDU = device.sendSAM(samslot,mdr_SAM_GetJCOPBM);

        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;
        return OK;
    }

    private int CTL_SelectApp() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] apdu			=  { 0x00, (byte)0xA4, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
        RAPDU = device.sendCTL(apdu);



        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;
        return OK;
    }

    private int CTL_GetBalance() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        RAPDU = null;
        byte[] mdr_getbalance={0x00,(byte)0xB5,0x00,0x00,0x0A};
        RAPDU = device.sendCTL(mdr_getbalance);

        if (RAPDU == null) {
            return MDR_CTL_ERR_GETBALANCE;
        }
        if (RAPDU.length < 2)
            return MDR_CTL_ERR_GETBALANCE;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return MDR_CTL_ERR_GETBALANCE;

        iBalance = converter.ByteArrayToInt(RAPDU, (byte) 0, (byte) 4, converter.LITTLE_ENDIAN);
        saldoAwal=String.valueOf(iBalance);
        passti.dbghelper.DebugInteger("iBalance",iBalance);
        passti.dbghelper.DebugString("saldoAwal",saldoAwal);
        passti.ctlinfo.bBalanceKartu=ChangeEndian(RAPDU,4);

        passti.ctlinfo.lBalanceKartu=converter.ByteArrayToInt(passti.ctlinfo.bBalanceKartu,0,4,converter.BIG_ENDIAN);

        passti.dbghelper.DebugHex("passti.ctlinfo.bBalanceKartu",passti.ctlinfo.bBalanceKartu);
        passti.dbghelper.DebugLong("passti.ctlinfo.lBalanceKartu",passti.ctlinfo.lBalanceKartu);
        Arrays.fill(RAPDU,(byte)0x00);



        return OK;
    }



    private int SAM_UpdateTerminalInfo(byte[] ins_id, byte[] tid) throws RemoteException {

        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] SAM_UpdateTerminalInfo 	= {(byte) 0x00, (byte) 0xC2, (byte) 0x0A, (byte) 0x00, (byte) 0x06};
        byte[] cmd = new byte[SAM_UpdateTerminalInfo.length + ins_id.length + tid.length];
        int offset = 0;
        System.arraycopy(SAM_UpdateTerminalInfo, 0, cmd, offset, SAM_UpdateTerminalInfo.length);
        offset += SAM_UpdateTerminalInfo.length;
        System.arraycopy(ins_id, 0, cmd, offset, ins_id.length);
        offset += ins_id.length;
        System.arraycopy(tid, 0, cmd, offset, tid.length);
        offset += tid.length;

         RAPDU = device.sendSAM(samslot,cmd);

        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;

        return OK;
    }

    private int DebitProcessJCOPBM(byte[] date, byte[] amount) throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;

        result=CTL_ReadCardInfo_JCOPBM();
        if (result != OK)
            return MDR_CTL_ERR_READCARDINFO;

        result= SAM_InitPurchase_JCOPBM(date,amount);
        if (result != OK)
            return MDR_SAM_ERR_INITPURCHASE;


        result = CTL_Debit_JCOPBM(RAPDU);
        //result=MDR_TestAutoCorrection();
        if (result != OK) {
            passti.SetAction(REPURCHASE);//2020-02-05,vivo
            return MDR_CTL_ERR_DEBITPURSE;
        }


        result = SAM_Getlog_JCOPBM(RAPDU);
        if (result != OK)
            return MDR_SAM_ERR_GETLOG;

        result = SAM_GenReport_JCOPBM();
        if (result != OK)
            return MDR_SAM_ERR_GETTRXREPORT;

        byte[] curBalance = new byte[4];
        byte[] be_amount = new byte[4];
        System.arraycopy(RAPDU,27,curBalance,0,4);//balance is in little endian, must changed to big-endian

        //byte[] devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
        byte[] devicetime = bcd_ddmmyyhhmmss_to_bcd_ddmmyyyyhhmmss(date);
        be_amount=ChangeEndian(amount,amount.length);
        Log.d(TAG,Hex.bytesToHexString(be_amount));
        Log.d(TAG,"devinfo.trxno "+ Hex.bytesToHexString(trxno));
        iBalance= converter.ByteArrayToInt(curBalance,0,4,converter.LITTLE_ENDIAN);
        saldoAkhir = String.valueOf(iBalance);

        byte[] be_curBalance = new byte[4];
        be_curBalance=ChangeEndian(curBalance,curBalance.length);
        //passti.log = passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value,params.ctype_mdr,devicetime,passti.ctlinfo.cardno,be_curBalance,be_amount,trxno,RAPDU,others);
        SaveLog(RAPDU);
        passti.log = passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value, params.ctype_mdr,devicetime,passti.ctlinfo.cardno,be_curBalance,be_amount,trxno,banklog,others);


        return result;
    }

    private int CTL_ReadCardInfo_JCOPBM() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] mdr_getcardinfojcopbm			=  {(byte) 0x00,(byte) 0xB6,(byte) 0x00,(byte) 0x00,(byte) 0x32 };
        RAPDU = null;
        RAPDU = device.sendCTL(mdr_getcardinfojcopbm);
        Log.d("RAPDUUUU mandiri", Hex.bytesToHexString(RAPDU));
        if (RAPDU == null) {
            return ERR_NO_RESP;
        }
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;

        ctl_mdr.appid = RAPDU[offset_appid];
        Log.d(TAG,"ctl_mdr.appid "+Hex.byteToHexString(ctl_mdr.appid));

        passti.ctlinfo.len_value=RAPDU.length - 2;
        System.arraycopy(RAPDU,0,passti.ctlinfo.value,0,passti.ctlinfo.len_value);

        passti.dbghelper.DebugHex("passti.ctlinfo.value",passti.ctlinfo.value);

        System.arraycopy(RAPDU, 0, passti.ctlinfo.cardno, 0, passti.ctlinfo.cardno.length);
        return OK;
    }

    private int SAM_InitPurchase_JCOPBM(byte[] bDate,byte[] amount) throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int offset = 0;

        byte[] bUID=new byte[7];
        byte[] UID = passti.ctlinfo.uid.value;

        byte[] header = {0x00,0x12,0x01,0x01,0x43};

        byte[] cmd = new byte[header.length + bDate.length + bUID.length + passti.ctlinfo.len_value + amount.length];
        System.arraycopy(header, 0, cmd, offset, header.length);
        offset += header.length;
        System.arraycopy(bDate, 0, cmd, offset, bDate.length);
        offset += bDate.length;

        System.arraycopy(UID, 0, bUID, bUID.length-UID.length, UID.length);
        System.arraycopy(bUID, 0, cmd, offset, bUID.length);
        offset += bUID.length;

        System.arraycopy( passti.ctlinfo.value, 0, cmd, offset,  passti.ctlinfo.len_value);
        offset +=  passti.ctlinfo.len_value;

        System.arraycopy(amount, 0, cmd, offset, amount.length);
        offset += amount.length;


        RAPDU = device.sendSAM(samslot,cmd);


        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;
        return OK;
    }

    private int SAM_Getlog_JCOPBM(byte[] data) throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        int offset = 0;
        String Data = BytesUtil.bytes2HexString(data);
        //Log.d(TAG, "Data "+Data);

        //byte[] header = ByteUtils.hexStringToByteArray(mandiricard.cmdsamgetlog());
        byte[] header = ByteUtils.hexStringToByteArray("0012020030");
        byte[] cmd = new byte[header.length + data.length - 2];
        System.arraycopy(header, 0, cmd, offset, header.length);
        offset += header.length;
        //System.arraycopy(RAPDU, 0, cmd, offset, data.length - 2);
        System.arraycopy(data, 0, cmd, offset, data.length - 2);
        offset += data.length - 2;


        RAPDU = device.sendSAM(samslot,cmd);


        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;

        if (RAPDU[0] > 0x01) {
            return ErrorCode.MDR_SAM_ERR_GETLOG;
        }
        if (RAPDU[0] == 0x01)
        {

            //correction_flag = 0;

            return ErrorCode.MDR_SAM_ERR_GETLOG;
        }
        return OK;
    }

    private int CTL_Debit_JCOPBM(byte[] data) throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        String Data = ByteUtils.bytesToHex(data).substring(0, (data.length - 2) * 2); //from previous command
        //String debitPurse = mandiricard.cmddebitpurse();
        String debitPurse = "00C6000030";
        RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(debitPurse + Data));

        if (RAPDU == null) {
            passti.SetAction(REPURCHASE);//2020-02-05,vivo
            return ErrorCode.ERR_NO_RESP;
        }
        //Log.d(TAG,"Debit_JCOPBM_RAPDU " + BytesUtil.bytes2HexString(RAPDU));
        if (RAPDU.length < 2) 
		{
            passti.SetAction(REPURCHASE);//2020-02-05,vivo
            return ErrorCode.ERR_NO_RESP;//2020-02-05,vivo
        }
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;

        return OK;
    }

    private int SAM_GenReport_JCOPBM() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        //Log.d(TAG, "SAM_GenReport_JCOPBM");

        byte[] SAM_GenReportJCOPBM 		= {(byte) 0x00, (byte) 0x12, (byte) 0x03, (byte) 0x00, (byte) 0x00};
        byte[] cmd = SAM_GenReportJCOPBM;



        RAPDU = device.sendSAM(samslot,cmd);

        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;



        iBalance = converter.ByteArrayToInt(RAPDU, (byte) 27, (byte) 4, converter.LITTLE_ENDIAN);

        return OK;
    }

    private int CTL_GetUID() {


        return OK;
    }


    int mandiri_correction(byte[] amount,byte[] datetime) throws RemoteException {
        /*datetime in BCD: ddmmyyhhmmss*/
        /*amount in LITTLE_ENDIAN, conversion has been done in mandiri_deduct*/
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        int result;
        byte[] temp = datetime;
        int iAmount = converter.ByteArrayToInt(amount,0,amount.length, converter.LITTLE_ENDIAN);
        passti.dbghelper.DebugInteger("iAmount",iAmount);
        passti.dbghelper.DebugInteger("iBalance",iBalance);



        if(iAmount> iBalance)
        {
            //*outLogLen=0;
            return CTL_ERR_INSUFFICIENTBALANCE;
        }


        Log.e(TAG,"ctl_mdr.appid "+Hex.byteToHexString(ctl_mdr.appid));
        passti.dbghelper.DebugBoolean("ctl_mdr.JCOP",ctl_mdr.JCOP);
        if(ctl_mdr.appid==APPID_83)
        {
            result=Mandiri_SAMInit_NewApplet();
            if(result!=OK)
                return result;

            result =Correction(temp,amount);
            return result;

        }

        if(ctl_mdr.appid==APPID_03)
        {

            if (ctl_mdr.JCOP)
            {
                passti.dbghelper.DebugPrintString("Correction for jcopbm app03");
                result=CorrectionJCOPBM(temp,amount);
                if (result != OK)
                {
                    return result;
                }
            }
            else
            {
                passti.dbghelper.DebugPrintString("Correction for nonjcopbm app03");


                result =Correction(temp,amount);
                if (result != OK)
                {
                    return result;
                }
            }

        }
        if(ctl_mdr.appid == APPID_02){
            passti.dbghelper.DebugPrintString("Correction for app02");

            result =Correction(temp,amount);
            if (result != OK)
            {
                return result;
            }

        }


        return OK;
    }

    int Correction(byte[] date, byte[] amount) throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        /*datetime in BCD: ddmmyyhhmmss*/
        /*amount in LITTLE_ENDIAN, conversion has been done in mandiri_deduct*/

        int idx=0;
        int step=1;
        int iResult;
        byte[] cmd = new byte[255];
        byte[] temp = new byte[4];
        byte[] result = new byte[255];
        boolean execute=true;
        byte[] testtime={(byte)0x21,(byte)0x11,(byte)0x18,(byte)0x17,(byte)0x22,(byte)0x52};
        byte[] testcard={(byte)0x60,(byte)0x32,(byte)0x98,(byte)0x60,(byte)0x13,(byte)0x76,(byte)0x20,(byte)0x74};
        byte[] testuid ={(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x5d,(byte)0x39,(byte)0x23,(byte)0xa3};
        byte[] mdr_SAM_InitCorrection	= {(byte) 0x00,(byte)  0xc3,(byte)  0x0D,(byte)  0x00,(byte)  0x50};

        //composing APDU


        idx=0;

        cmdSendToSAM = new byte[255];

        System.arraycopy(mdr_SAM_InitCorrection,0,cmdSendToSAM,0,mdr_SAM_InitCorrection.length);
        idx+=mdr_SAM_InitCorrection.length;
        //memcpy(&cmdSendToSAM[idx],date,datelen);
        System.arraycopy(date,0,cmdSendToSAM,idx,date.length);
        idx+=date.length;

        System.arraycopy(passti.deduct.prevUID,0,cmdSendToSAM,idx+7-passti.deduct.prevUID.length,passti.deduct.prevUID.length);
        //idx+=prevUID.length;
        idx+=7;//fixed length 7 bytes UID

        System.arraycopy(passti.ctlinfo.value,0,cmdSendToSAM,idx,passti.ctlinfo.len_value);
        idx+=passti.ctlinfo.len_value;

        System.arraycopy(amount,0,cmdSendToSAM,idx,amount.length);


        idx+=amount.length;


        iResult=MDR_SAM_ERR_STARTCORRECTION;
        //Log_processtep();
        byte[] command = new byte[idx];
        System.arraycopy(cmdSendToSAM,0,command,0,command.length);

        RAPDU = device.sendSAM(samslot,command);
        if(RAPDU == null){
            return MDR_SAM_ERR_STARTCORRECTION;
        }
        if(APDUHelper.CheckResult(RAPDU) != OK){
            return MDR_SAM_ERR_STARTCORRECTION;
        }
        iResult = OK;
        do {
            //Log.d(TAG,"RDR<-SAM : "+ BytesUtil.bytes2HexString(RAPDU));
            if(iResult!=OK)
                return iResult;

            cmd = new byte[300];
            iResult=RAPDU[1];
            result = new byte[iResult];
            //memcpy(result,&apdubuf[2],iResult);
            System.arraycopy(RAPDU,2,result,0,iResult);

            mdrtag("RAPDU[0]",RAPDU[0]);
            switch (RAPDU[0]) {
                case TAG_PICCTCL:
                    //Log.d(TAG, "TAG_PICCTCL");

                    //ambil saldo
                    //>>0200066100B200 00 1E CB
                    //<<020021 00 0112091626321234567800000000012001000000216F00002B00000000039000E7

                    if (RAPDU[1] == 0x05) {
                        byte[] readbin = {(byte) 0x00,(byte)  0xB2,(byte)  0x00,(byte)  0x00};
                        //memcpy(temp,result,4);
                        System.arraycopy(result,0,temp,0,4);

                        if(BytesUtil.bytecmp(temp,readbin,readbin.length) == OK){
                            fSaveSaldo = true;
                        }else fSaveSaldo = false;
//

                    }
                    RAPDU = device.sendCTL(result);
                    if(RAPDU == null){

                        passti.SetAction(REPURCHASE);

                        return ERR_REPURCHASEREQUIRED;
                    }
                    if(APDUHelper.CheckResult(RAPDU)!= OK){

                        passti.SetAction(REPURCHASE);
                        return ERR_REPURCHASEREQUIRED;
                    }

                    if (fSaveSaldo)
                    {

                        System.arraycopy(passti.deduct.prevBalance,0,RAPDU,20,passti.deduct.prevBalance.length);

                        fSaveSaldo = false;
                    }

                    cmd=SAM_ComposeCMD(RAPDU,RAPDU.length,cmd);
                    cmd = trim(cmd);
                    break;
                case TAG_TERMINAL:

                    mdrtagpurpose("RAPDU[2]",RAPDU[2]);
                    switch (RAPDU[2]) {
                        case PURPOSE_BALANCE:

                            cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                            cmd = trim(cmd);
                            iResult=MDR_SAM_ERR_PURPOSE_BALANCE;
                            break;
                        case PURPOSE_ERRORMSGFROMSAM:

                            mdrerrormessage("RAPDU[3]",RAPDU[3]);
                            switch (RAPDU[3]) {
                                case 0x54:
                                    step = 1;
                                    iResult =CTL_ERR_TIMEWINDOW;
                                    execute=false;
                                    //*outLen=0;
                                    cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                                    cmd=trim(cmd);
                                    //Log.d(TAG,"CMD: " + BytesUtil.bytes2HexString(cmd));
                                    RAPDU = device.sendSAM(samslot,cmd);
                                    if(RAPDU == null){
                                        return CTL_ERR_TIMEWINDOW;
                                    }
                                    if(APDUHelper.CheckResult(RAPDU) != OK){
                                        return CTL_ERR_TIMEWINDOW;
                                    }
                                    //SendSAM(port_mdr,cmd,idx,apdubuf,&apdulen);
                                    break;
                                case 0x68:
                                    step = 2;

                                    execute=false;
                                    //*outLen=0;
                                    cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                                    cmd = trim(cmd);
                                    RAPDU = device.sendSAM(samslot,cmd);
                                    if(RAPDU == null){
                                        return MDR_SAM_ERR_PURPOSE_SAMMESSAGE;
                                    }
                                    if(APDUHelper.CheckResult(RAPDU)!=OK){
                                        return MDR_SAM_ERR_PURPOSE_SAMMESSAGE;
                                    }
                                    //SendSAM(port_mdr,cmd,idx,apdubuf,&apdulen);
                                    break;
                                case 0x5E:
                                    step = 3;
                                    //Log.d(TAG,"Fail execute correction, reset autocorrection flag\r");
                                    //doCorrection=false;
                                    passti.SetAction(NORMAL);
                                    break;
                                default:
                                    //*outLen=0;
                                    step = 0;
                                    cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                                    cmd=trim(cmd);
                                    RAPDU = device.sendSAM(samslot,cmd);
                                    if(RAPDU == null){
                                        return MDR_SAM_ERR_PURPOSE_SAMMESSAGE;
                                    }
                                    if(APDUHelper.CheckResult(RAPDU)!=OK){
                                        return MDR_SAM_ERR_PURPOSE_SAMMESSAGE;
                                    }

                                    execute=false;

                                    break;

                            }

                            iResult=MDR_SAM_ERR_PURPOSE_SAMMESSAGE+step;
                            execute=false;
                            break;
                        case PURPOSE_SAMAUTOGENTRXREPORT:


                            cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                            cmd=trim(cmd);
                            break;
                        case PURPOSE_ENDOFSEQUENCE:
                            iResult=MDR_SAM_ERR_PURPOSE_ENDSEQ+step;
                            iResult=SAMCMD_GetTrxReport();
                            if(iResult==OK)
                            {
                                //doCorrection = false;
                                passti.SetAction(NORMAL);
                                iBalance = converter.ByteArrayToInt(RAPDU,27,4, converter.LITTLE_ENDIAN);
                                saldoAkhir = String.valueOf(iBalance);
                                byte[] curBalance = new byte[4];
                                System.arraycopy(RAPDU,27,curBalance,0,4);
                                //byte[] devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
                                byte[] devicetime = bcd_ddmmyyhhmmss_to_bcd_ddmmyyyyhhmmss(date);
                                byte[] be_amount = new byte[4];


                                be_amount=ChangeEndian(amount,amount.length);
                                //passti.log = passti.PASSTI_ComposeTransactionLog(aposUtility.UID,params.ctype_mdr,devicetime,passti.ctlinfo.cardno,curBalance,amount,trxno,RAPDU,others);
                                byte[] be_curBalance = new byte[4];
                                be_curBalance=ChangeEndian(curBalance,curBalance.length);
                                SaveLog(RAPDU);
                                passti.log = passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value, params.ctype_mdr,devicetime,passti.ctlinfo.cardno,be_curBalance,be_amount,trxno,banklog,others);

                                Arrays.fill(cmdSendToCL,(byte)0x00);
                                Arrays.fill(cmdSendToSAM,(byte)0x00);
                                Arrays.fill(RAPDU,(byte)0x00);

                            }
                            execute=false;
                            return OK;
                        default:
                            cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                            cmd=trim(cmd);
                            RAPDU = device.sendSAM(samslot,cmd);
                            if(RAPDU == null){
                                return MDR_SAM_ERR_DEBITUNKNOWNTAG;
                            }
                            if(APDUHelper.CheckResult(RAPDU)!=OK){
                                return MDR_SAM_ERR_DEBITUNKNOWNTAG;
                            }
                            //SendSAM(port_mdr,cmd,idx,apdubuf,&apdulen);
                            execute=false;
                            //return MDR_SAM_ERR_DEBITUNKNOWNTAG;

                    }
                    break;
            }
            if(execute)
            {

                RAPDU = device.sendSAM(samslot,cmd);
                if(RAPDU == null){
                    return MDR_SAM_ERR_DEBITUNKNOWNTAG;
                }
                if(APDUHelper.CheckResult(RAPDU)!=OK){
                    return MDR_SAM_ERR_DEBITUNKNOWNTAG;
                }
                iResult = OK;
                //iResult=SendSAM(port_mdr,cmd,idx,apdubuf,&apdulen);
            }


        }while (execute);

        return iResult;
    }

    int CorrectionJCOPBM(byte[] date, byte[] amount) throws RemoteException {
        /*date in BCD: ddmmyyhhmmss*/
        /*amount in LITTLE_ENDIAN, conversion has been done in mandiri_deduct*/
        Log.d(TAG, TAG+".CorrectionJCOPBM");
        int result;
        int iResult;

        result = CTL_MDRJCOP_ReadHistory();
        if (result != OK) {

            return MDR_CTL_ERR_READHISTORY;
        }
        //Log.d(TAG,"CTL History " + BytesUtil.bytes2HexString(RAPDU));
        iResult=SAM_GetCopyCardHistory(RAPDU,date);
        //Log.d(TAG,"SAM_GetCopyCardHistory "+ BytesUtil.bytes2HexString(RAPDU));
        if(iResult!=OK)
        {
            if((RAPDU.length==2)&&(RAPDU[0]==0x63))
            {
                iResult=Mandiri_SAMInitialization(); //201908121607,vivo, if card not yet deducted during autocompletion, has to init SAM
                if (iResult != OK)
                {
                    //deduct.doCorrection=0;
                    return iResult;
                }
                /*else
                {
                    return iResult;
                }*/
                iResult = DebitProcessJCOPBM(date,amount);


                if (iResult == OK)
                {
                    //doCorrection=false;
                    passti.SetAction(NORMAL);
                    return OK;
                }
                else
                {
                    return iResult;
                }
            }
        }
        if(RAPDU.length==2)
        {
            iResult=Mandiri_SAMInitialization(); //201908121607,vivo, if card not yet deducted during autocompletion, has to init SAM
            if (iResult != OK)
            {
                //deduct.doCorrection=0;
                return iResult;
            }

            //Log.d(TAG, "DEBIT CORRECTION - return 9000 tanpa data settlement, lakukan deduct ulang");
            iResult = DebitProcessJCOPBM(date,amount);
            if (iResult == OK)
            {
                //doCorrection=false;
                passti.SetAction(NORMAL);
            }

            return  iResult;

            //h2core_set_debug_console_output(H2HW_DISABLE_SERIAL_DEBUG);
            //ctlinfo.len_log=0;
            //return OK;
        }
        else
        {
            Log.d(TAG,"SAM auto generate report, tidak perlu deduct, kirim balance dan cardnum");
            //Log.d(TAG, "DEBIT CORRECTION - SAM auto generate report, tidak perlu deduct");
            iBalance = converter.ByteArrayToInt(RAPDU, (byte) 27, (byte) 4, converter.LITTLE_ENDIAN);
            saldoAkhir = String.valueOf(iBalance);
            passti.dbghelper.DebugInteger("iBalance",iBalance);
            passti.dbghelper.DebugString("saldoAkhir",saldoAkhir);
            byte[] curBalance = new byte[4];
            System.arraycopy(RAPDU,27,curBalance,0,4);
            //byte[] devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
            byte[] devicetime = bcd_ddmmyyhhmmss_to_bcd_ddmmyyyyhhmmss(date);
            byte[] be_amount = new byte[4];
            be_amount=ChangeEndian(amount,amount.length);
            //passti.log=passti.PASSTI_ComposeTransactionLog(aposUtility.UID,params.ctype_mdr,devicetime,passti.ctlinfo.cardno,curBalance,amount,trxno,RAPDU,others);
            byte[] be_curBalance = new byte[4];
            be_curBalance=ChangeEndian(curBalance,curBalance.length);
            //passti.log=passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value,params.ctype_mdr,devicetime,passti.ctlinfo.cardno,be_curBalance,be_amount,trxno,RAPDU,others);
            SaveLog(RAPDU);
            passti.log = passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value, params.ctype_mdr,devicetime,passti.ctlinfo.cardno,be_curBalance,be_amount,trxno,banklog,others);


            passti.SetAction(NORMAL);

            return OK;
        }
    }

    int CTL_MDRJCOP_ReadHistory() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] mdr_ReadHistory 	= {(byte)0x00,(byte)0xB2,(byte)0x00,(byte)0x00,(byte)0x28};
        int result;


        RAPDU = device.sendCTL(mdr_ReadHistory);

        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;
        return OK;
    }

    int SAM_GetCopyCardHistory(byte[] inData,byte[] date) throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] mdr_SAM_CopyCardHistory	= {(byte)0x00,(byte)0x12,(byte)0x04,(byte)0x00,(byte)0x67};
        byte[] cmd;
        int idx;
        int iResult;
        byte[] localUID=new byte[7];

        passti.dbghelper.DebugHex("inData",inData);
        passti.dbghelper.DebugHex("passti.ctlinfo.value",passti.ctlinfo.value);
        System.arraycopy(passti.ctlinfo.uid.value,0,localUID,7- passti.ctlinfo.uid.value.length, passti.ctlinfo.uid.value.length);
        //printf("localUID : ");printhex(localUID,sizeof(localUID));
        cmdSendToSAM = new byte[255];
        idx=0;

        System.arraycopy(mdr_SAM_CopyCardHistory,0,cmdSendToSAM,idx,mdr_SAM_CopyCardHistory.length);
        idx+=mdr_SAM_CopyCardHistory.length;
        System.arraycopy(date,0,cmdSendToSAM,idx,date.length);
        idx+=date.length;
        System.arraycopy(localUID,0,cmdSendToSAM,idx,localUID.length);
        idx+=localUID.length;
        System.arraycopy(passti.ctlinfo.value,0,cmdSendToSAM,idx,passti.ctlinfo.len_value);
        idx+=passti.ctlinfo.len_value;


        System.arraycopy(inData,0,cmdSendToSAM,idx,inData.length-2);
        idx+=inData.length-2;
        cmd = new byte[idx];
        System.arraycopy(cmdSendToSAM,0,cmd,0,cmd.length);

        RAPDU = device.sendSAM(samslot,cmd);


        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;
        return OK;

    }


    int CTL_ReadData()  throws RemoteException{

        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] mdr_readdata				={0x00,(byte)0xB3,0x00,0x00,0x3F};


        RAPDU = device.sendCTL(mdr_readdata);
        if (RAPDU == null)
            return ERR_NO_RESP;
        if (RAPDU.length < 2)
            return ERR_NO_RESP;
        if (APDUHelper.CheckResult(RAPDU) != 0)
            return ERR_SW1SW2;

        ctl_mdr.appid=RAPDU[offset_appid];
        Log.d(TAG,"ctl_mdr.appid "+Hex.byteToHexString(ctl_mdr.appid));
        Log.d(TAG,"RAPDU[offset_appid] "+Hex.byteToHexString(RAPDU[offset_appid]));

        System.arraycopy(RAPDU,0,passti.ctlinfo.cardno,0,8);
        passti.dbghelper.DebugHex("passti.ctlinfo.cardno",passti.ctlinfo.cardno);
        System.arraycopy(RAPDU,0,passti.ctlinfo.value,0,63);
        passti.ctlinfo.len_value=63;
        passti.dbghelper.DebugHex("passti.ctlinfo.value",passti.ctlinfo.value,passti.ctlinfo.len_value);
        passti.dbghelper.DebugInteger("passti.ctlinfo.len_value",passti.ctlinfo.len_value);


        return OK;

    }


    public int Mandiri_CTL_Initialization() throws RemoteException {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        int result = CTL_ReadCardInfo_JCOPBM();
        if (result != OK) {
            if(result==ERR_SW1SW2)
            {
                passti.dbghelper.DebugPrintString( "Mandiri Non-JCOPBM");
                result = CTL_SelectApp();
                if (result != OK)
                    return MDR_CTL_ERR_SELECTAPP;

                result = CTL_ReadData();
                if (result != OK)
                    return MDR_CTL_ERR_READDATA;

                passti.dbghelper.DebugPrintString( "Mandiri standard");
                ctl_mdr.JCOP=false;
                //return OK;
            }
            else
                return result;
        } else {
            passti.dbghelper.DebugPrintString( "Mandiri JCOPBM");
            ctl_mdr.JCOP=true;
            //return OK;
        }

        return result;

    }


    int CheckBalance_app02(byte[] date) throws RemoteException
    {
        //date in bcd format
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] commit = { 0x00, (byte)0xDD, 0x00, 0x00, 0x00};
        boolean execute=true;
        byte[] cmd=new byte[255];
        byte[] result=new byte[255];
        int idx;
        int iResult;
        int commit_flag = 0;
        byte[] temp = new byte[4];
        byte[] mdr_SAM_GetBalance		= { (byte)0x00,  (byte)0xC3,  (byte)0x01,  (byte)0x00,  (byte)0x4C};


        idx=0;

        cmdSendToSAM = new byte[mdr_SAM_GetBalance.length + date.length + 7 + passti.ctlinfo.len_value];
        Arrays.fill(cmdSendToSAM,(byte)0x00);
        System.arraycopy(mdr_SAM_GetBalance,0,cmdSendToSAM,idx,mdr_SAM_GetBalance.length);
        idx+=mdr_SAM_GetBalance.length;
        System.arraycopy(date,0,cmdSendToSAM,idx,6);
        idx+=6;

        if(passti.ctlinfo.uid.len<7)
        {
            idx+=(7-passti.ctlinfo.uid.len);
        }
        System.arraycopy(passti.ctlinfo.uid.value,0,cmdSendToSAM,idx,passti.ctlinfo.uid.len);
        idx+=passti.ctlinfo.uid.len;



        System.arraycopy(passti.ctlinfo.value,0,cmdSendToSAM,idx,passti.ctlinfo.len_value);
        idx+=passti.ctlinfo.len_value;




        iResult=MDR_SAM_ERR_STARTCHECKBALANCE;


        RAPDU = device.sendSAM(samslot,cmdSendToSAM);
        if(RAPDU == null){
            //Log.d(TAG,"Failed sendSAM DebitProcess");
            return MDR_SAM_ERR_DEBITUNKNOWNTAG;
        }
        if(APDUHelper.CheckResult(RAPDU) != 0){
            //Log.d(TAG,"Failed sendSAM DebitProcess !=9000");
            return MDR_SAM_ERR_DEBITUNKNOWNTAG;
        }
        iResult = OK;
        do
        {
            if(iResult!=OK)
                return iResult;

            cmd = new byte[300];
            iResult=RAPDU[1];


            passti.dbghelper.DebugInteger("iResult" , iResult);
            result = new byte[iResult];
            //memcpy(result,&apdubuf[2],iResult);
            System.arraycopy(RAPDU,2,result,0,iResult);


            passti.dbghelper.DebugHex("RAPDU[0]",RAPDU,0,1);
            mdrtag("RAPDU[0]",RAPDU[0]);
            switch(RAPDU[0])
            {
                case TAG_PICCTCL:
                    if(RAPDU[1]==0x05)
                    {


                        passti.dbghelper.DebugHex("result",result);
                        passti.dbghelper.DebugHex("commit",commit);

                        if(Arrays.equals(commit,result))
                        {
                            Log.d(TAG,"Commit command");
                            commit_flag = 1;
                        }

                    }

                    RAPDU = device.sendCTL(result);
                    if(RAPDU == null){
                        //MDR_SetCorrectionFlag();
                        passti.SetAction(REPURCHASE);
                        //Log.d(TAG,"Failed DebitProcess");
                        return ERR_NO_RESP;
                    }
                    if(APDUHelper.CheckResult(RAPDU) != 0){
                        //Log.d(TAG,"Failed DebitProcess");
                        //MDR_SetCorrectionFlag();
                        passti.SetAction(REPURCHASE);
                        return ERR_SW1SW2;
                    }

                    cmd=SAM_ComposeCMD(RAPDU,RAPDU.length,cmd);
                    cmd = trim(cmd);

                    break;



                case TAG_TERMINAL:

                    mdrtagpurpose("RAPDU[2]",RAPDU[2]);
                    switch(RAPDU[2])
                    {


                        //cmd = RAPDU;

                        case PURPOSE_BALANCE:
                            //passti.dbghelper.DebugPrintString("PURPOSE_BALANCE");
                           // passti.dbghelper.DebugPrintString("Save Balance");
                            System.arraycopy(RAPDU,3,temp,0,4);

                            //ChangeEndian(&apdubuf[3],4,ctlinfo.bBalanceKartu,&ctlinfo.len_BalanceKartu);
                            passti.ctlinfo.bBalanceKartu=ChangeEndian(temp,4);
                            passti.dbghelper.DebugHex("passti.ctlinfo.bBalanceKartu ",passti.ctlinfo.bBalanceKartu);

                            passti.ctlinfo.lBalanceKartu=converter.ByteArrayToInt(passti.ctlinfo.bBalanceKartu,0,4,converter.BIG_ENDIAN);
                            passti.dbghelper.DebugLong("passti.ctlinfo.lBalanceKartu ",passti.ctlinfo.lBalanceKartu);
                            iBalance=converter.ByteArrayToInt(passti.ctlinfo.bBalanceKartu,0,4,converter.BIG_ENDIAN);
                            saldoAwal = String.valueOf(iBalance);
                            passti.dbghelper.DebugLong("iBalance",iBalance);
                            cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                            cmd = trim(cmd);
                            iResult=MDR_SAM_ERR_PURPOSE_BALANCE;
                            break;
                        case PURPOSE_ERRORMSGFROMSAM:
                          //  passti.dbghelper.DebugPrintString("PURPOSE_ERRORMSGFROMSAM ");
                            mdrerrormessage("RAPDU[3]",RAPDU[3]);
                            switch(RAPDU[3])
                            {
                                case MDR_CARDEXPIRED:
                                case MDR_ENDOFGRACEPERIOD : //_ADD_MDR001_
                                    execute=false;
                                    iResult =MDR_ENDOFGRACEPERIOD;

                                    break;
                                case 0x6C:

                                    cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                                    cmd = trim(cmd);
                                    break;
                                case 0x54:
                                    iResult =CTL_ERR_TIMEWINDOW;
                                    execute=false;

                                    cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                                    cmd = trim(cmd);
                                    //iResult=SendSAM(port_mdr,cmd,idx,apdubuf,&apdulen);
                                    RAPDU = device.sendSAM(samslot,cmd);
                                    if(RAPDU == null){

                                        return ERR_NO_RESP;
                                    }
                                    if(APDUHelper.CheckResult(RAPDU) != 0){

                                        return ERR_SW1SW2;
                                    }
                                    break;
                                case 0x68:
                                    execute=false;

                                    cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                                    cmd = trim(cmd);
                                    //SendSAM(port_mdr,cmd,idx,apdubuf,&apdulen);
                                    RAPDU = device.sendSAM(samslot,cmd);
                                    if(RAPDU == null){

                                        return ERR_NO_RESP;
                                    }
                                    if(APDUHelper.CheckResult(RAPDU) != 0){

                                        return ERR_SW1SW2;
                                    }
                                    iResult=MDR_SAM_ERR_PURPOSE_SAMMESSAGE;
                                    break;
                                case 0x5E://error code dari SAM utk menandakan deduct ada kegagalan, selanjutnya lakukan deduct correction

                                    execute=false;
                                    passti.SetAction(REPURCHASE);	//2020-01,06,vivo, ask user to tap card again after lost contact
                                    return ERR_REPURCHASEREQUIRED;



                            }


                            //execute=false;
                            break;
                        case PURPOSE_SAMAUTOGENTRXREPORT:
                          //  passti.dbghelper.DebugPrintString("PURPOSE_SAMAUTOGENTRXREPORT");

                            cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                            cmd = trim(cmd);
                            break;
                        case PURPOSE_ENDOFSEQUENCE:
                         //   passti.dbghelper.DebugPrintString("PURPOSE_ENDOFSEQUENCE");
                            iResult=MDR_SAM_ERR_PURPOSE_ENDSEQ;

                            execute=false;
                            return OK;
                        default:
                            //idx=SAM_ComposeCMD(SW_9000,sizeof(SW_9000),cmd);
                            //SendSAM(port_mdr,cmd,idx,apdubuf,&apdulen);

                            cmd=SAM_ComposeCMD(SW_9000,SW_9000.length,cmd);
                            cmd = trim(cmd);
                            //SendSAM(port_mdr,cmd,idx,apdubuf,&apdulen);
                            RAPDU = device.sendSAM(samslot,cmd);
                            if(RAPDU == null){

                                return ERR_NO_RESP;
                            }
                            if(APDUHelper.CheckResult(RAPDU) != 0){

                                return ERR_SW1SW2;
                            }
                            return MDR_SAM_ERR_DEBITUNKNOWNTAG;
                    }
                    break;
            }
            if(execute)
            {
                //iResult=SendSAM(port_mdr,cmd,idx,apdubuf,&apdulen);
                RAPDU = device.sendSAM(samslot,cmd);
                if(RAPDU == null){

                    return ERR_NO_RESP;
                }
                if(APDUHelper.CheckResult(RAPDU) != 0){

                    return ERR_SW1SW2;
                }
                iResult = OK;
            }



        } while(execute);

        return iResult;

    }

    void mdrtag(String label ,byte tag)
    {
        switch(tag)
        {
            case  TAG_TERMINAL  : Log.e(TAG,label +",code : "+Hex.byteToHexString(tag) +  ",desc : TAG_TERMINAL"); break;
            case  TAG_PICCTCL : Log.e(TAG,label +",code : "+Hex.byteToHexString(tag) +  ",desc : TAG_PICCTCL");break;
            default:
                Log.e(TAG,label +",code : "+Hex.byteToHexString(tag));break;

        }

    }

    void mdrtagpurpose(String label ,byte tag)
    {
        switch(tag)
        {

            case  PURPOSE_BALANCE :Log.e(TAG,label+",code : "+Hex.byteToHexString(tag) + ",desc : PURPOSE_BALANCE");break;
            case  PURPOSE_SAMINFO :Log.e(TAG,label +",code : "+Hex.byteToHexString(tag) +  ",desc : PURPOSE_SAMINFO");break;
            case  PURPOSE_ERRORMSGFROMSAM :Log.e(TAG,label +",code : "+Hex.byteToHexString(tag) +  ",desc : PURPOSE_ERRORMSGFROMSAM");break;
            case  PURPOSE_SAMAUTOGENTRXREPORT :Log.e(TAG,label +",code : "+Hex.byteToHexString(tag) +  ",desc : PURPOSE_SAMAUTOGENTRXREPORT");break;
            case  PURPOSE_POINTINFO :Log.e(TAG,label +",code : "+Hex.byteToHexString(tag) +  ",desc : PURPOSE_POINTINFO");break;
            case  PURPOSE_DATAPACKAGE :Log.e(TAG,label +",code : "+Hex.byteToHexString(tag) +  ",desc : PURPOSE_DATAPACKAGE");break;
            case  PURPOSE_SAMCONFIG :Log.e(TAG,label +",code : "+Hex.byteToHexString(tag) +  ",desc : PURPOSE_SAMCONFIG");break;
            case  PURPOSE_ENDOFSEQUENCE :Log.e(TAG,label +",code : "+Hex.byteToHexString(tag) +  ",desc : PURPOSE_ENDOFSEQUENCE");break;
            default:
            Log.e(TAG,label +",code : "+Hex.byteToHexString(tag));break;


        }

    }
    void mdrerrormessage(String label ,byte tag)
    {
        switch(tag)
        {
            case  MDR_ENDOFGRACEPERIOD :Log.e(TAG,label +",code : "+Hex.byteToHexString(tag) +  ",desc : MDR_ENDOFGRACEPERIOD");break;
            case  MDR_CARDEXPIRED :Log.e(TAG,label +",code : "+Hex.byteToHexString(tag) +  ",desc  : MDR_CARDEXPIRED");break;
            default:
                Log.e(TAG,label +",code : "+Hex.byteToHexString(tag));break;

        }

    }



}

