package id.co.softorb.lib.smartcard.pax;

import android.os.SystemClock;
import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationListener;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.msr.MSRDevice;
import com.cloudpos.msr.MSROperationResult;
import com.cloudpos.msr.MSRTrackData;
import com.pax.dal.IDAL;
import com.pax.dal.IMag;
import com.pax.dal.entity.TrackData;
import com.pax.dal.exceptions.MagDevException;

import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.Base;
import id.co.softorb.lib.smartcard.posdevice.DeviceBase;

import static id.co.softorb.lib.helper.ErrorCode.ERR_INITMSR;
import static id.co.softorb.lib.helper.ErrorCode.ERR_OPENMSR;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;


class msr extends Base
{
    String tagclass= this.getClass().getSimpleName();
    private IMag iMagPAX;
    private IDAL dal;
    byte[][] msrtrackData = new byte[3][];
    PAXMagReadThread paxmsr= new PAXMagReadThread();
    private POSListener detectListener;
    public TrackData PAX_Mag_Read() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            TrackData trackData = iMagPAX.read();

            return trackData;
        } catch (MagDevException e) {
            e.printStackTrace();
            return null;
        }
    }


    class PAXMagReadThread extends Thread {
        @Override
        public void run() {
            long start;
            long finish;
            super.run();
            start = System.currentTimeMillis();
            while (!Thread.interrupted()) {
                finish= System.currentTimeMillis();
                if((finish-start) >waitingtime)
                {
                    detectListener.onMSRDetected(null);
                    break;
                }
                if (PAX_MagisSwiped()) {
                    TrackData trackData = PAX_Mag_Read();
                    if (trackData != null) {
                        String resStr = "";
                        if (trackData.getResultCode() == 0) {

                            Log.e(TAG,"Magstripe error");
                            continue;
                        }
                        msrtrackData = new byte[3][];
                        if ((trackData.getResultCode() & 0x01) == 0x01) {

                            Log.e(TAG,"Magstripe track1 "+trackData.getTrack1());
                            msrtrackData[0]=trackData.getTrack1().getBytes();
                        }
                        if ((trackData.getResultCode() & 0x02) == 0x02) {

                            Log.e(TAG,"Magstripe track2 "+trackData.getTrack2());
                            msrtrackData[1]=trackData.getTrack2().getBytes();
                        }
                        if ((trackData.getResultCode() & 0x04) == 0x04) {

                            Log.e(TAG,"Magstripe track3 "+trackData.getTrack3());
                            msrtrackData[2]=trackData.getTrack3().getBytes();

                        }
                        detectListener.onMSRDetected(msrtrackData);
                        break;
                    }

                }
                SystemClock.sleep(100);
            }
        }
    }

    // Check whether a card is swiped
    public boolean PAX_MagisSwiped() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        boolean b = false;
        try {
            b = iMagPAX.isSwiped();
            // logTrue("isSwiped");
        } catch (MagDevException e) {
            e.printStackTrace();
        }
        return b;
    }

    @Override
    public int init(POSListener detectListener,Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        this.detectListener=detectListener;
        dal = (IDAL)devInstance;
        if(dal==null)
            return NOT_OK;
        iMagPAX = dal.getMag();
        if(iMagPAX==null)
            return NOT_OK;
        //result=open();
        return OK;
    }



    @Override
    public int open() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            iMagPAX.open();
        } catch (MagDevException e) {
            e.printStackTrace();
            return ERR_OPENMSR;
        }
        return OK;
    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            iMagPAX.close();
        } catch (MagDevException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return OK;

    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        paxmsr.run();
        return WAITING;
    }

    @Override
    public void stopfind() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(paxmsr.isAlive()) {
            paxmsr.interrupt();
        }

    }



}
