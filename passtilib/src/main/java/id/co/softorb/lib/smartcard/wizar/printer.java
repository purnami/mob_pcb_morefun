package id.co.softorb.lib.smartcard.wizar;

import android.graphics.Bitmap;
import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.POSTerminal;
import com.cloudpos.printer.Format;
import com.cloudpos.printer.PrinterDevice;

import id.co.softorb.lib.helper.printerparam;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.POSPrinter;

import static id.co.softorb.lib.helper.ErrorCode.ERR_INITPRINTER;
import static id.co.softorb.lib.helper.ErrorCode.ERR_OPENPRINTER;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;

public class printer extends POSPrinter
{
    String tagclass= this.getClass().getSimpleName();
    private POSTerminal posTerminal;
    private PrinterDevice printer;

    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        posTerminal=(POSTerminal) devInstance;
        printer =(PrinterDevice) posTerminal.getDevice("cloudpos.device.printer");
        if (printer != null) {
            return OK;
        } else
            return ERR_INITPRINTER;




    }
/*
        @Override
        public int init() {
            return ERR_INITPRINTER;
        }*/

    @Override
    public int open() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            printer.open();



        } catch (DeviceException e) {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());
            return ERR_OPENPRINTER;
        }
        return OK;
    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            printer.close();



        } catch (DeviceException e)
        {
            Log.e(TAG,e.toString());
            return NOT_OK;
        }
        return OK;
    }

    @Override
    public int find() {
        return 0;
    }

    @Override
    public void stopfind() {

    }


    public int text(int fonttype,int alignment,String text,boolean newline)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        Format defaultformat = new Format();


        switch(fonttype)
        {
            case printerparam.FONT_BOLD:
                defaultformat.setParameter(Format.FORMAT_FONT_BOLD, Format.FORMAT_FONT_VAL_TRUE);
                defaultformat.setParameter(Format.FORMAT_FONT_ITALIC, Format.FORMAT_FONT_VAL_FALSE);
                break;
            case printerparam.FONT_ITALIC:
                defaultformat.setParameter(Format.FORMAT_FONT_ITALIC, Format.FORMAT_FONT_VAL_TRUE);
                defaultformat.setParameter(Format.FORMAT_FONT_BOLD, Format.FORMAT_FONT_VAL_FALSE);
                break;

            default:
                defaultformat.setParameter(Format.FORMAT_FONT_ITALIC, Format.FORMAT_FONT_VAL_FALSE);
                defaultformat.setParameter(Format.FORMAT_FONT_BOLD, Format.FORMAT_FONT_VAL_FALSE);
                break;
        }
        switch(alignment)
        {
            case printerparam.ALIGN_LEFT:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                break;
            case printerparam.ALIGN_CENTER:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                break;
            case printerparam.ALIGN_RIGHT:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_RIGHT);
                break;
            default:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                break;
        }
        try {

            if (newline) {
                printer.printlnText(defaultformat,text);
            } else {
                printer.printText(defaultformat,text);
            }
        }
        catch (DeviceException e)
        {
            Log.e(TAG,e.toString());
            return NOT_OK;
        }
        return OK;
    }

    @Override
    public int cutpaper() {
        return NOT_OK;
    }

    public int linefeed()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmd= {(byte)0x0A};
        try{
            printer.sendESCCommand(cmd);
        }
        catch (DeviceException e)
        {
            Log.e(TAG,e.toString());
        }
        return OK;
    }

    @Override
    public int status() {
        return 0;
    }

    public int bitmap(int alignment, Bitmap bitmap)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        Format defaultformat = new Format();
        switch(alignment)
        {
            case printerparam.ALIGN_LEFT:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                break;
            case printerparam.ALIGN_CENTER:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                break;
            case printerparam.ALIGN_RIGHT:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_RIGHT);
                break;
            default:
                defaultformat.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                break;
        }
        try {

            printer.printBitmap(defaultformat,bitmap);


        }
        catch (DeviceException e)
        {
            Log.e(TAG,e.toString());
            return NOT_OK;
        }
        return OK;
    }


}