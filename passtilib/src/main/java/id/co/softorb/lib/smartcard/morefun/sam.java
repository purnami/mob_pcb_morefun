package id.co.softorb.lib.smartcard.morefun;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationListener;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.card.ATR;
import com.cloudpos.card.CPUCard;
import com.cloudpos.card.Card;
import com.cloudpos.rfcardreader.RFCardReaderDevice;
import com.cloudpos.smartcardreader.SmartCardReaderDevice;
import com.cloudpos.smartcardreader.SmartCardReaderOperationResult;
import com.morefun.yapi.ServiceResult;
import com.morefun.yapi.card.cpu.APDUCmd;
import com.morefun.yapi.card.cpu.CPUCardHandler;
import com.morefun.yapi.device.reader.icc.ICCSearchResult;
import com.morefun.yapi.device.reader.icc.IccCardReader;
import com.morefun.yapi.device.reader.icc.IccCardType;
import com.morefun.yapi.device.reader.icc.IccReaderSlot;
import com.morefun.yapi.device.reader.icc.OnSearchIccCardListener;
import com.morefun.yapi.engine.DeviceServiceEngine;

import java.util.Arrays;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.ContactBase;

import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INITREADER;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.SAM_ERR_RELEASEFAIL;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;

public class sam extends ContactBase {
    String tagclass= this.getClass().getSimpleName();
    private DeviceServiceEngine deviceService;
    private POSListener detectListener;
    private IccCardReader samReader;
    private CPUCardHandler cpuCardHandler;
    public sam() {
    }

    @Override
    public byte[] sendCB(int slot, byte[] apdu) {
        byte[] result=new byte[0];
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] tmp;

        try {
            Thread.sleep(500);
            tmp = new byte[256];
            int ret = cpuCardHandler.exchangeCmd(tmp, apdu, apdu.length);
            result=Hex.subByte(tmp, 0, ret);

        } catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        try {
            Bundle bundle = new Bundle();
            deviceService=(DeviceServiceEngine) devInstance;
//            deviceService.login(bundle,"09000000");
            samReader=deviceService.getIccCardReader(IccReaderSlot.PSAMSlOT1);
            Log.d("samreader", ""+samReader.isCardExists());
//            cpuCardHandler= deviceService.getCPUCardHandler(samReader);
//            cpuCardHandler.active();

            if (samReader != null) {
                detectListener = listener;
                return OK;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return SAM_ERR_RELEASEFAIL;
    }

    @Override
    public int open() {
        //Tidak ada fungsi open()
        return OK;
    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return 0;
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            int result=samReader.searchCard(listener, 0, new String[]{IccCardType.PSAM});
            Log.d("ICcard2sam", String.valueOf(samReader.isCardExists())+" "+result);
            cpuCardHandler= deviceService.getCPUCardHandler(samReader);
            cpuCardHandler.active();
            cpuCardHandler.setPowerOn(new byte[]{0x00, 0x00});
        } catch (RemoteException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return WAITING;
    }

    @Override
    public void stopfind() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            samReader.stopSearch();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    OnSearchIccCardListener.Stub listener = new OnSearchIccCardListener.Stub() {
        @Override
        public void onSearchResult(final int retCode, Bundle bundle) throws RemoteException {
            byte[] tempuid=null;

            if (ServiceResult.Success == retCode) {
                String cardType = bundle.getString(ICCSearchResult.CARDTYPE);
                Log.d("cardType sam", cardType);
                int slot = bundle.getInt(ICCSearchResult.CARDOTHER);
                String uid = bundle.getString(ICCSearchResult.M1SN);
                tempuid=Hex.hexStringToByteArray(uid);
//                cpuCardHandler = deviceService.getCPUCardHandler(rfReader);
                cpuCardHandler.setPowerOn(new byte[]{0x00, 0x00});
                cpuCardHandler.active();
                Log.d("slot sam",""+slot+" uid="+uid+" tempuid="+Hex.bytesToHexString(tempuid));

            }
            detectListener.onSAMDetected(1,tempuid);
        }
    };

}
