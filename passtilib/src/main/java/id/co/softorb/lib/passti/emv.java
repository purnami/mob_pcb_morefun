package id.co.softorb.lib.passti;

import android.content.Context;
import android.util.Log;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.posdevice.reader;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import static id.co.softorb.lib.passti.helper.ErrorCode.*;

public class emv {

    public class tag
    {
        byte[] key=null;
        byte[] value=null;
        public tag(byte[] key,byte [] value)
        {
            this.key=key;
            this.value=value;
        }
        public byte[] Key()
        {
            return key;
        }
        public byte[] Value()
        {
            return value;
        }

    }
    final byte[] tag6F={(byte)0x6F};
    final byte[] tag70={(byte)0x70};

    final byte[] tag84={(byte)0x84};
    final byte[] tagA5={(byte)0xA5};
    final byte[] tag88={(byte)0x88};
    final byte[] tag61={(byte)0x61};
    final byte[] tag4F={(byte)0x4F};
    final byte[] tag50={(byte)0x50};
    final byte[] tag87={(byte)0x87};
    final byte[] tag80={(byte)0x80};

    final byte[] tag5F2D={(byte)0x5F,(byte)0x2D};
    final byte[] tag57={(byte)0x57};
    final byte[] tag5F20={(byte)0x5F,(byte)0x20};
    final byte[] tag9F1F={(byte)0x9F,(byte)0x1F};
    final byte[] tag5F24={(byte)0x5F,(byte)0x24};
    final byte[] tag5A={(byte)0x5A};
    final byte[] tag8C={(byte)0x8C};
    final byte[] tag8D={(byte)0x8D};
    final byte[] tag8E={(byte)0x8E};
    final byte[] tag5F25={(byte)0x5F,(byte)0x25};
    final byte[] tag5F28={(byte)0x5F,(byte)0x28};
    final byte[] tag5F30={(byte)0x5F,(byte)0x30};
    final byte[] tag5F34={(byte)0x5F,(byte)0x34};
    final byte[] tag9F07={(byte)0x9F,(byte)0x07};
    final byte[] tag9F08={(byte)0x9F,(byte)0x08};
    final byte[] tag9F0D={(byte)0x9F,(byte)0x0D};
    final byte[] tag9F0E={(byte)0x9F,(byte)0x0E};
    final byte[] tag9F0F={(byte)0x9F,(byte)0x0F};
    final byte[] tag9F42={(byte)0x9F,(byte)0x42};
    final byte[] tag9F44={(byte)0x9F,(byte)0x44};
    final byte[] tag9F11={(byte)0x9F,(byte)0x11};


    final byte[] tag8A={(byte)0x8A};

    final byte[] tag9F02={(byte)0x9F,(byte)0x02};
    final byte[] tag9F03={(byte)0x9F,(byte)0x03};
    final byte[] tag9F1A={(byte)0x9F,(byte)0x1A};
    final byte[] tag95={(byte)0x95};
    final byte[] tag9A={(byte)0x9A};
    final byte[] tag9C={(byte)0x9C};
    final byte[] tag9F37={(byte)0x9F,(byte)0x37};
    final byte[] tag5F2A={(byte)0x5F,(byte)0x2A};

    final byte[] tag9F38={(byte)0x9F,(byte)0x38};
    final byte[] tagBF0C={(byte)0xBF,(byte)0x0C};
    final byte[] tag9F12={(byte)0x9F,(byte)0x12};
    final byte[] tag9F4D={(byte)0x9F,(byte)0x4D};
    final byte[] tag77={(byte)0x77};
    final byte[] tag82={(byte)0x82};
    final byte[] tag94={(byte)0x94};
    final byte[] tag8F={(byte)0x8F};
    final byte[] tag92={(byte)0x92};
    final byte[] tag93={(byte)0x93};
    final byte[] tag9F32={(byte)0x9F,(byte)0x32};
    final byte[] tag9F4A={(byte)0x9F,(byte)0x4A};
    final byte[] tag90={(byte)0x90};
    final byte[] tag9F47={(byte)0x9F,(byte)0x47};
    final byte[] tag9F49={(byte)0x9F,(byte)0x49};

    final byte[] tag9F46={(byte)0x9F,(byte)0x46};

    final byte[] tag9F6C={(byte)0x9F,(byte)0x6C};




    Context ctx;
    reader device;
    byte[] apduresp;
    PASSTI passti;
    String tagclass;
    String TAG;

    Map<byte[], byte[]> gTaglist;
    int mode=0;//0= contact, 1=contactless
    public static final int CONTACT =0;
    public static final int CONTACTLESS =1;
    public emv(Context ctx,PASSTI passtiInstance,reader device)
    {
        this.ctx=ctx;
        this.device=device;
        passti=passtiInstance;
        tagclass = this.getClass().getSimpleName();
        TAG= passti.dbghelper.TAG;
        gTaglist=  new Hashtable<>();
    }

    boolean isConstructedDO(byte tag)
    {
        byte flag=0x20;
        if((tag&flag) == flag) {
            //passti.dbghelper.DebugPrintString("constructed DO");
            return true;
        }
        else {
            //passti.dbghelper.DebugPrintString("primitive DO");
            return false;
        }
    }
    public String EMVCard()
    {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        String emvdata="";
        //select pse
        String selectpse="00A404000E315041592E5359532E4444463031";
        apduresp=device.sendContact(Hex.hexStringToByteArray(selectpse));

        return emvdata;
    }

    public ArrayList<tag> InitTagList()
    {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        ArrayList<tag> taglist=new ArrayList<>();
        tag emvtag;
        emvtag= new tag(tag6F,null);taglist.add(emvtag);
        emvtag= new tag(tag70,null);taglist.add(emvtag);
        emvtag= new tag(tagA5,null);taglist.add(emvtag);
        emvtag= new tag(tag84,null);taglist.add(emvtag);
        emvtag= new tag(tag88,null);taglist.add(emvtag);


        emvtag= new tag(tag61,null);taglist.add(emvtag);

        emvtag= new tag(tag4F,null);taglist.add(emvtag);

        emvtag= new tag(tag50,null);taglist.add(emvtag);

        emvtag= new tag(tag87,null);taglist.add(emvtag);

        emvtag= new tag(tag80,null);taglist.add(emvtag);

        emvtag= new tag(tag5F2D,null);taglist.add(emvtag);


        emvtag= new tag(tag57,null);taglist.add(emvtag);


        emvtag= new tag(tag5F20,null);taglist.add(emvtag);

        emvtag= new tag(tag9F1F,null);taglist.add(emvtag);


        emvtag= new tag(tag5F24,null);taglist.add(emvtag);


        emvtag= new tag(tag5A,null);taglist.add(emvtag);


        emvtag= new tag(tag8C,null);taglist.add(emvtag);

        emvtag= new tag(tag8D,null);taglist.add(emvtag);


        emvtag= new tag(tag8E,null);taglist.add(emvtag);



        emvtag= new tag(tag5F25,null);taglist.add(emvtag);


        emvtag= new tag(tag5F28,null);taglist.add(emvtag);

        emvtag= new tag(tag5F30,null);taglist.add(emvtag);


        emvtag= new tag(tag5F34,null);taglist.add(emvtag);



        emvtag= new tag(tag9F07,null);taglist.add(emvtag);


        emvtag= new tag(tag9F08,null);taglist.add(emvtag);

        emvtag= new tag(tag9F0D,null);taglist.add(emvtag);


        emvtag= new tag(tag9F0E,null);taglist.add(emvtag);


        emvtag= new tag(tag9F0F,null);taglist.add(emvtag);



        emvtag= new tag(tag9F42,null);taglist.add(emvtag);


        emvtag= new tag(tag9F44,null);taglist.add(emvtag);

        emvtag= new tag(tag8A,null);taglist.add(emvtag);


        emvtag= new tag(tag9F02,null);taglist.add(emvtag);


        emvtag= new tag(tag9F03,null);taglist.add(emvtag);


        emvtag= new tag(tag9F1A,null);taglist.add(emvtag);

        emvtag= new tag(tag95,null);taglist.add(emvtag);

        emvtag= new tag(tag9A,null);taglist.add(emvtag);

        emvtag= new tag(tag9C,null);taglist.add(emvtag);

        emvtag= new tag(tag9F37,null);taglist.add(emvtag);


        emvtag= new tag(tag5F2A,null);taglist.add(emvtag);

        emvtag= new tag(tag9F08,null);taglist.add(emvtag);
        emvtag= new tag(tag9F11,null);taglist.add(emvtag);
        emvtag= new tag(tag9F38,null);taglist.add(emvtag);
        emvtag= new tag(tagBF0C,null);taglist.add(emvtag);
        emvtag= new tag(tag9F12,null);taglist.add(emvtag);
        emvtag= new tag(tag9F4D,null);taglist.add(emvtag);

        emvtag= new tag(tag77,null);taglist.add(emvtag);
        emvtag= new tag(tag82,null);taglist.add(emvtag);
        emvtag= new tag(tag94,null);taglist.add(emvtag);


        emvtag= new tag(tag9F6C,null);taglist.add(emvtag);

        emvtag= new tag(tag9F46,null);taglist.add(emvtag);
        emvtag= new tag(tag9F49,null);taglist.add(emvtag);
        emvtag= new tag(tag9F47,null);taglist.add(emvtag);
        emvtag= new tag(tag90,null);taglist.add(emvtag);
        emvtag= new tag(tag9F4A,null);taglist.add(emvtag);
        emvtag= new tag(tag9F32,null);taglist.add(emvtag);

        emvtag= new tag(tag93,null);taglist.add(emvtag);
        emvtag= new tag(tag92,null);taglist.add(emvtag);
        emvtag= new tag(tag8F,null);taglist.add(emvtag);

        return taglist;
    }


    ArrayList<tag> applicationtag;//store application tag
    ArrayList<tag>[] applet;
    public void InitApplicationArray()
    {
        applicationtag = InitTagList();
    }

    public ArrayList<tag>[] getContactCardData()
    {
        return applet;
    }

   /* public void processresponse(byte[] data,ArrayList<tag> taglist)
    {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());




        CheckSubTag(data,taglist);

    }*/

    public ArrayList<tag>[] FindAvailableApp(byte[] data)
    {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int datalength;
        int searchoffset=0;
        byte[] searchbuffer;
        tag foundtag;
        Iterator subtagitr;
        ArrayList<tag> apptemplate=new ArrayList<>();
        ArrayList<byte[]> foundapp=new ArrayList<>();
        ArrayList<tag>[] cardapplets;
        tag emvtag;
        tag apptag;
        searchbuffer=data;

        if(data==null)
            return null;
        datalength=data.length;
        emvtag= new tag(tag61,null);apptemplate.add(emvtag);
        subtagitr=apptemplate.iterator();
        while(subtagitr.hasNext())
        {
            if(datalength>searchoffset)
            {
                foundtag = (tag) subtagitr.next();
                //Log.d(TAG,"foundtag "+Hex.bytesToHexString(foundtag.key));
                if(ByteBuffer.wrap(foundtag.key, 0, foundtag.key.length).equals(ByteBuffer.wrap(searchbuffer, searchoffset,foundtag.key.length ))==true)
                {
                    foundtag.value= getTagData(foundtag.key, searchbuffer, searchoffset);
                    Log.d(TAG,"foundtag "+Hex.bytesToHexString(foundtag.key)+", value "+Hex.bytesToHexString(foundtag.value));
                    apptag= new tag(foundtag.key,foundtag.value);
                    foundapp.add(foundtag.value);
                    searchoffset += updateRawDataOffset(foundtag.key, foundtag.value);//total data processed
                    subtagitr=apptemplate.iterator();
                }
            }
            else
            {
                Log.d(TAG,"finish searching app");

                break;
            }

        }
        int start =0;
        int offset=0;
        cardapplets= new ArrayList[foundapp.size()];
        datalength=data.length;
        searchoffset=0;
        for(start=0;start<foundapp.size();start++) {
            cardapplets[start] = InitTagList();
            subtagitr=apptemplate.iterator();

            if(datalength>searchoffset)
            {
                    while(subtagitr.hasNext())
                    {

                            foundtag = (tag) subtagitr.next();
                            if(ByteBuffer.wrap(foundtag.key, 0, foundtag.key.length).equals(ByteBuffer.wrap(searchbuffer, searchoffset,foundtag.key.length ))==true)
                            {
                                foundtag.value=new byte[foundapp.get(start).length];
                                setTagData(cardapplets[start],foundtag.key,foundapp.get(start),0);
                                searchoffset += updateRawDataOffset(foundtag.key, foundtag.value);//total data processed
                            }


                    }
            }
                else
            {
                Log.d(TAG,"finish insert tag61 value");

                break;
            }

        }
        return cardapplets;

    }

    private void CheckSubTag(byte[] data,ArrayList<tag> taglist)
    {
        passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName() + " ,value "+Hex.bytesToHexString(data));
        int searchoffset=0;
        byte[] searchbuffer;
        tag searchtag;
        Iterator subtagitr=taglist.iterator();
        int datalength;
        tag foundtag;
        searchbuffer=data;

        if(data==null)
            return;
        datalength=data.length;

        while(subtagitr.hasNext())
        {
            if(datalength>searchoffset)
            {
                foundtag = (tag) subtagitr.next();
                //Log.d(TAG,"foundtag "+Hex.bytesToHexString(foundtag.key));
                if(ByteBuffer.wrap(foundtag.key, 0, foundtag.key.length).equals(ByteBuffer.wrap(searchbuffer, searchoffset,foundtag.key.length ))==true)
                {
                    foundtag.value= getTagData(foundtag.key, searchbuffer, searchoffset);
                    Log.d(TAG,"foundtag "+Hex.bytesToHexString(foundtag.key)+", value "+Hex.bytesToHexString(foundtag.value));

                    if (isConstructedDO(foundtag.key[0]) == false) {
                    } else {
                        CheckSubTag(foundtag.value, taglist);
                    }
                    searchoffset += updateRawDataOffset(foundtag.key, foundtag.value);//total data processed
                    //Log.d(TAG,"searchoffset "+searchoffset);
                    subtagitr=taglist.iterator();
                }
            }
            else
            {
                Log.d(TAG,"finish searching subtag");
                return;
            }

        }
    }

    int byte2int(byte value)
    {
        int result;
        result = value&0xFF;//cast to unsigned byte before convert to integer
        return result;
    }
    private byte[] getTagData(byte[]tag,byte[] data,int idx)
    {
        int length=0;
        byte[] tagvalue=null;
        int offset=0;
        if(data[idx+tag.length]==(byte)0x81)
        {
            length=byte2int(data[idx+(tag.length-1)+2]);//cast to unsigned byte before convert to integer
            offset=idx+(tag.length-1)+3;
        }
        else
        {
            length=(int)data[idx+(tag.length-1)+1];
            offset=idx+(tag.length-1)+2;
        }

        try {
            tagvalue = new byte[length];
            System.arraycopy(data, offset, tagvalue, 0, tagvalue.length);
        }
        catch(Exception e)
        {
            Log.e(TAG,e.getMessage());
        }
        return tagvalue;
    }
    int updateRawDataOffset(byte[] tag,byte[] tagvalue)
    {
        int offset=tag.length+tagvalue.length+1;
        if(tagvalue.length>(byte)0x7F)
        {
            offset++;
        }
        return offset;
    }




    public int ProcessContact()
    {
        byte[] data;
        byte[] tagvalue;
        byte[] PSO={(byte)0x83,(byte)0x00};
        int start;
        int recordstart;

        byte sfi;
        byte tableindex;
        byte[] AFL;
        int offset=0;


        InitApplicationArray();
        data = SelectPSE();
        if(data==null)
            return NOT_OK;
        CheckSubTag(data,applicationtag);
        tagvalue=getTagData(applicationtag,tag88);
        if(tagvalue==null)
            return NOT_OK;
        sfi=tagvalue[0];
        tagvalue=getTagData(applicationtag,tag9F11);
        if(tagvalue==null)
            tableindex=1;
        else tableindex=tagvalue[0];


        LogEMV("select record to get application aid");
        data=SelectRecord(sfi,tableindex);
        if(data==null)
            return NOT_OK;
        CheckSubTag(data,applicationtag);
        tagvalue = getTagData(applicationtag, tag70);
        if(tagvalue==null)
            return NOT_OK;
        applet=FindAvailableApp(tagvalue);
        for(start=0;start<applet.length;start++) {

            tagvalue = getTagData(applet[start], tag61);//get found applet data
            if (tagvalue == null)
                return NOT_OK;
            CheckSubTag(tagvalue,applet[start]);//load applet info to corresponding tag
            tagvalue = getTagData(applet[start], tag4F);//find AID
            if (tagvalue == null)
                return NOT_OK;

            LogEMV("select application aid");
            data = SelectAID(tagvalue);
            if (data == null)
                return NOT_OK;
            CheckSubTag(data,applet[start]);
            LogEMV("get processing option");
            data = GetProcessingOption(PSO);
            if (data == null)
                return NOT_OK;
            CheckSubTag(data,applet[start]);
            LogEMV("select record based on get processing option");
            tagvalue = getTagData(applet[start], tag80);
            if (tagvalue == null) {
                AFL = getTagData(applet[start], tag94);
            } else {
                AFL = new byte[tagvalue.length - 2];
                System.arraycopy(tagvalue, 2, AFL, 0, AFL.length);
            }
            offset = 0;
            while (offset < AFL.length) {
                for (recordstart = AFL[offset + 1]; recordstart <= AFL[offset + 2]; recordstart++) {
                    tagvalue = SelectRecord((byte) (AFL[offset] >> 3), (byte) recordstart);
                    if (tagvalue == null)
                        return NOT_OK;
                    CheckSubTag(tagvalue,applet[start]);
                }
                offset += 4;
            }
        }

        return OK;
    }
    void LogEMV(String message)
    {
        Log.e(TAG,message);
    }
    public byte[] getTagData(ArrayList taglist,byte[] targetkey)
    {
        Iterator itr;
        tag currenttag;
        itr=taglist.iterator();
        while(itr.hasNext()){
            currenttag = (tag) itr.next();
            if(currenttag.key==targetkey)
                return currenttag.value;
        }
        return null;
    }
    public int setTagData(ArrayList taglist,byte[] targetkey,byte[] data,int offset)
    {
        Iterator itr;
        tag currenttag;
        itr=taglist.iterator();
        while(itr.hasNext()){
            currenttag = (tag) itr.next();
            if(currenttag.key==targetkey)
            {
                currenttag.value=new byte[data.length-offset];
                System.arraycopy(data,offset,currenttag.value,0,currenttag.value.length);
                return OK;

            }

        }
        return NOT_OK;
    }
    public void setMode(int mode)
    {
        this.mode=mode;
    }
    byte[] SendAndGetData(byte[] apdu)
    {
        byte[] resp;
        int code;
        if(apdu==null)
            return null;
        resp = device.sendContact(apdu);
        code=APDUHelper.CheckResult(resp);
        switch(code)
        {
            case OK:
                return resp;
            case DATA_AVAILABLE:
                resp=GetResponse(resp[1]);
                return resp;
            default:
                return resp;
        }
    }
    byte[] SelectPSE()
    {
        byte[] resp;
        int code;
        byte[] data;
        byte[] apdu={(byte)0x00,(byte)0xA4,(byte)0x04,(byte)0x00,(byte)0x0E,(byte)0x31,(byte)0x50,(byte)0x41,(byte)0x59,(byte)0x2E,(byte)0x53,(byte)0x59,(byte)0x53,(byte)0x2E,(byte)0x44,(byte)0x44,(byte)0x46,(byte)0x30,(byte)0x31};
        if(mode==CONTACT) {

            resp=SendAndGetData(apdu);
        }
        else {
            resp = device.sendCTL(apdu);
        }
        Log.d("resp", Hex.bytesToHexString(resp));
        code=APDUHelper.CheckResult(resp);
        switch(code)
        {
            case OK:
                data=new byte[resp.length-2];
                System.arraycopy(resp,0,data,0,data.length);
                return data;
            default:
                return resp;
        }
    }
    byte[] GetProcessingOption(byte[] apdudata) {
        //byte[] apdu={(byte)0x80,(byte)0xA8,(byte)0x00,(byte)0x00,(byte)0x02,(byte)0x83,(byte)0x00};
        byte[] apdu = {(byte) 0x80, (byte) 0xA8, (byte) 0x00, (byte) 0x00, (byte) 0x00};
        byte[] resp = new byte[2];
        byte[] cmd;
        int code;
        byte[] data=null;
        boolean retry = true;
        if (apdudata == null)
            return null;
        apdu[4] = (byte) apdudata.length;
        cmd = new byte[apdu.length + apdudata.length];
        System.arraycopy(apdu, 0, cmd, 0, apdu.length);
        System.arraycopy(apdudata, 0, cmd, 5, apdudata.length);
        while (retry) {
            if (mode == CONTACT) {

                resp = SendAndGetData(cmd);
            } else {
                resp = device.sendCTL(cmd);
            }
            code = APDUHelper.CheckResult(resp);
            switch (code) {
                case OK:
                    data = new byte[resp.length - 2];
                    System.arraycopy(resp, 0, data, 0, data.length);
                    retry = false;
                    break;
                case ERR_LENGTH:
                    cmd = new byte[apdu.length + 4];
                    System.arraycopy(apdu, 0, cmd, 0, apdu.length);
                    cmd[4] = (byte) 0x04;
                    cmd[5] = (byte) 0x83;
                    cmd[6] = (byte) 0x02;
                    cmd[7] = (byte) 0x03;
                    cmd[8] = (byte) 0x60;
                    continue;
                default:
                    retry = false;
                    return resp;
            }
        }
        return data;

    }
    byte[] GetResponse(byte length)
    {
        byte[] apdu={(byte)0x00,(byte)0xC0,(byte)0x00,(byte)0x00,(byte)0x00};
        byte[] resp;
        apdu[4]=length;
        resp = device.sendContact(apdu);
        return resp;
    }
    byte[] SelectRecord(byte sfi,byte record)
    {
        byte[] apdu={(byte)0x00,(byte)0xB2,(byte)0x00,(byte)0x04,(byte)0x00};
        byte[] resp=new byte[2];
        int code;
        byte[] cmd;
        byte[] data;

        apdu[2]=record;
        apdu[3]=(byte)((sfi<<3)|apdu[3]);
        cmd = new byte[apdu.length];
        System.arraycopy(apdu,0,cmd,0,cmd.length);
        if(mode==CONTACT) {

            resp=SendAndGetData(cmd);
        }
        else {
            resp = device.sendCTL(cmd);
        }
        code=APDUHelper.CheckResult(resp);
        switch(code)
        {
            case OK:
                data=new byte[resp.length-2];
                System.arraycopy(resp,0,data,0,data.length);
                return data;
            default:
                return resp;
        }
    }
    byte[] SelectAID(byte[] aid)
    {
        byte[] apdu={(byte)0x00,(byte)0xA4,(byte)0x04,(byte)0x00,(byte)0x00};
        byte[] resp=new byte[2];
        byte[] cmd;
        int code;
        byte[] data;
        if(aid==null)
            return null;
        apdu[4]=(byte)aid.length;
        cmd = new byte[apdu.length+aid.length];
        System.arraycopy(apdu,0,cmd,0,apdu.length);
        System.arraycopy(aid,0,cmd,5,aid.length);
        if(mode==CONTACT) {

            resp=SendAndGetData(cmd);
        }
        else {
            resp = device.sendCTL(cmd);
        }
        code=APDUHelper.CheckResult(resp);
        switch(code)
        {
            case OK:
                data=new byte[resp.length-2];
                System.arraycopy(resp,0,data,0,data.length);
                return data;
            default:
                return null;
        }


    }

}
