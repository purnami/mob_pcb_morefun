package id.co.softorb.lib.smartcard.posdevice;

import android.graphics.Bitmap;

public abstract class POSPrinter extends Base {
    public abstract int cutpaper();
    public abstract int linefeed();
    public abstract int status();
    public abstract int bitmap(int alignment, Bitmap bitmap);
    public abstract int text(int fonttype,int alignment,String text,boolean newline);
}
