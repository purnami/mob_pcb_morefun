package id.co.softorb.lib.passti;

import android.content.Context;
import android.content.res.Resources;
import android.os.RemoteException;



import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Random;


import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.crypto.des;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.passti.helper.converter;

import id.co.softorb.lib.smartcard.posdevice.params;
import id.co.softorb.lib.smartcard.posdevice.reader;


import static id.co.softorb.lib.passti.helper.ErrorCode.*;
import static id.co.softorb.lib.smartcard.posdevice.params.*;

/**
 * Created by softorb.co.id on 4/23/2019.
 */

public class PASSTI {
    
    

    
    byte TAG_A1  = (byte)0xA1;
    byte TAG_A2 = (byte) 0xA2;
    byte TAG_A3 = (byte) 0xA3;
    byte TAG_A4 = (byte) 0xA4;
    byte TAG_A5 = (byte) 0xA5;
    byte TAG_A6 = (byte) 0xA6;
    byte TAG_A7 = (byte) 0xA7;
    byte TAG_A8 = (byte)0xA8;
    byte TAG_A9 = (byte) 0xA9;
    int TAG_SIZE =  1;
    int LEN_SIZE =  2;
    byte[] sCode_Success={(byte)0x00,(byte)0x00,(byte)0x00};
    byte TAG_USERNAME = (byte) 0x01;
    byte TAG_PASSWORD = (byte) 0x02;
    byte TAG_READERSN = (byte) 0x03;
    byte TAG_APPVER = (byte) 0x04;
    byte TAG_DEVICETYPE = (byte) 0x05;
    byte TAG_MID = (byte) 0x06;
    byte TAG_TID = (byte) 0x07;
    byte TAG_SRVTIMESTAMP = (byte)0x08;
    byte TAG_UPLOADINTERVAL = (byte) 0x09;
    byte TAG_MAXRECUPLOAD = (byte) 0x10;
    byte TAG_PORTTIMEOUT = (byte) 0x11;
    byte TAG_TOKEN = (byte) 0x12;
    byte TAG_SETTLETIME = (byte) 0x13;
    byte TAG_RESPMSG = (byte) 0x14;
    byte TAG_RFU = (byte)0x15;
    byte TAG_LOG = (byte) 0x16;
    byte TAG_KEEPALIVE = (byte) 0x17;

    final byte ERR_INVALID_USER_PASSWORD = (byte) 0x01;
    final byte ERR_INVALID_SN = (byte) 0x02;
    final   byte ERR_INVALID_FORMAT = (byte) 0x03;
    final   byte ERR_USER_INACTIVE = (byte) 0x04;
    final   byte ERR_INVALID_TOKEN = (byte) 0x05;
    final   byte ERR_INVALID_MID = (byte) 0x06;
    final   byte ERR_INVALID_TID = (byte)0x07;
    final   byte ERR_INVALID_TRXLOG = (byte) 0x08;
    final   byte ERR_INACTIVE_MERCHANT = (byte) 0x09;
    final   byte ERR_INACTIVE_SITE = (byte) 0x0A;
    final   byte ERR_INACTIVE_TERMINAL = (byte)0x0B;
    final   byte ERR_EXPIRED_MERCHANT = (byte) 0x0C;
    final   byte ERR_SERVER_ERROR = (byte) 0xFF;

    final byte CAT_GENERAL = (byte)0x00;
    final byte CAT_LOGIN = (byte)0x01;
    final byte CAT_SENDDATA  = (byte)0x02;
    int OFFSET_CMDDATA =  7;
    int OFFSET_CMDRESPCODE =  5;
    int OFFSET_CMD =  3;
    byte[] login={(byte)0xAE,(byte)0X01};
    byte[] keepAlive={(byte)0xAE,(byte)0X03};
     //byte[] login={(byte)0xAE};
    byte[] uploadtrxdata ={(byte)0xAE,(byte)0X02};
    byte[] dst = new byte[1024];
    //byte[] user="Admin".getBytes();
    //byte[] pw="Admin".getBytes();
    String TAG;
    
    byte STX = (byte)0x02;
    byte[] outData;
    int device;
    byte[] log;

    public  byte[] TID;
    public  byte[] MID;
    public  byte[] token;
    private static byte[] SN;
    private byte[] libVer;
    des des;
    Context ctx;
    Resources resources;
    params deduct;
    cardinfo ctlinfo;
    debughelper dbghelper;
    String encryptkey;
    reader reader;
    public PASSTI(Context ctx,reader devicereader) throws RemoteException {
        TAG = this.getClass().getSimpleName();
       des= new des();
	   this.reader=devicereader;
        SN = reader.SerialNumber();
        this.ctx = ctx;

        device=reader.DeviceType();
        resources = ctx.getResources();

        if(SN.length < 20){  //disini sn nilainya null
            byte[] backup = SN;
            SN = new byte[20];
            System.arraycopy(backup,0,SN,20-backup.length,backup.length);
        }
       dbghelper= new debughelper();
       deduct = new params();
       ctlinfo = new cardinfo();

    }
    void SetEncryptKey(String encryptkey)
    {
        this.encryptkey=encryptkey;
    }
	void SetLibVersion(String strLib)
    {
        String test=strLib.replace(".","");
        libVer=Hex.hexStringToByteArray(test);
        dbghelper.DebugString("libVer",Hex.bytesToHexString(libVer));
    }
    protected byte[] pcd_cmd_login(byte[] username,byte[] password)
    {
        //Log.d(TAG,"SerialNo: " + BytesUtil.bytes2HexString(SN));
        //Log.d(TAG,"Username: " + BytesUtil.bytes2HexString(username));
        //Log.d(TAG,"Password: " + BytesUtil.bytes2HexString(password));
        byte[] bappVer = libVer;
        byte[] outData;
        byte[] dst = new byte[100];
        //020044AE01011041646D696E2020202020202020202020021041646D696E2020202020202020202020031400000000000000000000000000000119000040FF040310110E05010551
        int offset;
        byte[] length = new byte[4];
        byte[] devsn = new byte[20];
        //Log.d(TAG,"pcd_cmd_login");
        //SaveTransactionHistoryDebug("LogToComposeResponse.txt",log,lenLog);
        //SaveTransactionHistoryDebug("ComposeDeductResponse1.txt",dst,offset);
        offset=0;
        dst[offset++]=STX;
        offset+=2;//2bytes of length
        //memcpy(&dst[offset],login,sizeof(login));
        System.arraycopy(login,0,dst,offset,login.length);
        offset+=login.length;
        dst[offset++]=TAG_USERNAME;
        //dst[offset++]=(byte)len_UserName;
        offset++;
        dst[offset++]=(byte)username.length;
       // memcpy(&dst[offset],username,len_UserName);
        System.arraycopy(username,0,dst,offset,username.length);
        //offset+=len_UserName;
        offset+=username.length;

        dst[offset++]=TAG_PASSWORD;
        //dst[offset++]=len_password;
        offset++;
        dst[offset++]=(byte)password.length;
        //memcpy(&dst[offset],password,len_password);
        System.arraycopy(password,0,dst,offset,password.length);
        offset+=password.length;
	/*
	dst[offset++]=TAG_READERSN;
	dst[offset++]=devinfo.lenSN;
	memcpy(&dst[offset],devinfo.SN,devinfo.lenSN);
	offset+=devinfo.lenSN;
	*/
        dst[offset++]=TAG_READERSN;
        offset++;
        dst[offset++]=20;
        //memset(devsn,0x00,20);
        Arrays.fill(devsn,(byte)0x00);
        //memcpy(&devsn[20-devinfo.lenSN],devinfo.SN,devinfo.lenSN);
        System.arraycopy(SN,0,devsn,20-SN.length,SN.length);
        //memcpy(&dst[offset],devsn,sizeof(devsn));
        System.arraycopy(devsn,0,dst,offset,devsn.length);
        offset+=20;

        dst[offset++]=TAG_APPVER;
        offset++;
        dst[offset++]=(byte)bappVer.length;
        //memcpy(&dst[offset],bappVer,len_AppVer);
        System.arraycopy(bappVer,0,dst,offset,bappVer.length);
        offset+=bappVer.length;

        dst[offset++]=TAG_DEVICETYPE;
        offset++;
        dst[offset++]=1;
        dst[offset++]=(byte)device;

        //copy length of data to frame
        //int2dword(offset-3,length);
        length = converter.IntegerToByteArray(offset-3);
       // memcpy(&dst[1],&length[2],2);
        System.arraycopy(length,2,dst,1,2);
        //calculate crc
        //dst[offset++]=XORing(dst[1],offset-1);
        dst[offset++]=calculateLRC(dst,1,offset-1);
        //debug_print(7,"CRC");

        //DebugHex("Output : ",dst,offset);
        //memcpy(outData,dst,offset);
        outData = new byte[offset];
        System.arraycopy(dst,0,outData,0,offset);
        //outLen=offset;
        //Log.d(TAG,"outData : " + BytesUtil.bytes2HexString(outData));
        //SaveTransactionHistoryDebug("ComposeDeductResponse14",dst,*dstLen);
        //return OK;
        return outData;

    }
    
    byte XORing(byte src, int len)
    {
        byte tmp;
        int offset;
        offset=0;
        tmp=0;
        while(offset<len)
        {
            tmp^= (src+offset);
            offset++;
        }
        return tmp;
    }


    byte[] FindTag(byte tag,byte[] data,int lenData, int offset)
    {

        int idx=offset;
        byte[] outdata;
        int iLen=0;
        byte[] length = new byte[4];
        while(lenData>idx)
        {

            if(data[idx]!=tag)
            {
                //Log.d(TAG,"Different TAG, continue search tag");
                System.arraycopy(data,idx+1,length,2,2);
                iLen = converter.ByteArrayToInt(length,0,4,converter.BIG_ENDIAN);
                idx+=TAG_SIZE+LEN_SIZE+iLen;

            }
            else
            {
                //Log.d(TAG,"Same TAG, return tag data");


               System.arraycopy(data,idx+1,length,2,2);
               iLen = converter.ByteArrayToInt(length,0,4,converter.BIG_ENDIAN);
               outdata= new byte[iLen];
                System.arraycopy(data,idx+TAG_SIZE+LEN_SIZE,outdata,0,iLen);
                return outdata;
            }
        }


        return null;
    }
    public String GetDescription(byte code) {
        switch (code) {
            case (byte)0x00: return "OK";
            case ERR_INVALID_USER_PASSWORD: return "ERR_INVALID_USER_PASSWORD";
            case ERR_INVALID_SN:  return "ERR_INVALID_SN";
            case ERR_INVALID_FORMAT:  return "ERR_INVALID_FORMAT";
            case ERR_USER_INACTIVE: return "ERR_USER_INACTIVE";
            case ERR_INVALID_TOKEN:  return "ERR_INVALID_TOKEN";
            case ERR_INVALID_MID:  return "ERR_INVALID_MID";
            case ERR_INVALID_TID:  return "ERR_INVALID_TID";
            case ERR_INVALID_TRXLOG:  return "ERR_INVALID_TRXLOG";
            case ERR_INACTIVE_MERCHANT:  return "ERR_INACTIVE_MERCHANT";
            case ERR_INACTIVE_SITE:  return "ERR_INACTIVE_SITE";
            case ERR_INACTIVE_TERMINAL:  return "ERR_INACTIVE_TERMINAL";
            case ERR_EXPIRED_MERCHANT:  return "ERR_EXPIRED_MERCHANT";
            case ERR_SERVER_ERROR:  return "ERR_SERVER_ERROR";

            default:
                return Byte.toString(code);
        }
    }

    byte pcd_checkresponsecode(byte[] resp,int resplen)
    {

        byte[] respcode = new byte[2];
        byte[] command = new byte[2];

        System.arraycopy(resp,OFFSET_CMDRESPCODE,respcode,0,2);
        System.arraycopy(resp,OFFSET_CMD,command,0,2);
        if(Arrays.equals(login,command)==true)
        {
            switch(respcode[0])
            {
                case CAT_GENERAL:
                    switch(respcode[1])
                    {
                        case OK:
                        token=FindTag(TAG_TOKEN,resp,resplen,OFFSET_CMDDATA);
                        return OK;
                    }
                    return respcode[1];
                case CAT_LOGIN:
                case CAT_SENDDATA:
                    return respcode[1];

                default:
                    //Log.d(TAG,"unknown response code category");
                    break;
            }
        }
        switch(respcode[0])
        {
            case CAT_GENERAL:
                switch(respcode[1])
                {
                    case (byte)0x00 : return OK;
                }
                break;
            case CAT_LOGIN:
                switch(respcode[1])
                {
                    case (byte)0x00 :
                        token=FindTag(TAG_TOKEN,resp,resplen,0);

                        return OK;
                }
                return respcode[1];
            case CAT_SENDDATA:
                return respcode[1];

            default:
                //Log.d(TAG,"unknown response code category");
                break;
        }

        return (byte)0xFF;
    }

   protected byte[] pcd_cmd_dataupload(byte[] token,byte[] mid,byte[] tid,byte[] banklog)
   {
       //banklog sudah dalam format passti
       int offset;
       byte[] length = new byte[4];
       //Log.d(TAG,"pcd_cmd_dataupload");

       offset=0;
       dst[offset++]=STX;

       offset+=2;//2bytes of length

       System.arraycopy(uploadtrxdata,0,dst,offset,uploadtrxdata.length);
       offset+=uploadtrxdata.length;

       dst[offset++]=TAG_TOKEN;

       offset++;
       dst[offset++] = (byte)token.length;

       System.arraycopy(token,0,dst,offset,token.length);

       offset += token.length;

       dst[offset++]=TAG_MID;

       offset++;
       dst[offset++] = (byte)mid.length;

       System.arraycopy(mid,0,dst,offset,mid.length);

       offset+=mid.length;

       dst[offset++]=TAG_TID;

       offset++;
       dst[offset++]=(byte)tid.length;

       System.arraycopy(tid,0,dst,offset,tid.length);

       offset+=tid.length;

       dst[offset++]=TAG_LOG;

       offset++;
       dst[offset++]=(byte)(banklog.length+sCode_Success.length);

       System.arraycopy(sCode_Success,0,dst,offset,sCode_Success.length);
       offset+=sCode_Success.length;


       System.arraycopy(banklog,0,dst,offset,banklog.length);
       offset+=banklog.length;

       //copy length of data to frame

       length = converter.IntegerToByteArray(offset-3);

       System.arraycopy(length,2,dst,1,2);

       //calculate crc

       dst[offset++]=calculateLRC(dst,1,offset-1);

       outData = new byte[offset];
       System.arraycopy(dst,0,outData,0,offset);

       return outData;
   }

   protected byte[] pcd_cmd_keepalive(byte[] token, byte[] mid, byte[] tid)
    {
        byte[] outData;
        int offset;
        byte[] length = new byte[4];
        byte[] taglen = new byte[4];

        offset=0;
        Arrays.fill(dst,(byte)0x00);
        dst[offset++]=STX;
        offset+=2;//2bytes of length
        System.arraycopy(keepAlive,0,dst,offset,keepAlive.length);
        offset+=keepAlive.length;
        dst[offset++]=TAG_TOKEN;

        taglen = converter.IntegerToByteArray(token.length);

        System.arraycopy(taglen,2,dst,offset,2);

        offset+=2;
        System.arraycopy(token,0,dst,offset,token.length);
        offset+=token.length;

        dst[offset++]=TAG_MID;

        taglen = converter.IntegerToByteArray(mid.length);

        System.arraycopy(taglen,2,dst,offset,2);
        offset+=2;

        System.arraycopy(mid,0,dst,offset,mid.length);
        offset+=mid.length;

        dst[offset++]=TAG_TID;

        taglen = converter.IntegerToByteArray(tid.length);

        System.arraycopy(taglen,2,dst,offset,2);
        offset+=2;

        System.arraycopy(tid,0,dst,offset,tid.length);
        offset+=tid.length;

        //copy length of data to frame

        length = converter.IntegerToByteArray(offset-3);

        System.arraycopy(length,2,dst,1,2);
        //calculate crc

        dst[offset++]=calculateLRC(dst,1,offset-1);

        outData = new byte[offset];
        System.arraycopy(dst,0,outData,0,offset);

        return outData;
    }


     static int ARC( final byte[] data, final int offset, final int length) {
        return CRC_New(0x8005, 0x0000, data, offset, length, true, true, 0x0000);
    }


     byte[] PASSTI_ComposeTransactionLog(byte[] uid,byte cardtype,byte[] date,byte[] cardno,byte[] balance,byte[] baAmount,byte[] trxNo,byte[] banklog,byte[] others)
    {
        byte[] iv = new byte[8];
        byte[] bAppVersion = libVer;
        byte[] result = new byte[512];
        //Log.d(TAG,"PASSTI_ComposeTransactionLog\r\n");
        byte[] oprlog = new byte[512];
        int idxoprloglen;
        byte[] stilog=new byte[512];
        int stiloglen;
        // byte[] crc = new byte[2];
        byte[] stilogencrypt = new byte[512];
        int stilogencryptlen;
        byte[] logrecord = new byte[512];
        byte[] logbuf = new byte[500];
        int logrecordlen;
        byte[] bankloglen = new byte[4];

        byte[] seed = new byte[4];
        byte[] temp = new byte[4];
        byte[] idx;

        int i;
        int ret;
        int crc_enc;

        Arrays.fill(iv,(byte)0x00);//2020-01-31,vivo,initiate encryption vector
        //Log.d(TAG,"uid : " + BytesUtil.bytes2HexString(uid));
        //Log.d(TAG,"trxNo : " + BytesUtil.bytes2HexString(trxNo));
        //Log.d(TAG,"banklog : "+ BytesUtil.bytes2HexString(banklog));
        //Log.d(TAG,"balance : " + BytesUtil.bytes2HexString(balance));
        //Log.d(TAG,"bAmount : " + BytesUtil.bytes2HexString(baAmount));
        //Log.d(TAG,"CardNo : " + BytesUtil.bytes2HexString(cardno));
        //Log.d(TAG,"Date : " + BytesUtil.bytes2HexString(date));
        //Log.d(TAG,"AppVer : " + BytesUtil.bytes2HexString(bAppVersion));
        //Log.d(TAG,"TID : " + BytesUtil.bytes2HexString(TID));
        //Log.d(TAG,"MID : " + BytesUtil.bytes2HexString(MID));
        //Log.d(TAG,"SN : " + BytesUtil.bytes2HexString(SN));
        String sSN = "";
        try {
            sSN = new String(SN, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //Log.d(TAG,"SN : " + sSN);


        if(SN.length < 20){//If serial number length < 20, padding with 0x00, Output: 0x00(n) + Serial No + nBytes Serial No Length
            byte[] backup = SN;
            SN = new byte[20];
            System.arraycopy(backup,0,SN,SN.length-backup.length-1,backup.length);
            byte[] nBytes = converter.IntegerToByteArray(backup.length);
            System.arraycopy(nBytes,3,SN,SN.length-1,1);
        }


        Random rand = new Random();
        for(i=0;i<seed.length;i++)
        {
            seed[i]=(byte)(rand.nextInt(100-1) +1) ;
        }

	/*
	PROCESSING OPRLOG
	*/

        idxoprloglen=0;
        oprlog[idxoprloglen++]=(byte)cardtype;
        //memcpy(&oprlog[idxoprloglen],devinfo.MID,sizeof(devinfo.MID));
        System.arraycopy(MID,0,oprlog,idxoprloglen,MID.length);
        idxoprloglen+=MID.length;
        //memcpy(&oprlog[idxoprloglen],devinfo.TID,sizeof(devinfo.TID));
        System.arraycopy(TID,0,oprlog,idxoprloglen,TID.length);
        idxoprloglen+=TID.length;
        //transdate, format ddmmyyyyhhmmss
        //memcpy(&oprlog[idxoprloglen],date,lendate);
        System.arraycopy(date,0,oprlog,idxoprloglen,date.length);
        idxoprloglen+=date.length;

        //card no
        //memcpy(&oprlog[idxoprloglen],cardno,lencardno);
        System.arraycopy(cardno,0,oprlog,idxoprloglen,cardno.length);
        idxoprloglen+=cardno.length;

        //memcpy(&oprlog[idxoprloglen],deduct.baAmount,sizeof(deduct.baAmount));
        System.arraycopy(baAmount,0,oprlog,idxoprloglen,baAmount.length);
        idxoprloglen+=baAmount.length;

        //memcpy(&oprlog[idxoprloglen],balance,4);
        System.arraycopy(balance,0,oprlog,idxoprloglen,4);
        idxoprloglen+=4;

        //memcpy(&oprlog[idxoprloglen],trxNo,lenTrxNo);
        System.arraycopy(trxNo,0,oprlog,idxoprloglen,trxNo.length);
        idxoprloglen+=trxNo.length;
        byte[] cmd = new byte[idxoprloglen];
        System.arraycopy(oprlog,0,cmd,0,idxoprloglen);
        oprlog = new byte[idxoprloglen];
        System.arraycopy(cmd,0,oprlog,0,cmd.length);
        //Log.d(TAG,"OPRLOG len "+idxoprloglen);
        //Log.d(TAG,"OPRLOG  : "+ BytesUtil.bytes2HexString(oprlog));
	/*
	PROCESSING STILOG
	
	*/
        //processing stilog
        //memset(stilog,0x00,sizeof(stilog));
        Arrays.fill(stilog,(byte)0x00);
        stiloglen=0;
        stilog[stiloglen++]=(byte)device;
        //memcpy(&stilog[stiloglen],bAppVersion,sizeof(bAppVersion));
        System.arraycopy(bAppVersion,0,stilog,stiloglen,bAppVersion.length);
        stiloglen+=bAppVersion.length;
        //memcpy(&stilog[stiloglen+(20-devinfo.lenSN)],devinfo.SN,devinfo.lenSN);
        System.arraycopy(SN,0,stilog,stiloglen+(20-SN.length),SN.length);
        stiloglen+=20;
        //memcpy(&stilog[stiloglen],seed,sizeof(seed));
        System.arraycopy(seed,0,stilog,stiloglen,seed.length);
        stiloglen+=seed.length;

        //memcpy(&stilog[stiloglen],oprlog,idxoprloglen);
        System.arraycopy(oprlog,0,stilog,stiloglen,oprlog.length);
        stiloglen+=oprlog.length;

        //int2dword(lenBankLog,bankloglen);
        bankloglen = converter.IntegerToByteArray(banklog.length);
        //memcpy(&stilog[stiloglen],&bankloglen[2],2);
        System.arraycopy(bankloglen,2,stilog,stiloglen,2);
        stiloglen+=2;
        //memcpy(&stilog[stiloglen],banklog,lenBankLog);
        System.arraycopy(banklog,0,stilog,stiloglen,banklog.length);
        stiloglen+=banklog.length;
        //calculate crc
        //Log.d(TAG,"lenBankLog: " + banklog.length);
        //Log.d(TAG,"banklog  : "+ BytesUtil.bytes2HexString(banklog));
        //Log.d(TAG,"bankloglen  : " + BytesUtil.bytes2HexString(bankloglen));
        //Log.d(TAG,"data to calculate CRC  : " + BytesUtil.bytes2HexString(stilog));
        //i=Calc_CRC_C_ARC(stilog, stiloglen);
        //i = crc16(stilog);
        i = ARC(stilog,0,stiloglen);
        //byte  buatupdate2 = calculateLRC(buatupdate,0,buatupdate.length);
        //Log.d(TAG,"coba CRC 473D: " + BytesUtil.bytes2HexString(buatupdate));
//        CRC32 crc = new CRC32();
//        crc.update(stilog,0,stiloglen);
        //i = Integer.valueOf(String.format("%08X", crc.getValue()));
        //i = (int) crc.getValue();
        //Log.d(TAG,"CRC_getValue1: " + i);
        //int2dword(i,temp);
        temp = converter.IntegerToByteArray(i);
        //Log.d(TAG,"CRC  : "+ BytesUtil.bytes2HexString(temp));
        //memcpy(&stilog[stiloglen],&temp[2],2);
        System.arraycopy(temp,2,stilog,stiloglen,2);
        stiloglen+=2;

        //Log.d(TAG,"STILOG len " + stiloglen);
        byte[] printstilog1 = new byte[stiloglen];
        System.arraycopy(stilog,0,printstilog1,0,stiloglen);
        //Log.d(TAG,"STILOG  : " + BytesUtil.bytes2HexString(printstilog1));
	
	/*
	ENCRYPTING STILOG
	
	*/
        //encrypt stilog, start from seed to crc

        ret=stiloglen%8;
        if(ret!=0)
            stiloglen+=(8-ret);
        byte[] wannabeSTILogEncryp = new byte[stiloglen];
        System.arraycopy(stilog,0,wannabeSTILogEncryp,0,stiloglen);


        //des.LoadKey(BytesUtil.hexString2Bytes(ctx.getResources().getString(R.string.encryptKey_dev)));
        des.LoadKey(BytesUtil.hexString2Bytes(encryptkey));
        des.InitEncryption(iv);//2020-01-31,vivo,init encryption with vector
        stilogencrypt = des.doEncrypt(wannabeSTILogEncryp);

        byte[] logbuv = des.doDecrypt(stilogencrypt);

	/*
	WRITING RESULT TO TARGET BUFFER
	
	*/

        byte[] templength = new byte[4];
        int offset=0;
        int offsetcrc;
        //memcpy(&result[offset],oprlog,idxoprloglen);

        System.arraycopy(oprlog,0,result,offset,idxoprloglen);
        //offset+=idxoprloglen;
        offset += oprlog.length;

        offsetcrc=offset;

        templength = converter.IntegerToByteArray(stiloglen);

        System.arraycopy(templength,2,result,offset,2);
        offset+=2;

        System.arraycopy(stilogencrypt,0,result,offset,stilogencrypt.length);

        offset += stilogencrypt.length;

        byte[] rfulength = converter.IntegerToByteArray(others.length);
        System.arraycopy(rfulength,3,result,offset,1);
        offset +=1;

        System.arraycopy(others,0,result,offset,others.length);
        offset+=others.length;

        System.arraycopy(result,offsetcrc,logbuf,0,offset-offsetcrc);
        crc_enc = ARC(result,0,offset-offsetcrc);
        temp = converter.IntegerToByteArray(crc_enc);
        System.arraycopy(temp,2,result,offset,2);
        offset+=2;
        byte[] out = new byte[offset];
        System.arraycopy(result,0,out,0,offset);
       return out;
    }


    static int CRC_New(final int poly, final int init,  final byte[] data, final int offset, final int length, final boolean refin, final boolean refout, final int xorout) {
        int crc = init;

        for (int i = offset; i < offset + length && i < data.length; ++i) {
            final byte b = data[i];
            for (int j = 0; j < 8; j++) {
                final int k = refin ? 7 - j : j;
                final boolean bit = ((b >> (7 - k) & 1) == 1);
                final boolean c15 = ((crc >> 15 & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit) crc ^= poly;
            }
        }

        if (refout) {
            return (Integer.reverse(crc) >>> 16) ^ xorout;
        } else {
            return (crc ^ xorout) & 0xFFFF;
        }
    }



    int Calc_CRC_C_ARC(byte[] bufferLocal, int  lenLocal)
    {
        int i;
        int crc;
        char bufdat;
        int crc_seed = 0;

        int idx = 0;
        crc = crc_seed;
        while(lenLocal>0)
        {
            bufdat = (char) bufferLocal[idx++];
            crc = crc ^ bufdat;
            for (i = 0;i < 8; i++)
            {
                if ((crc & 0x0001) !=0)
                {
// Expected poly should be bits reflected so 0xA001 used instead of 0x8005;
                    crc = (crc >> 1) ^ 0xA001;
                }
                else
                {
                    crc = crc >> 1;
                }
            }
            lenLocal = lenLocal -1;
        }
        return crc;
    }

    private static byte calculateLRC(byte[] bytes,int offset,int length) {
        byte LRC = 0;
        for (int i = offset; i <length+offset ; i++) {
            LRC ^= (bytes[i] & 0xFF);
        }
        return LRC;
    }


    protected void SetAction(int action)
    {
        dbghelper.DebugPrintString("SetAction");

        deduct.deductAction=action;
        if(action!= NORMAL)
        {


            dbghelper.DebugDeductAction("deduct.deductAction",deduct.deductAction);
            dbghelper.DebugInteger("ctlinfo.uid.value.length",ctlinfo.uid.value.length);
            Arrays.fill(deduct.prevUID,(byte)0x00);
            System.arraycopy(ctlinfo.cardno,0,deduct.prevCardNo,0,ctlinfo.cardno.length);

            dbghelper.DebugHex("deduct.prevCardNo",deduct.prevCardNo);
            deduct.prevUID=new byte[ctlinfo.uid.value.length];
            System.arraycopy(ctlinfo.uid.value,0,deduct.prevUID,0,ctlinfo.uid.value.length);
            dbghelper.DebugHex("deduct.prevUID",deduct.prevUID);


            System.arraycopy(deduct.baAmount,0,deduct.prevAmt,0,deduct.baAmount.length);
            dbghelper.DebugHex("deduct.prevAmt",deduct.prevAmt);
            System.arraycopy(ctlinfo.bBalanceKartu,0,deduct.prevBalance,0,deduct.prevBalance.length);

            dbghelper.DebugHex("deduct.prevBalance",deduct.prevBalance);


            deduct.prevcardtype =ctlinfo.type;
        }
        else
        {

            Arrays.fill(deduct.prevCardNo,(byte)0x00);
            Arrays.fill(deduct.prevBalance,(byte)0x00);
            Arrays.fill(deduct.prevUID,(byte)0x00);
            Arrays.fill(deduct.prevAmt,(byte)0x00);
        }

    }


    public int GetAction()
    {
        dbghelper.DebugPrintString("GetAction");
        return deduct.deductAction;
    }


}
