package id.co.softorb.lib.smartcard.morefun;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.card.CPUCard;
import com.morefun.yapi.ServiceResult;
import com.morefun.yapi.card.cpu.CPUCardHandler;
import com.morefun.yapi.device.reader.icc.ICCSearchResult;
import com.morefun.yapi.device.reader.icc.IccCardReader;
import com.morefun.yapi.device.reader.icc.IccCardType;
import com.morefun.yapi.device.reader.icc.IccReaderSlot;
import com.morefun.yapi.device.reader.icc.OnSearchIccCardListener;
import com.morefun.yapi.engine.DeviceServiceEngine;
import com.usdk.apiservice.aidl.data.ApduResponse;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.ContactBase;

import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INITREADER;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;

public class contact extends ContactBase {
    String tagclass= this.getClass().getSimpleName();
    private DeviceServiceEngine deviceService;
    private IccCardReader iccCardReader;
    private POSListener detectListener;
    private CPUCardHandler cpuCardHandler;
    public int getSlot() {
        return slot;
    }
    private final int contact_slot=0;
    boolean poweron;

    public void setSlot(int slot) {
        this.slot = slot;
    }

    private int slot;

    private contactcard[] cb;

    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        cb = new contactcard[1];
        cb[contact_slot]=new contactcard();

        try {
//            Bundle bundle = new Bundle();
            deviceService=(DeviceServiceEngine) devInstance;
//            deviceService.login(bundle,"09000000");
            iccCardReader = deviceService.getIccCardReader(IccReaderSlot.ICSlOT1);

            cb[contact_slot].setReader(iccCardReader);

            if (iccCardReader != null) {
                detectListener = listener;
                return OK;
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());

        }
        return NOT_OK;
    }

    @Override
    public byte[] sendCB(int slot, byte[] apdu) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] apduResult=null;
        byte[] tmp = new byte[256];
        int ret;
        Log.d(TAG,"RDR-->ICCaa : "+ Hex.bytesToHexString(apdu));
        try {
            ret=(cb[contact_slot].getCard()).exchangeCmd(tmp, apdu, apdu.length);
            Log.d("ret",""+ret);
            apduResult=Hex.subByte(tmp, 0, ret);
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
        Log.d(TAG,"RDR<--ICC : "+ Hex.bytesToHexString(apduResult));

        return apduResult;

    }

    @Override
    public int open() {
        //Tidak ada fungsi open()
        return OK;
    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return cb[contact_slot].Close();
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=NOT_OK;
        if(!cb[contact_slot].isOpen())
        {
            open();
        }
        result=cb[contact_slot].findcard(listener);
        return result;
    }

    @Override
    public void stopfind() {
        cb[contact_slot].Close();
    }

    OnSearchIccCardListener.Stub listener = new OnSearchIccCardListener.Stub() {
        @Override
        public void onSearchResult(final int retCode, Bundle bundle) throws RemoteException {
            byte[] tempuid={0x00};
            if (ServiceResult.Success == retCode) {
                String cardType = bundle.getString(ICCSearchResult.CARDTYPE);
                Log.d("cardType", cardType);
                if (IccCardType.CPUCARD.equals(cardType)) {
                    cpuCardHandler = deviceService.getCPUCardHandler(iccCardReader);
                    cpuCardHandler.setPowerOn(new byte[]{0x00, 0x00});
                    cpuCardHandler.active();
                    cb[contact_slot].setCard(cpuCardHandler);
                }
            }
            //tempuid nilainya null
            detectListener.onContactDetected(tempuid);
        }
    };
}
