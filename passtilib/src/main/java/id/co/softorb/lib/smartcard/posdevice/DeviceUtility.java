package id.co.softorb.lib.smartcard.posdevice;

import android.content.Context;
import android.util.Log;


import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.pax.pax;
import id.co.softorb.lib.smartcard.wizar.wizarpos;


import com.pax.dal.IDAL;
import com.pax.dal.IPicc;

import com.pax.dal.ISys;
import com.pax.dal.entity.EPiccType;
import com.pax.dal.exceptions.PiccDevException;

import static id.co.softorb.lib.helper.ErrorCode.*;
import static id.co.softorb.lib.smartcard.posdevice.params.*;

public class DeviceUtility {
    String TAG = "WizarPax";
    String tagclass= this.getClass().getSimpleName();

    private final int samslot=1;

    byte[] UID;
    Context ctx;

    private contactcard[] contact;

    private POSListener detectListener;
    private  IDAL dal;
    private ISys iSys = null;
    int devicetype;
    class contactcard{
       // public SmartCardReaderDevice pSamReader;
        int cardtype;
        boolean state;
        //private Card psamCard;
        public contactcard()
        {
            cardtype=ctype_unknown;
            state=false;
        }
        /*public Card getCard()
        {
            return psamCard;
        }
        public void setCard(Card card)
        {
            psamCard=card;
        }
        public contactcard(int cardtype)
        {
            this.cardtype=cardtype;
        }
        public SmartCardReaderDevice Reader()
        {
            return pSamReader;
        }
        public int setReader(SmartCardReaderDevice reader)
        {
            if(reader==null)
                return NOT_OK;

            pSamReader=reader;
            return OK;
        }
        public boolean isOpen()
        {
            return state;
        }
        public int Open(OperationListener listener,int timeout)
        {

            try {
                pSamReader.open();
                pSamReader.listenForCardPresent(listener, timeout);
                state=true;
            } catch (DeviceException e) {
                e.printStackTrace();
                Log.e(TAG,"error open sam");
                return NOT_OK;
            }
            return OK;
        }
        public int Close()
            { try{
                pSamReader.close();
                state=false;
            }
            catch (DeviceException e)
            {
                e.printStackTrace();
                Log.e(TAG,"Close Reader Exception");
                return NOT_OK;
            }
            return OK;

        }
        public int setCardType(int type)
        {
            switch(type)
            {
                //valid number for sam slot is 1 and 2
                case ctype_mdr:
                case ctype_bni:
                case ctype_bri:
                case ctype_luminos:
                case ctype_bca:
                    break;
                default:
                    return ERR_INCORRECT_CARDTYPE;
            }

            this.cardtype=type;
            return OK;
        }
*/

    }

   // public DeviceUtility(Context ctx) throws DeviceException {
   public DeviceUtility(Context ctx)  {

        this.ctx = ctx;

        devicetype=UNKNOWN;
/*
        contact = new contactcard[2];
        contact[contact_slot] = new contactcard();
        contact[samslot] = new contactcard();
        */


    }
    id.co.softorb.lib.smartcard.wizar.wizarpos wizarpos;
    pax paxpos;
    public DeviceUtility(Context ctx,POSListener callback)  {

        this.ctx = ctx;

        devicetype=UNKNOWN;


        detectListener=callback;
        wizarpos=new wizarpos(ctx);
        paxpos = new pax(ctx);


    }
    public void CloseContact()
    {

           // contact[contact_slot].Close();

    }










/*
    public byte[] sendSAM(int slot,byte[] apdu)  {

        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName()+", slot "+slot+", apdu "+ Hex.bytesToHexString(apdu));
        byte[] apduResult;
        int length=0;
        int idx=0;
        String message = "";
        try {
            ApduResponse apduResponse = appSAM[slot-1].Reader().exchangeApdu(apdu);
            message = "apduResponse :" + Integer.toHexString(apduResponse.getAPDURet()) + "|" + Hex.byteToHexString(apduResponse.getSW1()) + Hex.byteToHexString(apduResponse.getSW2());
            Log.d(TAG, message);


            apduResult = new byte[length+2];// 2 bytes SW1SW2
            if(apduResponse.getData()!=null) {

                length = apduResponse.getData().length;
                apduResult = new byte[length+2];//re-init apduResult length to (data + 2 bytes SW1SW2)
                System.arraycopy(apduResponse.getData(),0,apduResult,idx,length);
                idx+=length;
            }
            apduResult[idx++]=apduResponse.getSW1();
            apduResult[idx++]=apduResponse.getSW2();
            Log.d(TAG, message + " -- RAPDU: " + Hex.bytesToHexString(apduResult));
        }
        catch(RemoteException e)
        {
            Log.e(TAG,"sendSAM exception : "+e.getMessage());
            return null;

        }

        return apduResult;
    }
*/











    public int initSAM(int ctype,int slot)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        String message = "";
        /*if(slot==contact_slot)
            return ERR_INCORRECT_SAMSLOT;*/
        /*
        * valid valud : 0 - contactcard
        * */
   /*     samreader =(SmartCardReaderDevice) POSTerminal.getInstance(ctx).getDevice("cloudpos.device.smartcardreader",slot);
        contact[slot].setReader(samreader);
        if(contact[slot].Open(samlistener,TimeConstants.FOREVER)!=OK)
        {

            return ERR_INITREADER;
        }
*/

        return OK;

    }

    private EPiccType piccType;
    private IPicc picc;
    public int initCTL(){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        String message = "";
        switch(devicetype)
        {
            case DEV_WIZAR:
       /*         rfReader =(RFCardReaderDevice) POSTerminal.getInstance(ctx).getDevice("cloudpos.device.rfcardreader");

                try {
                    rfReader.open();

                } catch (DeviceException e) {
                    e.printStackTrace();
                    Log.e(TAG,e.toString());
                    return CTL_ERR_INITREADER;
                }*/

                break;
            case DEV_PAX:
                piccType=EPiccType.INTERNAL;
                if(dal!=null) {
                    picc = dal.getPicc(piccType);
                    try {
                        picc.open();
                        Log.e(TAG,"open picc success");
                    } catch (PiccDevException e) {
                        e.printStackTrace();
                        Log.e(TAG,"error open picc "+e.getMessage());
                    }
                }
                else
                {
                    Log.e(TAG,"dal is null");
                }
                break;

        }


        return OK;
    }








    public byte[] sendSAM(int slot,byte[] apdu)  {


        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName()+", slot "+slot+", apdu "+ Hex.bytesToHexString(apdu));
        byte[] apduResult=null;
       // if(slot==contact_slot)
         //   return null;

   /*     try {
            apduResult = ((CPUCard) (contact[samslot].getCard())).transmit(apdu);
            //Log.d(TAG,"apduResult "+Hex.bytesToHexString(apduResult));

        } catch (DeviceException e) {
            e.printStackTrace();
            return null;
        }*/


        return apduResult;
    }

/*


    OperationListener samlistener = new OperationListener() {

        @Override
        public void handleResult(OperationResult arg0) {

            Log.e(TAG,"arg0.getResultCode() = "+arg0.getResultCode());
            if (arg0.getResultCode() == OperationResult.SUCCESS) {
                psamCard = ((SmartCardReaderOperationResult) arg0).getCard();
                try {
                    contact[samslot].setCard(psamCard);

                     ((CPUCard) psamCard).connect();

                }
                catch (DeviceException e)
                {
                    Log.d(TAG, e.toString());

                }

            }
            detectListener.onSAMDetected();


        }
    };*/
}
