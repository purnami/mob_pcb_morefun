package id.co.softorb.lib.passti;

import android.content.Context;

import static id.co.softorb.lib.passti.helper.ErrorCode.*;

public class APDUHelper {
	private static final String TAG = "APDUHelper";
	//final public static byte OK =0;
	//final public static byte CONNECTION_LOST =1;
	//final public static byte INVALID_CARD =2;
	//final public static byte TRANSACTION_FAILED =3;
	//final public static byte DATA_NOT_AVAILABLE=4;
	//final public static byte NOT_ENOUGH_BALANCE =5;
	//final public static byte TOPUP_EXCEEDS_MAXBALANCE =6;
	//final public static byte DATA_AVALAIBLE =7;

	int length;
	Context ctx;

	public static int CheckResult(byte[] APDU) {
		int bResult=OK;

		if(APDU==null) {
			bResult=ERR_NO_RESP;
			return bResult;
		}

		if(APDU.length<=1) {
			bResult=ERR_NO_RESP;
			return bResult;
		}

		if(((APDU[APDU.length-2]==(byte)0X6E) && (APDU[APDU.length-1]==(byte)0X00))) {
			bResult=ERR_NODATA;
			return bResult;
		}

		if(((APDU[APDU.length-2]==(byte)0X6A) && (APDU[APDU.length-1]==(byte)0X86))) {
			bResult=ERR_NODATA;
			return bResult;
		}

		if(((APDU[APDU.length-2]==(byte)0X6A) && (APDU[APDU.length-1]==(byte)0X83))) {
			bResult=ERR_NODATA;
			return bResult;
		}

		if(((APDU[APDU.length-2]==(byte)0X92) && (APDU[APDU.length-1]==(byte)0X01))) {
			//bResult=TOPUP_EXCEEDS_MAXBALANCE;
			return bResult;
		}

		if(((APDU[APDU.length-2]==(byte)0X92) && (APDU[APDU.length-1]==(byte)0X00))) {
			bResult=CTL_ERR_INSUFFICIENTBALANCE;
			return bResult;
		}

		if(((APDU[APDU.length-2]==(byte)0X6A) && (APDU[APDU.length-1]==(byte)0X82))) {
			bResult=CTL_ERR_UNKNOWNCARD;
			return bResult;
		}
		if((APDU[APDU.length-2]==(byte)0X61)){
			bResult=DATA_AVAILABLE;
			return bResult; ///ok
		}
		if((APDU[APDU.length-2]==(byte)0X6C)){
			bResult=DATA_AVAILABLE;
			return bResult; ///ok
		}
		//desfire
		if((APDU[APDU.length-2]==(byte)0X67) && (APDU[APDU.length-1]==(byte)0X00)){
			bResult=ERR_LENGTH;
			return bResult; ///ok
		}
		//desfire
		if((APDU[APDU.length-2]==(byte)0X91) && (APDU[APDU.length-1]==(byte)0X00)){
			return bResult; ///ok
		}

		//Mega
		if((APDU[APDU.length-2]==(byte)0X91) && (APDU[APDU.length-1]==(byte)0XAF)){
			return bResult; ///ok
		}

		if((APDU[APDU.length-2]==(byte)0X90) && (APDU[APDU.length-1]==(byte)0XAF)){
			return bResult; ///ok
		}

		//LuminOS
		if((APDU[APDU.length-2]==(byte)0X69) && (APDU[APDU.length-1]==(byte)0X85)){
			bResult=ERR_NODATA;
			return bResult; ///ok
		}

		if(!((APDU[APDU.length-2]==(byte)0X90) && (APDU[APDU.length-1]==(byte)0X00))) {
			bResult=ERR_UNKNOWN;
			return bResult;
		}
		return bResult;
	}

	public APDUHelper(Context ctx) {
		this.ctx=ctx;
	}
}