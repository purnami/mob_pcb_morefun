package id.co.softorb.lib.passti;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

import java.util.Arrays;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.helper.ByteUtils;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.passti.helper.ErrorCode;
import id.co.softorb.lib.passti.helper.converter;
import id.co.softorb.lib.passti.helper.util;
import id.co.softorb.lib.smartcard.posdevice.params;
import id.co.softorb.lib.smartcard.posdevice.reader;


import static id.co.softorb.lib.passti.helper.ErrorCode.*;

import static id.co.softorb.lib.passti.devinfo.*;
import static id.co.softorb.lib.smartcard.posdevice.params.*;

class Luminos {
	
	//Command
	String tagclass;
	String TAG;

	private String Balance;
	private String Amount;
	private String ExpDate;
	private String Deposit;
	private byte[] RAPDU = new byte[255];
	private String[][] Hist = new String[11][5];
	//private Activity currentAct;
	private int histrec;
	private byte[] banklog = new byte[255];
	String lasttrxBalance="0";
	String lastcardno="0";
	String lastamount="0";


	String saldoAwal,saldoAkhir = "";

	byte[] selectapp_dki_prod ={(byte)0x00,(byte)0xA4,(byte)0x04,(byte)0x00,(byte)0x08,(byte)0xA0,(byte)0x00,(byte)0x00,(byte)0x05,(byte)0x71,(byte)0x4E,(byte)0x4A,(byte)0x43};
	PASSTI passti;

	int samslot;
	Context ctx;
	reader device;
	 void SetSamPort(int slot)
	 {
		 samslot=slot;
	 }

//	 Luminos(Context ctx, UDeviceService deviceService, DeviceInfo deviceInfo,PASSTI passtiInstance,APOSUtility aposUtilityInstance) throws RemoteException, UnsupportedEncodingException {
	 Luminos(Context ctx, PASSTI passtiInstance, reader reader)  {

		 tagclass = this.getClass().getSimpleName();


		this.ctx=ctx;
		 passti = passtiInstance;
		device=reader;
		 samslot=255;//default for unassigned sam slot
		 TAG= passti.dbghelper.TAG;
	}

	static byte[] trim(byte[] bytes)
	{
		int i = bytes.length - 1;
		while (i >= 0 && bytes[i] == 0)
		{
			if(bytes[i-1]== (byte)0x90 && bytes[i] == (byte)0x00)
			{
				break;
			}

			--i;

		}

		return Arrays.copyOf(bytes, i + 1);
	}

private int Luminos_SAM_SelectAID(String aid) throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		Log.d("masukluminosSelectAID", ""+aid);
		//byte[] AID_TRANSBATAM={(byte)0xD3,(byte)0x60,(byte)0x16,(byte)0x00,(byte)0x02,(byte)0x00,(byte)0x01};
		//byte[] AID_DKI_PROD={(byte)0xD3,(byte)0x60,(byte)0x01,(byte)0x00,(byte)0x02,(byte)0x00,(byte)0x01};

		//byte[] AID_DEV={(byte)0x57,(byte)0x10,(byte)0x0D,(byte)0xEF,(byte)0x00,(byte)0x00,(byte)0x01};
		//byte[] selectAID={(byte)0x00,(byte)0xA4,(byte)0x04,(byte)0x00,(byte)0x07,(byte)0xD3,(byte)0x60,(byte)0x01,(byte)0x00,(byte)0x02,(byte)0x00,(byte)0x01};
		byte[] selectAID={(byte)0x00,(byte)0xA4,(byte)0x04,(byte)0x00,(byte)0x00};
		int idx=0;
		//int result=0;
		byte[] AID ;
		AID = BytesUtil.hexString2ByteArray(aid);;
		byte[] command = new byte[AID.length + selectAID.length];
		selectAID[4]=(byte)AID.length;
		System.arraycopy(selectAID,0,command,idx,selectAID.length);
		idx+=selectAID.length;
		System.arraycopy(AID,0,command,idx,AID.length);
		idx+=AID.length;
		//byte[] command1 = new byte[idx];
		//System.arraycopy(command,0,command1,0,command1.length);

		byte[] RAPDU;
		Log.d("SENDSAM", ""+samslot+" "+Hex.bytesToHexString(command));
		 RAPDU = device.sendSAM(samslot,command);
		 Log.d("rapdu sam", ""+Hex.bytesToHexString(RAPDU));
		if(RAPDU == null){

		}
		if(APDUHelper.CheckResult(RAPDU) != 0){

			 return ERR_SW1SW2;
		}
		return OK;
	}

	
public int CTL_SelectApp(String aid) throws RemoteException {
			Log.i(TAG, tagclass+"."+new Throwable()
					.getStackTrace()[0]
					.getMethodName()+",aid "+aid);


		//String command ="00A4040008"+aid;
		int offset=0;
		byte[] header=new byte[]{0x00,(byte)0xa4,(byte)0x04,(byte)0x00,(byte)0x00};
		byte[] appaid=BytesUtil.hexString2ByteArray(aid);
		byte[] apdu=new byte[header.length+appaid.length];

		lastcardno=BytesUtil.bytes2HexString(passti.ctlinfo.cardno);
		header[4]=(byte)appaid.length;
		System.arraycopy(header,0,apdu,0,header.length);
		offset+=header.length;
		System.arraycopy(appaid,0,apdu,offset,appaid.length);
		RAPDU = device.sendCTL(apdu);
	    Log.d("RAPDUUUU luminos", Hex.bytesToHexString(RAPDU));
		if(RAPDU==null)
		{
			return ERR_NO_RESP;
		}
		if(APDUHelper.CheckResult(RAPDU)!=0)
		{
			return ErrorCode.ERR_SW1SW2;
		}

		System.arraycopy(RAPDU,8,passti.ctlinfo.cardno,0,8);
		ExpDate = ByteUtils.bytesToHex(RAPDU).substring(50, 58);
		Deposit = ByteUtils.bytesToHex(RAPDU).substring(74, 82);
		return OK;
	}
	private int CTL_GetBalance() throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());

		lasttrxBalance=Balance;
		Log.d("lasttrxBalance", ""+lasttrxBalance);
		Log.d("saldo awal1", ""+Balance);
//		device.sendCTL(BytesUtil.hexString2ByteArray("00A4040008A000000571504805"));
		Log.d("rapduu0", Hex.bytesToHexString(RAPDU));
		RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray("904C000004"));
		Log.d("rapduu1", Hex.bytesToHexString(RAPDU));
		if(RAPDU==null)
		{

			return ErrorCode.LUMINOS_CTL_ERR_GETBALANCE;
		}
		Log.d("saldo awal2", ""+Balance);
		if(APDUHelper.CheckResult(RAPDU) != 0){

			return ErrorCode.LUMINOS_CTL_ERR_GETBALANCE;
		}
//		Log.d(TAG,"RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
		System.arraycopy(RAPDU,0,passti.ctlinfo.bBalanceKartu,0,4);
		Log.d(TAG,"passti.ctlinfo.bBalanceKartu: " + BytesUtil.bytes2HexString(passti.ctlinfo.bBalanceKartu));
		Balance= String.valueOf(converter.ByteArrayToInt(RAPDU, (byte)0, (byte)4, converter.BIG_ENDIAN));
		Log.d("saldo awal3", ""+Balance);
		return OK;
	}

	private int CTL_GetBalance2() throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		int result=ERR_NO_RESP;

		result=LUMINOS_CEKBALANCE();
		if(result!=OK)

		{
			return LUMINOS_CTL_ERR_GETBALANCE;
		}


//		lasttrxBalance=Balance;
//
////		device.sendCTL(BytesUtil.hexString2ByteArray("00a4040007d3571000030003"));
//		RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray("904C000004"));
//		if(RAPDU==null)
//		{
//
//			return ErrorCode.LUMINOS_CTL_ERR_GETBALANCE;
//		}
//		if(APDUHelper.CheckResult(RAPDU) != 0){
//
//			return ErrorCode.LUMINOS_CTL_ERR_GETBALANCE;
//		}
//		Log.d(TAG,"RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
//		System.arraycopy(RAPDU,0,passti.ctlinfo.bBalanceKartu,0,4);
//		Balance= String.valueOf(converter.ByteArrayToInt(RAPDU, (byte)0, (byte)4, converter.BIG_ENDIAN));
		return OK;
	}

	private int LUMINOS_CEKBALANCE() throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		byte[] cmdCekBalance =  {(byte) 0x90,0x4C,0x00,0x00,0x04};
		RAPDU = device.sendCTL(cmdCekBalance);
		Log.d("rapdu luminoss", Hex.bytesToHexString(RAPDU));
		if(RAPDU == null){
			return ERR_NO_RESP;
		}
		if(APDUHelper.CheckResult(RAPDU) != 0){

			return ERR_NO_RESP;
		}
		System.arraycopy(RAPDU,0,passti.ctlinfo.bBalanceKartu,0,4);
		Balance= String.valueOf(converter.ByteArrayToInt(RAPDU, (byte)0, (byte)4, converter.BIG_ENDIAN));
		return OK;
	}

	private int SAM_InitPurchase() throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		byte[] bAmt = new byte[4];

		String send = "800200001BYYXXXXXXXX";
		byte[] sendRAPDU = new byte[RAPDU.length-2];
		System.arraycopy(RAPDU, 0, sendRAPDU, 0, RAPDU.length-2);
		bAmt= converter.IntegerToByteArray(Integer.valueOf(Amount));
		Log.d(TAG,"sendInitPurchase1 - " + send);
		send=send.replaceAll("XXXXXXXX", ByteUtils.bytesToHex(bAmt));
		Log.d(TAG,"sendInitPurchase2 - " + send);
		send=send.replaceAll("YY", ByteUtils.bytesToHex(sendRAPDU));
		Log.d(TAG,"sendInitPurchase3 - " + send);
		//RAPDU=util.WebServiceExecuteAndGetResult(soapPayment, send, currentAct);
		byte[] cmd = ByteUtils.hexStringToByteArray(send);
		//RAPDU=sam.SendAPDU(port_luminos,cmd);
		RAPDU = device.sendSAM(samslot,cmd);
		if(RAPDU==null)
			return ErrorCode.ERR_NO_RESP;
		if (RAPDU.length< 2) return ErrorCode.ERR_NO_RESP;
		if(APDUHelper.CheckResult(RAPDU)!=0)	return ERR_SW1SW2;

		return OK;
	}

	private int SAM_DebitLSAM() throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		byte[] bAmt = new byte[4];

		//String send =  luminos.lsam_debit()+Deposit+ExpDate;
		String send="8056000122YYXXXXXXXX";
		byte[] sendRAPDU = new byte[RAPDU.length-2];
		System.arraycopy(RAPDU, 0, sendRAPDU, 0, RAPDU.length-2);
		bAmt= converter.IntegerToByteArray(Integer.valueOf(Amount));
		send=send.replaceAll("XXXXXXXX", ByteUtils.bytesToHex(bAmt));
		send=send.replaceAll("YY", ByteUtils.bytesToHex(sendRAPDU));
		Log.d(TAG,send);

		byte[] cmd = ByteUtils.hexStringToByteArray(send);
		RAPDU = device.sendSAM(samslot,cmd);
		if(RAPDU==null)
			return ErrorCode.ERR_NO_RESP;
		if (RAPDU.length < 2) return ErrorCode.ERR_NO_RESP;
		if (APDUHelper.CheckResult(RAPDU) != 0) return ERR_SW1SW2;


		return OK;
	}

	private int CTL_InitPurchase() throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		byte[] bAmt = new byte[4];

		//String send = luminos.ctl_initpurchase();
		String send = "9040030004XXXXXXXX17";
		bAmt= converter.IntegerToByteArray(Integer.valueOf(Amount));
		send=send.replaceAll("XXXXXXXX", ByteUtils.bytesToHex(bAmt));

		RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(send));
		if(RAPDU==null)
		{
			return ErrorCode.ERR_NO_RESP;
		}
		if(APDUHelper.CheckResult(RAPDU)!=0)
			return ERR_SW1SW2;
		return OK;
	}

	private int CTL_InitTopup() throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		byte[] bAmt = new byte[4];
		//String send = initTopUp;
		//String send = luminos.ctl_inittopup();
		String send = "9040000004XXXXXXXX";
		bAmt= converter.IntegerToByteArray(Integer.valueOf(Amount));
		send=send.replaceAll("XXXXXXXX", ByteUtils.bytesToHex(bAmt));

		RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(send));
		if(RAPDU==null)
		{
			return ErrorCode.ERR_NO_RESP;
		}
		if(APDUHelper.CheckResult(RAPDU)!=0)	return ERR_SW1SW2;
		return OK;
	}

	private int CTL_Debit() throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		byte[] sendRAPDU = new byte[RAPDU.length-2];

		//String send = luminos.ctl_debitpurse();
		String send = "9046000012YY04";
		System.arraycopy(RAPDU, 0, sendRAPDU, 0, RAPDU.length-2);
		send=send.replaceAll("YY", ByteUtils.bytesToHex(sendRAPDU));

		RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(send));
		if(RAPDU==null)
		{
			passti.SetAction(REPURCHASE);//2020-02-18,vivo
			return ErrorCode.ERR_NO_RESP;
		}
		if (RAPDU.length < 2) {
			passti.SetAction(REPURCHASE);//2020-02-18,vivo
			return ErrorCode.ERR_NO_RESP;//2020-02-18,vivo
		}

		if(APDUHelper.CheckResult(RAPDU)!=0)
			return ERR_SW1SW2;
		return OK;
	}

	private int CTL_Topup() throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		byte[] sendRAPDU = new byte[RAPDU.length-2];

		//String send = luminos.ctl_creditpurse();
		String send = "9042000010YY";
		System.arraycopy(RAPDU, 0, sendRAPDU, 0, RAPDU.length-2);
		send=send.replaceAll("YY", ByteUtils.bytesToHex(sendRAPDU));

		RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(send));
		if(RAPDU==null)
		{
			return ERR_NO_RESP;
		}
		if(APDUHelper.CheckResult(RAPDU)!=0)	return ERR_SW1SW2;
		return OK;
	}

	private boolean parseLog(int idx, String Data){
		int inc=0;
		String TrxType = Data.substring(0,  2);
		byte []bAmount = new byte[4];
		byte []bCTC = new byte[4];
		byte []bSTC = new byte[4];

		//trx type
		if(TrxType.compareTo("01")==0)			Hist[idx][inc++]="SALE";
		else if(TrxType.compareTo("02")==0)		Hist[idx][inc++]="TOP UP";
		else									return false;
		//amount
		bAmount= ByteUtils.hexStringToByteArray(Data.substring(20, 28));
		Hist[idx][inc++]= String.valueOf(converter.ByteArrayToInt(bAmount, (byte)0, (byte)4, converter.BIG_ENDIAN));
		//CTC
		bCTC= ByteUtils.hexStringToByteArray(Data.substring(44, 52));
		Hist[idx][inc++]= String.valueOf(converter.ByteArrayToInt(bCTC, (byte)0, (byte)4, converter.BIG_ENDIAN));
		//sam id
		Hist[idx][inc++]=Data.substring(28, 44);
		//STC
		bSTC= ByteUtils.hexStringToByteArray(Data.substring(12, 20));
		Hist[idx][inc++]= String.valueOf(converter.ByteArrayToInt(bSTC, (byte)0, (byte)4, converter.BIG_ENDIAN));

		return true;
	}

	private String getLog(int idx){

		//String Send = luminos.ctl_getlogidx();
		String Send = "";
		byte []bIdx= converter.IntegerToByteArray(idx);
		String sIdx= ByteUtils.bytesToHex(bIdx);
		Send=Send.replaceAll("XX", sIdx.substring(6, 8));
		return Send;
	}

	private Boolean cGetTrxLog() throws RemoteException {
		int i,j;
		
		for(i=0;i<Hist.length;i++)
			for(j=0;j<Hist[i].length;j++)
				Hist[i][j]=null;
		
		for(i=1,j=1;i<11;i++){

			RAPDU = device.sendCTL(BytesUtil.hexString2ByteArray(getLog(i)));
			if(RAPDU==null)
			{
				histrec=j-1;
				return true;
			}
			if(APDUHelper.CheckResult(RAPDU)!=0)
			{
				histrec=j-1;
				return true;
			}
			if(parseLog(j, ByteUtils.bytesToHex(RAPDU))) j++;
		}
		histrec=j-1;
		return true;
	}
	
	private int GetHistoryNumber()
	{
		return histrec;
	}
	
	/*
	private int getCardInfo() throws RemoteException {
		int result;
		result =CTL_GetBalance();
		if(result!= OK)
			return result;

		if(!cGetTrxLog()) return ErrorCode.LUMINOS_CTL_ERR_GETTRXLOG;
		return OK;
	}
	*/
	 int checkbalance() throws RemoteException {
		int result;
		saldoAwal = "";
		result =CTL_GetBalance();
				if(result== OK) {

            saldoAwal = Balance;

        }


		 return result;
	}
	/*
	private int CheckHistory() throws RemoteException {

		if(!cGetTrxLog()) return ErrorCode.LUMINOS_CTL_ERR_GETTRXLOG;
		return OK;
	}
*/
	 int deduct(String amount) throws RemoteException {
	    saldoAwal = "";
	    saldoAkhir = "";
		int i = Payment(amount);

		return i;
	}
/*
     String getCardNo(){

         String cardNo;

         passti.dbghelper.DebugHex("passti.ctlinfo.cardno",passti.ctlinfo.cardno);
         if(passti.ctlinfo.cardno != null) {
             cardNo = Hex.bytesToHexString(passti.ctlinfo.cardno);
         }else cardNo = "Card Not Found";
         return cardNo;
    }
*/
     String getBalance(){return saldoAwal;}
     String getLastBalance(){
        return saldoAkhir;
    }

     /*byte[] getLogPASSTILuminos()
    {
        return reportRAPDU;

    }*/

	 int LMNS_TestAutoCorrection()
	 {
		 passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				 .getStackTrace()[0]
				 .getMethodName());
		 //if(deduct.doCorrection==0)
		 if(passti.GetAction()==NORMAL)
		 {

			 RAPDU=new byte[1];
			 return ERR_NO_RESP;
		 }
		 if(passti.GetAction()==REPURCHASE)
		 {
			 return OK;
		 }
		 return OK;
	 }
	private int Payment(String sAmount) throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());

		Amount=sAmount;
		int result;
		byte[] amount= converter.IntegerToByteArray(Integer.valueOf(Amount));
		System.arraycopy(amount,0,passti.deduct.baAmount,0,4);

		passti.dbghelper.DebugInteger("passti.GetAction ",passti.GetAction());
		if(passti.GetAction()== REPURCHASE)
		{
			Log.d(TAG,"Execute autocompletion");

			passti.dbghelper.DebugHex("passti.ctlinfo.cardno", passti.ctlinfo.cardno);
			passti.dbghelper.DebugHex("passti.deduct.prevCardNo",passti.deduct.prevCardNo);
			passti.dbghelper.DebugHex("passti.ctlinfo.uid.value",passti.ctlinfo.uid.value);
			passti.dbghelper.DebugHex("passti.deduct.prevUID",passti.deduct.prevUID);
			saldoAwal = Integer.toString(converter.ByteArrayToInt(passti.ctlinfo.bBalanceKartu,0,4,converter.BIG_ENDIAN));
			if(BytesUtil.bytes2HexString(passti.ctlinfo.cardno).matches(BytesUtil.bytes2HexString(passti.deduct.prevCardNo))  && BytesUtil.bytes2HexString(passti.ctlinfo.uid.value).matches(BytesUtil.bytes2HexString(passti.deduct.prevUID)))
			{
				byte[] curbalance=new byte[4];
				result = LMNS_CTL_ReadEFPurse();
				if (result != OK)
					return result;
				System.arraycopy(RAPDU,2,curbalance,0,4);
				passti.dbghelper.DebugHex("curbalance",curbalance);
				passti.dbghelper.DebugHex("passti.deduct.prevBalance",passti.deduct.prevBalance);
				if(Arrays.equals(passti.deduct.prevBalance,curbalance)==true)
				{
					passti.dbghelper.DebugPrintString("previous balance = current balance, do normal deduct process");
					passti.SetAction(NORMAL);
				}
				else
				{
					result=LMNS_DeductCorrection(amount,amount.length);
					return result;
				}
			}

		}
		if(passti.GetAction()== NORMAL) {

			Log.d(TAG,"Normal transaction");
			result = CTL_GetBalance();
			if(result!= OK)
				return result;
			saldoAwal = Balance;
			if(Integer.valueOf(Amount)> Integer.valueOf(Balance))
				return ErrorCode.CTL_ERR_INSUFFICIENTBALANCE;
			result = CTL_InitPurchase();
			if (result != OK)
				return LUMINOS_CTL_ERR_INITPURCHASE;
			//result = Luminos_SAM_SelectAID(ctx.getString(R.string.sam_aid_passtikit));
			result = Luminos_SAM_SelectAID(ctx.getString(R.string.sam_aid_bali));
			if (result != OK)
				return LUMINOS_SAM_ERR_SELECTAID;
			result = SAM_InitPurchase();
			if (result != OK)
				return LUMINOS_SAM_ERR_INITPURCHASE;

			result = CTL_Debit();
			//result =LMNS_TestAutoCorrection();
			if (result != OK) {
				try {
					int a = RAPDU.length;
					if (a < 2) {
						Log.e(TAG,"response length incomplete, set flag to REPURCHASE");
						passti.SetAction(REPURCHASE);//2020-02-18,vivo


					}
				} catch (Exception e) {
					Log.e(TAG, "CTL_Debit " + e.getMessage());
					passti.SetAction(REPURCHASE);//2020-02-18,vivo

				}
				return LUMINOS_CTL_ERR_DEBIT;
			}


			result = SAM_CreditPSAM();
			if (result != OK)
				return LUMINOS_SAM_ERR_CREDITPSAM;


			byte[] curBalance = new byte[4];
			System.arraycopy(RAPDU, 15, curBalance, 0, 4);
			Balance = String.valueOf(converter.ByteArrayToInt(curBalance, (byte) 0, (byte) 4, converter.BIG_ENDIAN));

			saldoAkhir = Balance;
			byte[] devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
			//byte[] amount = converter.IntegerToByteArray(Integer.valueOf(sAmount));
			passti.log = passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value, params.ctype_luminos, devicetime, passti.ctlinfo.cardno, curBalance, amount, trxno, banklog, others);


		}
		return OK;
	}
	
	private int TopUp(String sAmount) throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		Amount=sAmount;
		int result;
		result = CTL_GetBalance();
		if(result!= OK)
			return LUMINOS_CTL_ERR_GETBALANCE;
		if(Integer.valueOf(Amount)+ Integer.valueOf(Balance)>1000000)
								return ErrorCode.CTL_ERR_TOPUPEXCEEDMAXBALANCE;

		result = CTL_InitTopup();
		if(result!= OK)
			return LUMINOS_CTL_ERR_INITTOPUP;

		result = SAM_DebitLSAM();
		if(result!= OK)
			return LUMINOS_SAM_ERR_DEBITLSAM;
		result = CTL_Topup();
		if(result!= OK)
			return LUMINOS_CTL_ERR_TOPUP;
		
		result = SAM_ConfirmLSAM();
		if(result!= OK)
			return LUMINOS_SAM_ERR_CONFIRMLSAM;

		
		return OK;
	}


	
	private String[][]getHistory(){
		int inc=0;
		Hist[0][inc++]="Transaction Type";
		Hist[0][inc++]="Amount";
		Hist[0][inc++]="Card Trans Counter";
		Hist[0][inc++]="SAM ID";
		Hist[0][inc++]="SAM Trans Counter";		
		return Hist;
	}
	


	private int SAM_ConfirmLSAM() throws RemoteException {
		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		byte[] sendRAPDU = new byte[RAPDU.length-2];
		System.arraycopy(RAPDU, 0, sendRAPDU, 0, RAPDU.length-2);
		String send = "8058000008"+ ByteUtils.bytesToHex(sendRAPDU);

		//RAPDU=util.WebServiceExecuteAndGetResult(soapTopUp, send, currentAct);
		byte[] cmd = ByteUtils.hexStringToByteArray(send);
		//RAPDU=sam.SendAPDU(port_luminos,cmd);
		RAPDU = device.sendSAM(samslot,cmd);
		if(RAPDU==null)
			return ERR_NO_RESP;

		if (RAPDU.length< 2) return ErrorCode.ERR_NO_RESP;
		if(APDUHelper.CheckResult(RAPDU)!=0)	return ERR_SW1SW2;

		return OK;
	}

	private int SAM_SelectAID() throws RemoteException {

		passti.dbghelper.DebugPrintString(tagclass+"."+new Throwable()
				.getStackTrace()[0]
				.getMethodName());
		if(RAPDU==null)
		{
			return ErrorCode.LUMINOS_SAM_ERR_SELECTAID;
		}

		byte apdu_samaid []= {(byte)0x00,(byte)0xA4,(byte)0x04,(byte)0x00,(byte)0x07,(byte)0x57,(byte)0x10,(byte)0x0D,(byte)0xEF,(byte)0x00,(byte)0x00,(byte)0x01};
		//byte[] res = sam.SendAPDU(port, ByteUtils.hexStringToByteArray(card.ctl_selectapp()));
		//byte[] res = sam.SendAPDU(port_luminos, apdu_samaid);
		RAPDU = device.sendSAM(samslot,apdu_samaid);
		if(RAPDU==null)
		{
			return ErrorCode.LUMINOS_SAM_ERR_SELECTAID;
		}
		if(APDUHelper.CheckResult(RAPDU)!=0)
			return ErrorCode.LUMINOS_SAM_ERR_SELECTAID;
		return OK;

	}


	void SaveLog(byte[] resp)
	{
		banklog =new byte[resp.length-2];
		System.arraycopy(resp,0, banklog,0, banklog.length);
		//sharedata.logDeduct = logDeductHex;
	}

	private int SAM_CreditPSAM() throws RemoteException {

		Log.d(TAG,"SAM_CreditPSAM");
		if(RAPDU==null)
		{
			return ErrorCode.LUMINOS_SAM_ERR_CREDITPSAM;
		}
		String send= "8004000004";
		byte[] cmd = Hex.hexStringToBytes(send);
		byte[] sendRAPDU = new byte[cmd.length+RAPDU.length-2];
		System.arraycopy(cmd, 0, sendRAPDU, 0, cmd.length);
		System.arraycopy(RAPDU, 0, sendRAPDU, cmd.length, RAPDU.length-2);
		//Log.d(TAG, "APP -> SAM : "+ Hex.bytesToHexString(sendRAPDU));
		//RAPDU= util.WebServiceExecuteAndGetResult(soapPayment, Hex.bytesToHexString(sendRAPDU), currentAct);
		cmd = sendRAPDU;
		//RAPDU=sam.SendAPDU(port_luminos,cmd);
		RAPDU = device.sendSAM(samslot,cmd);
		if(RAPDU==null)
			return ErrorCode.ERR_NO_RESP;
		//Log.d(TAG, "SAM -> APP : "+ Hex.bytesToHexString(RAPDU));
		if (RAPDU.length< 2) return ErrorCode.ERR_NO_RESP;
		if(APDUHelper.CheckResult(RAPDU)!=0)
			return ERR_SW1SW2;
		else
		{
			//process trxlog here
			if((RAPDU[RAPDU.length-2]==(byte)0x90)&&(RAPDU[RAPDU.length-1]==(byte)0x00))
			{

				SaveLog(RAPDU);


			}
			if(RAPDU[RAPDU.length-2]==(byte)0x61)
			{
				sendRAPDU = new byte[5];
				sendRAPDU[0]=(byte)0x00;
				sendRAPDU[1]=(byte)0xC0;
				sendRAPDU[2]=(byte)0x00;
				sendRAPDU[3]=(byte)0x00;
				sendRAPDU[4]=RAPDU[RAPDU.length-1];
				//Log.d(TAG, "APP -> SAM : "+ Hex.bytesToHexString(sendRAPDU));

				cmd = sendRAPDU;
				//RAPDU=sam.SendAPDU(port_luminos,cmd);
				RAPDU = device.sendSAM(samslot,cmd);
				if(RAPDU==null)
					return ERR_NO_RESP;
			

				SaveLog(RAPDU);

			}
		}
		return OK;
	}


	 int LMNS_DeductCorrection(byte[] amount, int amountlen)
	 {
		 Log.d(TAG, "LMNS_DeductCorrection");
		 int result;


		 result=LMNS_CTL_InitRepurchase(amount,amountlen);
		 if(result!=OK)
		 {

			 return LUMINOS_CTL_ERR_INITREPURCHASE;
		 }

		 result = LMNS_SAM_ReinitPurchase(RAPDU,RAPDU.length-2,amount,amountlen);
		 if(result!=OK)
		 {
			 return LUMINOS_SAM_ERR_INITREPURCHASE;
		 }

		 result = LMNS_CTL_RePurchase(RAPDU,RAPDU.length-2);
		 if(result!=OK)
		 {
			 return LUMINOS_CTL_ERR_REPURCHASE;
		 }

		 result = LMNS_SAMConfirmRepurchase(RAPDU,RAPDU.length-2);
		 if(result!=OK)
		 {
			 return LUMINOS_SAM_ERR_REPURCHASE;
		 }
		 SaveLog(RAPDU);//save log reponse from PSAM


		 saldoAkhir = Balance;
		 byte[] curBalance = new byte[4];
		 System.arraycopy(RAPDU, 15, curBalance, 0, 4);
		 byte[] devicetime = ByteUtils.hexStringToByteArray(util.Datenow("ddMMyyyyHHmmss"));
		 //byte[] amount = converter.IntegerToByteArray(Integer.valueOf(sAmount));
		 passti.log = passti.PASSTI_ComposeTransactionLog(passti.ctlinfo.uid.value,params.ctype_luminos,devicetime,passti.ctlinfo.cardno,curBalance,amount,trxno, banklog,others);

		 //deduct.doCorrection=0;
		 passti.SetAction(NORMAL);





		 return OK;

	 }

	 int LMNS_SAMConfirmRepurchase(byte[] inData, int inLen)
	 {
		 Log.d(TAG, "LMNS_SAMConfirmRepurchase");
		 byte[] apdu={(byte)0x80,0x44,0x00,0x00,0x04};
		 byte[] cmd=new byte[apdu.length+inLen+1];
		 int result=0;
		 int offset=0;
		 Arrays.fill(cmd,(byte)0x00);

		 System.arraycopy(apdu,0,cmd,offset,apdu.length);
		 offset+=apdu.length;
		// memcpy(&cmd[offset],inData,inLen);
		 System.arraycopy(inData,0,cmd,offset,inLen);
		 offset+=inLen;
		 cmd[offset++]=0x31;//Le

		 RAPDU= device.sendSAM(samslot,cmd);
		 if(RAPDU==null)
			 return ERR_NO_RESP;

		 if(APDUHelper.CheckResult(RAPDU)!=0)
			 return ERR_SW1SW2;


		 return OK;
	 }
	 int LMNS_CTL_ReadEFPurse()
	 {

		 Log.d(TAG, "LMNS_CTL_ReadEFPurse");
		 byte[] apdu={0x00,(byte)0xB2,0x01,0x24,0x2E};
		 int result=0;

		 RAPDU = device.sendCTL(apdu);
		 if(RAPDU==null)
			 return ERR_NO_RESP;

		 if(APDUHelper.CheckResult(RAPDU)!=0)
			 return ERR_SW1SW2;

		//System.arraycopy(RAPDU,2,);
		 Balance= String.valueOf(converter.ByteArrayToInt(RAPDU, (byte)2, (byte)4, converter.BIG_ENDIAN));

		 return OK;
	 }

	 int LMNS_CTL_InitRepurchase(byte[] data,int datalen)
	 {
		 Log.d(TAG, "LMNS_CTL_InitRepurchase");
		 byte[] apdu={(byte)0x90,0x40,0x04,0x00,0x04};
		 int idx=0;
		 int result;
		 byte[] cmd=new byte[apdu.length+datalen];

		 Arrays.fill(cmd,(byte)0x00);

		 System.arraycopy(apdu,0,cmd,idx,apdu.length);
		 idx+=apdu.length;
		 System.arraycopy(data,0,cmd,idx,datalen);
		 idx+=datalen;


		 RAPDU = device.sendCTL(cmd);
		 if(RAPDU==null)
			 return ERR_NO_RESP;

		 if(APDUHelper.CheckResult(RAPDU)!=0)
			 return ERR_SW1SW2;


		 return OK;
	 }

	 int LMNS_SAM_ReinitPurchase(byte[] pursedata,int pursedatalen,byte[] amount, int lenAmt)
	 {
		 Log.d(TAG,"LMNS_SAM_ReinitPurchase");
		 byte[] apdu={(byte)0x80,0x22,0x00,0x00,0x2B};
		 byte[]  cmd=new byte[apdu.length+pursedatalen+lenAmt];
		 int result=0;
		 int offset=0;

		 Arrays.fill(cmd,(byte)0x00);

		 System.arraycopy(apdu,0,cmd,offset,apdu.length);
		 offset+=apdu.length;
		 System.arraycopy(pursedata,0,cmd,offset,pursedatalen);
		 offset+=pursedatalen;
		 System.arraycopy(amount,0,cmd,offset,lenAmt);
		 offset+=lenAmt;

		 RAPDU=device.sendSAM(samslot,cmd);
		 if(RAPDU==null)
			 return ERR_NO_RESP;

		 if(APDUHelper.CheckResult(RAPDU)!=0)
			 return ERR_SW1SW2;

		 return OK;
	 }

	 int LMNS_CTL_RePurchase(byte[] data, int datalen)
	 {
		 Log.d(TAG,"LMNS_CTL_RePurchase");
		 byte[] apdu ={(byte)0x90,0x48,0x00,0x00,0x12};
		 byte[]  cmd=new byte[apdu.length+datalen+1];
		 int idx=0;
		 int result;
		 Arrays.fill(cmd,(byte)0x00);
		 System.arraycopy(apdu,0,cmd,idx,apdu.length);
		 idx+=apdu.length;
		 System.arraycopy(data,0,cmd,idx,datalen);
		 idx+=datalen;


		 cmd[idx++]=0x04;

		 RAPDU=device.sendCTL (cmd);
		 if(RAPDU==null)
			 return ERR_NO_RESP;

		 if(APDUHelper.CheckResult(RAPDU)!=0)
			 return ERR_SW1SW2;


		 return OK;


	 }


 }
