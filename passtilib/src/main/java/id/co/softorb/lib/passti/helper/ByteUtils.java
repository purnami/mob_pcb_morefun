package id.co.softorb.lib.passti.helper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ByteUtils {
	
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	public static byte[] convertHexAsciiToByteArray(byte[] bytes) {
		return convertHexAsciiToByteArray(bytes, 0, bytes.length);
	}

	public static byte[] convertHexAsciiToByteArray(byte[] bytes, int offset, int length) {
		byte[] bin = new byte[length / 2];
		for (int x = 0; x < length / 2; x++) {
			bin[x] = (byte) Integer.parseInt(new String(bytes, offset + (x * 2), 2), 16);
		}
		return bin;
	}
	
	public static byte[] hexStringToByteArray(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}

	public static byte[] convertASCIIToBin(String ascii) {
		byte[] bin = new byte[ascii.length() / 2];
		for (int x = 0; x < bin.length; x++) {
			bin[x] = (byte) Integer.parseInt(ascii.substring(x * 2, x * 2 + 2), 10);
		}
		return bin;
	}

	public static String convertBinToASCII(byte[] bin) {
		return convertBinToASCII(bin, 0, bin.length);
	}

	public static String convertBinToASCII(byte[] bin, int offset, int length) {
		StringBuilder sb = new StringBuilder();
		for (int x = offset; x < offset + length; x++) {
			String s = Integer.toHexString(bin[x]);

			if (s.length() == 1)
				sb.append('0');
			else
				s = s.substring(s.length() - 2);
			sb.append(s);
		}
		return sb.toString().toUpperCase();
	}

	public static byte[] toByteArray(Number data) {
		Class<? extends Number> dataType = data.getClass();
		int length;
		long value;
		if (Byte.class == dataType) {
			length = Byte.SIZE / Byte.SIZE;
			value = (Byte) data;
		} else if (Short.class == dataType) {
			length = Short.SIZE / Byte.SIZE;
			value = (Short) data;
		} else if (Integer.class == dataType) {
			length = Integer.SIZE / Byte.SIZE;
			value = (Integer) data;
		} else if (Long.class == dataType) {
			length = Long.SIZE / Byte.SIZE;
			value = (Long) data;
		} else
			throw new IllegalArgumentException("Parameter must be one of the following types:\n Byte, Short, Integer, Long");
		byte[] byteArray = new byte[length];
		for (int i = 0; i < length; i++) {
			byteArray[i] = (byte) ((value >> (8 * (length - i - 1))) & 0xff);
		}
		return byteArray;
	} 

	public static byte[] intTo4Bytes(int i) {
		return new byte[] { (byte)((i >> 24) & 255), (byte)((i >> 16) & 255), (byte)((i >> 8) & 255), (byte)(i & 255) };
	}

	public static int twoBytesToInt(byte[] b, int offset) {
		return (b[offset] & 255) << 8 | (b[offset + 1] & 255);
	}
	
	public static int bytesToInt(byte[] b, int offset) {
		return (b[offset + 0] & 255) << 24 | (b[offset + 1] & 255) << 16 | (b[offset + 2] & 255) << 8
		| (b[offset + 3] & 255);
	}
	
	public static short bytesToShort(byte[] input, int offset) {
		return (short)( ((input[offset]&(byte)0xFF)<<8) | (input[offset+1]&(byte)0xFF) );
	}

	public static int bytesAsciiToByte(byte[] b, int offset) {
		return ((b[offset] << 4 | b[offset + 1]) << 24) | ((b[offset + 2] << 4 | b[offset + 3]) << 16)
		| ((b[offset + 4] << 4 | b[offset + 5]) << 8) | ((b[offset + 6] << 4 | b[offset + 7]));
	}

	public static byte bytesToByte(byte[] b, int offset) {
		return (byte)(b[offset] << 4 | b[offset + 1]);
	}

	public static String encodeUrl(String url) {
		try {
			return URLEncoder.encode(url, "utf8");
		}
		catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static boolean isEqualArray(byte[] b1, byte[] b2) {
		if (b1.length != b2.length)
			return false;

		for (int i = 0; i < b1.length; i++) {
			if (b1[i] != b2[i])
				return false;
		}
		return true;
	}

	public static byte[] getBytesFromStream(int length, ByteArrayInputStream bais) {
		byte[] bytes = new byte[length];
		bais.read(bytes, 0, bytes.length);
		return bytes;
	}	

	public static byte[] getBytesFromStream(int length, InputStream bais) throws IOException {
		byte[] bytes = new byte[length];
		bais.read(bytes, 0, bytes.length);
		return bytes;
	}

	public static String fromUTF8ByteArray(byte[] bytes)
	{
		int i = 0;
		int length = 0;

		while (i < bytes.length)
		{
			length++;
			if ((bytes[i] & 0xf0) == 0xf0)
			{
				// surrogate pair
				length++;
				i += 4;
			}
			else if ((bytes[i] & 0xe0) == 0xe0)
			{
				i += 3;
			}
			else if ((bytes[i] & 0xc0) == 0xc0)
			{
				i += 2;
			}
			else
			{
				i += 1;
			}
		}

		char[] cs = new char[length];

		i = 0;
		length = 0;

		while (i < bytes.length)
		{
			char ch;

			if ((bytes[i] & 0xf0) == 0xf0)
			{
				int codePoint = ((bytes[i] & 0x03) << 18) | ((bytes[i+1] & 0x3F) << 12) | ((bytes[i+2] & 0x3F) << 6) | (bytes[i+3] & 0x3F);
				int U = codePoint - 0x10000;
				char W1 = (char)(0xD800 | (U >> 10));
				char W2 = (char)(0xDC00 | (U & 0x3FF));
				cs[length++] = W1;
				ch = W2;
				i += 4;
			}
			else if ((bytes[i] & 0xe0) == 0xe0)
			{
				ch = (char)(((bytes[i] & 0x0f) << 12)
						| ((bytes[i + 1] & 0x3f) << 6) | (bytes[i + 2] & 0x3f));
				i += 3;
			}
			else if ((bytes[i] & 0xd0) == 0xd0)
			{
				ch = (char)(((bytes[i] & 0x1f) << 6) | (bytes[i + 1] & 0x3f));
				i += 2;
			}
			else if ((bytes[i] & 0xc0) == 0xc0)
			{
				ch = (char)(((bytes[i] & 0x1f) << 6) | (bytes[i + 1] & 0x3f));
				i += 2;
			}
			else
			{
				ch = (char)(bytes[i] & 0xff);
				i += 1;
			}

			cs[length++] = ch;
		}

		return new String(cs);
	}
	
	public static byte[] toByte(long data) {
		return new byte[] {
				(byte)((data >> 56) & 0xff),
				(byte)((data >> 48) & 0xff),
				(byte)((data >> 40) & 0xff),
				(byte)((data >> 32) & 0xff),
				(byte)((data >> 24) & 0xff),
				(byte)((data >> 16) & 0xff),
				(byte)((data >> 8) & 0xff),
				(byte)((data >> 0) & 0xff),
		};
	}

	public static byte[] toByte(long[] data) {
		if (data == null) 
			return null;

		byte[] bytes = new byte[data.length * 8];

		for (int i = 0; i < data.length; i++)
			System.arraycopy(toByte(data[i]), 0, bytes, i * 8, 8);

		return bytes;
	} 


	public static long toLong(byte[] data) {
		if (data == null || data.length != 8) 
			return 0x0;

		return (long)((long)(0xff & data[0]) << 56 | 
				(long)(0xff & data[1]) << 48 |
				(long)(0xff & data[2]) << 40 |
				(long)(0xff & data[3]) << 32 |
				(long)(0xff & data[4]) << 24 |
				(long)(0xff & data[5]) << 16 |
				(long)(0xff & data[6]) << 8 |
				(long)(0xff & data[7]) << 0 );
	}
}
