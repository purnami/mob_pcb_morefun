package id.co.softorb.lib.smartcard.pax;

import android.os.Message;
import android.os.SystemClock;
import android.util.Log;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationListener;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.card.ATR;
import com.cloudpos.card.CPUCard;
import com.cloudpos.card.Card;
import com.cloudpos.smartcardreader.SmartCardReaderDevice;
import com.cloudpos.smartcardreader.SmartCardReaderOperationResult;
import com.pax.dal.IDAL;
import com.pax.dal.IIcc;
import com.pax.dal.ISys;
import com.pax.dal.entity.EPiccType;
import com.pax.dal.exceptions.IccDevException;

import java.io.UnsupportedEncodingException;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.ContactBase;

import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INITREADER;
import static id.co.softorb.lib.helper.ErrorCode.ERR_INCORRECT_CARDTYPE;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_bca;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_bni;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_bri;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_luminos;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_mdr;
import static id.co.softorb.lib.smartcard.posdevice.params.ctype_unknown;

public class contact extends ContactBase {
    String tagclass= this.getClass().getSimpleName();

    private POSListener detectListener;
    private IDAL dal;
    private ISys iSys = null;

    private contactcard cb;
    private IIcc icc;
    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=CTL_ERR_INITREADER;

        detectListener=listener;


        dal = (IDAL)devInstance;
        if(dal==null)
            return result;

        icc = dal.getIcc();
        if(icc==null)
            return result;
        cb = new contactcard();
        cb.setCard(icc);

        return OK;

    }

    @Override
    public int open() {
        return cb.Open();
    }

    @Override
    public int close() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        return cb.Close();
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result=WAITING;
        try {
            icc.light(true);
        }
        catch (IccDevException e){
            e.printStackTrace();
        }
        if(!cb.isOpen())
        {

            open();
        }


        IccDectedThread iccDectedThread = new IccDectedThread();
        iccDectedThread.start();

        return result;
    }

    @Override
    public void stopfind() {
        cb.Close();
    }

    /*@Override
    public byte[] sendCB( byte[] apdu) {


        byte[] apduResult=null;



        apduResult=cb.sendapdu(apdu);



        return apduResult;
    }*/

    @Override
    public byte[] sendCB(int slot, byte[] apdu) {
        byte[] apduResult=null;



        apduResult=cb.sendapdu(apdu);



        return apduResult;
    }

    public class IccDectedThread extends Thread {
        @Override
        public void run() {
            super.run();
            String resString = "";
            int result;
            while (!Thread.interrupted()) {
                result = cb.findcard();
                if (result==OK) {
                    cb.Open();
                    detectListener.onContactDetected(cb.getAtr());


                    try {
                        icc.light(false);
                    }
                    catch (IccDevException e)
                    {
                        Log.e(TAG,e.getMessage());
                    }
                    close();

                    break;

                } else {

                }
            }
        }
    }

    class contactcard
    {
        private final int contact_slot=0;
        private int cardtype;
        private boolean state;
        private IIcc contactrdr;
        private byte[] atr;
        public contactcard()
        {
            cardtype=ctype_unknown;
            state=false;
        }
        public IIcc getCard()
        {
            return contactrdr;
        }
        public void setCard(IIcc card)
        {
            contactrdr=card;
        }
        public contactcard(int cardtype)
        {
            this.cardtype=cardtype;
        }
        public byte[] sendapdu(byte[] apdu)
        {

            byte[] apduResult=null;
            try {
                apduResult = icc.isoCommand((byte) contact_slot, apdu);
            } catch (IccDevException e) {
                e.printStackTrace();
            }
            return apduResult;
        }
        public int findcard()
        {
            Log.i(TAG, tagclass+"."+new Throwable()
                    .getStackTrace()[0]
                    .getMethodName());
            boolean cardfound=false;
            try {

                cardfound=contactrdr.detect((byte)contact_slot);
                if(cardfound)
                    return OK;
            } catch (IccDevException e) {
                e.printStackTrace();
                Log.e(TAG,"error open sam");
                return NOT_OK;
            }
            return WAITING;
        }
        public byte[] getAtr()
        {
            return atr;
        }
        public boolean isOpen()
        {
            return state;
        }
        public int Open()
        {
            Log.i(TAG, tagclass+"."+new Throwable()
                    .getStackTrace()[0]
                    .getMethodName());
            try {

                atr=icc.init((byte)contact_slot);
                state=true;
            } catch (IccDevException e) {
                e.printStackTrace();
                Log.e(TAG,"error open sam");
                return NOT_OK;
            }
            return OK;
        }
        public int Close()
            {
                Log.i(TAG, tagclass+"."+new Throwable()
                        .getStackTrace()[0]
                        .getMethodName());

                try{
                    icc.close((byte)contact_slot);
                state=false;
            }
            catch (IccDevException e)
            {
                e.printStackTrace();
                Log.e(TAG,"Close Reader Exception");
                return NOT_OK;
            }
            return OK;

        }
        public int setCardType(int type)
        {
            Log.i(TAG, tagclass+"."+new Throwable()
                    .getStackTrace()[0]
                    .getMethodName());
            switch(type)
            {
                //valid number for sam slot is 1 and 2
                case ctype_mdr:
                case ctype_bni:
                case ctype_bri:
                case ctype_luminos:
                case ctype_bca:
                    break;
                default:
                    return ERR_INCORRECT_CARDTYPE;
            }

            this.cardtype=type;
            return OK;
        }


    }
}
