package id.co.softorb.lib.smartcard.posdevice;

public abstract class ContactBase extends Base
{

    public abstract byte[] sendCB(int slot,byte[] apdu);
}
