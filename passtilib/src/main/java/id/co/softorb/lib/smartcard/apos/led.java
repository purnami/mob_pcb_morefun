package id.co.softorb.lib.smartcard.apos;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.usdk.apiservice.aidl.UDeviceService;
import com.usdk.apiservice.aidl.constants.RFDeviceName;
import com.usdk.apiservice.aidl.led.Light;
import com.usdk.apiservice.aidl.led.ULed;

import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.LED;

import static id.co.softorb.lib.helper.ErrorCode.ERR_INITLED;
import static id.co.softorb.lib.passti.helper.ErrorCode.OK;

public class led extends LED {
    String tagclass= this.getClass().getSimpleName();
    private ULed led;
    private UDeviceService deviceService;
    private boolean redIsOpen = false;
    private boolean greenIsOpen = false;
    private boolean yellowIsOpen = false;
    private boolean blueIsOpen = false;
    @Override
    public int blink() {
        return 0;
    }

    @Override
    public int open(int color) {
        try {
            switch (color) {
                case Light.BLUE:
                    led.turnOn(color);
                    blueIsOpen = true;
                    break;
                case Light.YELLOW:
                    led.turnOn(color);
                    yellowIsOpen = true;
                    break;
                case Light.GREEN:
                    led.turnOn(color);
                    greenIsOpen = true;
                    break;
                case Light.RED:
                    led.turnOn(color);
                    redIsOpen = true;
                    break;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public int on() {
        return 0;
    }

    @Override
    public int off() {

        return 0;
    }

    @Override
    public boolean ison() {
        return false;
    }

    @Override
    public int init(POSListener detectListener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            deviceService = (UDeviceService) devInstance;
            Log.d("device service",""+deviceService.getVersion());
            Bundle param = new Bundle();
            param.putString("rfDeviceName", RFDeviceName.INNER);
            led=(ULed) ULed.Stub.asInterface(deviceService.getLed(param));
            if(led!=null){
                Log.d("result init led apos","ok");
                return OK;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return ERR_INITLED;
    }

    @Override
    public int open() {
        return 0;
    }

    @Override
    public int close() {
        return 0;
    }

    @Override
    public int find() {
        return 0;
    }

    @Override
    public void stopfind() {

    }
}
