package id.co.softorb.lib.smartcard.posdevice;

public abstract class LED extends Base{
    public final int BLUE=0;
    public final int YELLOW=1;
    public final int GREEN=2;
    public final int RED=3;
    protected int selectedColor;
    public abstract int blink();
    public abstract int open(int color);
    public abstract int on();
    public abstract int off();
    public abstract boolean ison();
}
