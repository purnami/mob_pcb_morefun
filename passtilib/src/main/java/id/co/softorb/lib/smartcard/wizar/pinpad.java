package id.co.softorb.lib.smartcard.wizar;
import android.util.Log;

import com.cloudpos.AlgorithmConstants;
import com.cloudpos.DeviceException;
import com.cloudpos.OperationListener;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.TimeConstants;
import com.cloudpos.pinpad.KeyInfo;
import com.cloudpos.pinpad.PINPadDevice;
import com.cloudpos.pinpad.PINPadOperationResult;
import com.cloudpos.pinpad.extend.PINPadExtendDevice;
import com.cloudpos.sdk.common.Common;
import com.cloudpos.smartcardreader.SmartCardReaderDevice;
import com.wizarpos.jni.PinPadCallbackHandler;
import com.wizarpos.jni.PinPadInterface;

import java.util.Map;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.Base;
import id.co.softorb.lib.smartcard.posdevice.PinpadBase;

import static id.co.softorb.lib.helper.ErrorCode.*;

public class pinpad extends PinpadBase {
    //PinPadCallbackHandler callback;
    String tagclass= this.getClass().getSimpleName();
    private POSListener detectListener;
    private PINPadExtendDevice device = null;
    private POSTerminal posTerminal;
    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        detectListener=listener;
        posTerminal=(POSTerminal) devInstance;
        device = (PINPadExtendDevice) posTerminal.getDevice("cloudpos.device.pinpad");
        return OK;

    }
    public int setPinpadCallback(Object obj)
    {

        //callback = (PinPadCallbackHandler)obj;

        //PinPadInterface.setupCallbackHandler(callback);
        return OK;
    }

    @Override
    public String SN() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        String sn;
        try {
            sn = device.getSN();

        } catch (DeviceException e) {
            e.printStackTrace();
            return null;
        }
        return sn;
    }



    @Override
    public int SetPINLength(int min, int max) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            device.setPINLength(min, max);
        } catch (DeviceException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return OK;
    }

    @Override
    public int UpdateUserKey(int mkid, int userkeyid, byte[] value) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        try {
            device.updateUserKey(mkid, userkeyid, value);
        } catch (DeviceException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return OK;
    }

    @Override
    public int UpdateUserKey(int mkid, int userkeyid, byte[] value, byte[] checksum) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        try {
            device.updateUserKey(mkid, userkeyid, value, PINPadDevice.CHECK_TYPE_CUP,checksum);
        } catch (DeviceException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return OK;


    }

    @Override
    public byte[] getSessionKey(int mkid, int userkeyid, int algo) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] value;
        try {
            value = device.getSessionKeyCheckValue(mkid, userkeyid, algo);

        } catch (DeviceException e) {
            e.printStackTrace();
            return null;

        }
        return value;
    }
/*
    @Override
    public byte[] GenerateRandom(int length) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] random;
        try {
             random = device.getRandom(length);

        } catch (DeviceException e) {
            e.printStackTrace();
            return null;
        }
        return random;
    }
*/
    @Override
    public int open() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        /*if(PinPadInterface.open() < 0)
        {

            return ERR_OPENPINPAD;
        }
        //setPinpadCallback(this);*/
        int result=ERR_OPENPINPAD;
        try {
            device.open();
            result=OK;
        } catch (DeviceException e) {
            e.printStackTrace();
            Log.e(TAG,e.toString());
            return ERR_OPENPINPAD;
        }
        return result;
    }

    @Override
    public int close() {
       /* int result;
        result=PinPadInterface.close();
        if(result<0)
            return NOT_OK;
        else return OK;*/
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            device.close();



        }catch (DeviceException e)
        {
            Log.e(TAG,e.toString());
            return NOT_OK;

        }
        return OK;
    }

    @Override
    public int find() {
        return 0;
    }

    @Override
    public void stopfind() {

    }


    @Override
    protected int SelectedKeyAlgo(int algo)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        switch(algo)
        {
            case 1 : return AlgorithmConstants.ALG_3DES;
            default :
                return AlgorithmConstants.ALG_3DES;
        }
    }
    @Override
    public byte[] encrypt(int mkid, int userkeyid, int algo,byte[] data) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int selectedalgo;

        KeyInfo keyInfo;

        byte[] cipher;
        selectedalgo=SelectedKeyAlgo(algo);
        keyInfo= new KeyInfo(PINPadDevice.KEY_TYPE_MK_SK, mkid, userkeyid,
                selectedalgo);
        try {
             cipher= device.encryptData(keyInfo, data);

        } catch (DeviceException e) {
            e.printStackTrace();
           return null;
        }
        return cipher;
    }


    public  byte[] calculatemac(int mkid,int userkeyid,int keyalgo,int macalgo,byte[] data)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int selectedmacalgo;
        int selectedkeyalgo;
        KeyInfo keyInfo;

        byte[] mac=null;
        selectedkeyalgo=SelectedKeyAlgo(keyalgo);
        selectedmacalgo=SelectedMACAlgo(macalgo);
        keyInfo= new KeyInfo(PINPadDevice.KEY_TYPE_MK_SK, mkid, userkeyid,
                selectedkeyalgo);
        try {
             mac = device.calculateMac(keyInfo, selectedmacalgo,
                    data);

        } catch (DeviceException e) {
            e.printStackTrace();

        }
        return mac;
    }

    protected  int SelectedMACAlgo(int algo)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        switch(algo)
        {
            case 1 : return AlgorithmConstants.ALG_MAC_METHOD_X99;
            default :
                return AlgorithmConstants.ALG_MAC_METHOD_X99;
        }
    }
    public  byte[] calculatedukptmac(byte[] data)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] mac=null;
        KeyInfo keyInfo = new KeyInfo(PINPadDevice.KEY_TYPE_TDUKPT_2009, 0, 0,
                AlgorithmConstants.ALG_3DES);
        try {
            mac = device.calculateMac(keyInfo, AlgorithmConstants.ALG_MAC_METHOD_SE919, data);
        } catch (DeviceException e) {
            e.printStackTrace();
        }
        return mac;
    }

    public int verifymac(int mkid,int keyid,byte[]data,byte[] mac)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        KeyInfo keyInfo = new KeyInfo(PINPadDevice.KEY_TYPE_TDUKPT_2009, mkid, keyid,
                AlgorithmConstants.ALG_3DES);
        try {

            int macFlag = 2;
            int nDirection = 0;
            boolean b = device.verifyResponseMac(keyInfo, data, macFlag, mac, nDirection);
            if (b) {
            }
        } catch (DeviceException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return OK;
    }


    OperationListener pinblocklistener = new OperationListener() {

        @Override
        public void handleResult(OperationResult arg0) {
            Log.e(TAG,"arg0.getResultCode() "+arg0.getResultCode());
            if (arg0.getResultCode() == OperationResult.SUCCESS) {
                byte[] pinBlock = ((PINPadOperationResult) arg0).getEncryptedPINBlock();
                Log.d(TAG,"encrypted pinblock "+pinBlock);
            } else {
            }
        }
    };

    public  int async_pinblock(int mkid,int keyid,byte[] data)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        KeyInfo keyInfo = new KeyInfo(PINPadDevice.KEY_TYPE_MK_SK, mkid, keyid, AlgorithmConstants.ALG_DES);
        //String pinblock= "0123456789012345678";
        String pinblock= "012345678901234567";
        //String pinblock= Hex.bytesToHexString(data);
        Log.d(TAG,"pinblock "+pinblock);
        try {
        device.listenForPinBlock(keyInfo, pinblock, false, pinblocklistener, TimeConstants.FOREVER);
        } catch (DeviceException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return WAITING;
    }
    public  byte[] sync_pinblock(int mkid,int keyid,byte[] data)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        KeyInfo keyInfo = new KeyInfo(PINPadDevice.KEY_TYPE_MK_SK, mkid, keyid, AlgorithmConstants.ALG_DES);
        String pinblock= Hex.bytesToHexString(data);
        byte[] pinBlock;
        try {
            OperationResult operationResult = device.waitForPinBlock(keyInfo, pinblock, false,
                    TimeConstants.FOREVER);
            if (operationResult.getResultCode() == OperationResult.SUCCESS) {
                 pinBlock = ((PINPadOperationResult) operationResult).getEncryptedPINBlock();
            } else {
                return null;
            }
        } catch (DeviceException e) {
            e.printStackTrace();
            return null;
        }
        return pinBlock;
    }

    public  int getMKStatus(int mkid)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            int result = device.getMkStatus(mkid);
            if (result == 0) {
                return ERR_NORECORD;
            } else if (result > 0) {
                return OK;
            } else {
                return ERR_NO_RESP;
            }

        } catch (DeviceException e) {
            e.printStackTrace();
            return ERR_NO_RESP;
        }
    }
    public  int getSKStatus(int mkid,int skid)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            int result = device.getSkStatus(mkid,skid);
            if (result == 0) {
                return ERR_NORECORD;
            } else if (result > 0) {
                return OK;
            } else {
                return ERR_NO_RESP;
            }

        } catch (DeviceException e) {
            e.printStackTrace();
            return ERR_NO_RESP;
        }
    }
    public  int getDUKPTStatus(int mkid,byte[] uniquekey)
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            int result = device.getDukptStatus(mkid,uniquekey);
            if (result == 0) {
                return ERR_NORECORD;
            } else if (result > 0) {
                return OK;
            } else {
                return ERR_NO_RESP;
            }

        } catch (DeviceException e) {
            e.printStackTrace();
            return ERR_NO_RESP;
        }
    }
    public  int cancel()
    {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            device.cancelRequest();
        } catch (DeviceException e) {
            e.printStackTrace();
            return NOT_OK;
        }
        return OK;
    }

}
