package id.co.softorb.lib.smartcard.morefun;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.morefun.yapi.device.beeper.Beeper;
import com.morefun.yapi.engine.DeviceServiceEngine;

import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.Base;

import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;

public class buzzer extends Base {
    String tagclass= this.getClass().getSimpleName();
    private DeviceServiceEngine deviceService;
    private Beeper beeper;

    public static final int NORMAL = 0;
    public static final int SUCCESS = 1;
    public static final int FAIL = 2;
    public static final int INTERVAL = 3;
    public static final int ERROR = 4;

    @Override
    public int init(POSListener detectListener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        try {
//            Bundle bundle = new Bundle();
            deviceService=(DeviceServiceEngine) devInstance;
//            deviceService.login(bundle,"09000000");
            beeper=deviceService.getBeeper();
            startBeep(SUCCESS);
            if (beeper!=null) {
                Log.d("result buzzer morefun","ok");
                return OK;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return NOT_OK;
    }

    public void startBeep(int beep){
        try {
            beeper.beep(beep);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int open() {
        //Tidak ada fungsi open()
        return OK;
    }

    @Override
    public int close() {
        //Tidak ada fungsi close()
        return OK;
    }

    @Override
    public int find() {
        //Tidak ada fungsi find()
        return OK;
    }

    @Override
    public void stopfind() {
        //Tidak ada fungsi stopfind()
    }
}
