package id.co.softorb.lib.smartcard.apos;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.usdk.apiservice.aidl.UDeviceService;
import com.usdk.apiservice.aidl.beeper.UBeeper;
import com.usdk.apiservice.aidl.constants.RFDeviceName;
import com.usdk.apiservice.aidl.data.ApduResponse;
import com.usdk.apiservice.aidl.data.BytesValue;
import com.usdk.apiservice.aidl.data.IntValue;
import com.usdk.apiservice.aidl.icreader.DriverID;
import com.usdk.apiservice.aidl.icreader.ICError;
import com.usdk.apiservice.aidl.icreader.OnInsertListener;
import com.usdk.apiservice.aidl.icreader.PowerMode;
import com.usdk.apiservice.aidl.icreader.UICCpuReader;
import com.usdk.apiservice.aidl.icreader.Voltage;
import com.usdk.apiservice.aidl.led.ULed;
import com.usdk.apiservice.aidl.rfreader.RFError;
import com.usdk.apiservice.aidl.rfreader.URFReader;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.smartcard.POSListener;
import id.co.softorb.lib.smartcard.posdevice.ContactBase;

import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INITREADER;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.helper.ErrorCode.WAITING;

public class contact extends ContactBase {
    String tagclass= this.getClass().getSimpleName();
    private UDeviceService deviceService;
    private UICCpuReader icCpuReader;
    private POSListener detectListener;
    private contactcard[] cb;
    @Override
    public byte[] sendCB(int slot, byte[] apdu) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] rapdu=new byte[0];
        Log.d(TAG,"RDR-->ICCxx : "+ Hex.bytesToHexString(apdu));
        try {
            ApduResponse apduResponse = icCpuReader.exchangeApdu(apdu);
            byte[] data=apduResponse.getData();
            byte[] sw={apduResponse.getSW1(),apduResponse.getSW2()};
            if(Hex.bytesToHexString(sw).equals("9000")){
                if(Hex.bytesToHexString(data)!=null){
                    rapdu=new byte[data.length+ sw.length];
                    rapdu= BytesUtil.merge(data, sw, new byte[]{});
                }else{
                    rapdu=new byte[sw.length];
                    rapdu=sw;
                }

            }else{
                rapdu=new byte[sw.length];
                rapdu=sw;
            }
            Log.d("new rapdu contact1", ""+ Hex.bytesToHexString(rapdu)+" ");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return rapdu;
    }

    @Override
    public int init(POSListener listener, Object devInstance) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            deviceService = (UDeviceService) devInstance;
            Log.d("device service",""+deviceService.getVersion());
//            buzzer.init(listener,deviceService);
            Bundle param = new Bundle();
            param.putString("rfDeviceName", RFDeviceName.INNER);
            BytesValue responseData = new BytesValue();
            icCpuReader = (UICCpuReader) UICCpuReader.Stub.asInterface(deviceService.getICReader(DriverID.ICCPU, null));
            int initModul = icCpuReader.initModule(Voltage.ICCpuCard.VOL_DEFAULT, PowerMode.DEFAULT);
            BytesValue atr = new BytesValue();
            IntValue protocol = new IntValue();
            int powerUp = icCpuReader.powerUp(atr, protocol);
            if (initModul == ICError.SUCCESS && powerUp==ICError.SUCCESS) {
                Log.d("result init contact","ok");
                detectListener = listener;
                return OK;

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());
        }
        return NOT_OK;
    }

    @Override
    public int open() {
        return 0;
    }

    @Override
    public int close() {
        return 0;
    }

    @Override
    public int find() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        try {
            icCpuReader.searchCard(listener);
            Log.d("apos cardin", icCpuReader.isCardIn()+"");
            if(icCpuReader.isCardIn()){
                return OK;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return WAITING;
    }

    @Override
    public void stopfind() {

    }

    OnInsertListener.Stub listener=new OnInsertListener.Stub() {
        @Override
        public void onCardInsert() throws RemoteException {
            Log.d(TAG, "----- onCardInsert -----"+icCpuReader.isCardIn());
            icCpuReader.isCardIn();
            byte[] tempuid={0x00};
            detectListener.onContactDetected(tempuid);
        }

        @Override
        public void onFail(int i) throws RemoteException {
            Log.d(TAG, "----- onFail -----");
        }
    };
}
