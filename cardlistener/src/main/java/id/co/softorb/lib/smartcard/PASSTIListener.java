package id.co.softorb.lib.smartcard;

import java.util.ArrayList;
/**
 * callback function from stiutility to application
 */
public interface PASSTIListener {
    void onRFDetected(byte[] uid,int mode,ArrayList[] tagdata);
    void onContactDetected(byte[] uid, ArrayList[] tagdata);
    void onMSRDetected(byte[][] trackdata);
    void onSAMDetected(int slot,byte[] atr);
    void onPinPadHandler(byte[ ]data);
}
