package id.co.softorb.lib.smartcard;
/**
 * callback function from smartcardlib to stiutility
 */

public interface POSListener {
    void onRFDetected(byte[] uid,int mode);
    void onContactDetected(byte[] uid);
    void onMSRDetected(byte[][] trackdata);
    void onSAMDetected(int slot,byte[] atr);
    void onPinPadHandler(byte[ ]data);

}
